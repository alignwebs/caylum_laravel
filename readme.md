## Steps to Setup

	- Make sure your domain is pointed to `/public` as root directory
	- `composer install`
	- Copy `.env.example` to `.env`
	- Edit configurations like Database, SMTP, Comet Chat etc. in `/.env`
	- Edit wordpress news website put wp-admin login credentials in `app/Console/Commands/InitSettings.php`
	- Setup cronjob by putting this line on crontab `* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1`
	- run `php artisan migrate`
	- run `php artisan db:seed`
	- To create admin user, run this command `php artisan adminuser:create`. You can edit admin deatils here at `app/Console/Commands/CreateAdminUser.php`
	- run `php artisan settings:default`

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $appends = ['file_url','thumbnail_url'];
    
    public function attachmentable()
      {
          return $this->morphTo();
      }

    public function getFileUrlAttribute()
    {
        return asset('storage/'.$this->file);
    }

    public function getThumbnailUrlAttribute()
    {
        return asset('storage/'.$this->thumbnail);
    }
}



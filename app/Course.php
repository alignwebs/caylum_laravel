<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\SoftDeletes;
class Course extends Model
{
    use SoftDeletes;
	use HasSlug;
    protected $revisionEnabled = true;

    protected $appends = ['enrollments_count','paid_enrollments_count','formatted_money','formatted_std_money'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription','default','subscribe_default');
    }

    public function enrollments()
    {
        return $this->hasMany('App\CourseEnrollment');   
    }

    public function getEnrollmentsCountAttribute()
    {
        return $this->enrollments()->count();   
    }

    public function getPaidEnrollmentsCountAttribute()
    {
        return $this->enrollments()->where('payment_id','!=',null)->count();   
    }

    public function getFormattedMoneyAttribute()
    {
         return formatted_money($this->price); 
    }

    public function getFormattedStdMoneyAttribute()
    {
         return formatted_money($this->std_rate);   
    }

    public function custom_form()
    {
        return $this->belongsTo(CustomForm::class);
    }
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Overtrue\LaravelFollow\Traits\CanLike;
use TaylorNetwork\UsernameGenerator\FindSimilarUsernames;
use BeyondCode\Comments\Contracts\Commentator;


class User extends Authenticatable implements MustVerifyEmail, Commentator
{
    use HasRoles;
    use Notifiable;
    //use SoftDeletes;
    use CanFollow, CanBeFollowed, CanLike;
    use FindSimilarUsernames;
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name','first_name', 'mobile', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'wp_news_password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $appends = ['cover_pic_url', 'profile_pic_url', 'thumbnail_url', 'profile_url'];

    /**
     * Check if a comment for a specific model needs to be approved.
     * @param mixed $model
     * @return bool
     */

    public function needsCommentApproval($model): bool
    {
        return false;    
    }

    public function subscriber()
    {
        return $this->hasOne('App\Subscriber','user_id');
    }


    public function subscription()
    {
        return $this->hasOne(Subscriber::class);
    }

    public function hasSubscription()
    {
        if($this->subscription()->count() > 0)
            return true;
        else
            return false;
    }

    public function getNameAttribute()
    {
        return ucwords($this->first_name . " " . $this->last_name);
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucwords(strtolower($value));
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucwords(strtolower($value));
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function getDisplayNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getCoverPicUrlAttribute()
    {
        return asset($this->cover_pic);
    }

    public function getProfilePicUrlAttribute()
    {
        return asset($this->profile_pic);
    }

    public function getThumbnailUrlAttribute()
    {
        return get_user_profile_pic($this, true);
    }
    
    public function getProfileUrlAttribute()
    {
        if($this->username)
            return route('portal.user.profile',$this->username);
    }
    public function attachment()
    {
        return $this->morphOne('App\Attachment', 'attachmentable');
    }

    public function comment()
    {
        return $this->morphOne('App\Comment', 'commentable');
    }

    public function feed()
    {
        return $this->hasMany(Feed::class)->with('attachment');
    }
}

<?php

namespace App\Listeners;

use App\Events\CourseEnrolled;
use App\Events\PaySlipUploaded;
use App\Events\UserSubscribed;
use App\Mail\CourseEnrolledAdmin;
use App\Mail\NewUserSubscribed;
use App\Mail\PaySlipUploaded as PaySlipMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use Validator;

class NotifyOnMailToAdmin implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $notification_emails = setting('notification_emails');

        if(!$notification_emails)
            return false;

        $notification_emails = explode(',', $notification_emails);
        foreach ($notification_emails as $email) {
            $email = trim($email);
            $validator = Validator::make(['email' => $email],[
              'email' => 'required|email'
            ]);

            if($validator->passes()){

                if($event instanceof CourseEnrolled)
                    Mail::to($email)->send(new CourseEnrolledAdmin($event->enrollment));

                if($event instanceof PaySlipUploaded)
                    Mail::to($email)->send(new PaySlipMail($event->payslip, 'New Pay Slip Uploaded.'));

                if($event instanceof UserSubscribed)
                    Mail::to($email)->send(new NewUserSubscribed($event->subscriber));

            }
        }
    }
}

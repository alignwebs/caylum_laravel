<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
use Akaunting\Money\Money;
class Payment extends Model
{
    protected $revisionEnabled = true;

    protected $appends = ['formatted_money'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function subscriber()
    {
        return $this->belongsTo(Subscriber::class,'subscriber_id');
    }

    public function payment_mode()
    {
    	return $this->belongsTo(PaymentMode::class);
    }

    public function payment_type()
    {
    	return $this->belongsTo(PaymentType::class);
    }

    public function getFormattedMoneyAttribute()
    {
        return  formatted_money($this->amount);
    }
}

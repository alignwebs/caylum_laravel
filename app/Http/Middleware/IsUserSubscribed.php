<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class IsUserSubscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $wp_url = getWpNewsLoginUrl();
            $user = Auth::user();
            $enable_member_portal = setting('enable_member_portal');

        if($user->hasRole('admin'))
        {
             return redirect()->route('admin.home');
        }

         if(!$user->hasSubscription())
         {
             Auth::logout();
             return redirect()->to($wp_url);
         }

         if($user->subscription->isExpired())
         {
            return redirect()->route('subscription.change', 2);
        }

        if(!$user->subscription->active)
        {
            flash('Your subscription is inactive. Please contact administration.')->error();
            Auth::logout();
            return redirect()->route('login');
        }

        if(!$enable_member_portal)
        {
            Auth::logout();
            return redirect()->to($wp_url);
        }
    }   
    return $next($request);
}
}

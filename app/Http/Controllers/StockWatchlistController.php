<?php

namespace App\Http\Controllers;

use App\Stock;
use App\StockWatchlist;
use App\Watchlist;
use Auth;
use Illuminate\Http\Request;

class StockWatchlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Analysis – My Watchlist";

        return view('portal.watchlist.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stocks = Stock::all();
        // $watchlist = Watchlist::pluck('name','id');
        return view('portal.watchlist.add', compact('stocks', 'watchlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'watchlist_id' => 'required',
            'stock' => 'required'
        ]);

        $stock = Stock::whereSymbol($request->input('stock'))->firstOrFail();


        StockWatchlist::firstOrCreate([
                'watchlist_id'     =>  $request->input('watchlist_id'),
                'stock_symbol'  =>  $stock->symbol,
                'stock_name'    =>  $stock->name,
            ], [ 'user_id'   =>  Auth::id() ]);

        return response()->json(['success' => 'true']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StockWatchlist  $stockWatchlist
     * @return \Illuminate\Http\Response
     */
   

    public function view(Request $request)
    {
        
        $request->validate([
            'watchlist_id' => 'required'
        ]);

        $watchlist_id  = $request->input('watchlist_id');

        $watchlistStocks = StockWatchlist::with('stockalerts')->where('watchlist_id', $watchlist_id)->orderBy('stock_symbol')->get();

        if(!$watchlistStocks)
            return response('No entries found');

        return view('portal.watchlist.listDetails', compact('watchlistStocks'));
    }

    public function getWatchlistSelectHtml()
    {   
        $watchlist = Watchlist::pluck('name','id');
        // $watchlist = StockWatchlist::getListNames(Auth::id())->pluck('list_name','list_name');
        return \Form::select('stockwatchlistnames', $watchlist, null, ['class' => 'form-control', 'id' => 'stockwatchlistNames']);
    }


    public function createWatchlist()
    {
        return view('portal.watchlist.addWatchlistModal');
    }

    public function storeWatchlist(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $Watchlist = new Watchlist;
        $Watchlist->user_id = Auth::id();
        $Watchlist->name = $request->name;
        $Watchlist->save();

        return response()->json([ 'success' => 'true', 'watchlist_id' => $Watchlist->id ]);
    }

    public function destroy($id)
    {
        StockWatchlist::where(['user_id'=>Auth::id(),'id'=>$id])->delete();
        return response()->json(['success'=>'true','message'=>'Stock Watchlist has been deleted successfully']);
    }
}

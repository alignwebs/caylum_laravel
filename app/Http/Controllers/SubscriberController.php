<?php

namespace App\Http\Controllers;
use App\Course;
use App\Mail\Subscribed;
use App\Subscriber;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Mail;
use Rap2hpoutre\FastExcel\FastExcel;
use TaylorNetwork\UsernameGenerator\Generator;
use Validator;
use Yajra\DataTables\Html\Builder;
use Illuminate\Validation\Rule;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
                return DataTables::of(User::has('subscription')->with(['subscriber.subscription']))
                      ->editColumn('last_name', function(User $user) {
                        return  @$user->last_name;
                        })
                       ->editColumn('first_name', function(User $user) {
                        return  @$user->first_name;
                        })
                       ->editColumn('email', function(User $user) {
                        return  @$user->email;
                        })
                        ->editColumn('mobile', function(User $user) {
                        return  @$user->mobile;
                        })
                        ->editColumn('gender', function(User $user) {
                        return  @$user->gender;
                        })
                        ->editColumn('birthday', function(User $user) {
                        return  @$user->birthday;
                        })
                        ->editColumn('alumini_course', function(User $user) {
                        return  @$user->alumini_course;
                        })
                        ->editColumn('alumini_attend', function(User $user) {
                        return  @$user->alumini_attend;
                        })
                        ->editColumn('subscriber.subscription.name', function(User $user) {
                        return  @$user->subscriber->subscription->name;
                        })
                        ->editColumn('subscriber.comments', function(User $user) {
                        return  @$user->subscriber->comments;
                        })
                        ->editColumn('created_at', function(User $user) {
                        return  Carbon::parse(@$user->created_at)->format('Y-m-d');
                        })
                      
                      ->editColumn('action', function(User $user) {
                          if($user->subscriber)
                            return  ModelBtn2('subscriber',$user->subscriber->id);
                          else
                            return false;
                    })
                    ->toJson(); 
                }


                $builder->columns([
                               ['data' => 'id', 'name' => 'id', 'title' => '#'],
                               ['data' => 'last_name', 'name' => 'last_name', 'title' => 'Last Name'],
                               ['data' => 'first_name', 'name' => 'first_name', 'title' => 'First Name'],
                               ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
                               ['data' => 'mobile', 'name' => 'mobile', 'title' => 'Contact'],
                               ['data' => 'gender', 'name' => 'gender', 'title' => 'Gender'],
                               ['data' => 'birthday', 'name' => 'birthday', 'title' => 'Birthdate'],
                               ['data' => 'alumini_course', 'name' => 'alumini_course', 'title' => 'Alumnus'],
                               ['data' => 'alumini_attend', 'name' => 'alumini_attend', 'title' => 'Year Attended'],
                               ['data' => 'subscriber.subscription.name', 'name' => 'subscriber.subscription.name', 'title' => 'Subscription'],
                               ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Date Registered'],
                               ['data' => 'subscriber.comments', 'name' => 'subscriber.comments', 'title' => 'Comments'],
                               ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                           ]);

                $datatable =  $builder->parameters([
                      'searchDelay' => 500,
                      'stateSave' => 'true',
                      'order' => [[ 0, "desc" ]]
                  ]);


                $title = "Manage Subscribers";
                $subscriptions = Subscription::get()->pluck('complete_package','id'); 
                $courses = Course::all()->pluck('name','name');
                return view('admin.subscriber.index', compact('datatable','title','subscriptions','courses'));
    }

    public function registeredUsers(Builder $builder)
    {
        if (request()->ajax()) {
                    return DataTables::of(User::doesntHave('subscription')->role('user'))
                      ->editColumn('last_name', function(User $user) {
                        return  @$user->last_name;
                        })
                       ->editColumn('first_name', function(User $user) {
                        return  @$user->first_name;
                        })
                       ->editColumn('email', function(User $user) {
                        return  @$user->email;
                        })
                        ->editColumn('mobile', function(User $user) {
                        return  @$user->mobile;
                        })
                        ->editColumn('gender', function(User $user) {
                        return  @$user->gender;
                        })
                        ->editColumn('birthday', function(User $user) {
                        return  @$user->birthday;
                        })
                        ->editColumn('alumini_course', function(User $user) {
                        return  @$user->alumini_course;
                        })
                        ->editColumn('alumini_attend', function(User $user) {
                        return  @$user->alumini_attend;
                        })
                        ->editColumn('created_at', function(User $user) {
                        return  Carbon::parse(@$user->created_at)->format('Y-m-d');
                        })
                      
                      ->editColumn('action', function(User $user) {
                        $id = $user->id;
                        $html = '<div class="btn-group btn-options" role="group">';
                        $html.= '<button type="button" onclick="editAjax(\''.route('subscriber.registered.edit',$id).'\',\'reg_user\')" class="btn btn-primary btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></button>&nbsp;';
                        $html.= \Form::open(['route' => ['subscriber.registered.delete', $id], 'method' => 'delete', 'class' => 'deleteFrmAjax']);
                        $html.= '<button type="submit" class="btn-delete btn btn-danger btn-sm"><span class="fa fa-trash" aria-hidden="true"></span></button>&nbsp;&nbsp;';
                        $html.= \Form::close();

                            return $html;
                        })
                    ->toJson(); 
                }


                $builder->columns([
                             
                               ['data' => 'last_name', 'name' => 'last_name', 'title' => 'Last Name'],
                               ['data' => 'first_name', 'name' => 'first_name', 'title' => 'First Name'],
                               ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
                               ['data' => 'mobile', 'name' => 'mobile', 'title' => 'Contact'],
                               ['data' => 'gender', 'name' => 'gender', 'title' => 'Gender'],
                               ['data' => 'birthday', 'name' => 'birthday', 'title' => 'Birthdate'],
                               ['data' => 'alumini_course', 'name' => 'alumini_course', 'title' => 'Alumnus'],
                               ['data' => 'alumini_attend', 'name' => 'alumini_attend', 'title' => 'Year Attended'],
                               ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Date Registered'],
                               ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                           ]);

                $datatable =  $builder->parameters([
                      'searchDelay' => 500,
                      'stateSave' => 'true',
                      'order' => [[ 0, "desc" ]]
                  ]);


                $title = "Manage Registered Users";
                return view('admin.subscriber.registered', compact('datatable','title'));
    }

    public function editRegisteredUser($id)
    {
        $user = User::find($id);
        $subscriptions = Subscription::get()->pluck('complete_package','id'); 
        $courses = Course::all()->pluck('name','name');
        return view('admin.subscriber.ajax.edit_registered', compact('user','subscriptions','courses')); 
    }

    public function updateRegisteredUser(Request $request)
    {
           
        $validator = Validator::make($request->all(), [
                        'last_name' => 'required',
                        'first_name' => 'required',
                        'mobile' => 'required|numeric|unique:users,mobile,'.$request->id,
                        'gender' => 'required',
                        'email' => 'required|string|email|unique:users,email,'.$request->id,
                        'active' => 'required',
                        'subscription_id' => 'nullable',
                        'starts_at' => Rule::requiredIf(function () use ($request) {
                                        return ($request->subscription_id > 0);
                                    }),
                        'alumini_member' =>'required',
                        'alumini_course' =>'required_if:alumini_member,1',
                        'alumini_attend' =>'required_if:alumini_member,1',
                 ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }

                DB::beginTransaction();
                try{
                    
                    $user = User::find($request->id);
                    // USER
                    $user->last_name = $request->input('last_name');
                    $user->first_name = $request->input('first_name');
                    $user->mobile = $request->input('mobile');
                    $user->birthday = $request->input('birthday');
                    $user->gender = $request->input('gender');
                    $user->email = $request->input('email');
                    $user->alumini_subscription_id = $request->subscription_id;
                    $user->alumini_member =$request->input('alumini_member');
                    $user->alumini_course = $request->input('alumini_course');
                    $user->alumini_attend = $request->input('alumini_attend');

                    $password = $request->input('password');
                    if(!empty($password))
                        $user->password = bcrypt($password);
                  
                    $user->active = $request->input('active');
                    $user->save(); 
                    // return $request;
                    //END

                   
                     if(!empty($request->subscription_id))
                        {     
                            $day = Subscription::where('id',$request->subscription_id)->first('days_duration');
                            $starts_at = new Carbon($request->starts_at);
                            $expires_at = $starts_at->addDays($day->days_duration);
                            
                            // USER SUBSCRIPTION
                            $Subscriber = new Subscriber;
                            $Subscriber->user_id = $user->id;
                            $Subscriber->subscription_id = $request->subscription_id;
                            $Subscriber->active = $request->active_subscription;
                            $Subscriber->starts_at = $request->starts_at;
                            $Subscriber->expires_at = $expires_at;
                            $Subscriber->comments = $request->comments;
                            $Subscriber->save();

                            Mail::to($user)->send(new Subscribed($Subscriber, "******"));
                        }
                    DB::commit();
                    return response()->json(['success'=>'true','message'=>'Subscriber has been updated successfully']);

                } catch (Exception $e) {
                    
                    DB::rollBack();
                    return response()->json(['errors'=>'true','message'=>$e->getMessage()]);
                    
                }
    }

    public function deleteRegisteredUser($id)
    {
        $user = User::find($id);
        deleteWpNewsUser($user);
        $user->delete();
        return response()->json(['success'=>'true','message'=>'Subscriber has been deleted successfully']);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [

                        'last_name' => 'required',
                        'first_name' => 'required',
                        'mobile' => 'required|numeric|unique:users',
                        'gender' => 'required',
                        'email' => 'required|string|email|unique:users',
                        'password' => 'required|string|min:6|confirmed',
                        'active' => 'required',
                        'subscription_id' => 'required',
                        'starts_at' => 'required',
                        'alumini_member' =>'required',
                        'alumini_course' =>'required_if:alumini_member,1',
                        'alumini_attend' =>'required_if:alumini_member,1',
                 ]);

                
                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }

                DB::beginTransaction();
                try{
                    $generator = new Generator();
                    $User = new User;
                    // USER
                    $User->last_name = $request->input('last_name');
                    $User->first_name = $request->input('first_name');
                    $User->display_name = $request->input('first_name') . " " . $request->input('last_name');
                    $User->username= $generator->generate($request->input('first_name')." ".$request->input('last_name'));
                    $User->mobile = $request->input('mobile');
                    $User->birthday = $request->input('birthday');
                    $User->gender = $request->input('gender');
                    $User->email = $request->input('email');
                    $User->password = bcrypt($request->input('password'));
                    $User->alumini_member =$request->input('alumini_member');
                    $User->alumini_course = $request->input('alumini_course');
                    $User->alumini_attend = $request->input('alumini_attend');
                    $User->alumini_subscription_id = $request->subscription_id;
                    $User->active = $request->input('active');
                    $User->save(); 
                    $User->assignRole('user');
                    //END

                    /* CREATE USER AT WP NEWS WEBSITE */
                    $wp_resp = createWpNewsUser($request, $User);
                    $wp_resp = json_decode($wp_resp, true);
                    
                    $User->wp_news_user_id = $wp_resp['id'];
                    $User->save();

                    $day = Subscription::where('id',$request->subscription_id)->first('days_duration');
                    $starts_at = new Carbon($request->starts_at);
                    $expires_at = $starts_at->addDays($day->days_duration);
                    
                    // USER SUBSCRIPTION
                    $Subscriber = new Subscriber;
                    $Subscriber->user_id = $User->id;
                    $Subscriber->subscription_id = $request->subscription_id;
                    $Subscriber->active = $request->active_subscription;
                    $Subscriber->starts_at = $request->starts_at;
                    $Subscriber->expires_at = $expires_at;
                    $Subscriber->comments = $request->comments;
                    $Subscriber->save();
                    
                    $password = $request->input('password');

                    DB::commit();

                    Mail::to($User)->send(new Subscribed($Subscriber, $password));

                    return response()->json(['success'=>'true','message'=>'Subscriber has been added successfully']);

                } catch (Exception $e) {
                    
                    DB::rollBack();
                    return response()->json(['errors'=>'true','message'=>$e->getMessage()]);
                    
                }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function show(Subscriber $subscriber)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscriber $subscriber)
    {
        $subscriptions = Subscription::get()->pluck('complete_package','id'); 
        $courses = Course::all()->pluck('name','name');
        return view('admin.subscriber.ajax.edit', compact('subscriber','subscriptions','courses')); 
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
           
        $validator = Validator::make($request->all(), [
                        'last_name' => 'required',
                        'first_name' => 'required',
                        'mobile' => 'required|numeric',
                        'gender' => 'required',
                        'email' => 'required|string|email|unique:users,email,'.$request->input('id'),
                        'active' => 'required',
                        'subscription_id' => 'required',
                        'starts_at' => 'nullable',
                        'expires_at' => 'required',
                        'alumini_member' =>'required',
                        'alumini_course' =>'required_if:alumini_member,1',
                        'alumini_attend' =>'required_if:alumini_member,1',
                 ]);


                
                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }

                DB::beginTransaction();
                try{
                    
                    $user = User::find($request->id);
                    // USER
                    $user->last_name = $request->input('last_name');
                    $user->first_name = $request->input('first_name');
                    $user->mobile = $request->input('mobile');
                    $user->birthday = $request->input('birthday');
                    $user->gender = $request->input('gender');
                    $user->email = $request->input('email');
                    $user->alumini_subscription_id = $request->subscription_id;
                    $user->alumini_member =$request->input('alumini_member');
                    $user->alumini_course = $request->input('alumini_course');
                    $user->alumini_attend = $request->input('alumini_attend');

                    $password = $request->input('password');
                    if(!empty($password))
                        $user->password = bcrypt($password);
                  
                    $user->active = $request->input('active');
                    $user->save(); 
                    // return $request;
                    //END

                   
                     if(!empty($request->subscription_id))
                        {     
                          $subscriber = Subscriber::where('user_id',$user->id)->first();
                          
                          if(!$subscriber)
                             return response()->json(['errors'=>'true','message'=> 'Subscriber not found']);

                          $subscriber->user_id = $user->id;
                          $subscriber->subscription_id = $request->subscription_id;
                          $subscriber->active = $request->active_subscription;
                          $subscriber->starts_at = $request->starts_at;
                          $subscriber->expires_at = $request->expires_at;
                          $subscriber->comments = $request->comments;
                          $subscriber->save();
                        }
                    DB::commit();
                    return response()->json(['success'=>'true','message'=>'Subscriber has been updated successfully']);

                } catch (Exception $e) {
                    
                    DB::rollBack();
                    return response()->json(['errors'=>'true','message'=>$e->getMessage()]);
                    
                }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscriber $subscriber)
    {
        $user = User::find($subscriber->user_id);
        deleteWpNewsUser($user);
        $user->delete();
        $subscriber->delete();
        return response()->json(['success'=>'true','message'=>'Subscriber has been deleted successfully']);
    }

    public function export()
    {
      
       return (new FastExcel(Subscriber::all()))->download('subscribers.csv', function ($subscriber) {
                      
           return [
               'Last Name' => @$subscriber->user->last_name,
               'First Name' => @$subscriber->user->first_name,
               'Email' => @$subscriber->user->email,
               'Contact' => @$subscriber->user->mobile,
               'Gender' => @$subscriber->user->gender,
               'Birthdate' => @$subscriber->user->birthday,
               'Alumnus' => @$subscriber->user->alumini_course,
               'Year Attended' => @$subscriber->user->alumini_attend,
               'Subscription' => @$subscriber->subscription->name,
               'Date Registered' => Carbon::parse(@$subscriber->created_at)->format('Y-m-d'),
           ];

       });
    }

    public function exportRegisteredUsers()
    {
      
       return (new FastExcel(User::doesntHave('subscription')->role('user')->get()))->download('registered_users.csv', function ($user) {               
           return [
               'Last Name' => @$user->last_name,
               'First Name' => @$user->first_name,
               'Email' => @$user->email,
               'Contact' => @$user->mobile,
               'Gender' => @$user->gender,
               'Birthdate' => @$user->birthday,
               'Alumnus' => @$user->alumini_course,
               'Year Attended' => @$user->alumini_attend,
               'Date Registered' => Carbon::parse(@$user->created_at)->format('Y-m-d'),
           ];

       });
    }
}

<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use DataTables;
use Yajra\DataTables\Html\Builder;
use App\PaymentMode;
use App\PaymentType;
use App\Subscriber;
use App\Subscription;
use App\Course;
use App\User;
use App\CourseEnrollment;
use Validator;
use DB;
use Auth;
use Rap2hpoutre\FastExcel\FastExcel;
use Akaunting\Money\Money;
use Carbon\Carbon;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use Sample\PayPalClient;
use App\Providers\PayPalClientServiceProvider as PayPalClientLive;

class PaymentController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Builder $builder)
  {
    if (request()->ajax()) {
      return DataTables::of(Payment::with(['payment_mode', 'payment_type', 'user']))
        ->editColumn('payment_mode', function (Payment $payment) {
          return  $payment->payment_mode->name;
        })
        ->editColumn('subscriber_id', function (Payment $payment) {
          return  @$payment->subscriber->subscription->name;
        })
        ->editColumn('action', function (Payment $payment) {
          return  ModelBtn2('payment', $payment->id);
        })
        ->toJson();
    }
    $builder->columns([

      ['data' => 'date', 'name' => 'date', 'title' => 'Date Paid'],
      ['data' => 'payment_mode', 'name' => 'payment_mode.name', 'title' => 'Mode'],
      ['data' => 'subscriber_id', 'name' => 'subscriber_id', 'title' => 'Subscription'],
      ['data' => 'formatted_money', 'name' => 'formatted_money', 'title' => 'Amount'],
      ['data' => 'last_name', 'name' => 'last_name', 'title' => 'Last Name'],
      ['data' => 'first_name', 'name' => 'first_name', 'title' => 'First Name'],
      ['data' => 'payer_email', 'name' => 'payer_email', 'title' => 'Email'],
      ['data' => 'payer_mobile', 'name' => 'payer_mobile', 'title' => 'Mobile'],
      ['data' => 'txn_id', 'name' => 'txn_id', 'title' => 'Transaction ID'],
      ['data' => 'remark', 'name' => 'remark', 'title' => 'Comments'],
      ['data' => 'action', 'name' => 'action', 'title' => 'Action', 'searchable' => 'false', 'orderable' => 'false'],

    ]);
    $datatable =  $builder->parameters([
      'searchDelay' => 500,
      'order' => [[0, "desc"]]
    ]);

    $title = "Manage Payments";
    $payment_modes = PaymentMode::get()->pluck('name', 'id');
    $payment_types = PaymentType::get()->pluck('name', 'id');
    return view('admin.payment.index', compact('datatable', 'title', 'payment_types', 'payment_modes'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [

      'payment_mode_id' => 'required',
      'amount' => 'required',
      'payment_type_id' => 'required',

      'date' => 'required',

    ]);


    if ($validator->fails()) {
      return response()->json(['errors' => $validator->errors()->first()]);
    }


    $Payment = new Payment;


    $Payment->user_id = $request->input('user_id');
    $Payment->payment_mode_id = $request->input('payment_mode_id');
    $Payment->amount = $request->input('amount');
    $Payment->txn_id = $request->input('txn_id');
    $Payment->transaction = 'credit'; //$request->input('transaction')
    $Payment->payment_type_id = $request->input('payment_type_id');
    $Payment->payer_name = $request->input('payer_name');
    $Payment->payer_email = $request->input('payer_email');
    $Payment->payer_mobile = $request->input('payer_mobile');
    $Payment->course_enrollment_id = $request->input('course_enrollment_id');
    $Payment->date = $request->input('date');
    $Payment->remark = $request->input('remark');

    $Payment->save();


    return response()->json(['success' => 'true', 'message' => 'Payment has been added successfully']);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Payment  $payment
   * @return \Illuminate\Http\Response
   */
  public function show(Payment $payment)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Payment  $payment
   * @return \Illuminate\Http\Response
   */
  public function edit(Payment $payment)
  {
    $payment_modes = PaymentMode::get()->pluck('name', 'id');
    $payment_types = PaymentType::get()->pluck('name', 'id');
    return view('admin.payment.ajax.edit', compact('payment', 'payment_modes', 'payment_types'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Payment  $payment
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Payment $payment)
  {

    $validator = Validator::make($request->all(), [

      'payment_mode_id' => 'required',
      'amount' => 'required',
      'payment_type_id' => 'required',
      'date' => 'required',

    ]);


    if ($validator->fails()) {
      return response()->json(['errors' => $validator->errors()->first()]);
    }


    $payment->user_id = $request->input('user_id');
    $payment->payment_mode_id = $request->input('payment_mode_id');
    $payment->amount = $request->input('amount');
    $payment->txn_id = $request->input('txn_id');
    $payment->transaction = 'credit'; //$request->input('transaction')
    $payment->payment_type_id = $request->input('payment_type_id');
    $payment->payer_name = $request->input('payer_name');
    $payment->payer_email = $request->input('payer_email');
    $payment->payer_mobile = $request->input('payer_mobile');
    $payment->course_enrollment_id = $request->input('course_enrollment_id');
    $payment->date = $request->input('date');
    $payment->remark = $request->input('remark');

    $payment->save();


    return response()->json(['success' => 'true', 'message' => 'Payment has been added successfully']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Payment  $payment
   * @return \Illuminate\Http\Response
   */
  public function destroy(Payment $payment)
  {
    $payment->delete();
    return response()->json(['success' => 'true', 'message' => 'Payment has been deleted successfully']);
  }

  public function userSearch(Request $request)
  {
    $q = $request->input('term');

    $result = User::role('user')->where('first_name', 'LIKE', "$q%")->orWhere('last_name', 'LIKE', "$q%")->get();
    foreach ($result as $row) {
      $data[] = array('id' => $row->id, 'label' => $row->name . " - " . $row->email . " - " . $row->mobile, 'value' => $row->name . " - " . $row->email . " - " . $row->mobile);
    }
    return $data;
  }

  public function courseSearch(Request $request)
  {
    $user_id = $request->input('user_id');
    $result = CourseEnrollment::where('user_id', $user_id)->get();

    foreach ($result as $row) {
      $data[] = array('value' => $row->id, 'text' => $row->course->name);
    }
    return $data;
  }

  public function changePlan($paymenttypeid, Request $request)
  {
    $payment_type = PaymentType::findOrFail($paymenttypeid);

    $subscriptions = Subscription::where('fee', '>', 0)->get();

    $user_plan_expire_date = Auth::user()->subscriber->expires_at;

    # Check if user plan has been expired
    $plan_expired = false;
    if(\Carbon\Carbon::parse($user_plan_expire_date)->isPast())
      $plan_expired = true;

    $title = "Change Subscription Plan";

    return view('portal.payment.subscription-upgrade', compact('title', 'payment_type', 'subscriptions', 'plan_expired'));
  }

  public function execute(Request $request)
  {
    $validator = Validator::make($request->all(), [

      'subscription_id' => 'required',

    ]);

    if ($validator->fails()) {
      return response()->json(['errors' => $validator->errors()->first()]);
    }

    if (!empty($request->getSubscriptionFee) && !empty($request->subscription_id)) {
      $fee = Subscription::where('id', $request->subscription_id)->first()->fee;
      return response()->json(['success' => 'true', 'fee' => $fee]);
    }

    if (env('PAYPAL_MODE') == 'live')
      $client = PayPalClientLive::client();
    else
      $client = PayPalClient::client();

    $response = $client->execute(new OrdersGetRequest($request->orderID));

    if ($response->result->status == 'COMPLETED') {
      $result = $response->result;
      $dt = Carbon::now();
      $purchase_amount = 0;
      foreach ($response->result->purchase_units as $purchase_unit) {
        $purchase_amount += $purchase_unit->amount->value;
      }

      DB::beginTransaction();
      try {
        $payment = new Payment;
        $payment->user_id = Auth::id();
        $payment->payment_mode_id = 1; //paypal
        $payment->amount = $purchase_amount;
        $payment->txn_id = $request->input('orderID');
        $payment->transaction = 'credit';
        $payment->payment_type_id = $request->paymentTypeId;
        $payment->payer_name = Auth::user()->name;
        $payment->payer_email = Auth::user()->email;
        $payment->payer_mobile = Auth::user()->mobile;
        $payment->date = $dt->toDateString();
        $payment->callback_response = json_encode($response);
        $payment->save();

        $subscription = Subscription::where('id', $request->subscription_id)->first();
        $starts_at = $dt->toDateString();
        $expires_at = $dt->addDays($subscription->days_duration)->toDateString();

        $subscriber = Subscriber::where('user_id', Auth::id())->first();

        if (!$subscriber)
          $subscriber = new Subscriber;

        $subscriber->user_id = Auth::id();
        $subscriber->subscription_id = $subscription->id;
        $subscriber->starts_at = $starts_at;
        $subscriber->expires_at = $expires_at;
        $subscriber->save();

        $payment->subscriber_id = $subscriber->id;
        $payment->save();
        DB::commit();
        return response()->json(['success' => 'true', 'payment' => $payment, 'url' => route('pay-success', $payment->id)]);
      } catch (\Exception $e) {

        DB::rollBack();
        return response()->json(['errors' => 'true', 'message' => $e->getMessage()]);
      }
    }
  }

  public function success($payment_id)
  {
    $title = 'Payment Successful';
    $payment = Payment::where(['id' => $payment_id, 'user_id' => Auth::id()])->First();
    $subscription = Subscription::
    default()->firstOrFail();
    return view('portal.payment.success', compact('title', 'payment', 'subscription'));
  }

  public function failed($payment_id)
  {
    $title = 'Payment Receipt';
    $payment = Payment::where(['id' => $payment_id, 'user_id' => Auth::id()])->firstOrFail();
    return view('portal.payment.receipt', compact('title', 'payment'));
  }

  public function enrollmentSuccess($payment_id = "", Request $request)
  {

    if ($request->session()->has('CourseEnrollmentID'))
      $CourseEnrollmentID = $request->session()->get('CourseEnrollmentID');
    else
      return response(401);

    $courseEnrollment = CourseEnrollment::where("id", $CourseEnrollmentID)->firstOrFail();
    $title = 'Success';

    $payment = $courseEnrollment->payment->first();

    $subscription = Subscription::Active()->firstOrFail();

    return view('frontend.payment.success', compact('title', 'payment', 'subscription', 'courseEnrollment'));
  }

  public function enrollmentFailed($user_id, $payment_id = "")
  {
    $title = 'Payment Receipt';
    $payment = Payment::where(['id' => $payment_id, 'user_id' => Auth::id()])->firstOrFail();
    return view('portal.payment.receipt', compact('title', 'payment'));
  }

  public function export()
  {

    return (new FastExcel(Payment::all()))->download('payments.csv', function ($payment) {

      return [
        'Date' => $payment->date,
        'Payment Mode' => $payment->payment_mode->name,
        'Subscription' => @$payment->subscriber->subscription->name,
        'Amount' => $payment->amount,
        'Last Name' => $payment->last_name,
        'First Name' => $payment->first_name,
        'Email' => $payment->payer_email,
        'Mobile' => $payment->payer_mobile,
        'Txn ID' => $payment->txn_id,
      ];
    });
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Country;
use App\AnalysisHistoricalWatchlist;
use App\AnalysisConsensus;
use App\AnalysisBtmStock;
use App\AnalysisDividend;
use App\AnalysisCompanyBuyback;
use App\AnalysisInsiderTransaction;
use App\AnalysisInsiderTransactionHistory;
use App\AnalysisPHData;
use App\AnalysisWatchlist;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Carbon\Carbon;
class AnalysisPageController extends Controller
{

    // INSIDER TRANSACTION HISTORY
    public function watchlist(Builder $builder, Request $request)
    {
        // return $request;
        $search = false;
        $q = AnalysisWatchlist::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('date_added','>=', $request->from)->whereDate('date_added','<=', $date_to);
        }

        if (request()->ajax()) {
            return DataTables::of($q)
                    ->toJson();
        }
        $title = "Analysis - Watchlist";
        $stocks = Stock::all();
        $route = 'portal.analysis.watchlist.index';
        $builder->columns([
                        
                        ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                        ['data' => 'price_added', 'name' => 'price_added', 'title' => 'Price Added (Php)'],
                        ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
                        ['data' => 'date_added', 'name' => 'date_added', 'title' => 'Date Added'],
                        ['data' => 'comments', 'name' => 'comments', 'title' => 'Comments'],
                        
                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);

        if(AnalysisWatchlist::count() > 0){
          $last_updated_at = AnalysisWatchlist::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";
      
        return view('portal.analysis.watchlist', compact('datatable','title','stocks','search','request','route','last_updated_at'));
    }

    // HISTORICAL WATCHLISTS
    public function historicalWatchlist(Builder $builder, Request $request)
    {
        
        $search = false;
        $q = AnalysisHistoricalWatchlist::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('date_added','>=', $request->from)->whereDate('date_added','<=', $date_to);
        }

        if (request()->ajax()) {
            return DataTables::of($q)
            ->toJson();
        }
        $title = "Analysis - Historical Watchlist";
        $stocks = Stock::all();
        $route = 'portal.historical.watchlist';
        $builder->columns([
          
         ['data' => 'date_added', 'name' => 'date_added', 'title' => 'Date Added'],
         ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
         ['data' => 'price_added', 'name' => 'price_added', 'title' => 'Price Added'],
         ['data' => 'setup', 'name' => 'setup', 'title' => 'Setup'],
         ['data' => 'date_deleted', 'name' => 'date_deleted', 'title' => 'Date Deleted'],
         ['data' => 'price_deleted', 'name' => 'price_deleted', 'title' => 'Price Deleted'],
         ['data' => 'return', 'name' => 'return', 'title' => 'Return (%)'],
         ['data' => 'comments', 'name' => 'comments', 'title' => 'Comments']

     ]);
        $datatable =  $builder->parameters([
          'searchDelay' => 500,
          'stateSave' => 'true',
          'order' => [[ 0, "desc" ]]
      ]);

        if(AnalysisHistoricalWatchlist::count() > 0){
          $last_updated_at = AnalysisHistoricalWatchlist::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";

        return view('portal.analysis.historical_watchlist', compact('datatable','title','stocks','search','request','route','last_updated_at'));
    }

    // CONSENSUS
    public function consensus(Builder $builder, Request $request)
    {
        // return $request;
        $search = false;
        $q = AnalysisConsensus::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('created_at','>=', $request->from)->whereDate('created_at','<=', $date_to);
        }

        if (request()->ajax()) {
            return DataTables::of($q)
          
                    ->toJson();
        }
        $title = "Analysis - Consensus";
        $builder->columns([
                      
                       ['data' => 'ticker', 'name' => 'ticker', 'title' => 'Ticker'],
                       ['data' => 'market_cap', 'name' => 'market_cap', 'title' => 'Market Cap'],
                       ['data' => 'highest_target_price', 'name' => 'highest_target_price', 'title' => 'Highest Target Price'],

                       ['data' => 'sales_4Q18', 'name' => 'sales_4Q18', 'title' => '4Q18'],
                       ['data' => 'sales_1Q19', 'name' => 'sales_1Q19', 'title' => '1Q19'],
                       ['data' => 'net_income_4Q18', 'name' => 'net_income_4Q18', 'title' => '4Q18'],
                       ['data' => 'net_income_1Q19', 'name' => 'net_income_1Q19', 'title' => '1Q19'],
                       ['data' => 'pe', 'name' => 'pe', 'title' => 'P/E'],
                       ['data' => 'roe', 'name' => 'roe', 'title' => 'ROE (%)'],
                       ['data' => 'div_yield', 'name' => 'div_yield', 'title' => 'DIV YIELD']

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]],
               'rowsGroup'=>2
          ]);

        if(AnalysisConsensus::count() > 0){
          $last_updated_at = AnalysisConsensus::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";

        return view('portal.analysis.consensus', compact('datatable','title','stocks','search','request','last_updated_at'));
    }

    // STOCK BTM
    public function btmStock(Builder $builder, Request $request)
    {

        // return $request;
        $search = false;
        $q = AnalysisBtmStock::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('created_at','>=', $request->from)->whereDate('created_at','<=', $date_to);
        }

        if (request()->ajax()) {
           return DataTables::of($q)
                   ->toJson();
       }
       $title = "Analysis - BTM Stocks";
       $builder->columns([
                     
                      ['data' => 'company_name', 'name' => 'company_name', 'title' => 'Company Name'],
                      ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
                      ['data' => 'market_cap', 'name' => 'market_cap', 'title' => 'Mkt Cap (PHP Bil)'],
                      ['data' => 'narrative', 'name' => 'narrative', 'title' => 'Narrative'],
                      ['data' => 'last_price', 'name' => 'last_price', 'title' => 'Last Price (PHP)'],
                      ['data' => 'tp', 'name' => 'tp', 'title' => 'TP (PHP)'],
                      ['data' => 'fy19', 'name' => 'fy19', 'title' => 'FY19 EV/EBITDA'],

                  ]);
        $datatable =  $builder->parameters([
             'searchDelay' => 500,
             'stateSave' => 'true',
              'order' => [[ 0, "desc" ]]
         ]);

         if(AnalysisBtmStock::count() > 0){
          $last_updated_at = AnalysisBtmStock::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";

        return view('portal.analysis.btm_stock', compact('datatable','title','stocks','search','request','last_updated_at'));
    }

    // DIVIDEND
    public function dividend(Builder $builder, Request $request)
    {
        // return $request;
        $search = false;
        $q = AnalysisDividend::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('date_closed','>=', $request->from)->whereDate('date_closed','<=', $date_to);
        }

        if (request()->ajax()) {
            return DataTables::of($q)
                    ->toJson();
        }
        $title = "Analysis - Dividends";
        $stocks = Stock::all();
        $route = 'portal.dividend';
        $builder->columns([
                      
                       ['data' => 'type_of_dividends', 'name' => 'type_of_dividends', 'title' => 'Type of Dividends'],
                       ['data' => 'date_closed', 'name' => 'date_closed', 'title' => 'Date Disclosed'],
                       ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                       ['data' => 'amount', 'name' => 'amount', 'title' => 'Amount'],
                       ['data' => 'ex_date', 'name' => 'ex_date', 'title' => 'Ex-Date'],
                       ['data' => 'record_date', 'name' => 'record_date', 'title' => 'Record Date'],
                       ['data' => 'payment_date', 'name' => 'payment_date', 'title' => 'Payment Date'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);

        if(AnalysisDividend::count() > 0){
          $last_updated_at = AnalysisDividend::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";

        return view('portal.analysis.dividend', compact('datatable','title','stocks','search','request','route','last_updated_at'));
    }

    // COMPANY BUYBACK
    public function companyBuyback(Builder $builder, Request $request)
    {
        // return $request;
        $search = false;
        $q = AnalysisCompanyBuyback::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('created_at','>=', $request->from)->whereDate('created_at','<=', $date_to);
        }

       if (request()->ajax()) {
           return DataTables::of($q)
                   ->toJson();
           }
           $title = "Analysis - Company Buyback";
           $stocks = Stock::all();
           $builder->columns([
                         
                          ['data' => 'stock_code', 'name' => 'stock_code', 'title' => 'Stock Code'],
                          ['data' => 'buyback_program', 'name' => 'buyback_program', 'title' => 'Buyback Program'],
                          ['data' => 'amount_left', 'name' => 'amount_left', 'title' => 'Amount Left to Repurchase'],
                          ['data' => 'total_amount', 'name' => 'total_amount', 'title' => 'Total Repurchased Amount'],
                          ['data' => 'repurchased_amount', 'name' => 'repurchased_amount', 'title' => 'Repurchased Amount for the Week Indicated'],


                      ]);
           $datatable =  $builder->parameters([
                 'searchDelay' => 500,
                 'stateSave' => 'true',
                  'order' => [[ 0, "desc" ]]
             ]);

          if(AnalysisCompanyBuyback::count() > 0){
            $last_updated_at = AnalysisCompanyBuyback::latest()->first()->updated_at;
            $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
          }else
            $last_updated_at = "N/A";

        return view('portal.analysis.buyback', compact('datatable','title','stocks','search','request','last_updated_at'));
    }

    // INSIDER TRANSACTION
    public function insiderTransaction(Builder $builder, Request $request)
    {
        // return $request;
        $search = false;
        $q = AnalysisInsiderTransaction::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('created_at','>=', $request->from)->whereDate('created_at','<=', $date_to);
        }

       if (request()->ajax()) {
            return DataTables::of($q)
                    ->toJson();
        }
        $title = "Analysis - Insider Transactions";
        $stocks = Stock::all();
        $builder->columns([
                      
                       ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                       ['data' => 'no_of_shares', 'name' => 'no_of_shares', 'title' => 'Number of Shares'],
                       ['data' => 'price', 'name' => 'price', 'title' => 'Price'],
                       ['data' => 'total', 'name' => 'total', 'title' => 'Total'],
                       ['data' => 'ad', 'name' => 'ad', 'title' => 'A/D'],
                  
                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);

        if(AnalysisInsiderTransaction::count() > 0){
          $last_updated_at = AnalysisInsiderTransaction::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";

        return view('portal.analysis.transaction', compact('datatable','title','stocks','search','request','last_updated_at'));
    }

    // INSIDER TRANSACTION HISTORY
    public function insiderTransactionHistory(Builder $builder, Request $request)
    {
        // return $request;
        $search = false;
        $q = AnalysisInsiderTransactionHistory::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('date','>=', $request->from)->whereDate('date','<=', $date_to);
        }

        if (request()->ajax()) {
            return DataTables::of($q)
                    ->toJson();
        }
        $title = "Analysis - Historical Insider Transaction";
        $stocks = Stock::all();
        $route = 'portal.insider.transaction.history';
        $builder->columns([
                        
                       ['data' => 'date', 'name' => 'date', 'title' => 'Date'],
                       ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                       ['data' => 'no_of_shares', 'name' => 'no_of_shares', 'title' => 'Number of Shares'],
                       ['data' => 'price', 'name' => 'price', 'title' => 'Price'],
                       ['data' => 'total', 'name' => 'total', 'title' => 'Total'],
                       ['data' => 'ad', 'name' => 'ad', 'title' => 'A/D'],
                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);

        if(AnalysisInsiderTransactionHistory::count() > 0){
          $last_updated_at = AnalysisInsiderTransactionHistory::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";

        return view('portal.analysis.transaction_history', compact('datatable','title','stocks','search','request','route','last_updated_at'));
    }

    // PHP DATA
    public function PHData(Builder $builder, Request $request)
    {
        // return $request;
        $search = false;
        $q = AnalysisPHData::query();

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('created_at','>=', $request->from)->whereDate('created_at','<=', $date_to);
        }

        if (request()->ajax()) {
                    return DataTables::of($q)
                            ->toJson();
                }
                $title = "Analysis - PH Data";
                $countries = Country::pluck('country_name','country_name');
                $builder->columns([
                              
                               ['data' => 'country', 'name' => 'country', 'title' => 'Country'],
                               ['data' => 'landbased', 'name' => 'landbased', 'title' => 'Landbased'],
                               ['data' => 'seabased', 'name' => 'seabased', 'title' => 'Seabased'],
                               ['data' => 'month', 'name' => 'month', 'title' => 'Month'],
                               ['data' => 'year', 'name' => 'year', 'title' => 'Year'],

                           ]);
                $datatable =  $builder->parameters([
                      'searchDelay' => 500,
                      'stateSave' => 'true',
                       'order' => [[ 0, "desc" ]]
                  ]);

        if(AnalysisPHData::count() > 0){
          $last_updated_at = AnalysisPHData::latest()->first()->updated_at;
          $last_updated_at = Carbon::parse($last_updated_at)->isoFormat('MMMM D, Y');
        }else
          $last_updated_at = "N/A";
 
        return view('portal.analysis.ph_data', compact('datatable','title','stocks','search','request','countries','last_updated_at'));
    }
}

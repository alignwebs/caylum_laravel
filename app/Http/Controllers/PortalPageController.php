<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiDataMarket;
class PortalPageController extends Controller
{
    public function realTimeMonitoring()
    {
        $title = "Real Time Monitoring";
        $rows = ApiDataMarket::makertComapreData('pse');
        return view('portal.stock.real_time_monitoring', compact('rows','title'));
    }

    public function marketSummary()
    {
        $title = "Market Summary";
        $rows = ApiDataMarket::makertComapreData('pse');
        return view('portal.stock.market_summary', compact('rows','title'));
    }

    public function marketStatus()
    {
        $title = "Market Status";
        $rows = ApiDataMarket::makertComapreData('pse');
        return view('portal.stock.market_status', compact('rows','title'));
    }
}

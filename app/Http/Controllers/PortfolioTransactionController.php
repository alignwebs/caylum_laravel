<?php

namespace App\Http\Controllers;
use App\TradePortfolioCategory;
use App\TradePortfolioSubCategory;
use App\Stock;
use App\User;
use App\Market;
use App\PortfolioTransaction;
use Illuminate\Http\Request;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;
use Auth;
class PortfolioTransactionController extends Controller
{

    public function index(Builder $builder,Request $request,$market)
    {    
        // return $request;
        $search = false;
        $q = PortfolioTransaction::where('user_id',Auth::id());

        if(!empty($request->type))
        {
            $search =true;
            $q->where("type",$request->type);
        }

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('created_at','>=', $request->from)->whereDate('created_at','<=', $date_to);
        }

        if (request()->ajax()) {
            return DataTables::of($q)
                     ->addColumn('trade_name', function(PortfolioTransaction $portfolioTransaction){
                              if($portfolioTransaction->type == 'buy' || $portfolioTransaction->type == 'sell')
                                return '<a href="'.route('portal.portfolio.trade.detail',[$portfolioTransaction->market->symbol,$portfolioTransaction->portfolio_trade_id]).'">'.$portfolioTransaction->portfolio_trade->trade_name.'</a>';
                          })
                    ->editColumn('action', function(PortfolioTransaction $portfolioTransaction) {
                        return  ModelBtnDelete('portal.portfolio.transaction',$portfolioTransaction->id);
                    })
                    ->rawColumns(['trade_name','action'])
                    ->toJson();
        }
        $title = "Manage Portfolio Transactions";
        $builder->columns([
                      
                       ['data' => 'date', 'name' => 'date', 'title' => 'Date'],
                       ['data' => 'trade_name', 'name' => 'trade_name', 'title' => 'Trade Name'],
                       ['data' => 'type', 'name' => 'type', 'title' => 'Type'],
                       ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                       ['data' => 'stock_qty', 'name' => 'stock_qty', 'title' => 'Stock Qty'],
                       ['data' => 'per_stock_rate', 'name' => 'per_stock_rate', 'title' => 'Per Stock Rate'],
                        ['data' => 'fee_percentage', 'name' => 'fee_percentage', 'title' => 'Fee Percentage'],
                       ['data' => 'fee', 'name' => 'fee', 'title' => 'Fee'],
                       ['data' => 'amount', 'name' => 'amount', 'title' => 'Amount'],

                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);
        $title = 'Portfolio Transactions';
        $user = User::find(Auth::id());
        
        $market = Market::where('symbol',$market)->firstOrFail();
        return view('portal.portfolio_transaction.index', compact('title','datatable','user','request','search','market'));
    }

    // DEPOSIT
    public function deposit()
    {   
        return view('portal.portfolio_transaction.ajax.deposit');
    }

    public function addDeposit(Request $request,$market_id)
    {
       
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'amount' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $PortfolioTransaction = new PortfolioTransaction;
        $PortfolioTransaction->user_id = Auth::id();
        $PortfolioTransaction->market_id = $market_id;
        $PortfolioTransaction->date = $request->input('date');
        $PortfolioTransaction->amount = $request->input('amount');
        $PortfolioTransaction->type = 'deposit';
        $PortfolioTransaction->save(); 

        PortfolioTransaction::calculateBalance('deposit', $PortfolioTransaction->amount,Auth::id());

        return response()->json(['success'=>'true','message'=>'Trade Portfolio deposit has been added successfully']);
    }


    // WITHDRAW
    public function withdraw()
    {
        return view('portal.portfolio_transaction.ajax.withdraw');
    }

    public function addWithdraw(Request $request,$market_id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'amount' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $PortfolioTransaction = new PortfolioTransaction;
        $PortfolioTransaction->user_id = Auth::id();
        $PortfolioTransaction->market_id = $market_id;
        $PortfolioTransaction->date = $request->input('date');
        $PortfolioTransaction->amount = $request->input('amount');
        $PortfolioTransaction->type = 'withdraw';
        $PortfolioTransaction->save();

        PortfolioTransaction::calculateBalance('withdraw', $PortfolioTransaction->amount,Auth::id());

        return response()->json(['success'=>'true','message'=>'Trade Portfolio Withdraw has been added successfully']);
    }

    // BUY
    public function buy()
    {
        $portfolio_category = TradePortfolioCategory::select('id','name')->get();
        $stocks = Stock::all();
        return view('portal.portfolio_transaction.ajax.buy',compact('title','portfolio_category','stocks'));
    }

    public function addBuy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'amount' => 'required',
            'portfolio_category' => 'required',
            'portfolio_subcategory' => 'required',
            'stock' => 'required',
            'stock_qty' => 'required',
            'per_stock_rate' => 'required',
            'fee' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $PortfolioTransaction = new PortfolioTransaction;
        $PortfolioTransaction->user_id = Auth::id();
        $PortfolioTransaction->date = $request->input('date');
        $PortfolioTransaction->amount = $request->input('amount');
        $PortfolioTransaction->type = 'buy';
        $PortfolioTransaction->portfolio_category = $request->input('portfolio_category');
        $PortfolioTransaction->portfolio_subcategory = $request->input('portfolio_subcategory');
        $PortfolioTransaction->stock = $request->input('stock');
        $PortfolioTransaction->stock_qty = $request->input('stock_qty');
        $PortfolioTransaction->per_stock_rate = $request->input('per_stock_rate');
        $PortfolioTransaction->fee = $request->input('fee');
        $PortfolioTransaction->save();

        PortfolioTransaction::calculateBalance('buy', $PortfolioTransaction->amount,Auth::id());

        return response()->json(['success'=>'true','message'=>'Trade Portfolio Buy has been added successfully']);
    }

    // SELL
    public function sell()
    {
        $portfolio_category = TradePortfolioCategory::pluck('name','id');
        $stocks = Stock::all();
        return view('portal.portfolio_transaction.ajax.sell',compact('portfolio_category','stocks'));
    }

    public function addSell(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'amount' => 'numeric|required',
            'stock' => 'required',
            'stock_qty' => 'numeric|required',
            'per_stock_rate' => 'numeric|required',
            'fee' => 'numeric|required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $PortfolioTransaction = new PortfolioTransaction;
        $PortfolioTransaction->user_id = Auth::id();
        $PortfolioTransaction->date = $request->input('date');
        $PortfolioTransaction->amount = $request->input('amount');
        $PortfolioTransaction->type = 'sell';
        $PortfolioTransaction->stock = $request->input('stock');
        $PortfolioTransaction->stock_qty = $request->input('stock_qty');
        $PortfolioTransaction->per_stock_rate = $request->input('per_stock_rate');
        $PortfolioTransaction->fee = $request->input('fee');
        $PortfolioTransaction->save();

        PortfolioTransaction::calculateBalance('sell', $PortfolioTransaction->amount,Auth::id());

        return response()->json(['success'=>'true','message'=>'Trade Portfolio Sell has been added successfully']);
    }

    // SUB CATEGORY
    public function subcategory(Request $request)
    {
        $trade_portfolio_category_id = $request->trade_portfolio_category_id;
        $portfolio_subcategories = TradePortfolioSubCategory::where('trade_portfolio_category_id',$trade_portfolio_category_id)->select('name','id')->get();
        return view('portal.portfolio_transaction.ajax.subcategory',compact('portfolio_subcategories'));
    }

    public function destroy($id)
    {
        PortfolioTransaction::where(['user_id'=>Auth::id(),'id'=>$id])->delete();
        return response()->json(['success'=>'true','message'=>'Trade Portfolio Transaction has been deleted successfully']);
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use Hash;
use App\Payment;
use App\CourseEnrollment;
use App\Subscriber;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $title = "Dashboard";
        $Subscribers = Subscriber::pluck('user_id');   
        $total['totalRegistrantsCount'] = User::WhereNotIn('id',$Subscribers)->role('user')->count();
        $total['totalRegistrants'] = User::WhereNotIn('id',$Subscribers)->role('user')->get();
        $total['enrollmentCounts'] = CourseEnrollment::count();
        $total['subscriberCounts'] = Subscriber::count();
        $total['payments'] = Payment::latest()->take(10)->get();
        $total['enrollments'] = CourseEnrollment::latest()->take(10)->get();
        $total['subscribers'] = Subscriber::latest()->take(10)->get();
        return view('admin.dashboard', compact('title', 'total'));
    }

    public function changePassword()
    {
        $title = "Change Password";
       return view('admin.change_password', compact('title'));
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
                       'old_password' => 'required',
                       'new_password' => 'required|string|min:6|confirmed' 
                ]);

               
               if ($validator->fails())
               {
                   return response()->json(['errors'=>$validator->errors()->first()]);
               }

                $old_password = $request->input('old_password');

                $hashedPassword = User::where('id',Auth::id())->first()->password;

                if(Hash::check($old_password, $hashedPassword)) {
                    
                    $User = User::find(Auth::id());

                    $User->password = bcrypt($request->input('new_password'));

                    $User->save();

                    return response()->json(['success'=>'true','message'=>'Password has been changed successfully']);

                }else{

                    return response()->json(['success'=>'false','message'=>'Old password does not match']);
                }
              

    }

    // PORTAL

    public function userIndex()
    {
        $title = "Dashboard";
        $user = User::find(Auth::id());
        $followings = $user->followings()->pluck('id');
        $followings[] = Auth::id();
        $suggested_followings = User::has('subscription')->whereNotIn('id',$followings)->inRandomOrder()->role('user')->take(5)->get();
        return view('portal.social.feed', compact('title','suggested_followings','user'));
    }

    public function userChangePassword()
    {
        $title = "Change Password";
        return view('portal.change_password', compact('title'));
    }

    public function chatroom()
    {   
        $title = "Chat Room";
        $chat_popup_disable = true;
        return view('portal.chatroom.index', compact('title', 'chat_popup_disable'));
    }

    public function viewSearchPage(Request $request)
    {
        $request->validate([ 'q' => 'required' ]);

        $q = strtolower($request->input('q'));
        $title = "Search Result - ".$q;

        $users = User::has('subscription')->where('id', '!=', Auth::id())->where('first_name', $q)->OrWhere('last_name')->limit(25)->get();

        return view('portal.search', compact('title','q', 'users'));
    }
}

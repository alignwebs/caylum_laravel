<?php

namespace App\Http\Controllers;

use App\AnalysisInsiderTransaction;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
use App\Stock;
class AnalysisInsiderTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(AnalysisInsiderTransaction::query())
                    ->editColumn('action', function(AnalysisInsiderTransaction $transaction) {
                        return  ModelBtn2('transaction',$transaction->id);
                    })
                    ->toJson();
        }
        $title = "Analysis - Insider Transactions";
        $stocks = Stock::all();
        $builder->columns([
                      
                       ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                       ['data' => 'no_of_shares', 'name' => 'no_of_shares', 'title' => 'Number of Shares'],
                       ['data' => 'price', 'name' => 'price', 'title' => 'Price'],
                       ['data' => 'total', 'name' => 'total', 'title' => 'Total'],
                       ['data' => 'ad', 'name' => 'ad', 'title' => 'A/D'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);

        return view('admin.analysis.transaction.index', compact('datatable','title','stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $validator = Validator::make($request->all(), [
                    'stock' => 'required',
                    'no_of_shares' => 'required',
                    'price' => 'required',
                    'total' => 'required',
                    'ad' => 'required',
                   
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisInsiderTransaction = new AnalysisInsiderTransaction;
                $AnalysisInsiderTransaction->stock = $request->input('stock');
                $AnalysisInsiderTransaction->no_of_shares = $request->input('no_of_shares');
                $AnalysisInsiderTransaction->price = $request->input('price');
                $AnalysisInsiderTransaction->total = $request->input('total');
                $AnalysisInsiderTransaction->ad = $request->input('ad');
                $AnalysisInsiderTransaction->save();

                return response()->json(['success'=>'true','message'=>'Analysis Insider Transaction has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisInsiderTransaction  $analysisInsiderTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisInsiderTransaction $analysisInsiderTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisInsiderTransaction  $analysisInsiderTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisInsiderTransaction $transaction)
    {
        $stocks = Stock::all();
        return view('admin.analysis.transaction.ajax.edit', compact('transaction','stocks')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisInsiderTransaction  $analysisInsiderTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisInsiderTransaction $transaction)
    {
                    $validator = Validator::make($request->all(), [
                           'stock' => 'required',
                           'no_of_shares' => 'required',
                            'price' => 'required',
                            'total' => 'required',
                            'ad' => 'required',
                          
                       ]);

                       if ($validator->fails())
                       {
                           return response()->json(['errors'=>$validator->errors()->first()]);
                       }
                       
                       $AnalysisInsiderTransaction = AnalysisInsiderTransaction::find($transaction->id);
                       $AnalysisInsiderTransaction->stock = $request->input('stock');
                       $AnalysisInsiderTransaction->no_of_shares = $request->input('no_of_shares');
                       $AnalysisInsiderTransaction->price = $request->input('price');
                       $AnalysisInsiderTransaction->total = $request->input('total');
                       $AnalysisInsiderTransaction->ad = $request->input('ad');
                       $AnalysisInsiderTransaction->save();

                       return response()->json(['success'=>'true','message'=>'Analysis Insider Transaction has been added successfully']);
    }

    // IMPORT EXCEL
    public function import(Request $request)
        {
         
            $validator = Validator::make($request->all(), [
             'insider_transaction' => 'required',
            ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('insider_transaction')->getClientOriginalExtension();  
            $filename = $request->file('insider_transaction')->storeAs('insider_transaction',time().".".$getClientOriginalExtension);
            $path = $request->file('insider_transaction')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['stock']) || empty($row['stock']) || !isset($row['no_of_shares']) || empty($row['no_of_shares']) || !isset($row['price']) || empty($row['price']) || !isset($row['total']) || empty($row['total']) || !isset($row['ad']) || empty($row['ad']))
                {
                    continue;
                }

                $data[] = [
                  'stock'  =>  $row['stock'],
                  'no_of_shares'  =>  isset($row['no_of_shares']) ? $row['no_of_shares'] : null,
                  'price'  =>  isset($row['price']) ? $row['price'] : null,
                  'total'  =>  isset($row['total']) ? $row['total'] : null,
                  'ad'  =>  isset($row['ad']) ? $row['ad'] : null
              ]; 
          
      }
          AnalysisInsiderTransaction::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisInsiderTransaction  $analysisInsiderTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisInsiderTransaction $transaction)
    {
        $transaction->delete();
        return response()->json(['success'=>'true','message'=>'Analysis Company Buyback has been deleted successfully']);
    }
}

<?php

namespace App\Http\Controllers;

use App\PortfolioTrade;
use App\PortfolioTransaction;
use App\TradePortfolioCategory;
use App\User;
use App\Market;
use App\Attachment;
use App\Stock;
use Validator;
use Yajra\DataTables\Html\Builder;
use DataTables;
use Auth;
use Image;
use Storage;
use Illuminate\Http\Request;

class PortfolioTradeController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth', ['except' => ['tradeDetail']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Builder $builder,Request $request,$market)
          {    

              if (request()->ajax()) {

                  return DataTables::of(PortfolioTrade::where('user_id',Auth::id()))
                          
                          ->editColumn('trade_name', function(PortfolioTrade $portfolioTrade){
                              return '<a href="'.route('portal.portfolio.trade.detail',[$portfolioTrade->market->symbol,$portfolioTrade->id]).'">'.$portfolioTrade->trade_name.'</a>';
                          })
                          ->editColumn('is_private', function(PortfolioTrade $portfolioTrade){
                                return is_private($portfolioTrade->is_private);
                          })
                          ->editColumn('action', function(PortfolioTrade $portfolioTrade) {
                              return  ModelBtn2('portal.portfolio.trade',$portfolioTrade->id,'','',$portfolioTrade);
                          })
                          ->rawColumns(['trade_name','action'])
                          ->toJson();
              }
              $title = "Manage Portfolio Trades";
              $builder->columns([
                            
                             ['data' => 'trade_name', 'name' => 'trade_name', 'title' => 'Trade Name'],
                             ['data' => 'portfolio_category', 'name' => 'portfolio_category', 'title' => 'Trade Setup'],
                             ['data' => 'is_private', 'name' => 'is_private', 'title' => 'Is Private'],
                             ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                         ]);
              $datatable =  $builder->parameters([
                    'searchDelay' => 500,
                    'stateSave' => 'true',
                     'order' => [[ 0, "desc" ]]
                ]);
             
              $user = User::find(Auth::id());
              $market = Market::where('symbol',$market)->firstOrFail();
              $portfolio_categories = TradePortfolioCategory::all();
              return view('portal.portfolio_trade.index', compact('title','datatable','user','request','search','market','portfolio_categories'));
          }

    public function add(Request $request, $market_id)
    {
        $validator = Validator::make($request->all(), [
            'trade_name' => 'required',
            'portfolio_category' => 'required',
            
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $PortfolioTrade = new PortfolioTrade;
        $PortfolioTrade->user_id = Auth::id();
        $PortfolioTrade->market_id = $market_id;
        $PortfolioTrade->trade_name = $request->input('trade_name');
        $PortfolioTrade->portfolio_category = $request->input('portfolio_category');
        $PortfolioTrade->comments = $request->input('comments');
        $PortfolioTrade->is_private = $request->has('is_private') ? 1 : 0;
        $PortfolioTrade->save(); 

        return response()->json(['success'=>'true','message'=>'Portfolio Trade has been added successfully']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PortfolioTrade  $portfolioTrade
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio_trade = PortfolioTrade::where(['id'=>$id,'user_id'=>Auth::id()])->firstOrFail();
        
        $portfolio_categories = TradePortfolioCategory::all();
        return view('portal.portfolio_trade.ajax.edit', compact('portfolio_trade','portfolio_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PortfolioTrade  $portfolioTrade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PortfolioTrade $portfolioTrade)
    {
                $validator = Validator::make($request->all(), [
                    'trade_name' => 'required',
                    'portfolio_category' => 'required',
                    
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $PortfolioTrade = PortfolioTrade::where(['id' => $request->id,'user_id' => Auth::id()])->firstOrFail();
                $PortfolioTrade->trade_name = $request->input('trade_name');
                $PortfolioTrade->portfolio_category = $request->input('portfolio_category');
                $PortfolioTrade->comments = $request->input('comments');
                $PortfolioTrade->is_private = $request->has('is_private') ? 1 : 0;
                $PortfolioTrade->save(); 

                return response()->json(['success'=>'true','message'=>'Portfolio Trade has been updated successfully']);
    }

    // TRANSACTION TABLE

    public function tradeDetail(Builder $builder,$market,$id,Request $request)
    {

        $PortfolioTrade = PortfolioTrade::where('id',$id)->first();
        $is_owner = false;
        if(Auth::id() != $PortfolioTrade->user_id)
        {
          if($PortfolioTrade->is_private)
              abort(404);
        }
        else{

            $is_owner = true;
        }

        $search = false;
        $q = PortfolioTransaction::where(['portfolio_trade_id'=>$id]);

        if(!empty($request->type))
        {
            $search =true;
            $q->where("type",$request->type);
        }

        if(!empty($request->from))
        {   
            $search =true;
            $date_to = $request->from;
            if(!empty($request->to))
                $date_to = $request->to;
        
            $q->whereDate('created_at','>=', $request->from)->whereDate('created_at','<=', $date_to);
        }

        if (request()->ajax()) {

            return DataTables::of($q)
                    ->addColumn('image', function(PortfolioTransaction $portfolioTransaction) {
                        return '<div class="profile-image float-md-right" data-fancybox="gallery" href="'.get_image_path(@$portfolioTransaction->attachment->file).'" style="cursor: pointer;"> <img src="'.get_image_path(@$portfolioTransaction->attachment->file).'" alt="" width="100"> </div>';
                    })
                     ->editColumn('action', function(PortfolioTransaction $portfolioTransaction) {
                              if(Auth::id() == $portfolioTransaction->user_id)
                                return  ModelBtnDelete('portal.portfolio.transaction',$portfolioTransaction->id);
                     })
                    ->rawColumns(['image','action'])
                    ->toJson();
        }
        $title = "Manage Portfolio Trade Transactions";
        $user = User::find(Auth::id());
        $stocks = Stock::all();
        $market = Market::where('symbol',$market)->firstOrFail();
        $builder->columns([
                      
                       ['data' => 'date', 'name' => 'date', 'title' => 'Date'],
                       ['data' => 'image', 'name' => 'image', 'title' => 'Image'],
                       ['data' => 'type', 'name' => 'type', 'title' => 'Type'],
                       ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                       ['data' => 'stock_qty', 'name' => 'stock_qty', 'title' => 'Stock Qty'],
                       ['data' => 'per_stock_rate', 'name' => 'per_stock_rate', 'title' => 'Per Stock Rate'],
                       ['data' => 'fee_percentage', 'name' => 'fee_percentage', 'title' => 'Fee Percentage'],
                       ['data' => 'fee', 'name' => 'fee', 'title' => 'Fee'],
                       ['data' => 'amount', 'name' => 'amount', 'title' => 'Amount'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);
        return view('portal.portfolio_trade.detail',compact('portfolio_transactions','title','datatable','user','market','stocks','id','search','request','is_owner','PortfolioTrade'));
    }

    public function createTransaction(Request $request, $id)
    {
        $type = $request->type;
        $portfolio_trade = PortfolioTrade::where(['user_id'=>Auth::id(),'id'=>$id])->first();
        $portfolio_trades = PortfolioTrade::pluck('trade_name','id');
        $stocks = Stock::all();
        return view('portal.portfolio_trade.ajax.common',compact('stocks','portfolio_trade','portfolio_trades','type'));
    }

    public function addTransaction(Request $request)
    {       

                $validator = Validator::make($request->all(), [
                    'portfolio_trade_id' => 'required',
                    'date'                  =>'required',
                    'stock'                 =>'required',
                    'stock_qty'              =>"required",
                    'per_stock_rate'         =>"required",
                    'per_stock_rate'         =>"required",
                    'fee_percentage'         =>"required",
                    'fee'                   =>"required",
                    'amount'                =>"required",
                    'trade_transaction_image' => 'nullable|mimes:jpeg,bmp,png',

                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                if($request->hasFile('trade_transaction_image'))
                {
                    $file = $request->file('trade_transaction_image');
                    $image = Image::make($file)->widen(1200, function ($constraint) {
                        $constraint->upsize();
                    });
                    // Normal Image
                    $directory = 'trade_uploads';
                    Storage::makeDirectory($directory);
                    $file_name = 'trade_uploads/'.time().$file->getClientOriginalName();
                    $file_path = storage_path('app/public/'.$file_name);
                    $image->save($file_path);
                }


                $PortfolioTransaction = new PortfolioTransaction;
                $PortfolioTransaction->portfolio_trade_id = $request->input('portfolio_trade_id');
                $PortfolioTransaction->user_id = Auth::id();
                $PortfolioTransaction->market_id =$request->input('market_id');;
                $PortfolioTransaction->date = $request->input('date');
                $PortfolioTransaction->type = $request->input('type');
                $PortfolioTransaction->stock = $request->input('stock');
                $PortfolioTransaction->stock_qty = $request->input('stock_qty');
                $PortfolioTransaction->per_stock_rate = $request->input('per_stock_rate');
                $PortfolioTransaction->fee_percentage = $request->input('fee_percentage');
                $PortfolioTransaction->fee = $request->input('fee');
                $PortfolioTransaction->amount = $request->input('amount');
                $PortfolioTransaction->save(); 

                if(isset($file_name))
                {
                    $Attachment = new Attachment;
                    $Attachment->file = $file_name;
                    $PortfolioTransaction->attachment()->save($Attachment);
                }

                $PortfolioTransaction::calculateBalance($request->input('type'),$request->input('amount'),Auth::id());
                return response()->json(['success'=>'true','message'=>'Portfolio Trade Transaction has been added successfully']);
    }
    
    // END TRANSACTION TABLE

    // ADD IMAGES
    public function uploadImages(Request $request)
    {
          if($request->hasFile('file'))
          {
              $file = $request->file('file');
              $image = Image::make($file)->widen(1200, function ($constraint) {
                  $constraint->upsize();
              });
              // Normal Image
              $directory = 'portfolio_trade_uploads';
              Storage::makeDirectory($directory);
              $file_name = 'portfolio_trade_uploads/'.$file->getClientOriginalName();
              $file_path = storage_path('app/public/'.$file_name);
              $image->save($file_path);

              // thumbnails
              $image = Image::make($file)->fit(300,150);
            
              $directory = 'portfolio_trade_thumbnails';
              Storage::makeDirectory($directory);
              $thumbnail_file_name = 'portfolio_trade_thumbnails/'.$file->getClientOriginalName();
              $file_path = storage_path('app/public/'.$thumbnail_file_name);
              $image->save($file_path);
          }
        $PortfolioTrade = PortfolioTrade::find($request->portfolio_trade_id);    
        if(isset($file_name))
        {
            $Attachment = new Attachment;
            $Attachment->file = $file_name;
            $Attachment->thumbnail = $thumbnail_file_name;
            $PortfolioTrade->attachments()->save($Attachment);
        }
    }



    public function imageDestroy(Request $request)
    {
        $filename = $request->filename;
        $Attachment = Attachment::where('file','portfolio_trade_uploads/'.$filename)->first();
        $Attachment->delete();
        unlink(storage_path('app/public/portfolio_trade_uploads/'.$filename));
        unlink(storage_path('app/public/portfolio_trade_thumbnails/'.$filename));

        if(isset($request->is_not_dropzone))
           return response()->json(['success'=>'true','message'=>'Image has been deleted successfully']);
    }

    public function destroy($id)
    {
        PortfolioTrade::where(['user_id'=>Auth::id(),'id'=>$id])->delete();
        PortfolioTransaction::where(['user_id'=>Auth::id(),'portfolio_trade_id'=>$id])->delete();
        return response()->json(['success'=>'true','message'=>'Portfolio Trade has been deleted successfully']);
    }
}

<?php

namespace App\Http\Controllers;

use App\AnalysisPHData;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
use App\Country;
class AnalysisPHDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
                if (request()->ajax()) {
                    return DataTables::of(AnalysisPHData::query())
                            ->editColumn('action', function(AnalysisPHData $ph) {
                                return  ModelBtn2('ph',$ph->id);
                            })
                            ->toJson();
                }
                $title = "Analysis - PH Data";
                $countries = Country::pluck('country_name','country_name');
                $builder->columns([
                              
                               ['data' => 'country', 'name' => 'country', 'title' => 'Country'],
                               ['data' => 'landbased', 'name' => 'landbased', 'title' => 'Landbased'],
                               ['data' => 'seabased', 'name' => 'seabased', 'title' => 'Seabased'],
                               ['data' => 'month', 'name' => 'month', 'title' => 'Month'],
                               ['data' => 'year', 'name' => 'year', 'title' => 'Year'],
                               ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                           ]);
                $datatable =  $builder->parameters([
                      'searchDelay' => 500,
                      'stateSave' => 'true',
                       'order' => [[ 0, "desc" ]]
                  ]);

                return view('admin.analysis.ph.index', compact('datatable','title','stocks','countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
                $validator = Validator::make($request->all(), [
                        'country' => 'required',
                        'landbased' => 'required',
                        'seabased' => 'required',
                         'month' => 'required',
                         'year' => 'required'
                    ]);

                    if ($validator->fails())
                    {
                        return response()->json(['errors'=>$validator->errors()->first()]);
                    }
              
                $AnalysisPHData = new AnalysisPHData;
                $AnalysisPHData->country = $request->input('country');
                $AnalysisPHData->landbased = $request->input('landbased');
                $AnalysisPHData->seabased = $request->input('seabased');
                $AnalysisPHData->month = $request->input('month');
                $AnalysisPHData->year = $request->input('year');
                $AnalysisPHData->save();

                return response()->json(['success'=>'true','message'=>'Analysis PH Data has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisPHData  $analysisPHData
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisPHData $analysisPHData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisPHData  $analysisPHData
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisPHData $ph)
    {       
        $countries = Country::pluck('country_name','country_name');
        return view('admin.analysis.ph.ajax.edit', compact('ph','countries')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisPHData  $analysisPHData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisPHData $ph)
    {               
         $validator = Validator::make($request->all(), [
                'country' => 'required',
                'landbased' => 'required',
                'seabased' => 'required',
                 'month' => 'required',
                 'year' => 'required'
            ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->first()]);
            }
            
            $AnalysisPHData = AnalysisPHData::find($ph->id);
            $AnalysisPHData->country = $request->input('country');
            $AnalysisPHData->landbased = $request->input('landbased');
            $AnalysisPHData->seabased = $request->input('seabased');
            $AnalysisPHData->month = $request->input('month');
            $AnalysisPHData->year = $request->input('year');
            $AnalysisPHData->save();

            return response()->json(['success'=>'true','message'=>'Analysis PH Data has been updated successfully']);
    }

    // IMPORT EXCEL
    public function import(Request $request)
        {
         
            $validator = Validator::make($request->all(), [
             'ph_data' => 'required',
            ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('ph_data')->getClientOriginalExtension();  
            $filename = $request->file('ph_data')->storeAs('ph_data',time().".".$getClientOriginalExtension);
            $path = $request->file('ph_data')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['country']) || empty($row['country']) || !isset($row['landbased']) || empty($row['landbased']) || !isset($row['seabased']) || empty($row['seabased']) || !isset($row['month']) || empty($row['month']) || !isset($row['year']) || empty($row['year']))
                {
                    continue;
                }

                $data[] = [
                  'country'  =>  isset($row['country']) ? $row['country'] : null,
                  'landbased'  =>  isset($row['landbased']) ? $row['landbased'] : null,
                  'seabased'  =>  isset($row['seabased']) ? $row['seabased'] : null,
                  'month'  =>  isset($row['month']) ? $row['month'] : null,
                  'year'  =>  isset($row['year']) ? $row['year'] : null
              ]; 
          
      }
          AnalysisPHData::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisPHData  $analysisPHData
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisPHData $ph)
    {
        $ph->delete();
        return response()->json(['success'=>'true','message'=>'Analysis PH Data has been deleted successfully']);
    }
}

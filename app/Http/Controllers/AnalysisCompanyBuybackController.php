<?php

namespace App\Http\Controllers;

use App\AnalysisCompanyBuyback;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
use App\Stock;
class AnalysisCompanyBuybackController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
       if (request()->ajax()) {
           return DataTables::of(AnalysisCompanyBuyback::query())
                   ->editColumn('action', function(AnalysisCompanyBuyback $buyback) {
                       return  ModelBtn2('buyback',$buyback->id);
                   })
                   ->toJson();
       }
       $title = "Analysis - Company Buyback";
       $stocks = Stock::all();
       $builder->columns([
                     
                      ['data' => 'stock_code', 'name' => 'stock_code', 'title' => 'Stock Code'],
                      ['data' => 'buyback_program', 'name' => 'buyback_program', 'title' => 'Buyback Program'],
                      ['data' => 'amount_left', 'name' => 'amount_left', 'title' => 'Amount Left to Repurchased'],
                      ['data' => 'total_amount', 'name' => 'total_amount', 'title' => 'Total Repurchased Amount'],
                      ['data' => 'repurchased_amount', 'name' => 'repurchased_amount', 'title' => 'Repurchased Amount for the week'],
                 
                      ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                  ]);
       $datatable =  $builder->parameters([
             'searchDelay' => 500,
             'stateSave' => 'true',
              'order' => [[ 0, "desc" ]]
         ]);

       return view('admin.analysis.buyback.index', compact('datatable','title','stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $validator = Validator::make($request->all(), [
                    'stock_code' => 'required',
                    'buyback_program' => 'required',
                    'amount_left' => 'required',
                    'total_amount' => 'required',
                    'repurchased_amount' => 'required',
                   
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisCompanyBuyback = new AnalysisCompanyBuyback;
                $AnalysisCompanyBuyback->stock_code = $request->input('stock_code');
                $AnalysisCompanyBuyback->buyback_program = $request->input('buyback_program');
                $AnalysisCompanyBuyback->amount_left = $request->input('amount_left');
                $AnalysisCompanyBuyback->total_amount = $request->input('total_amount');
                $AnalysisCompanyBuyback->repurchased_amount = $request->input('repurchased_amount');
                $AnalysisCompanyBuyback->save();

                return response()->json(['success'=>'true','message'=>'Analysis Company Buyback has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisCompanyBuyback  $analysisCompanyBuyback
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisCompanyBuyback $analysisCompanyBuyback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisCompanyBuyback  $analysisCompanyBuyback
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisCompanyBuyback $buyback)
    {
        $stocks = Stock::all();
        return view('admin.analysis.buyback.ajax.edit', compact('buyback','stocks')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisCompanyBuyback  $analysisCompanyBuyback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisCompanyBuyback $buyback)
    {
                $validator = Validator::make($request->all(), [
                    'stock_code' => 'required',
                    'buyback_program' => 'required',
                    'amount_left' => 'required',
                    'total_amount' => 'required',
                    'repurchased_amount' => 'required',
                   
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisCompanyBuyback = AnalysisCompanyBuyback::find($buyback->id);
                $AnalysisCompanyBuyback->stock_code = $request->input('stock_code');
                $AnalysisCompanyBuyback->buyback_program = $request->input('buyback_program');
                $AnalysisCompanyBuyback->amount_left = $request->input('amount_left');
                $AnalysisCompanyBuyback->total_amount = $request->input('total_amount');
                $AnalysisCompanyBuyback->repurchased_amount = $request->input('repurchased_amount');
                $AnalysisCompanyBuyback->save();

                return response()->json(['success'=>'true','message'=>'Analysis Company Buyback has been updated successfully']);
    }


    // IMPORT EXCEL
    public function import(Request $request)
        {
         
            $validator = Validator::make($request->all(), [
             'company_buyback' => 'required',
            ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('company_buyback')->getClientOriginalExtension();  
            $filename = $request->file('company_buyback')->storeAs('company_buyback',time().".".$getClientOriginalExtension);
            $path = $request->file('company_buyback')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['stock_code']) || empty($row['stock_code']) || !isset($row['buyback_program']) || empty($row['buyback_program']) || !isset($row['amount_left']) || empty($row['amount_left']) || !isset($row['total_amount']) || empty($row['total_amount']) || !isset($row['repurchased_amount']) || empty($row['repurchased_amount']))
                {
                    continue;
                }

                $data[] = [
                  'stock_code'  =>  $row['stock_code'],
                  'buyback_program'  =>  isset($row['buyback_program']) ? $row['buyback_program'] : null,
                  'amount_left'  =>  isset($row['amount_left']) ? $row['amount_left'] : null,
                  'total_amount'  =>  isset($row['total_amount']) ? $row['total_amount'] : null,
                  'repurchased_amount'  =>  isset($row['repurchased_amount']) ? $row['repurchased_amount'] : null
              ]; 
          
      }
          AnalysisCompanyBuyback::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisCompanyBuyback  $analysisCompanyBuyback
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisCompanyBuyback $buyback)
    {
        $buyback->delete();
        return response()->json(['success'=>'true','message'=>'Analysis Company Buyback has been deleted successfully']);
    }
}

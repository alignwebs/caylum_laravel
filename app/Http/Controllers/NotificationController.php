<?php

namespace App\Http\Controllers;

use App\User;
use App\Feed;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationController extends Controller
{
    public function redirect($notification_id)
    {
    	$notification = DatabaseNotification::find($notification_id);
    	$data = $notification->data;

    	switch($notification->type)
    	{
    		case "App\Notifications\NewUserFollower":
    		{
    			$user_id = $data['follower_user_id'];
    			$user = User::select('username')->findOrFail($user_id);

    			return redirect()->route('portal.user.profile', $user->username);
    			break;
    		}

            case "App\Notifications\NewFeedLike":
            {
                $user_id = $data['liked_user_id'];
                //$user = User::select('username')->findOrFail($user_id);

                return "Redirected to post";
                break;
            }
    	}
    }
}

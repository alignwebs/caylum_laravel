<?php

namespace App\Http\Controllers;

use App\AnalysisWatchlist;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
use App\Stock;

class AnalysisWatchlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
                if (request()->ajax()) {
                    return DataTables::of(AnalysisWatchlist::query())
                            ->editColumn('action', function(AnalysisWatchlist $watchlist) {
                                return  ModelBtn2('watchlist',$watchlist->id);
                            })
                            ->toJson();
                }
                $title = "Analysis - Caylum Watchlist";
                $stocks = Stock::all();
                $builder->columns([
                                
                               ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                               ['data' => 'price_added', 'name' => 'price_added', 'title' => 'Price Added (Php)'],
                               ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
                               ['data' => 'date_added', 'name' => 'date_added', 'title' => 'Date Added'],
                               ['data' => 'comments', 'name' => 'comments', 'title' => 'Comments'],
                               ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                           ]);
                $datatable =  $builder->parameters([
                      'searchDelay' => 500,
                      'stateSave' => 'true',
                       'order' => [[ 0, "desc" ]]
                  ]);

                return view('admin.analysis.watchlist.index', compact('datatable','title','stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $validator = Validator::make($request->all(), [
                   
                    'stock' => 'required',
                    'price_added' => 'required',
                    'category' => 'required',
                    'date_added' => 'required',
                   
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisWatchlist = new AnalysisWatchlist;
                $AnalysisWatchlist->stock = $request->input('stock');
                $AnalysisWatchlist->price_added = $request->input('price_added');
                $AnalysisWatchlist->category = $request->input('category');
                $AnalysisWatchlist->date_added = $request->input('date_added');
                $AnalysisWatchlist->comments = $request->input('comments');
                $AnalysisWatchlist->save();

                return response()->json(['success'=>'true','message'=>'Analysis Watchlist has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisWatchlist  $analysisWatchlist
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisWatchlist $analysisWatchlist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisWatchlist  $analysisWatchlist
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisWatchlist $watchlist)
    {
        $stocks = Stock::all();
        return view('admin.analysis.watchlist.ajax.edit', compact('watchlist','stocks')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisWatchlist  $analysisWatchlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisWatchlist $watchlist)
    {
                $validator = Validator::make($request->all(), [
                   
                    'stock' => 'required',
                    'price_added' => 'required',
                    'category' => 'required',
                    'date_added' => 'required',
                   
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisWatchlist = AnalysisWatchlist::find($watchlist->id);
                $AnalysisWatchlist->stock = $request->input('stock');
                $AnalysisWatchlist->price_added = $request->input('price_added');
                $AnalysisWatchlist->category = $request->input('category');
                $AnalysisWatchlist->date_added = $request->input('date_added');
                $AnalysisWatchlist->comments = $request->input('comments');
                $AnalysisWatchlist->save();

                return response()->json(['success'=>'true','message'=>'Analysis Watchlist has been updated successfully']);
    }

        // IMPORT EXCEL
    public function import(Request $request)
        {
         
            $validator = Validator::make($request->all(), [
             'watchlist' => 'required',
            ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('watchlist')->getClientOriginalExtension();  
            $filename = $request->file('watchlist')->storeAs('watchlist',time().".".$getClientOriginalExtension);
            $path = $request->file('watchlist')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['stock']) || empty($row['stock']) || !isset($row['price_added']) || empty($row['price_added']) || !isset($row['category']) || empty($row['category']) || !isset($row['date_added']) || empty($row['date_added']))
                {
                    continue;
                }

                $data[] = [
                  'stock'  => $row['stock'],
                  'price_added'  =>  isset($row['price_added']) ? $row['price_added'] : null,
                  'category'  =>  isset($row['category']) ? $row['category'] : null,
                  'date_added'  =>  isset($row['date_added']) ? $row['date_added'] : null,
                  'comments'  =>  isset($row['comments']) ? $row['comments'] : null,
              ]; 
          
      }
          AnalysisWatchlist::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisWatchlist  $analysisWatchlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisWatchlist $watchlist)
    {
        $watchlist->delete();
        return response()->json(['success'=>'true','message'=>'Analysis Watchlist has been deleted successfully']);
    }
}

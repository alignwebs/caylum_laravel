<?php

namespace App\Http\Controllers;

use App\AnalysisBtmStock;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
class AnalysisBtmStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
            if (request()->ajax()) {
                   return DataTables::of(AnalysisBtmStock::query())
                           ->editColumn('action', function(AnalysisBtmStock $btm) {
                               return  ModelBtn2('btm',$btm->id);
                           })
                           ->toJson();
               }
               $title = "Analysis - BTM Stocks";
               $builder->columns([
                             
                              ['data' => 'company_name', 'name' => 'company_name', 'title' => 'Company Name'],
                              ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
                              ['data' => 'market_cap', 'name' => 'market_cap', 'title' => 'Mkt Cap (php Bill)'],
                              ['data' => 'narrative', 'name' => 'narrative', 'title' => 'Narrative'],
                              ['data' => 'last_price', 'name' => 'last_price', 'title' => 'Last Price (Php)'],
                              ['data' => 'tp', 'name' => 'tp', 'title' => 'TP (Php)'],
                              ['data' => 'fy19', 'name' => 'fy19', 'title' => 'FY19 EV/EBITDA'],
                              ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                          ]);
                $datatable =  $builder->parameters([
                     'searchDelay' => 500,
                     'stateSave' => 'true',
                      'order' => [[ 0, "desc" ]]
                 ]);

               return view('admin.analysis.btm_stock.index', compact('datatable','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'category' => 'required',
            'market_cap' => 'required',
            'narrative' => 'required',
            'last_price' => 'required',
            'tp' => 'required',
            'fy19' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $AnalysisBtmStock = new AnalysisBtmStock;
        $AnalysisBtmStock->company_name = $request->input('company_name');
        $AnalysisBtmStock->category = $request->input('category');
        $AnalysisBtmStock->market_cap = $request->input('market_cap');
        $AnalysisBtmStock->narrative = $request->input('narrative');
        $AnalysisBtmStock->last_price = $request->input('last_price');
        $AnalysisBtmStock->tp = $request->input('tp');
        $AnalysisBtmStock->fy19 = $request->input('fy19');
        $AnalysisBtmStock->save();

        return response()->json(['success'=>'true','message'=>'Analysis Btm Stock has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisBtmStock  $analysisBtmStock
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisBtmStock $analysisBtmStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisBtmStock  $analysisBtmStock
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisBtmStock $btm)
    {
        return view('admin.analysis.btm_stock.ajax.edit', compact('btm')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisBtmStock  $analysisBtmStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisBtmStock $btm)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'category' => 'required',
            'market_cap' => 'required',
            'narrative' => 'required',
            'last_price' => 'required',
            'tp' => 'required',
            'fy19' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $AnalysisBtmStock = AnalysisBtmStock::find($btm->id);
        $AnalysisBtmStock->company_name = $request->input('company_name');
        $AnalysisBtmStock->category = $request->input('category');
        $AnalysisBtmStock->market_cap = $request->input('market_cap');
        $AnalysisBtmStock->narrative = $request->input('narrative');
        $AnalysisBtmStock->last_price = $request->input('last_price');
        $AnalysisBtmStock->tp = $request->input('tp');
        $AnalysisBtmStock->fy19 = $request->input('fy19');
        $AnalysisBtmStock->save();

        return response()->json(['success'=>'true','message'=>'Analysis Btm Stock has been updated successfully']);
    }

    // IMPORT EXCEL
    public function import(Request $request)
        {
        
            $validator = Validator::make($request->all(), [
             'btm_stocks' => 'required',
         ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('btm_stocks')->getClientOriginalExtension();  
            $filename = $request->file('btm_stocks')->storeAs('btm_stocks',time().".".$getClientOriginalExtension);
            $path = $request->file('btm_stocks')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['company_name']) || empty($row['company_name']) || !isset($row['category']) ||  empty($row['category']) || !isset($row['market_cap']) ||  empty($row['market_cap']) || !isset($row['narrative']) ||  empty($row['narrative']) || !isset($row['last_price']) ||  empty($row['last_price']) || !isset($row['tp']) ||  empty($row['tp']) || !isset($row['fy19']) ||  empty($row['fy19']))
                {
                    continue;
                }

                $data[] = [
                  'company_name'  =>  $row['company_name'],
                  'category'  =>  $row['category'],
                  'market_cap'  =>  $row['market_cap'],
                  'narrative'  =>  isset($row['narrative']) ? $row['narrative'] : null,
                  'last_price'  =>  isset($row['last_price']) ? $row['last_price'] : null,
                  'tp'  =>  isset($row['tp']) ? $row['tp'] : null,
                  'fy19'  =>  isset($row['fy19']) ? $row['fy19'] : null
              ]; 
          
      }
          AnalysisBtmStock::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisBtmStock  $analysisBtmStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisBtmStock $btm)
    {
        $btm->delete();
        return response()->json(['success'=>'true','message'=>'Analysis Btm Stock has been deleted successfully']);
    }
}

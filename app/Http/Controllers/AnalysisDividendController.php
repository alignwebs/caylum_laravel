<?php

namespace App\Http\Controllers;

use App\AnalysisDividend;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
use App\Stock;
class AnalysisDividendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
                if (request()->ajax()) {
                    return DataTables::of(AnalysisDividend::query())
                            ->editColumn('action', function(AnalysisDividend $dividend) {
                                return  ModelBtn2('dividend',$dividend->id);
                            })
                            ->toJson();
                }
                $title = "Analysis - Dividend";
                $stocks = Stock::all();
                $builder->columns([
                              
                               ['data' => 'type_of_dividends', 'name' => 'type_of_dividends', 'title' => 'Type of Dividends'],
                               ['data' => 'date_closed', 'name' => 'date_closed', 'title' => 'Date Closed'],
                               ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                               ['data' => 'amount', 'name' => 'amount', 'title' => 'Amount'],
                               ['data' => 'ex_date', 'name' => 'ex_date', 'title' => 'Ex Date'],
                               ['data' => 'record_date', 'name' => 'record_date', 'title' => 'Record Date'],
                               ['data' => 'payment_date', 'name' => 'payment_date', 'title' => 'Payment Date'],
                               ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                           ]);
                $datatable =  $builder->parameters([
                      'searchDelay' => 500,
                      'stateSave' => 'true',
                       'order' => [[ 0, "desc" ]]
                  ]);

                return view('admin.analysis.dividend.index', compact('datatable','title','stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $validator = Validator::make($request->all(), [
                    'type_of_dividends' => 'required',
                    'date_closed' => 'required',
                    'stock' => 'required',
                    'amount' => 'required',
                    'ex_date' => 'required',
                    'record_date' => 'required',
                    'payment_date' => 'required'
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisDividend = new AnalysisDividend;
                $AnalysisDividend->type_of_dividends = $request->input('type_of_dividends');
                $AnalysisDividend->date_closed = $request->input('date_closed');
                $AnalysisDividend->stock = $request->input('stock');
                $AnalysisDividend->amount = $request->input('amount');
                $AnalysisDividend->ex_date = $request->input('ex_date');
                $AnalysisDividend->record_date = $request->input('record_date');
                $AnalysisDividend->payment_date = $request->input('payment_date');
                $AnalysisDividend->save();

                return response()->json(['success'=>'true','message'=>'Analysis Dividend has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisDividend  $analysisDividend
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisDividend $analysisDividend)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisDividend  $analysisDividend
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisDividend $dividend)
    {
        $stocks = Stock::all();
        return view('admin.analysis.dividend.ajax.edit', compact('dividend','stocks')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisDividend  $analysisDividend
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisDividend $dividend)
    {
                    $validator = Validator::make($request->all(), [
                            'type_of_dividends' => 'required',
                            'date_closed' => 'required',
                            'stock' => 'required',
                             'amount' => 'required',
                            'ex_date' => 'required',
                            'record_date' => 'required',
                            'payment_date' => 'required'
                        ]);

                        if ($validator->fails())
                        {
                            return response()->json(['errors'=>$validator->errors()->first()]);
                        }
                        
                        $AnalysisDividend = AnalysisDividend::find($dividend->id);
                        $AnalysisDividend->type_of_dividends = $request->input('type_of_dividends');
                        $AnalysisDividend->date_closed = $request->input('date_closed');
                        $AnalysisDividend->stock = $request->input('stock');
                        $AnalysisDividend->amount = $request->input('amount');
                        $AnalysisDividend->ex_date = $request->input('ex_date');
                        $AnalysisDividend->record_date = $request->input('record_date');
                        $AnalysisDividend->payment_date = $request->input('payment_date');
                        $AnalysisDividend->save();

                        return response()->json(['success'=>'true','message'=>'Analysis Dividend has been updated successfully']);
    }


    // IMPORT EXCEL
    public function import(Request $request)
        {
         
            $validator = Validator::make($request->all(), [
             'dividend' => 'required',
            ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('dividend')->getClientOriginalExtension();  
            $filename = $request->file('dividend')->storeAs('dividend',time().".".$getClientOriginalExtension);
            $path = $request->file('dividend')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['type_of_dividends']) || empty($row['type_of_dividends']) || !isset($row['date_closed']) ||  empty($row['date_closed']) || !isset($row['stock']) ||  empty($row['stock']) || !isset($row['amount']) ||  empty($row['amount']) || !isset($row['ex_date']) ||  empty($row['ex_date']) || !isset($row['record_date']) ||  empty($row['record_date']) || !isset($row['payment_date']) ||  empty($row['payment_date']))
                {
                    continue;
                }

                $data[] = [
                  'type_of_dividends'  =>  $row['type_of_dividends'],
                  'date_closed'  =>  $row['date_closed'],
                  'stock'  =>  $row['stock'],
                  'amount'  =>  isset($row['amount']) ? $row['amount'] : null,
                  'ex_date'  =>  isset($row['ex_date']) ? $row['ex_date'] : null,
                  'record_date'  =>  isset($row['record_date']) ? $row['record_date'] : null,
                  'payment_date'  =>  isset($row['payment_date']) ? $row['payment_date'] : null
              ]; 
          
      }
          AnalysisDividend::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisDividend  $analysisDividend
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisDividend $dividend)
    {
        $dividend->delete();
        return response()->json(['success'=>'true','message'=>'Analysis Dividend has been deleted successfully']);
    }
}

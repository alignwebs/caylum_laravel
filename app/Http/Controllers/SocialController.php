<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Country;
use App\Feed;
use App\Notifications\NewUserFollower;
use App\User;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Image;
use Response;
use Storage;
use Validator;
class SocialController extends Controller
{
    public function profile($username)
     {  
          $user = User::has('subscription')->where('username',$username)->firstOrFail();
         
          $title = "Profile";
          $data = $this->getCounts($user);
          return view('portal.social.profile.show',compact('title','user','data'));
     }

     public function toggleFollow(Request $request)
     {
        $loggedInUser = Auth::user();
        $user = User::findOrFail($request->user_id);
        $checkFollowUnfollow = $loggedInUser->toggleFollow($user);

        if(sizeof($checkFollowUnfollow['attached']) > 0){

            $user->notify(new NewUserFollower($loggedInUser));
            return response()->json(['success'=>'true', 'action'=>'followed' ,'message'=>'You are now following this user.']);
        }
        else
            return response()->json(['success'=>'true','action'=>'unfollowed','message'=>'You unfollowed this user.']);
     }

     public function editProfile($username)
     {
        $user = User::find(Auth::id());
        // return $user->attachment;
        $countries = Country::pluck('country_name','country_code');
        $title = "Edit Profile";
        return view('portal.social.profile.edit', compact('user','title','countries'));
     }

     public function updateProfile(Request $request)
     {
        $user_id = Auth::id();
        
        $validator = Validator::make($request->all(), [
            'profile_pic' => 'nullable|mimes:jpg,jpeg,bmp,png',
            'cover_pic' => 'nullable|mimes:jpg,jpeg,bmp,png',
            'username' => 'required|unique:users,username,'.$user_id,
            'display_name' => 'required',
            'facebook_url' => 'nullable|url',
            'twitter_url' => 'nullable|url',
            'instagram_url' => 'nullable|url',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
       
        $User = User::find(Auth::id());

        if($request->hasFile('profile_pic'))
        {
            $file = $request->file('profile_pic');
            $image = Image::make($file)->widen(1200, function ($constraint) {
                $constraint->upsize();
            });
            $directory = 'profile_pic_uploads';
            Storage::makeDirectory($directory);
            $file_name = 'profile_pic_uploads/'.time().$file->getClientOriginalName();
            $file_path = storage_path('app/public/'.$file_name);
            $image->save($file_path);
            $User->profile_pic =  $file_name;

            $image = Image::make($file);
            $image->fit(200, 200);
            $directory = 'profile_pic_thumbnail_uploads';
            Storage::makeDirectory($directory);
            $file_name = 'profile_pic_thumbnail_uploads/'.time().$file->getClientOriginalName();
            $file_path = storage_path('app/public/'.$file_name);
            $image->save($file_path);
            $User->thumbnail =  $file_name;
        }

        if($request->hasFile('cover_pic'))
        {
            $file = $request->file('cover_pic');
            $image = Image::make($file)->widen(1200, function ($constraint) {
                $constraint->upsize();
            });

            $directory = 'cover_pic_uploads';
            Storage::makeDirectory($directory);
            
            $file_name = 'cover_pic_uploads/'.time().$file->getClientOriginalName();
            $file_path = storage_path('app/public/'.$file_name);
            $image->save($file_path);
            $User->cover_pic =  $file_name;
        }

        $User->username = removeSpaceFromString($request->input('username'));
        $User->display_name = $request->input('display_name');
        $User->bio = $request->input('bio');
        $User->country = $request->input('country');
        $User->facebook_url = $request->input('facebook_url');
        $User->twitter_url = $request->input('twitter_url');
        $User->instagram_url = $request->input('instagram_url');
        $User->save();

        return response()->json(['success' => true,'message'=>'Profile updated Successfully']);
     }

     public function followers($username)
     {
        $user = User::where('username',$username)->firstOrFail();
        $data = $this->getCounts($user);
        $followers = $user->followers()->paginate(20);;
        $title = "Followers";
        return view('portal.social.profile.followers', compact('followers','title','user','data'));
     }

     public function following($username)
     {
        $user = User::where('username',$username)->firstOrFail();
        $data = $this->getCounts($user);
        $followings = $user->followings()->paginate(20);
        $title = "Followings";
        return view('portal.social.profile.following', compact('followings','title','user','data'));
     }

     public function photos($username)
     {
        $user = User::where('username',$username)->firstOrFail();
        $photos = \App\Attachment::whereHasMorph(
                    'attachmentable', 
                    ['App\Feed'], 
                    function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                 }
                )->get();
        $data = $this->getCounts($user);
        $title = "Photos";
        return view('portal.social.profile.photos', compact('photos','title','user','data'));
     }

     public function getCounts($user)
     {
            $data['photos'] = \App\Attachment::whereHasMorph(
                        'attachmentable', 
                        ['App\Feed'], 
                        function (Builder $query) use ($user) {
                            $query->where('user_id', $user->id);
                     }
                    )->count();
            $data['comments'] = \App\Comment::whereHasMorph(
                                'commentable', 
                                ['App\Feed'], 
                                function (Builder $query) use ($user) {
                                    $query->where('user_id', $user->id);
                             }
                            )->count();
            
            $data['posts'] = Feed::where('user_id',$user->id)->count();
            $data['likes'] = Feed::getAllLikes($user);
            return $data;
     }

     public function userNotificationsHeader()
     {
        $user = Auth::user();

        $data['unread'] = $user->unreadNotifications->count();
        $data['notifications'] = $user->notifications->take(10);

        return $data;
     }

     public function userNotificationsMarkAsRead()
     {
        $user = Auth::user();
        return $user->unreadNotifications->markAsRead();
     }

}

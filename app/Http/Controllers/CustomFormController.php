<?php

namespace App\Http\Controllers;

use App\CustomForm;
use Illuminate\Http\Request;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;
use Image;
use Storage;
class CustomFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(CustomForm::query())
                    ->editColumn('action', function(CustomForm $form) {
                        return  ModelBtn('form',$form->id);
                    })
                    ->toJson();
        }
        $title = "Manage Custom Form";
        $builder->columns([
                      
                       ['data' => 'form_name', 'name' => 'form_name', 'title' => 'Name'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);
        return view('admin.custom_form.index', compact('datatable','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $title = "Add Custom Form";
        return view('admin.custom_form.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
     

        $validator = Validator::make($request->all(), [
                
                'form_name' => 'required|unique:custom_forms',
            
        ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $CustomForm = new CustomForm;
        $CustomForm->form_name = $request->input('form_name');
        $CustomForm->form_json = $request->form_json;
        $CustomForm->save(); 
    
        return response()->json(['success'=>'true','message'=>'Custom form has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomForm  $customForm
     * @return \Illuminate\Http\Response
     */
    public function show(CustomForm $customForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomForm  $customForm
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomForm $form)
    {   
        $title = 'Edit Custom Form';

        return view('admin.custom_form.edit',compact('form','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomForm  $customForm
     * @return \Illuminate\Http\Response
     */
    public function customFormUpdate(Request $request)
    {       
            $validator = Validator::make($request->all(), [
                      
                      'form_name' => 'required|unique:custom_forms,id,'.$request->id
                  
              ]);

              
              if ($validator->fails())
              {
                  return response()->json(['errors'=>$validator->errors()->first()]);
              }

              
              $CustomForm = CustomForm::find($request->id);
           
              $CustomForm->form_name = $request->input('form_name');
              $CustomForm->form_json = $request->form_json;
              $CustomForm->save(); 
          
              return response()->json(['success'=>'true','message'=>'Custom form has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomForm  $customForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomForm $form)
    {
        $form->delete();
        flash('Custom Form deleted successfully','success');
        return redirect()->back(); 
    }
}

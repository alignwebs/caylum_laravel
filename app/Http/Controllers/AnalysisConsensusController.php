<?php

namespace App\Http\Controllers;

use App\AnalysisConsensus;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
class AnalysisConsensusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
                    return DataTables::of(AnalysisConsensus::query())
                            ->editColumn('action', function(AnalysisConsensus $consensus) {
                                return  ModelBtn2('consensus',$consensus->id);
                            })
                            ->toJson();
                }
                $title = "Analysis - Consensus";
                $builder->columns([
                              
                               ['data' => 'ticker', 'name' => 'ticker', 'title' => 'Ticker'],
                               ['data' => 'market_cap', 'name' => 'market_cap', 'title' => 'Market Cap'],
                               ['data' => 'highest_target_price', 'name' => 'highest_target_price', 'title' => 'Highest Target Price'],
                               ['data' => 'sales_4Q18', 'name' => 'sales_4Q18', 'title' => '4Q18'],
                               ['data' => 'sales_1Q19', 'name' => 'sales_1Q19', 'title' => '1Q19'],
                               ['data' => 'net_income_4Q18', 'name' => 'net_income_4Q18', 'title' => '4Q18'],
                               ['data' => 'net_income_1Q19', 'name' => 'net_income_1Q19', 'title' => '1Q19'],
                               ['data' => 'pe', 'name' => 'pe', 'title' => 'P/E'],
                               ['data' => 'roe', 'name' => 'roe', 'title' => 'ROE (%)'],
                               ['data' => 'div_yield', 'name' => 'div_yield', 'title' => 'DIV YIELD'],
                               ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                           ]);
                $datatable =  $builder->parameters([
                      'searchDelay' => 500,
                      'stateSave' => 'true',
                       'order' => [[ 0, "desc" ]]
                  ]);

                return view('admin.analysis.consensus.index', compact('datatable','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $validator = Validator::make($request->all(), [
                    'ticker' => 'required',
                    'market_cap' => 'required',
                    'highest_target_price' => 'required',
                    'sales_4Q18' => 'required',
                    'sales_1Q19' => 'required',
                    'net_income_4Q18' => 'required',
                    'net_income_1Q19' => 'required',
                    'pe' => 'required',
                    'roe' => 'required',
                    'div_yield' => 'required'
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisConsensus = new AnalysisConsensus;
                $AnalysisConsensus->ticker = $request->input('ticker');
                $AnalysisConsensus->market_cap = $request->input('market_cap');
                $AnalysisConsensus->highest_target_price = $request->input('highest_target_price');
                $AnalysisConsensus->sales_4Q18 = $request->input('sales_4Q18');
                $AnalysisConsensus->sales_1Q19 = $request->input('sales_1Q19');
                $AnalysisConsensus->net_income_4Q18 = $request->input('net_income_4Q18');
                $AnalysisConsensus->net_income_1Q19 = $request->input('net_income_1Q19');
                $AnalysisConsensus->pe = $request->input('pe');
                $AnalysisConsensus->roe = $request->input('roe');
                $AnalysisConsensus->div_yield = $request->input('div_yield');
                $AnalysisConsensus->save();

                return response()->json(['success'=>'true','message'=>'Analysis Consensus has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisConsensus  $analysisConsensus
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisConsensus $consensus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisConsensus  $analysisConsensus
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisConsensus $consensus)
    {
        return view('admin.analysis.consensus.ajax.edit', compact('consensus')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisConsensus  $analysisConsensus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisConsensus $consensus)
    {
            $validator = Validator::make($request->all(), [
                   'ticker' => 'required',
                   'market_cap' => 'required',
                   'highest_target_price' => 'required',
                     'sales_4Q18' => 'required',
                    'sales_1Q19' => 'required',
                    'net_income_4Q18' => 'required',
                    'net_income_1Q19' => 'required',
                    'pe' => 'required',
                    'roe' => 'required',
                    'div_yield' => 'required'
               ]);

               if ($validator->fails())
               {
                   return response()->json(['errors'=>$validator->errors()->first()]);
               }
               
               $AnalysisConsensus = AnalysisConsensus::find($consensus->id);
               $AnalysisConsensus->ticker = $request->input('ticker');
               $AnalysisConsensus->market_cap = $request->input('market_cap');
               $AnalysisConsensus->highest_target_price = $request->input('highest_target_price');
               $AnalysisConsensus->sales_4Q18 = $request->input('sales_4Q18');
               $AnalysisConsensus->sales_1Q19 = $request->input('sales_1Q19');
               $AnalysisConsensus->net_income_4Q18 = $request->input('net_income_4Q18');
               $AnalysisConsensus->net_income_1Q19 = $request->input('net_income_1Q19');
               $AnalysisConsensus->pe = $request->input('pe');
               $AnalysisConsensus->roe = $request->input('roe');
               $AnalysisConsensus->div_yield = $request->input('div_yield');
               $AnalysisConsensus->save();

               return response()->json(['success'=>'true','message'=>'Analysis Consensus has been updated successfully']);
    }

    // IMPORT EXCEL
    public function import(Request $request)
        {
         
            $validator = Validator::make($request->all(), [
             'consensus' => 'required',
         ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('consensus')->getClientOriginalExtension();  
            $filename = $request->file('consensus')->storeAs('consensus',time().".".$getClientOriginalExtension);
            $path = $request->file('consensus')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['ticker']) || empty($row['ticker']) || !isset($row['market_cap']) ||  empty($row['market_cap']) || !isset($row['highest_target_price']) ||  empty($row['highest_target_price']) || !isset($row['sales_4Q18']) ||  empty($row['sales_4Q18']) || !isset($row['sales_1Q19']) ||  empty($row['sales_1Q19']) || !isset($row['net_income_4Q18']) ||  empty($row['net_income_4Q18']) || !isset($row['net_income_1Q19']) ||  empty($row['net_income_1Q19']) || !isset($row['pe']) ||  empty($row['pe']) || !isset($row['roe']) ||  empty($row['roe']) || !isset($row['div_yield']) ||  empty($row['div_yield']))
                {
                    continue;
                }

                $data[] = [
                  'ticker'  =>  $row['ticker'],
                  'market_cap'  =>  $row['market_cap'],
                  'highest_target_price'  =>  $row['highest_target_price'],
                  'sales_4Q18'  =>  isset($row['sales_4Q18']) ? $row['sales_4Q18'] : null,
                  'sales_1Q19'  =>  isset($row['sales_1Q19']) ? $row['sales_1Q19'] : null,
                  'net_income_4Q18'  =>  isset($row['net_income_4Q18']) ? $row['net_income_4Q18'] : null,
                  'net_income_1Q19'  =>  isset($row['net_income_1Q19']) ? $row['net_income_1Q19'] : null,
                  'pe'  =>  isset($row['pe']) ? $row['pe'] : null,
                  'roe'  =>  isset($row['roe']) ? $row['roe'] : null,
                  'div_yield'  =>  isset($row['div_yield']) ? $row['div_yield'] : null
              ]; 
          
      }
          AnalysisConsensus::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisConsensus  $analysisConsensus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisConsensus $consensus)
    {
        $consensus->delete();
        return response()->json(['success'=>'true','message'=>'Analysis Consensus has been deleted successfully']);
    }
}

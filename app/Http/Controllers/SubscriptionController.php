<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use DataTables;
use Akaunting\Money\Money;
use Yajra\DataTables\Html\Builder;
use Validator;

class SubscriptionController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Subscription::query())

                  
                    ->editColumn('active', function(Subscription $subscription) {
                        return  status($subscription->active);
                    })
                    ->editColumn('action', function(Subscription $subscription) {
                        return  ModelBtn2('subscription',$subscription->id);
                    })
                    ->toJson();
        }
        $builder->columns([
                      
                       ['data' => 'name', 'name' => 'name', 'title' => 'Subscription'],
                       ['data' => 'formatted_money', 'name' => 'formatted_money', 'title' => 'Fee'],
                       ['data' => 'days_duration', 'name' => 'days_duration', 'title' => 'Duration (days)'],
                       ['data' => 'active', 'name' => 'active', 'title' => 'Status'],
                       ['data' => 'subscribers_count', 'name' => 'subscribers_count', 'title' => 'Total Subscribers'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);
        
        $title = "Manage Subscriptions";
        return view('admin.subscription.index', compact('datatable','title'));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                
                'name' => 'required',
                'fee' => 'required',
                'days_duration' => 'required',      
        ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        if($request->input('default'))
        {
            $defaulCount = Subscription::where("default",1)->count();
            if($defaulCount > 0)
            {
                return response()->json(['errors'=>'You have already assigned default to other subscritpions']);
            }           
        }
    
        $Subscription = new Subscription;
        
        
        $Subscription->name = $request->input('name');
        $Subscription->fee = $request->input('fee');
        $Subscription->days_duration = $request->input('days_duration');
        $Subscription->default = $request->input('default');
        $Subscription->active = $request->input('active');
       
        $Subscription->save(); 
        

        return response()->json(['success'=>'true','message'=>'Subscription has been added successfully']);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        return view('admin.subscription.ajax.edit', compact('subscription')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Subscription $subscription, Request $request)
    {
         $validator = Validator::make($request->all(), [
                
                'name' => 'required',
                'fee' => 'required',
                'days_duration' => 'required',

            
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        $subscription->name = $request->input('name');
        $subscription->fee = $request->input('fee');
        $subscription->days_duration = $request->input('days_duration');
        $subscription->default = $request->input('default');
        $subscription->active = $request->input('active');
       
        $subscription->save(); 
        

        return response()->json(['success'=>'true','message'=>'Subscription has been added successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        $subscription->delete();
        return response()->json(['success'=>'true','message'=>'Subscription has been deleted successfully']);
    }
}

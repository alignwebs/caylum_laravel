<?php
namespace App\Http\Controllers;
use App\Course;
use App\Events\UserSubscribed;
use App\Mail\Subscribed;
use App\Payment;
use App\Subscriber;
use App\Subscription;
use App\User;
use Auth;
use Mail;
use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use Sample\PayPalClient;
use App\Providers\PayPalClientServiceProvider as PayPalClientLive;
use TaylorNetwork\UsernameGenerator\Generator;
use Validator;
use Yajra\DataTables\Html\Builder;
class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder) {
        if (request()->ajax()) {
            return DataTables::of(User::role(['admin', 'sub_admin']))->editColumn('action', function (User $user) {
                return ModelBtn2('user', $user->id);
            })->editColumn('active', function (User $user) {
                return intToBool($user->active);
            })->editColumn('role', function (User $user) {
                return $user->roles->first()->name;
            })->toJson();
        }
        $title = "Manage Administration Users";
        $builder->columns([['data' => 'last_name', 'name' => 'last_name', 'title' => 'Last Name'], ['data' => 'first_name', 'name' => 'first_name', 'title' => 'First Name'], ['data' => 'email', 'email' => 'name', 'title' => 'Email'], ['data' => 'active', 'name' => 'active', 'title' => 'Active'], ['data' => 'role', 'name' => 'role', 'title' => 'Role', 'searchable' => 'false', 'orderable' => 'false'], ['data' => 'action', 'name' => 'action', 'title' => 'Action', 'searchable' => 'false', 'orderable' => 'false'], ]);
        $datatable = $builder->parameters(['searchDelay' => 500, 'stateSave' => 'true', 'order' => [[0, "asc"]]]);
        return view('admin.user.index', compact('datatable', 'title'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), ['last_name' => 'required', 'first_name' => 'required', 'mobile' => 'nullable', 'email' => 'required|string|email|unique:users', 'password' => 'required|string|min:6|confirmed', 'active' => 'required']);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->first() ]);
        }
        $User = new User;
        $User->last_name = $request->input('last_name');
        $User->first_name = $request->input('first_name');
        $User->mobile = $request->input('mobile');
        $User->email = $request->input('email');
        $User->password = bcrypt($request->input('password'));
        $User->active = $request->input('active');
        $User->save();
        $User->assignRole($request->input('role'));
        return response()->json(['success' => 'true', 'message' => 'Admin User has been added successfully']);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course) {
        //
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Request $request) {
        $type = $request->type;
        $subscriptions = Subscription::get()->pluck('complete_package', 'id');
        $courses = Course::all()->pluck('name', 'id');
        $subscriber = User::where('id', $user->id)->firstOrFail();
        return view('admin.user.ajax.edit', compact('user', 'subscriptions', 'courses', 'subscriber', 'type'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Request $request) {
        $validator = Validator::make($request->all(), ['last_name' => 'required', 'first_name' => 'required', 'mobile' => 'nullabke', 'email' => 'required|string|email|unique:users,email,' . $user->id, 'active' => 'required']);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->first() ]);
        }
        $user->last_name = $request->input('last_name');
        $user->first_name = $request->input('first_name');
        $user->mobile = $request->input('mobile');
        $user->email = $request->input('email');
        $password = $request->input('password');
        if (!empty($password)) $user->password = bcrypt($password);
        $user->active = $request->input('active');
        $user->save();
        $user->syncRoles($request->input('role'));
        return response()->json(['success' => 'true', 'message' => 'Admin User has been updated successfully']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        $user->delete();
        return response()->json(['success' => 'true', 'message' => 'Admin User has been deleted successfully']);
    }
    //  FRONTEND
    public function register(Request $request) {
        $validator = Validator::make($request->all(), ['last_name' => 'required', 'first_name' => 'required', 'email' => 'required|string|email', 'mobile' => 'required|numeric', 'birthday' => 'required', 'gender' => 'required', 'alumini_member' => 'required', 'alumini_course' => 'required_if:alumini_member,1', 'alumini_attend' => 'required_if:alumini_member,1', 'subscription_id' => 'required', 'password' => 'required|string|min:6|confirmed', ]);

        if ($validator->fails()) 
            return response()->json(['errors' => $validator->errors()->first() ]);

        $User = User::where('email', $request->input('email'))->first();
        if ($User) {
            $checkSubscriber = Subscriber::where('user_id', $User->id)->count();
            if ($checkSubscriber) 
                return response()->json(['errors' => 'Subscriber account already exists with current email.']);
        } else $User = new User;

        $is_free = false;
        if (!empty($request->getSubscriptionFee) && !empty($request->subscription_id)) {
            $fee = Subscription::where('id', $request->subscription_id)->first()->fee;

            if($fee > 0)
                return response()->json(['success' => 'true', 'fee' => $fee]);
            else
                $is_free = true;
        }
        DB::beginTransaction();
        try {
            $generator = new Generator();
            $User->last_name = $request->input('last_name');
            $User->first_name = $request->input('first_name');
            $User->display_name = $request->input('first_name') . " " . $request->input('last_name');
            $User->username = $generator->generate($request->input('first_name') . " " . $request->input('last_name'));
            $User->mobile = $request->input('mobile');
            $User->email = $request->input('email');
            $User->password = bcrypt($request->input('password'));
            $User->wp_news_password = $request->input('password'); # Password for news wordpress website
            $User->birthday = $request->input('birthday');
            $User->gender = $request->input('gender');
            $User->alumini_member = $request->input('alumini_member');
            $User->alumini_course = $request->input('alumini_course');
            $User->alumini_attend = $request->input('alumini_attend');
            $User->alumini_subscription_id = $request->input('subscription_id');
            $User->active = 1;
            $User->save();
            $User->assignRole('user');

            /* For Paypal Payment */
            if (isset($request->orderID)) {
                
                if (env('PAYPAL_MODE') == 'live') 
                    $client = PayPalClientLive::client();
                else 
                    $client = PayPalClient::client();
                    
                $response = $client->execute(new OrdersGetRequest($request->orderID));

                if ($response->result->status == 'COMPLETED') {
                    $result = $response->result;
                    $dt = Carbon::now();
                    $purchase_amount = 0;
                    foreach ($response->result->purchase_units as $purchase_unit) {
                        $purchase_amount+= $purchase_unit->amount->value;
                    }
                    $payment = new Payment;
                    $payment->user_id = $User->id;
                    $payment->payment_mode_id = 1; //paypal
                    $payment->amount = $purchase_amount;
                    $payment->txn_id = $result->id;
                    $payment->transaction = 'credit';
                    $payment->payment_type_id = $request->paymentTypeId;
                    $payment->payer_name = $request->input('first_name') . ' ' . $request->input('last_name');
                    $payment->last_name = $request->input('last_name');
                    $payment->first_name = $request->input('first_name');
                    $payment->payer_email = $request->input('email');
                    $payment->payer_mobile = $request->input('mobile');
                    $payment->date = $dt->toDateString();
                    $payment->callback_response = json_encode($response);
                    $payment->save();
                }
                if (!empty($request->subscription_id)) {
                    $Subscriber = $this->subscribeUser($request, $User);
                    $payment->subscriber_id = $Subscriber->id;
                    $payment->save();
                }
            }

            /* For Trial Payment */
            if($is_free)
                $Subscriber = $this->subscribeUser($request, $User);

            try {
                // SEND VERIFICATION EMAIL
                $User->sendEmailVerificationNotification();
            }
            catch(\Exception $e) {
            }

            DB::commit();

            /* CREATE USER AT WP NEWS WEBSITE */
            $wp_resp = createWpNewsUser($request, $User);
            $wp_resp = json_decode($wp_resp, true);
            $User->wp_news_user_id = (isset($wp_resp['id']) ? $wp_resp['id'] : 0);
            $User->save();

            /* Notify all admin users for new enrollment */
            event(new UserSubscribed($Subscriber));

            return response()->json(['success' => 'true', 'message' => 'User added Successfully']);
        }
        catch(Exception $e) {
            DB::rollBack();
            return response()->json(['errors' => 'true', 'message' => $e->getMessage() ]);
        }
    }

    private function subscribeUser(Request $request, User $user)
    {
        $subscription = Subscription::where('id', $request->subscription_id)->first();
        $month = $subscription->days_duration;
        $dt = Carbon::now();
        $today = new Carbon($dt->toDateString());
        $starts_at = $today;
        $expires_at = $starts_at->addDays($subscription->days_duration);
        // USER SUBSCRIPTION
        $Subscriber = new Subscriber;
        $Subscriber->user_id = $user->id;
        $Subscriber->subscription_id = $subscription->id;
        $Subscriber->starts_at = $starts_at;
        $Subscriber->expires_at = $expires_at;
        $Subscriber->active = 1;
        $Subscriber->save();
        
        try {
            Mail::to($user)->send(new Subscribed($Subscriber));
        }
        catch(\Exception $e) {
        }

        return $Subscriber;
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('login');
    }

    public function changeWordpressPassword(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $email = $request->input('email');
        $password = $request->input('password');
        
        $user = User::where('email', $email)->firstOrFail();
        $user->password = \Hash::make($password);
        $user->wp_news_password = $password;
        $user->save();
        
        return response()->json([ 'success' => true ]);
    }
}

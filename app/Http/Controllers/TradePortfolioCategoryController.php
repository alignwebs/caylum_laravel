<?php

namespace App\Http\Controllers;
use App\TradePortfolioCategory;
use App\TradePortfolioSubCategory;
use Illuminate\Http\Request;
use Validator;
class TradePortfolioCategoryController extends Controller
{
            public function index()
            {   

                $title = "Manage Trade Setup";
                $trade_portfolio_categories = TradePortfolioCategory::pluck('name','id');
                $categories = TradePortfolioCategory::all();

                return view('admin.trade_portfolio_category.index', compact('title','trade_portfolio_categories','categories'));
                
            }

            public function store(Request $request)
            {

                $validator = Validator::make($request->all(), [
                        
                        'name' => 'required|unique:trade_portfolio_categories,name',
                    
                ]);

                
                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }

                if(empty($request->trade_portfolio_category_id))
                {   
                    $TradePortfolioCategory = new TradePortfolioCategory;
                    $TradePortfolioCategory->name = $request->input('name');
                    $TradePortfolioCategory->save();    

                }else
                {
                    $TradePortfolioCategory = TradePortfolioCategory::find($request->trade_portfolio_category_id);
                    $TradePortfolioSubCategory = new TradePortfolioSubCategory;
                    $TradePortfolioSubCategory->trade_portfolio_category_id = $TradePortfolioCategory->id;
                    $TradePortfolioSubCategory->name = $request->input('name');
                    $TradePortfolioSubCategory->save(); 
                }

                

                return response()->json(['success'=>'true','message'=>'Portfolio Category has been added successfully']);
            }

            public function edit(TradePortfolioCategory $category)
            {
                return view('admin.trade_portfolio_category.ajax.edit', compact('category','portfolio_categories')); 
            }

            public function update(TradePortfolioCategory $category, Request $request)
            {

                $validator = Validator::make($request->all(), [
                        
                        'name' => 'required|unique:trade_portfolio_categories,name,' . $category->id
                    
                ]);
                
                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
 
                $category->name = $request->input('name');
                $category->save();    
                return response()->json(['success'=>'true','message'=>'Trade Portfolio Category has been updated successfully']);
            }

            public function destroy(TradePortfolioCategory $category)
            {   
                TradePortfolioSubCategory::where('trade_portfolio_category_id',$category->id)->delete();
                $category->delete();
                return response()->json(['success'=>'true','message'=>'Trade Portfolio Category deleted successfully']);
            }

            // SUB CATEGORY UPDATE

            public function subcategoryEdit($subcategory_id)
            {   
                $subcategory = TradePortfolioSubCategory::where('id',$subcategory_id)->first();
                $portfolio_categories = TradePortfolioCategory::pluck('name','id');
                return view('admin.trade_portfolio_category.ajax.subcategory_edit', compact('subcategory','portfolio_categories')); 
               
            }

            public function subcategoryUpdate(Request $request, $subcategory_id)
            {   

                $validator = Validator::make($request->all(), [
                        
                        'name' => 'required|unique:trade_portfolio_categories,name',
                        'trade_portfolio_category_id' => 'required',
                    
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                    
                $TradePortfolioSubCategory = TradePortfolioSubCategory::find($subcategory_id);
                $TradePortfolioSubCategory->trade_portfolio_category_id = $request->trade_portfolio_category_id;
                $TradePortfolioSubCategory->name = $request->input('name');
                $TradePortfolioSubCategory->save(); 
       

                return response()->json(['success'=>'true','message'=>'Portfolio Sub Category has been added successfully']);
            }

            // SUB CATEGORY DESTROY
            public function subcategoryDestroy($subcategory_id)
            {   
               TradePortfolioSubCategory::where('id',$subcategory_id)->delete();
               return response()->json(['success'=>'true','message'=>'Trade Portfolio Sub Category deleted successfully']);
            }



}

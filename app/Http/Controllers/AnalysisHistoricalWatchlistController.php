<?php

namespace App\Http\Controllers;

use App\AnalysisHistoricalWatchlist;
use Illuminate\Http\Request;
use App\Stock;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
use Carbon\Carbon;
class AnalysisHistoricalWatchlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
       
        if (request()->ajax()) {
            return DataTables::of(AnalysisHistoricalWatchlist::query())
            ->editColumn('action', function(AnalysisHistoricalWatchlist $historical) {
                return  ModelBtn2('historical',$historical->id);
            })
            ->toJson();
        }
        $title = "Analysis - Historical Watchlist";
        $stocks = Stock::all();
        $builder->columns([
          
         ['data' => 'date_added', 'name' => 'date_added', 'title' => 'Date Added'],
         ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
         ['data' => 'price_added', 'name' => 'price_added', 'title' => 'Price Added'],
         ['data' => 'setup', 'name' => 'setup', 'title' => 'Setup'],
         ['data' => 'date_deleted', 'name' => 'date_deleted', 'title' => 'Date Deleted'],
         ['data' => 'price_deleted', 'name' => 'price_deleted', 'title' => 'Price Deleted'],
         ['data' => 'return', 'name' => 'return', 'title' => 'Return (%)'],
         ['data' => 'comments', 'name' => 'comments', 'title' => 'Comments'],
         ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

     ]);
        $datatable =  $builder->parameters([
          'searchDelay' => 500,
          'stateSave' => 'true',
          'order' => [[ 0, "desc" ]]
      ]);

        return view('admin.analysis.historical_watchlist.index', compact('datatable','title','stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_added' => 'required',
            'stock' => 'required',
            'price_added' => 'required',
            'setup' => 'required',
            'date_deleted' => 'required',
            'price_deleted' => 'required',
            'return' => 'required'
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        
        $AnalysisHistoricalWatchlist = new AnalysisHistoricalWatchlist;
        $AnalysisHistoricalWatchlist->date_added = $request->input('date_added');
        $AnalysisHistoricalWatchlist->stock = $request->input('stock');
        $AnalysisHistoricalWatchlist->price_added = $request->input('price_added');
        $AnalysisHistoricalWatchlist->setup = $request->input('setup');
        $AnalysisHistoricalWatchlist->date_deleted = $request->input('date_deleted');
        $AnalysisHistoricalWatchlist->price_deleted = $request->input('price_deleted');
        $AnalysisHistoricalWatchlist->return = $request->input('return');
        $AnalysisHistoricalWatchlist->comments = $request->input('comments');
        $AnalysisHistoricalWatchlist->save();

        return response()->json(['success'=>'true','message'=>'Historical Watchlist has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisHistoricalWatchlist  $analysisHistoricalWatchlist
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisHistoricalWatchlist $historical)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisHistoricalWatchlist  $historical
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisHistoricalWatchlist $historical)
    {   
        $stocks = Stock::all();
        return view('admin.analysis.historical_watchlist.ajax.edit', compact('historical','stocks')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisHistoricalWatchlist  $historical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisHistoricalWatchlist $historical)
    {
        $validator = Validator::make($request->all(), [
            'date_added' => 'required',
            'stock' => 'required',
            'price_added' => 'required',
            'setup' => 'required',
            'date_deleted' => 'required',
            'price_deleted' => 'required',
            'return' => 'required'
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
        $AnalysisHistoricalWatchlist = AnalysisHistoricalWatchlist::find($historical->id);
        $AnalysisHistoricalWatchlist->date_added = $request->input('date_added');
        $AnalysisHistoricalWatchlist->stock = $request->input('stock');
        $AnalysisHistoricalWatchlist->price_added = $request->input('price_added');
        $AnalysisHistoricalWatchlist->setup = $request->input('setup');
        $AnalysisHistoricalWatchlist->date_deleted = $request->input('date_deleted');
        $AnalysisHistoricalWatchlist->price_deleted = $request->input('price_deleted');
        $AnalysisHistoricalWatchlist->return = $request->input('return');
        $AnalysisHistoricalWatchlist->comments = $request->input('comments');
        $AnalysisHistoricalWatchlist->save();

        return response()->json(['success'=>'true','message'=>'Historical Watchlist has been updated successfully']);
    }


    public function import(Request $request)
    {
        $validator = Validator::make($request->all(), [
         'historical_watchlists' => 'required',
     ]);

        if ($validator->fails())
        return response()->json(['errors'=>$validator->errors()->first()]);

        $getClientOriginalExtension = $request->file('historical_watchlists')->getClientOriginalExtension();  
        $filename = $request->file('historical_watchlists')->storeAs('historical_watchlists',time().".".$getClientOriginalExtension);
        $path = $request->file('historical_watchlists')->getRealPath();
        
        $csv_data = (new FastExcel)->import('storage/'.$filename);

        foreach ($csv_data as $key => $row) { 

            if(!isset($row['date_added']) || empty($row['date_added']) || !isset($row['stock']) || empty($row['stock']) || !isset($row['price_added']) ||  empty($row['price_added']) || !isset($row['setup']) ||  empty($row['setup']) || !isset($row['date_deleted']) ||  empty($row['date_deleted']) || !isset($row['price_deleted']) ||  empty($row['price_deleted']) || !isset($row['return']) ||  empty($row['return']))
            {
                continue;
            }

            $data[] = [
              'date_added'  =>  $row['date_added']->format('Y-m-d'),
              'stock'  =>  $row['stock'],
              'price_added'  =>  $row['price_added'],
              'setup'  =>  isset($row['setup']) ? $row['setup'] : null,
              'date_deleted'  =>  isset($row['date_deleted']) ? $row['date_deleted'] : null,
              'price_deleted'  =>  isset($row['price_deleted']) ? $row['price_deleted'] : null,
              'return'  =>  isset($row['return']) ? $row['return'] : null,
              'comments'  =>  isset($row['comments']) ? $row['comments'] : null
          ]; 
      
  }
      AnalysisHistoricalWatchlist::insert($data);
      return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisHistoricalWatchlist  $historical
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisHistoricalWatchlist $historical)
    {
        $historical->delete();
        return response()->json(['success'=>'true','message'=>'Historical Analysis has been deleted successfully']);
    }
}

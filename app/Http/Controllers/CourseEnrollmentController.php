<?php

namespace App\Http\Controllers;

use Akaunting\Money\Money;
use App\Course;
use App\CourseEnrollment;
use App\CustomForm;
use App\Mail\BankDeposit;
use App\Mail\CourseEnrolled;
use App\Payment;
use App\PaymentMode;
use App\PaymentType;
use App\Subscriber;
use App\Subscription;
use App\User;

use App\Events\CourseEnrolled as CourseEnrolledEvent;

use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use Rap2hpoutre\FastExcel\FastExcel;
use Sample\PayPalClient;
use App\Providers\PayPalClientServiceProvider as PayPalClientLive;
use Storage;
use TaylorNetwork\UsernameGenerator\Generator;
use Validator;
use Yajra\DataTables\Html\Builder;

class CourseEnrollmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {

     if (request()->ajax()) {
        return DataTables::of(CourseEnrollment::with(['user', 'payment2', 'course']))

        ->editColumn('course.name', function(CourseEnrollment $enrollment) {
            return  $enrollment->course->name;
        })

        ->editColumn('date', function(CourseEnrollment $enrollment) {
            return  @$enrollment->payment2->date;
        })
        ->editColumn('created_at', function(CourseEnrollment $enrollment) {
            return  Carbon::parse(@$enrollment->created_at)->format('Y-m-d');
        })
        ->addColumn('action', function(CourseEnrollment $enrollment) {
            return ModelBtn2('enrollment', $enrollment->id);
        })
        ->toJson(); 
    }
    $builder->columns([
        ['data' => 'id', 'name' => 'id', 'title' => '#'],
        ['data' => 'last_name', 'name' => 'last_name', 'title' => 'Last Name'],
        ['data' => 'first_name', 'name' => 'first_name', 'title' => 'First Name'],
        ['data' => 'course.name', 'name' => 'course.name', 'title' => 'Course'],
        ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
        ['data' => 'mobile', 'name' => 'mobile', 'title' => 'Contact'],
        ['data' => 'gender', 'name' => 'gender', 'title' => 'Gender'],
        ['data' => 'birthday', 'name' => 'birthday', 'title' => 'Birthdate'],
        // ['data' => 'formatted_money', 'name' => 'formatted_money', 'title' => 'Amount Paid'],
        ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Date Registered'],
        /*['data' => 'date', 'name' => 'date', 'title' => 'Date Paid'],*/
        ['data' => 'comments', 'name' => 'comments', 'title' => 'Comments'],
        ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

    ]);
    $datatable =  $builder->parameters([
      'searchDelay' => 500,
      'stateSave' => 'true',
       'order' => [[ 0, "desc" ]]
  ]);


    $title = "Manage Course Enrollments";
    $subscriptions = Subscription::get()->pluck('complete_package','id'); 
    $courses = Course::pluck('name','id'); 

    return view('admin.course_enrollment.index', compact('datatable','title','courses','subscriptions'));
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'last_name' => 'required',
            'first_name' => 'required',
            'mobile' => 'required|numeric',
            'gender' => 'required',
            'email' => 'required|string|email',
            'active' => 'required',
            'course_id' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        DB::beginTransaction();
        try{

            $CourseEnrollment = new CourseEnrollment;
            $CourseEnrollment->course_id = $request->course_id;
            $CourseEnrollment->comments = $request->comments;
            $CourseEnrollment->last_name = $request->input('last_name');
            $CourseEnrollment->first_name = $request->input('first_name');
            $CourseEnrollment->mobile = $request->input('mobile');
            $CourseEnrollment->email = $request->input('email');
            $CourseEnrollment->birthday = $request->input('birthday');
            $CourseEnrollment->gender = $request->input('gender');
            $courseEnrollment->active = $request->input('active');
            $CourseEnrollment->save();

            DB::commit();
            
            return response()->json(['success'=>'true','message'=>'Course Enrollment has been added successfully']);


        } catch (Exception $e) {

            DB::rollBack();
            return response()->json(['errors'=>'true','message'=>$e->getMessage()]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseEnrollment  $courseEnrollment
     * @return \Illuminate\Http\Response
     */
    public function show(CourseEnrollment $enrollment)
    {
//        return $enrollment;
        $subscriber = Subscriber::where('user_id',$enrollment->user_id)->first();
        $courses = Course::pluck('name','id');
        $subscriptions = Subscription::get()->pluck('complete_package','id');
        $title = "View Course Enrollment";
        return view('admin.course_enrollment.view', compact('enrollment','courses','subscriber','subscriptions','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseEnrollment  $courseEnrollment
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseEnrollment $enrollment)
    {
      
        $subscriber = Subscriber::where('user_id',$enrollment->user_id)->first();
        $courses = Course::pluck('name','id');
        $subscriptions = Subscription::get()->pluck('complete_package','id');
        return view('admin.course_enrollment.ajax.edit', compact('enrollment','courses','subscriber','subscriptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseEnrollment  $courseEnrollment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $courseEnrollment = CourseEnrollment::where('id',$id)->first();

        $validator = Validator::make($request->all(), [
            'last_name' => 'required',
            'first_name' => 'required',
            'mobile' => 'required|numeric',
            'gender' => 'required',
            'email' => 'required|string|email',
            'active' => 'required',
            'course_id' => 'required',
            
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        DB::beginTransaction();
        try{
 
        $courseEnrollment->course_id = $request->course_id;
        $courseEnrollment->comments = $request->comments;
        $courseEnrollment->last_name = $request->input('last_name');
        $courseEnrollment->first_name = $request->input('first_name');
        $courseEnrollment->mobile = $request->input('mobile');
        $courseEnrollment->birthday = $request->input('birthday');
        $courseEnrollment->gender = $request->input('gender');
        $courseEnrollment->email = $request->input('email');
        $courseEnrollment->active = $request->input('active');
        $courseEnrollment->save();

        DB::commit();

        return response()->json(['success'=>'true','message'=>'Course Enrollment has been updated successfully']);


    } catch (Exception $e) {

        DB::rollBack();
        return response()->json(['errors'=>'true','message'=>$e->getMessage()]);

    }
}


    //  FRONTEND
public function enrollCourse($slug)
{
    $courseDetail = Course::where('slug',$slug)->firstOrFail();
    // return $courseDetail->custom_form->form_json;
    $subscription_id = "";
    if($courseDetail->subscribe_default)
    {
        $subscription = Subscription::where("default",1)->first();
        $subscription_id = $subscription->id;
    }
    return view('frontend.course_enrollment', compact('courseDetail','subscription_id'));
}

public function enrollCourseStore(Request $request)
{   

    $validator = Validator::make($request->all(), [
        'last_name' => 'required',
        'first_name' => 'required',
        'gender' => 'required',
        'birthday' => 'required',
        'mobile' => 'required|numeric',
        'email' => 'required|string|email',
        'course_id' => 'required',
    ]);


    if ($validator->fails())
    {
        return response()->json(['errors'=>$validator->errors()->first()]);
    }

    $course = Course::where("id",$request->course_id)->first();

    if($course->is_paid and $request->input('is_paid_review') > 0)
    {
        if($request->input('payment_mode') == 'paypal')
            return response()->json(['payment'=>true]);
    }
     
    $custom_form_data = [];

    if($request->custom_form)
    {
      foreach($request->custom_form as $key => $val)
      {

          if(is_object($request->custom_form[$key]))
          {
           
              $file = $request->custom_form[$key];
              
              $directory = 'custom_form_uploads';
              
              $file_path = $request->file("custom_form")[$key]->storeAs(
                                            'custom_form_uploads', time().$file->getClientOriginalName()
                                        );
              $custom_form_data[] = [$key => $file_path, 'is_file' => true];
          }
          else
          {
              $custom_form_data[] = [$key => $val];
          }
      }
    }


     DB::beginTransaction();
     try{
  
     $CourseEnrollment = new CourseEnrollment;
     $CourseEnrollment->course_id = $request->course_id;
     $CourseEnrollment->payment_mode = $request->input('payment_mode');

     $CourseEnrollment->custom_form = json_encode($custom_form_data);
     $CourseEnrollment->last_name = $request->input('last_name');
     $CourseEnrollment->first_name = $request->input('first_name');
     $CourseEnrollment->mobile = $request->input('mobile');
     $CourseEnrollment->email = $request->input('email');
     $CourseEnrollment->birthday = $request->input('birthday');
     $CourseEnrollment->gender = $request->input('gender');
     $CourseEnrollment->course_details = $course;

     $CourseEnrollment->save();

     if($course->is_paid && $request->input('payment_mode') != 'bank_deposit')
     {

         if (env('PAYPAL_MODE') == 'live') 
            $client = PayPalClientLive::client();
        else 
            $client = PayPalClient::client();

        $response = $client->execute(new OrdersGetRequest($request->orderID));

        if($response->result->status == 'COMPLETED')
        {   

              $result = $response->result;
              $dt = Carbon::now();
              $purchase_amount = 0;
              foreach($response->result->purchase_units as $purchase_unit)
              {
                    $purchase_amount += $purchase_unit->amount->value;
              }

              $payer_name = $result->payer->name->given_name." ".$result->payer->name->surname;

                $payment = new Payment;
                $payment->payment_mode_id = 1; //paypal
                $payment->amount = $purchase_amount;
                $payment->txn_id = $request->orderID;
                $payment->transaction = 'credit';
                $payment->payment_type_id = $request->paymentTypeId; 
                $payment->last_name = $request->input('last_name');
                $payment->first_name = $request->input('first_name');
                $payment->payer_name = $payer_name;
                $payment->payer_email = $request->input('email');
                $payment->payer_mobile = $request->input('mobile');
                $payment->date = $dt->toDateString();
                $payment->course_enrollment_id = $CourseEnrollment->id;
                $payment->callback_response = json_encode($response);
                $payment->save();
        }
        
     }

     /*if(!empty($request->subscription_id))
     {
         $subscription = Subscription::where('id',$request->subscription_id)->first();
         if($subscription->default){
             $month = $subscription->month_duration;
             $dt = Carbon::now();
             $starts_at = new Carbon($dt->toDateString());
             $expires_at = $starts_at->addMonths($subscription->month_duration);
                        // USER SUBSCRIPTION
             $Subscriber = new Subscriber;
             $Subscriber->user_id = $User->id;
             $Subscriber->subscription_id = $subscription->id;
             $Subscriber->starts_at = $starts_at;
             $Subscriber->expires_at = $expires_at;
             $Subscriber->save();

             $payment->subscriber_id = $Subscriber->id;  
             $payment->save(); 
         }
     }*/

     DB::commit();
     
     $request->session()->put('CourseEnrollmentID',$CourseEnrollment->id);

     Mail::to($CourseEnrollment->email)->send(new CourseEnrolled($CourseEnrollment));

     /* Send bank deposit details to user in case of payment mode = bank deposit*/
     if($request->input('payment_mode') == 'bank_deposit')
        Mail::to($CourseEnrollment->email)->send(new BankDeposit($CourseEnrollment));

    /* Notify all admin users for new enrollment */
    event(new CourseEnrolledEvent($CourseEnrollment));

    return response()->json(['success'=>'true','url'=>route('enrollment-pay-success','')]);


 } catch (Exception $e) {

     DB::rollBack();
     return response()->json(['errors'=>'true','message'=>$e->getMessage()]);

 }




}

    // END FRONTEND

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseEnrollment  $courseEnrollment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
       CourseEnrollment::where('id',$id)->delete();
       return response()->json(['success'=>'true','message'=>'Course Enrollment has been deleted successfully']);
    }

    public function email()
    {
        return view('admin.email');
    }

      public function export()
      {
         return (new FastExcel(CourseEnrollment::all()))->download('course_enrollments.csv', function ($courseEnrollment) {

             return [
                 'Last Name' => $courseEnrollment->last_name,
                 'First Name' => $courseEnrollment->first_name,
                 'Course' => $courseEnrollment->course->name,
                 'Email' => $courseEnrollment->email,
                 'Contact' => $courseEnrollment->mobile,
                 'Gender' => $courseEnrollment->gender,
                 'Birthdate' => $courseEnrollment->birthday,
                 'Amount Paid' => $courseEnrollment->formatted_money,
                 'Date Registered' => Carbon::parse($courseEnrollment->created_at)->format('Y-m-d'),
                 'Date Paid' => @$courseEnrollment->payment2->date
             ];

         });
      }


}

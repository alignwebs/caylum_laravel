<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseEnrollment;
use App\CustomForm;
use DataTables;
use Illuminate\Http\Request;
use Validator;
use Akaunting\Money\Money;
use Yajra\DataTables\Html\Builder;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
      
        if (request()->ajax()) {
            return DataTables::of(Course::withTrashed())
                    ->editColumn('is_paid', function(Course $course) {
                        return  intToBool($course->is_paid);
                    })
                    
                    ->editColumn('subscribe_default', function(Course $course) {
                        return  intToBool($course->subscribe_default);
                    })
                    ->setRowAttr([
                        'class' => function($course) {
                            if($course->trashed())
                            {
                                return "table-inactive";
                            }
                        },
                    ])
                    ->editColumn('action', function(Course $course) {
                        return  ModelBtn2('course',$course->id,'',$course->slug,$course);
                    })
                    ->toJson();
        }
        $title = "Manage Courses";
        $custom_forms = CustomForm::pluck('form_name','id');
        $builder->columns([
                      
                       ['data' => 'name', 'name' => 'name', 'title' => 'Course Title'],
                       ['data' => 'formatted_std_money', 'name' => 'formatted_std_money', 'title' => 'Standard Rate'],
                       ['data' => 'formatted_money', 'name' => 'formatted_money', 'title' => 'Published Rate'],
                       ['data' => 'enrollments_count', 'name' => 'enrollments_count', 'title' => 'Total Enrollees'],
                       ['data' => 'paid_enrollments_count', 'name' => 'paid_enrollments_count', 'title' => 'Registered | Paid'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);
        return view('admin.course.index', compact('datatable','title','custom_forms'));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                
                'name' => 'required',
                'std_rate' => 'required',
                'price' => 'required',
                'is_paid' => 'required',
                'subscribe_default' => 'required'
            
        ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

    
        $Course = new Course;
        
        
        $Course->name = $request->input('name');
        $Course->description = $request->input('description');
        $Course->std_rate = $request->input('std_rate');
        $Course->price = $request->input('price');
        $Course->is_paid = $request->input('is_paid');
        $Course->subscribe_default = $request->input('subscribe_default');
        $Course->custom_form_id = $request->input('custom_form_id');
       
        $Course->save(); 
        

        return response()->json(['success'=>'true','message'=>'Course has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {   
        $custom_forms = CustomForm::pluck('form_name','id');
        return view('admin.course.ajax.edit', compact('course','custom_forms')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Course $course, Request $request)
    {
        $validator = Validator::make($request->all(), [
                
                'name' => 'required',
                'std_rate' => 'required',
                'price' => 'required',
                'is_paid' => 'required',
                'subscribe_default' => 'required'
            
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }
  
        $course->name = $request->input('name');
        $course->description = $request->input('description');
        $course->std_rate = $request->input('std_rate');
        $course->price = $request->input('price');
        $course->is_paid = $request->input('is_paid');
        $course->subscribe_default = $request->input('subscribe_default');
        $course->custom_form_id = $request->input('custom_form_id');
        $course->save(); 
        

        return response()->json(['success'=>'true','message'=>'Course has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */

    public function restore(Request $request)
    {
      Course::withTrashed()->where('slug',$request->slug)->restore();
      flash('Customer Restored Successfully')->success();
      return redirect()->back();
    }

    public function destroy(Course $course)
    {   
        $courseEnrollment = CourseEnrollment::where('course_id',$course->id)->count();
        if($courseEnrollment > 0)
            $course->delete();
        else
            $course->forceDelete();

        return response()->json(['success'=>'true','message'=>'Course has been deleted successfully']);
    }

   
}

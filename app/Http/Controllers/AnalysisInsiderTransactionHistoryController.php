<?php

namespace App\Http\Controllers;

use App\AnalysisInsiderTransactionHistory;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\Html\Builder;
use App\Stock;

class AnalysisInsiderTransactionHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(AnalysisInsiderTransactionHistory::query())
                    ->editColumn('action', function(AnalysisInsiderTransactionHistory $history) {
                        return  ModelBtn2('history',$history->id);
                    })
                    ->toJson();
        }
        $title = "Analysis - Historical Insider Transactions";
        $stocks = Stock::all();
        $builder->columns([
                        
                       ['data' => 'date', 'name' => 'date', 'title' => 'Date'],
                       ['data' => 'stock', 'name' => 'stock', 'title' => 'Stock'],
                       ['data' => 'no_of_shares', 'name' => 'no_of_shares', 'title' => 'Number of Shares'],
                       ['data' => 'price', 'name' => 'price', 'title' => 'Price'],
                       ['data' => 'total', 'name' => 'total', 'title' => 'Total'],
                       ['data' => 'ad', 'name' => 'ad', 'title' => 'A/D'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);

        return view('admin.analysis.transaction_history.index', compact('datatable','title','stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $validator = Validator::make($request->all(), [
                    'date' => 'required',
                    'stock' => 'required',
                    'no_of_shares' => 'required',
                     'price' => 'required',
                     'total' => 'required',
                     'ad' => 'required',
                   
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisInsiderTransactionHistory = new AnalysisInsiderTransactionHistory;
                $AnalysisInsiderTransactionHistory->date = $request->input('date');
                $AnalysisInsiderTransactionHistory->stock = $request->input('stock');
                $AnalysisInsiderTransactionHistory->no_of_shares = $request->input('no_of_shares');
                $AnalysisInsiderTransactionHistory->price = $request->input('price');
                $AnalysisInsiderTransactionHistory->total = $request->input('total');
                $AnalysisInsiderTransactionHistory->ad = $request->input('ad');
                $AnalysisInsiderTransactionHistory->save();

                return response()->json(['success'=>'true','message'=>'Analysis Insider Transaction History has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnalysisInsiderTransactionHistory  $analysisInsiderTransactionHistory
     * @return \Illuminate\Http\Response
     */
    public function show(AnalysisInsiderTransactionHistory $analysisInsiderTransactionHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnalysisInsiderTransactionHistory  $analysisInsiderTransactionHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalysisInsiderTransactionHistory $history)
    {
        $stocks = Stock::all();
        return view('admin.analysis.transaction_history.ajax.edit', compact('history','stocks')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnalysisInsiderTransactionHistory  $analysisInsiderTransactionHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalysisInsiderTransactionHistory $history)
    {
                $validator = Validator::make($request->all(), [
                    'date' => 'required',
                    'stock' => 'required',
                    'no_of_shares' => 'required',
                     'price' => 'required',
                     'total' => 'required',
                     'ad' => 'required',
                   
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }
                
                $AnalysisInsiderTransactionHistory = AnalysisInsiderTransactionHistory::find($history->id);
                $AnalysisInsiderTransactionHistory->date = $request->input('date');
                $AnalysisInsiderTransactionHistory->stock = $request->input('stock');
                $AnalysisInsiderTransactionHistory->no_of_shares = $request->input('no_of_shares');
                $AnalysisInsiderTransactionHistory->price = $request->input('price');
                $AnalysisInsiderTransactionHistory->total = $request->input('total');
                $AnalysisInsiderTransactionHistory->ad = $request->input('ad');
                $AnalysisInsiderTransactionHistory->save();

                return response()->json(['success'=>'true','message'=>'Analysis Insider Transaction History has been added successfully']);
    }

    // IMPORT EXCEL
    public function import(Request $request)
        {
         
            $validator = Validator::make($request->all(), [
             'insider_transaction_history' => 'required',
            ]);

            if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

            $getClientOriginalExtension = $request->file('insider_transaction_history')->getClientOriginalExtension();  
            $filename = $request->file('insider_transaction_history')->storeAs('insider_transaction_history',time().".".$getClientOriginalExtension);
            $path = $request->file('insider_transaction_history')->getRealPath();
            
            $csv_data = (new FastExcel)->import('storage/'.$filename);

            foreach ($csv_data as $key => $row) { 

                if(!isset($row['date']) || empty($row['date']) || !isset($row['stock']) || empty($row['stock']) || !isset($row['no_of_shares']) || empty($row['no_of_shares']) || !isset($row['price']) || empty($row['price']) || !isset($row['total']) || empty($row['total']) || !isset($row['ad']) || empty($row['ad']))
                {
                    continue;
                }

                $data[] = [
                  'date'  => $row['date'],
                  'stock'  =>  $row['stock'],
                  'no_of_shares'  =>  isset($row['no_of_shares']) ? $row['no_of_shares'] : null,
                  'price'  =>  isset($row['price']) ? $row['price'] : null,
                  'total'  =>  isset($row['total']) ? $row['total'] : null,
                  'ad'  =>  isset($row['ad']) ? $row['ad'] : null
              ]; 
          
      }
          AnalysisInsiderTransactionHistory::insert($data);
          return response()->json(['success'=>'true','message'=> count($data).' records has been imported successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnalysisInsiderTransactionHistory  $analysisInsiderTransactionHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalysisInsiderTransactionHistory $history)
    {
        $history->delete();
        return response()->json(['success'=>'true','message'=>'Analysis Insider Transaction History has been deleted successfully']);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated()
    {
        if(Auth::user()->active && Auth::user()->hasRole('user')){
            $user = User::find(Auth::id());
            $user->is_online = 1;
            $user->save();
        }

        if (Auth::user()->active == 0) { 
           Auth::logout();
           flash('User is not active')->error();
           return redirect()->route('login');
        }

        if ( Auth::user()->hasRole('admin') ) {
            return redirect()->route('admin.home');
        }
    }

    public function logout(Request $request)
    {
        $user = User::find(Auth::id());
        $user->is_online = 0;
        $user->save();
        Auth::logout();
        return redirect()->route('login');
    }
}

<?php

namespace App\Http\Controllers;

use App\PaySlip;
use App\PaymentType;
use App\Attachment;
use App\Events\PaySlipUploaded;
use Illuminate\Http\Request;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
class PaySlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {

        if (request()->ajax()) {
            return DataTables::eloquent(PaySlip::with('enrollment.course'))
                   ->editColumn('enrollment.course.name', function(PaySlip $payslip) {
                       return  @$payslip->enrollment->course->name;
                   })
                 
                    /*->editColumn('status', function(PaySlip $payslip) {
                        return  intToBool($payslip->status,'payslip');
                    })*/
                    ->editColumn('payment_date', function(PaySlip $payslip) {
                        return  $payslip->payment_date;
                    })
                 
                    ->editColumn('action', function(PaySlip $payslip) {
                        return  ModelBtn2('payslip',$payslip->id,'','',$payslip);
                    })
                    ->toJson();
        }

        $builder->columns([
                       ['data' => 'id', 'name' => 'id', 'title' => 'ID'],
                       ['data' => 'name', 'name' => 'name', 'title' => 'Uploaded By'],
                       ['data' => 'mobile', 'name' => 'mobile', 'title' => 'Mobile'],
                       ['data' => 'type', 'name' => 'type', 'title' => 'Type'],
                       ['data' => 'ref_id', 'name' => 'ref_id', 'title' => 'Ref ID'],
                       ['data' => 'enrollment.course.name', 'name' => 'enrollment.course.name', 'title' => 'Course'],
                       
                       /*['data' => 'status', 'name' => 'status', 'title' => 'Status'],*/
                       ['data' => 'payment_date', 'name' => 'payment_date', 'title' => 'Payment Date'],
                       ['data' => 'comments', 'name' => 'comments', 'title' => 'Comments'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);

        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => true,
               'order' => [[ 0, "desc" ]]
          ]);
        
        $title = "Manage Pay Slips";
        return view('admin.payslip.index', compact('datatable','title'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPayslip(Request $request)
    {
        $ref_id = $request->ref_id;
        $type = $request->type;
        return view('frontend.payslip', compact('ref_id','type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePayslip(Request $request)
    {

        $validator = Validator::make($request->all(), [
                
                'type' => 'required',
                'ref_id' => 'required',
                'name' => 'required',
                'mobile' => 'required',
                'payment_date' => 'required',
                'file' => 'required|mimes:jpeg,bmp,png',
                'comments' => 'nullable'
        ]);

        
        if ($validator->fails())
            return response()->json(['errors'=>$validator->errors()->first()]);

        $path = "payslips/nofile.jpg";
        if ($request->hasFile('file')) {
           $getClientOriginalExtension = $request->file('file')->getClientOriginalExtension();
           $path = $request->file('file')->storeAs('payslips',time().".".$getClientOriginalExtension,'public');
        }

        $PaySlip = new PaySlip;
        $PaySlip->type = $request->input('type');
        $PaySlip->ref_id = $request->input('ref_id');
        $PaySlip->name = $request->input('name');
        $PaySlip->mobile = $request->input('mobile');
        $PaySlip->payment_date = $request->input('payment_date');
        $PaySlip->comments = $request->input('comments');
        $PaySlip->save(); 
        
        $Attachment = new Attachment;
        $Attachment->file = $path;
        
        $PaySlip->attachment()->save($Attachment);

        event(new PaySlipUploaded($PaySlip));

        return response()->json(['success'=>'true','message'=>'Pay Slip has been submitted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function show(PaySlip $paySlip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function edit(PaySlip $paySlip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaySlip $paySlip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       PaySlip::where('id',$id)->delete();
       return response()->json(['success'=>'true','message'=>'Pay Slip has been deleted successfully']);
    }   
}

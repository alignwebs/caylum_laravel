<?php

namespace App\Http\Controllers;

use App\Stock;
use App\StockAlert;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StockAlertController extends Controller
{
  
    public function index()
    {
        //
    }

    public function getUserAlerts(Request $request)
    {
        $user_id = Auth::id();
        $stock_symbol = $request->input('stock_symbol');

        if($stock_symbol)
            $stock_alerts = StockAlert::with('stock')->where('stock_symbol', $stock_symbol)->get();
        else
            $stock_alerts = StockAlert::with('stock')->get();

        return view('portal.stock.alert.list', compact('stock_alerts'));
    }
    
    public function create(Request $request)
    {
        $request->validate([
            'stock_symbol' => 'required'
        ]);
        
        $user_id = Auth::id();

        $stock_symbol = $request->input('stock_symbol');

        $stock = Stock::with('market')->whereSymbol($stock_symbol)->firstOrFail();

        return view('portal.stock.alert.add', compact('stock','stock_alerts'));
    }

    public function store(Request $request)
    {
        //return $request->all();
        $request->validate([
            'stock_symbol' => 'required',
            'type' => 'required',
            'value' => 'required'
        ]);

        $user_id = Auth::id();
        $stock_symbol = $request->input('stock_symbol');
        $type = $request->input('type');
        $value = $request->input('value');

        $data[] = array(
            'user_id'       =>  $user_id,
            'stock_symbol'  =>  $stock_symbol, 
            'type'          =>  $type, 
            'value'         =>  $value,
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now()
        );

        StockAlert::insert($data);

        return response()->json([ 'success' => true ]);
    }

    public function updateStatus(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $request->validate([
            'alert_id' => 'required'
        ]);

        $alert_id = $request->input('alert_id');

        StockAlert::where('id', $alert_id)->update([
                'alerted' => DB::raw('!alerted')
            ]);

        return response()->json([ 'success' => true ]);
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $request->validate([
            'alert_id' => 'required_if:reset,false',
            'stock_symbol' => 'required_if:reset,true'
        ]);

        $reset = $request->input('reset');
        $stock_symbol = $request->input('stock_symbol');
        $alert_id = $request->input('alert_id');

        if($reset)
            StockAlert::where('stock_symbol', $stock_symbol)->delete();
        else
            StockAlert::where('id', $alert_id)->delete();


        return response()->json([ 'success' => true ]);
    }
}

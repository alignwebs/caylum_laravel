<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function index()
    {
    		$title = "Charting";
    		return view('portal.charting.index', compact('title'));
    }
}

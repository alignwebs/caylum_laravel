<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Comment;
use App\Followable;
use App\Feed;
use App\Notifications\NewFeedLike;
use Auth;
use Illuminate\Http\Request;
use Image;
use Storage;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'feed_status_content' => 'required',
            'feed_status_image' => 'nullable|mimes:jpeg,bmp,png',
        ]);

        $user_id = Auth::id();

        if($request->hasFile('feed_status_image'))
        {
            $file = $request->file('feed_status_image');
            $image = Image::make($file)->widen(1200, function ($constraint) {
                $constraint->upsize();
            });
            // Normal Image
            $directory = 'feed_uploads';
            Storage::makeDirectory($directory);
            $file_name = 'feed_uploads/'.time().$file->getClientOriginalName();
            $file_path = storage_path('app/public/'.$file_name);
            $image->save($file_path);

            // thumbnails
            $image = Image::make($file)->widen(800, function ($constraint) {
                $constraint->upsize();
            });
            $directory = 'thumbnail_uploads';
            Storage::makeDirectory($directory);
            $thumbnail_file_name = 'thumbnail_uploads/'.time().$file->getClientOriginalName();
            $file_path = storage_path('app/public/'.$thumbnail_file_name);
            $image->save($file_path);
        }

        $Feed = new Feed;
        $Feed->user_id = $user_id;
        
        $Feed->feed_content = $request->input('feed_status_content');
        $Feed->save();

        if(isset($file_name))
        {
            $Attachment = new Attachment;
            $Attachment->file = $file_name;
            $Attachment->thumbnail = $thumbnail_file_name;
            $Feed->attachment()->save($Attachment);
        }

                /* Find all the urls in text */
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $request->feed_status_content, $match);
        
        $urls = $match[0];

        foreach ($urls as $url)
        {
            
            $imageInfo = removeSpaceFromString(getimagesize($url));
            if(@is_array($imageInfo) and fileIsImage($imageInfo['mime'])){

                // Normal Image
                $file = $url;
                $orig_file_name = getFileExtension($file, $imageInfo['mime']);
                $image = Image::make($file)->widen(1200, function ($constraint) {
                    $constraint->upsize();
                });
                $directory = 'feed_uploads';
                Storage::makeDirectory($directory);
                $file_name = 'feed_uploads/'.time().$orig_file_name;
                $file_path = storage_path('app/public/'.$file_name);
                $image->save($file_path);

                // Thumbnail Image
                $image = Image::make($file);
                $image->widen(800, function ($constraint) {
                    $constraint->upsize();
                });
                $directory = 'thumbnail_uploads';
                Storage::makeDirectory($directory);
                $thumbnail_file_name = 'thumbnail_uploads/'.time().$orig_file_name;
                $file_path = storage_path('app/public/'.$thumbnail_file_name);
                $image->save($file_path);
                

                $Attachment = new Attachment;
                $Attachment->file = $file_name;
                $Attachment->thumbnail = $thumbnail_file_name;
                $Feed->attachment()->save($Attachment);
               
            } 
        }


        return response()->json(['success' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $feed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed $feed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feed $feed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $request->validate([ 'feed_id' => 'required' ]);
        $feed_id = $request->input('feed_id');

        $feed = Feed::where('id', $feed_id)->where('user_id', Auth::id())->firstOrFail();
        if($feed->attachment)
            $feed->attachment->delete();

        if($feed->comments()->count() > 0)
            $comment_ids = $feed->comments()->delete();

        if($feed->likes()->count() > 0)
        {
             $pivot = $feed->likes()->get()->pluck('pivot')->toArray();
             foreach($pivot as $row)
             {
                Followable::where($row)->delete();
             }
        }
        
        $feed->delete();

        return response()->json(['success' => true]);
    }

    public function generateUserFeed(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $user_ids = [];
        
        $user = Auth::user();
        $following_users = $user->followings()->select('id')->get();

        $user_ids[] = $user->id;
        foreach ($following_users as $user) {
           $user_ids[] = $user->id;
        }
        
        $feeds = Feed::with(['user', 'attachment', 'likes', 'comments.commentator'])->whereIn('user_id', $user_ids)->latest("updated_at")->paginate(3);

        return $feeds;
    }

    public function searchFeed(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $q = $request->input('q');
        
        $feeds = Feed::with(['user', 'attachment', 'likes', 'comments.commentator'])->where('feed_content', 'LIKE',"%$q%")->latest("updated_at")->paginate(5);

        return $feeds;
    }

    public function getUserFeed($user_id, Request $request)
    {
        if(!$request->ajax())
            abort(404);
        
        $feeds = Feed::with(['user', 'attachment', 'likes', 'comments.commentator'])->where('user_id', $user_id)->latest("updated_at")->paginate(3);

        return $feeds;
    }

    public function likeFeed(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $request->validate([
            'feed_id' => 'required'
        ]);

        $feed_id = $request->input('feed_id');

        $feed = Feed::findOrFail($feed_id);

        $user = Auth::user();
        $resp = $user->toggleLike($feed);

        if(count($resp['attached']) > 0 && $user->id != $feed->user_id)
        {
            $feed_user = $feed->user;
            $feed_user->notify(new NewFeedLike($user, $feed));
        }

        return response()->json(['success' => true, 'resp' => $resp]);
    }

    public function getFeedLikes(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $request->validate([
            'feed_id' => 'required'
        ]);

        $feed_id = $request->input('feed_id');
        $feed = Feed::findOrFail($feed_id);
        $likes = $feed->likes()->get();
        $user = Auth::user();

        return view('portal.social.feed.likes', compact('likes', 'user'));
    }

    public function commentFeed(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $request->validate([
            'feed_id' => 'required',
            'comment' => 'required',
        ]);

        $user = Auth::user();
        $feed_id = $request->input('feed_id');
        $feed = Feed::findOrFail($feed_id);

        $comment = $request->input('comment');
        $comment = $feed->commentAsUser($user, $comment);
        $comment->commentator = $user; 
        return response()->json(['success' => true, 'resp' => $comment]);
    }

    public function commentDelete(Request $request)
    {
        if(!$request->ajax())
            abort(404);

        $request->validate([
            'comment_id' => 'required',
        ]);

        $user_id = Auth::id();
        $comment_id = $request->input('comment_id');

        Comment::where('user_id', $user_id)->find($comment_id)->delete();
       
        return response()->json(['success' => true]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;
use DataTables;
use Akaunting\Money\Money;
use Yajra\DataTables\Html\Builder;
use Validator;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Media::query())
                    ->editColumn('action', function(Media $media) {
                        return  ModelBtn2('media',$media->id);
                    })
                    ->toJson();
        }
        $builder->columns([
                       ['data' => 'title', 'name' => 'title', 'title' => 'Title'],
                       ['data' => 'type', 'name' => 'type', 'title' => 'Type'],
                       ['data' => 'url', 'name' => 'url', 'title' => 'URL'],
                       ['data' => 'sort_order', 'name' => 'sort_order', 'title' => 'Sort Order'],
                       ['data' => 'action', 'name' => 'action', 'title' => 'Action','searchable'=>'false','orderable'=>'false'],

                   ]);
        $datatable =  $builder->parameters([
              'searchDelay' => 500,
              'stateSave' => 'true',
               'order' => [[ 0, "desc" ]]
          ]);
        
        $title = "Manage Media";
        return view('admin.media.index', compact('datatable','title'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $validator = Validator::make($request->all(), [
                        'title' => 'required',
                        'type' => 'required',
                        'url' => 'required|url',
                        'sort_order' => 'required',
                ]);

                
                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }

                if($request->input('featured') > 0)
                    Media::where('featured', 1)->update(['featured' => 0]);

                $url = $request->input('url');
                if($request->input('type') == 'soundcloud')
                {
                    if(strpos($url, "api.soundcloud.com") > -1)
                    {

                    }
                    else
                    {
                        $soundcloud_client_id = env('SOUNDCLOUD_CLIENT_ID');
                        $resp = file_get_contents('https://api.soundcloud.com/resolve.json?url='.$url.'&client_id='.$soundcloud_client_id);
                        $resp = json_decode($resp, true);
                        $url  = $resp['uri'];
                    }
                }
  
                $Media = new Media;
                $Media->title = $request->input('title');
                $Media->type = $request->input('type');
                $Media->url = $url;
                $Media->sort_order = $request->input('sort_order');
                $Media->featured = $request->input('featured');
             
                $Media->save(); 
                

                return response()->json(['success'=>'true','message'=>'Media has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $media = Media::where('id',$id)->first();
        return view('admin.media.ajax.edit', compact('media')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Media $media)
    {
        
                $validator = Validator::make($request->all(), [
                        'title' => 'required',
                        'type' => 'required',
                        'url' => 'required|url',
                        'sort_order' => 'required',
                
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->first()]);
                }

                if($request->input('featured') > 0)
                    Media::where('featured', 1)->update(['featured' => 0]);

                $url = $request->input('url');
                if($request->input('type') == 'soundcloud')
                {
                    if(strpos($url, "api.soundcloud.com") > -1)
                    {

                    }
                    else
                    {
                        $soundcloud_client_id = env('SOUNDCLOUD_CLIENT_ID');
                        $resp = file_get_contents('https://api.soundcloud.com/resolve.json?url='.$url.'&client_id='.$soundcloud_client_id);
                        $resp = json_decode($resp, true);
                        $url  = $resp['uri'];
                    }
                }
  
            
                $Media = Media::find($request->id);
                $Media->title = $request->input('title');
                $Media->type = $request->input('type');
                $Media->url = $url;
                $Media->sort_order = $request->input('sort_order');
                $Media->featured = $request->input('featured');
             
                $Media->save(); 
                

                return response()->json(['success'=>'true','message'=>'Media has been updated successfully']);
    }

    public function medialList(Request $request)
    {   
        $title = "Media";
        $youtube = Media::where('type','youtube')->orderBy('sort_order','desc')->get();
        $soundcloud = Media::where('type','soundcloud')->orderBy('sort_order','desc')->get();

        $featured = $youtube->where('featured', 1)->first();
        if($request->input('id'))
        {
            $featured = $youtube->where('id', $request->input('id'))->first();
        }

        if(!$featured)
            $featured = $youtube->first();

        return view('portal.media.index', compact('youtube','soundcloud','title', 'featured'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy(Media $media)
    {
        //
    }
}

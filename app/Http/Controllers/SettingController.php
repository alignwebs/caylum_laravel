<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Storage;
class SettingController extends Controller
{
    public function create(Request $request)
    {
        $title = "Portal Setting";
        return view('admin.setting.index', compact('title'));
    }

    public function store(Request $request)
    {
        $settings['youtube_url'] = !empty($request->youtube_url) ? $request->youtube_url : '';
        $settings['poll_code'] = !empty($request->poll_code) ? $request->poll_code : '';
        $settings['ad_link'] = !empty($request->ad_link) ? $request->ad_link : '';
        $settings['facebook'] = !empty($request->facebook) ? $request->facebook : '';
        $settings['twitter'] = !empty($request->twitter) ? $request->twitter : '';
        $settings['youtube'] = !empty($request->youtube) ? $request->youtube : '';
        $settings['sound_cloud'] = !empty($request->sound_cloud) ? $request->sound_cloud : '';
        $settings['enable_member_portal'] = !empty($request->enable_member_portal) ? $request->enable_member_portal : 0;
        $settings['notification_emails'] = !empty($request->notification_emails) ? $request->notification_emails : '';
        $settings['course_enrollment_expiry_days'] = !empty($request->course_enrollment_expiry_days) ? $request->course_enrollment_expiry_days : 0;
        $settings['course_enrollment_bank_deposit_mail_text'] = !empty($request->course_enrollment_bank_deposit_mail_text) ? $request->course_enrollment_bank_deposit_mail_text : '';

        if($request->hasFile('ad_image'))
        {
            $file = $request->file('ad_image');
            $image = Image::make($file)->widen(800, function ($constraint) {
                $constraint->upsize();
            });
            $directory = 'ad_image_uploads';
            Storage::makeDirectory($directory);
            $file_name = 'ad_image_uploads/'.time().$file->getClientOriginalName();
            $file_path = storage_path('app/public/'.$file_name);
            $image->save($file_path);
            $settings['ad_image'] =  $file_name;

        }
        
        setting($settings)->save();
        flash('Setting has been updated successfully.')->success();
        return redirect()->back();
    }
}

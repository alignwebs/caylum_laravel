<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApiDataMarket extends Model
{
    public $timestamps = false;

    public static function makertComapreData($exchange, $date=null)
    {

    		if(is_null($date))
    		{
    			$query = self::select('date')->where('exchange', $exchange)->groupBy('date')->orderBy('date', 'desc')->take(2)->get()->pluck('date');
    			if(!$query)
    				return false;

    			$dates['first'] = $query[0];
    			$dates['second'] = $query[1];
    		}
    		else
    		{
    			$query = self::select('date')->where('exchange', $exchange)->where('date','<', $date)->groupBy('date')->orderBy('date', 'desc')->first();
    			$dates['first'] = $date;
    			if($query)
    				$dates['second'] = $query->date;
    			else
    				$dates['second'] = $date;
    		}

    		$rows = DB::select("SELECT 
										curr.*,
										prev.close AS prev_close,
										CAST((curr.close - prev.close) AS DECIMAL(10,2))  AS close_change,
										CAST((((curr.close - prev.close)/prev.close)*100) AS DECIMAL(10,2))  AS close_change_percent,
										(case 
											when ((curr.close - prev.close) = 0) THEN 'nochange'
											when ((curr.close - prev.close) > 0) THEN 'green'
											when ((curr.close - prev.close) < 0) THEN 'red' END
										) AS close_change_color,
										(case 
											when ((curr.open - prev.open) = 0) THEN 'nochange'
											when ((curr.open - prev.open) > 0) THEN 'green'
											when ((curr.open - prev.open) < 0) THEN 'red' END
										) AS open_color,
										(case 
											when ((curr.high - prev.high) = 0) THEN 'nochange'
											when ((curr.high - prev.high) > 0) THEN 'green'
											when ((curr.high - prev.high) < 0) THEN 'red' END
										) AS high_color,
										(case 
											when ((curr.low - prev.low) = 0) THEN 'nochange'
											when ((curr.low - prev.low) > 0) THEN 'green'
											when ((curr.low - prev.low) < 0) THEN 'red' END
										) AS low_color,
										(case 
											when ((curr.close - prev.close) = 0) THEN 'nochange'
											when ((curr.close - prev.close) > 0) THEN 'green'
											when ((curr.close - prev.close) < 0) THEN 'red' END
										) AS close_color,
										(case 
											when ((curr.volume - prev.volume) = 0) THEN 'nochange'
											when ((curr.volume - prev.volume) > 0) THEN 'green'
											when ((curr.volume - prev.volume) < 0) THEN 'red' END
										) AS volume_color,
										(case 
											when ((curr.value - prev.value) = 0) THEN 'nochange'
											when ((curr.value - prev.value) > 0) THEN 'green'
											when ((curr.value - prev.value) < 0) THEN 'red' END
										) AS value_color
										FROM api_data_markets AS curr 
										LEFT JOIN api_data_markets AS prev ON (prev.stock = curr.stock) 
										WHERE curr.date = ? AND prev.date = ? AND curr.exchange = ? AND prev.exchange = ? 
										", [$dates['first'], $dates['second'], $exchange, $exchange]);

    		return $rows;
    }
}

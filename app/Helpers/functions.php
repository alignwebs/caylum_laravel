<?php

if (! function_exists('ModelBtn')) {

    function ModelBtn($route, $id, $title = "")
    {
        $html = '<div class="btn-group btn-options" role="group">';

        $html.= Form::open(['route' => [$route.'.destroy', $id], 'method' => 'delete', 'class' => 'deleteFrm']);
        $html.= '<a href="'.URL::route($route.'.edit',$id).'" class="btn btn-primary btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></a>';
        $html.= '<button type="submit" class="btn-delete btn btn-danger btn-sm" data-title="'.$title.'"><span class="fa fa-trash" aria-hidden="true"></span> </button>';
        $html.= Form::close();

        $html.= '</div>';
        return $html;
    }
}

// for ajax

if (! function_exists('ModelBtn2')) {

    function ModelBtn2($route, $id, $user_id = "",$slug="",$object="",$type='')
    {
        $html = '<div class="btn-group btn-options" role="group">';
        
        $html.= Form::open(['route' => [$route.'.destroy', $id], 'method' => 'delete', 'class' => 'deleteFrmAjax']);
        if($route != 'payslip'){

         $html.= '<button type="button" onclick="editAjax(\''.route($route.'.edit',$id).'\',\''.$type.'\')" class="btn btn-primary btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></button>&nbsp;';

         if($route == 'enrollment')
            $html.= '<a  href=\''.route($route.'.show',$id).'\' class="btn btn-warning btn-sm"><span class="fa fa-eye" aria-hidden="true"></span></a>&nbsp;';

        }else
        $html.= '<a href="'.asset('storage/'.$object->attachment->file).'" target="_blank" class="btn btn-success btn-sm"><span class="fa fa-eye" aria-hidden="true"></span></a>&nbsp;';

        if(Auth::id() != $user_id)
        {
        $html.= '<button type="submit" class="btn-delete btn btn-danger btn-sm"><span class="fa fa-trash" aria-hidden="true"></span></button>&nbsp;&nbsp;';
        }
        $html.= Form::close();
        
        if($route == 'course'){
        $html.='<button onclick="copy(\''.route('enroll.course',$slug).'\')" class="btn btn-sm btn-info"><span class="fa fa-copy" aria-hidden="true"></span> Copy Link</button>&nbsp;';
  
            if($object->trashed()){
                $html.= Form::open(['route' => [$route.'.restore'],'style'=>'display:inline']);
                $html.= '<input type="hidden" name="slug" value="'.$slug.'">';
                $html.= '<button type="submit" class="btn-delete btn btn-success btn-sm" title="Restore"><span class="fa fa-undo" aria-hidden="true"></span></button>';
                $html.= Form::close();
            }
        }


        $html.= '</div>';
        return $html;
    }
}

if (! function_exists('ModelBtnDelete')) {

    function ModelBtnDelete($route, $id)
    {
        $html = '<div class="btn-group btn-options" role="group">';
        
        $html.= Form::open(['route' => [$route.'.destroy', $id], 'method' => 'delete', 'class' => 'deleteFrmAjax']);
      
        $html.= '<button type="submit" class="btn-delete btn btn-danger btn-sm"><span class="fa fa-trash" aria-hidden="true"></span> </button>&nbsp;&nbsp;';

        $html.= Form::close();

        $html.= '</div>';
        return $html;
    }
}

if(!function_exists('intToBool'))
{
    function intToBool($int,$type='')
    {   
        switch ($type) {
            case 'payslip':
                return ($int ? 'Accept' : 'Reject');
                break;
            
            default:
                return ($int ? 'True' : 'False');
                break;
        }
        
    }
}

if(!function_exists('status'))
{
    function status($status)
    {
        return ($status ? 'Active' : 'Inactive');
    }
}

if(!function_exists('binary'))
{
    function binary()
    {
        return $binary = [

                1 => 'Yes',
                0 => 'No',
            ];
    }
}

if(!function_exists('is_private'))
{
    function is_private($is_private)
    {
        return ($is_private ? 'Yes' : 'No');
    }
}

if(!function_exists('payment_mode'))
{
    function payment_mode()
    {
        return $payment_mode = [

                'cash' => 'Cash',
                'Online'=> 'Online',
            ];
    }
}

if(!function_exists('getRoles'))
{
    function getRoles()
    {
        $roles = array(

            'admin'    => 'Admin',
            'sub_admin' => 'Sub Admin'
        );
        return $roles;
    }

}

if(!function_exists('years'))
{
    function years()
    {
        $years = array_combine(range(date("Y"), 2013), range(date("Y"), 2013));
        return $years;
    }
}

if(!function_exists('makeCurrencyFormat'))
{
    function makeCurrencyFormat(float $amount, string $currency_symbol=null)
    {
        if(is_null($currency_symbol))
            $currency_symbol = setting('currency');

        return money($amount, strtoupper($currency_symbol), true);
    }
}

if(!function_exists('makeCurl'))
{
    function makeCurlPost($url, $fields=[], $token=null, $method="POST")
    {
        $authorization = null;
        if(!is_null($token))
            $authorization = "Authorization: Bearer $token";
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'ALIGNWEBS',
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array( $authorization ),
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        return $resp;
    }
}

if(!function_exists('createWpNewsUser'))
{
    function createWpNewsUser(Illuminate\Http\Request $request, \App\User $user)
    {
        $wp_url         = setting('news_website_url');
        $wp_token       = setting('news_website_token');
        $api_url        = $wp_url."/wp-json/wp/v2/users";

        $data['username']   =   $user->email;
        $data['email']      =   $user->email;
        $data['first_name'] =   $user->first_name;
        $data['last_name']  =   $user->last_name;
        $data['nickname']   =   $user->name;
        $data['password']   =   $request->input('password');

        $resp = makeCurlPost($api_url, $data, $wp_token);

        \Log::info($resp);
        return $resp;
    }
}

if(!function_exists('deleteWpNewsUser'))
{
    function deleteWpNewsUser(\App\User $user)
    {
        $wp_url         = setting('news_website_url');
        $wp_token       = setting('news_website_token');
        $api_url        = $wp_url."/wp-json/wp/v2/users/".$user->wp_news_user_id;

        $data['force']      =   true;
        $data['reassign']   =   -1;
        
        $resp = makeCurlPost($api_url, $data, $wp_token);
        \Log::info($resp);
        return $resp;
    }
}

if(!function_exists('getWpNewsLoginUrl'))
{
    function getWpNewsLoginUrl()
    {
        return setting('news_website_url')."/wp-login.php?autologin=true&username=".\Auth::user()->email."&password=".\Auth::user()->wp_news_password;
    }
}

if(!function_exists('gender'))
{
    function gender()
    {
        return $gender = [

                'male' => 'Male',
                'female'=> 'Female',
            ];
    }
}

if(!function_exists('get_image_path'))
{
    function get_image_path($image)
    {
        if(empty($image))
            return asset('theme/images/noimg.jpg');
        else
            return asset('storage/'.$image);
    }
}

if(!function_exists('get_user_profile_pic'))
{
    function get_user_profile_pic($user, $thumbnail=false)
    {
        if(empty($user->profile_pic)){

            if($user->gender == 'male')
                return asset('theme/images/male.png');
            else
                return asset('theme/images/female.png');
        }
        else
        {
            if($thumbnail)
                return asset('storage/'.$user->thumbnail);

            return asset('storage/'.$user->profile_pic);
        }
    }
}

if(!function_exists('fileIsImage'))
{
    function fileIsImage($mime)
    {
        $mimes = ['image/png','image/jpg','image/jpeg','image/bmp'];

        return in_array($mime, $mimes);
    }
}

if(!function_exists('getFileExtensionByMime'))
{
    function getFileExtensionByMime($mime)
    {
        switch($mime)
        {
            case 'image/png':
            $ext = '.png';
            break;
            case 'image/jpg':
            $ext = '.jpg';
            break;
            case 'image/jpeg':
            $ext = '.jpg';
            break;
            case 'image/bmp':
            $ext = '.bmp';
            break;
            default:
            $ext = '';
            break;

        }
        
        return $ext;
    }
}

if(!function_exists('getFileExtension'))
{
    function getFileExtension($filename, $mime=null)
    {
        $ext = basename($filename);
        $ext = explode('.', $ext);
        $ext = end($ext);

        if(!is_null($mime))
            $ext = getFileExtensionByMime($mime);

        return $ext;
    }
}


if(!function_exists('removeSpaceFromString'))
{
    function removeSpaceFromString($username){
        return str_replace(" ","",$username);
    }
}

if(!function_exists('getUserChannel'))
{
    function getUserChannel($user_id){
        return "user.$user_id";
    }
}

if(!function_exists('analysisPagesCombo'))
{
    function analysisPagesCombo()
    {
        return $analysisPagesCombo = [
                route('portal.watchlist.index') => 'My Watchlist',
                route('portal.analysis.watchlist.index') => 'Caylum Watchlist',
                route('portal.historical.watchlist') => 'Historical Watchlist',
                route('portal.consensus') => 'Consensus',
                route('portal.btm.stock') => 'BTM Stocks',
                route('portal.dividend') => 'Dividends',
                route('portal.company.buyback') => 'Company Buyback',
                route('portal.insider.transaction') => 'Insider Transactions',
                route('portal.insider.transaction.history') => 'Historical Insider Transactions',
                route('portal.ph.data') => 'PH Data'
            ];
    }
}

if(!function_exists('formatted_money'))
{
    function formatted_money($money)
    {
        return \Akaunting\Money\Money::PHP($money,true)->format();  
    }
}

if(!function_exists('textWrap')){

    function textWrap($str){
        
        return wordwrap($str,15);
    }
}

if(!function_exists('getYoutubeId')){

    function getYoutubeId($url)
    {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        return $my_array_of_vars['v'];    
    }
}

if(!function_exists('getCountryFlag')){

    function getCountryFlag($country_code, $size = 16)
    {
        $country_code = strtolower($country_code);
        return "https://www.countryflags.io/$country_code/flat/$size.png";
    }
}
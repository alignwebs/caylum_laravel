<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
class Subscriber extends Model
{
	protected $revisionEnabled = true;
	protected $dates = ['expires_at'];

    public function user()
       {
           return $this->belongsTo('App\User','user_id');
       }

    public function subscription()
       {
           return $this->belongsTo('App\Subscription','subscription_id');
       }

      public function isExpired()
     {
         if (is_null($this->expires_at)) {
             return true;
         }
         if ($this->expires_at->isPast() and !$this->expires_at->isToday()) {
             return true;
         }
         return false;
     }
  



}

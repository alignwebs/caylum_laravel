<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\SoftDeletes;
use Akaunting\Money\Money;
class Subscription extends Model
{
    use HasSlug;
    use SoftDeletes;
    protected $revisionEnabled = true;
    protected $appends = ['complete_package','subscribers_count','formatted_money'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function getCompletePackageAttribute()
    {
        if($this->days_duration > 1)
            $day = " days";
        else
            $day = " day";

        return $this->name." - ".makeCurrencyFormat($this->fee)." - ".$this->days_duration.$day;
    }

    public static function scopeDefault($query)
    {
        return $query->where('default',1);
    }

    public function subscribers()
    {
        return $this->hasMany('App\Subscriber');   
    }

    public function getSubscribersCountAttribute()
    {
        return $this->subscribers->count();   
    }

    public static function scopeActive($query)
    {
        return $query->where('active',1);
    }

    public function getFormattedMoneyAttribute()
    {
        return formatted_money($this->fee);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $appends = ['full_stock_name', 'full_stock_market_name'];
    public function getFullStockNameAttribute()
    {
        	return $this->symbol . ' - ' . $this->name;
    }

    public function getFullStockMarketNameAttribute()
    {
	    	if($this->relationLoaded('market'))
	    	{
	        return $this->market->symbol. ': '. $this->symbol . ' (' . $this->name . ')';
	    	}
    }

    public function market()
    {
    		return $this->belongsTo(Market::class);
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Stock;

class StockSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stocks:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync stocks table via api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api_resp = file_get_contents('http://phisix-api2.appspot.com/stocks.json');

        if($api_resp)
        {
            $api_resp = json_decode($api_resp, true);
            $stocks = $api_resp['stock'];

            $data = [];

            foreach($stocks as $stock)
            {
                $data[] = array('market_id'=>1, 'symbol'=>$stock['symbol'], 'name'=>$stock['name']);
            }

            Stock::truncate();

            Stock::insert($data);

            print count($data)." stocks synced successfully!";
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class WpNewsAuthenticate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wpnews:authenticate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate WP API JWT for API Request';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $wp_url         = setting('news_website_url');
        $api_url        = $wp_url."/wp-json/jwt-auth/v1/token";
        
        $data['username']    = setting('news_website_username');
        $data['password']    = setting('news_website_password');
        
            // $this->info(json_encode($data));
            // return;
        try {
            $resp = makeCurlPost($api_url, $data);
            $resp = json_decode($resp, true);

            $token = "";
            if(isset($resp['token']))
                setting(['news_website_token' => $resp['token']])->save();
            
            print_r($resp);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

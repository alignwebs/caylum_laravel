<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class CreateAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adminuser:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates super admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = "info@cayluminstitute.com";
        $password = "UkV2DDV46EijJF0t9iyq";

        $user = new User;
        $user->first_name = "Administrator";
        $user->last_name = null;
        $user->username = "admin";
        $user->mobile = "9876543210";
        $user->email = $email; // replace admin with your own custom email
        $user->password = \Hash::make($password); // replace admin with your custom password 
        $user->active = 1;
        $user->save();

        $user->assignRole('admin');

        print "Admin user created: \nEmail: {$email} \nPassword: {$password} ";
    }
}

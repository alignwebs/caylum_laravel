<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Curl;
use League\Csv\Reader;
use App\ApiDataMarket;

class SyncPseApiMarketData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pseapi:market {date : yyyy-mm-dd}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync market data from pseapi.com ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->argument('date');
        $dt = Carbon::parse($date);
        $exchange = 'pse';

        if(!$dt)
        {
            print "Invalid Date";
            exit;
        }

        $response = Curl::to('http://pseapi.com/api/Base')
                    ->withData( array( 'date' => $dt->format('m-d-Y') ) )
                    ->post();
        
        $response = json_decode($response, true);

        if(!$response['success'])
        {
            print "Couldnt find any data";
            exit;
        }

        $csv = $response['csv'];
        $csv = "stock,date,open,high,low,close,volume,value\n".$csv;
       
        $reader = Reader::createFromString($csv);
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();

        $rows = [];
        $category = null;
        foreach ($records as $offset => $record) {
            $record['date'] = Carbon::createFromFormat('m/d/Y', $record['date'])->toDateString();
            $record['exchange'] = $exchange;

            $record['is_category'] = false;
            if(strpos($record['stock'], '^') > -1){
                $record['is_category'] = true;
                $category = str_replace('^', '', $record['stock']);
                $record['stock'] = $category;
            }

            $record['category'] = $category;

            $rows[] = $record;
        }

        ApiDataMarket::where('exchange', $exchange)->where('date', $date)->delete();

        ApiDataMarket::insert($rows);

        print "$exchange exchange data for $date synced successfully.";
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InitSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initiate default app settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $settings['currency'] = 'PHP';
        $settings['news_website_url'] = 'https://news.caylum.com';
        $settings['news_website_username'] = 'caylum';
        $settings['news_website_password'] = 'ESVdqnK%knD6IrzTN*ZEy#6A';
        $settings['news_website_token'] = '';
        
        setting($settings)->save();
    }
}

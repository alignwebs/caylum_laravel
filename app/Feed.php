<?php

namespace App;

// use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use BeyondCode\Comments\Traits\HasComments;
use Auth;

class Feed extends Model
{
	 use CanBeLiked, HasComments;
	 
    public $appends = ['posted_at', 'feed_content_html', 'likes_count', 'is_liked'];

    public function attachment()
    {
        return $this->morphOne('App\Attachment', 'attachmentable');
    }

    public function comment()
    {
        return $this->morphOne('App\Comment', 'commentable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getLikesCountAttribute()
    {
        return $this->likers()->count();
    }

    public function getIsLikedAttribute()
    {
        if(Auth::check())
            return $this->isLikedBy(Auth::id());

        return false;
    }

    public function likes()
    {
        return $this->likers();
    }

    public function getFeedContentHtmlAttribute()
    {
    		$html = nl2br($this->feed_content);
    		$url = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';   
			$html= preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $html);
	
    		return $html;
    }

    public function getPostedAtAttribute()
    {
    		return \Carbon\Carbon::parse($this->updated_at)->diffForHumans();
    }

    public static function getAllLikes($user)
    {
        $likes =  $user->likes(Feed::class)->get();
        $likes = collect($likes)->map(function($row) {
            return $row->likes_count;
        })->toArray();
        return array_sum($likes);  
    }

        
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradePortfolioCategory extends Model
{
    public function trade_portfolio_subcategory()
        {
               return $this->hasMany(TradePortfolioSubcategory::class);
        }
}

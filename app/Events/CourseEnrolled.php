<?php

namespace App\Events;

use App\CourseEnrollment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CourseEnrolled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $enrollment;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CourseEnrollment $courseEnrollment)
    {
        $this->enrollment = $courseEnrollment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\UserScope;

class StockWatchlist extends Model
{
   	protected $table = 'watchlist_stocks';
	
	protected $fillable = ['user_id', 'watchlist_id', 'stock_symbol', 'stock_name'];

	protected static function boot()
	{
	   parent::boot();
	   static::addGlobalScope(new UserScope);
	}

	public static function getListNames($user_id)
	{
		return self::select('watchlist_id')->where('user_id', $user_id)->groupBy('watchlist_id')->orderBy('watchlist_id')->get();
	}

	public function stockalerts()
	{
		return $this->hasMany(StockAlert::class, 'stock_symbol', 'stock_symbol');
	}
}
	
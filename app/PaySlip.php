<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaySlip extends Model
{
        public function attachment()
        {
            return $this->morphOne('App\Attachment', 'attachmentable');
        }

        public function enrollment()
        {
            return $this->belongsTo('App\CourseEnrollment','ref_id');
        }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PaySlip;

class PaySlipUploaded extends Mailable
{
    use Queueable, SerializesModels;

    public $payslip;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PaySlip $payslip, $subject)
    {
        $this->payslip = $payslip;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $file = public_path('storage/'.$this->payslip->attachment->file);
        return $this->markdown('emails.payslip')->attach($file)->subject($this->subject);
    }
}

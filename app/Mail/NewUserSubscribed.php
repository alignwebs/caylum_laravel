<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Subscriber;

class NewUserSubscribed extends Mailable
{
    use Queueable, SerializesModels;

    public $subscriber;
    
    public $tries = 2;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscriber = $this->subscriber;
        return $this->markdown('emails.subscription.newsubscriber', compact('subscriber'))
                    ->subject("New Subscriber Added");
    }
}

<?php

namespace App\Mail;
use App\CourseEnrollment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourseEnrolled extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $courseEnrollment;
    
    public $tries = 2;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CourseEnrollment $courseEnrollment)
    {
        $this->courseEnrollment = $courseEnrollment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $courseEnrollment = $this->courseEnrollment;
        return $this->markdown('emails.course.enroll', compact('courseEnrollment'))
                    ->subject("Enrollment Successful #".$courseEnrollment->id. " - " . $courseEnrollment->course->name);
    }
}

<?php

namespace App\Mail;

use App\CourseEnrollment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CourseEnrolledAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $courseEnrollment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CourseEnrollment $courseEnrollment)
    {
        $this->courseEnrollment = $courseEnrollment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $courseEnrollment = $this->courseEnrollment;
        return $this->markdown('emails.course.enrolladmin', compact('courseEnrollment'))
                    ->subject("New Enrollment - ".$courseEnrollment->course->name." #".$courseEnrollment->id);
    }
}

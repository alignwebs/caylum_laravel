<?php

namespace App\Mail;
use App\CourseEnrollment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BankDeposit extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $CourseEnrollment;
    public $tries = 2;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CourseEnrollment $CourseEnrollment)
    {
        $this->CourseEnrollment = $CourseEnrollment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $courseEnrollment = $this->CourseEnrollment;
        
        $course_enrollment_expiry_days = setting('course_enrollment_expiry_days');
        $course_enrollment_bank_deposit_mail_text = setting('course_enrollment_bank_deposit_mail_text');

        $course_expiry_date = $courseEnrollment->created_at->addDays($course_enrollment_expiry_days);
        $mail_text = str_replace("[EXPIRY_DATE]", $course_expiry_date->toFormattedDateString(), $course_enrollment_bank_deposit_mail_text);

        return $this->view('emails.bank_deposit', compact('courseEnrollment', 'mail_text', 'course_enrollment_expiry_days'));
    }
}

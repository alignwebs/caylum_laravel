<?php

namespace App\Mail;

use App\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Subscribed extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $subscriber;
    public $password;

    public $tries = 2;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Subscriber $subscriber, $password = "****")
    {
        $this->subscriber = $subscriber;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscription = $this->subscriber->subscription;
        $subscriber = $this->subscriber;
        $password = $this->password;
        return $this->markdown('emails.subscription.subscribed', compact('subscriber', 'password'))
                    ->subject($subscription->name . ' subscribed successfully.');
    }
}

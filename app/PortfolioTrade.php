<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioTrade extends Model
{
    public function market()
    {
        return $this->belongsTo(Market::class);
    }

    public function attachments()
        {
               return $this->morphMany('App\Attachment', 'attachmentable');
        }
}

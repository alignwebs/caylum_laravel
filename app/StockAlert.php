<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\UserScope;

class StockAlert extends Model
{
    //protected $fillable = ['user_id','stock_symbol', 'type', 'value', 'alerted'];

	protected static function boot()
	{
	   parent::boot();
	   static::addGlobalScope(new UserScope);
	}

	public function stock()
	{
		return $this->belongsTo(Stock::class, 'stock_symbol', 'symbol');
	}
}

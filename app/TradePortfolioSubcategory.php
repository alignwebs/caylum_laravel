<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradePortfolioSubcategory extends Model
{
        public function trade_portfolio_category()
        {
            return $this->belongsTo(TradePortfolioCategory::class);
        }
}

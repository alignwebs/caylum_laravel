<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
class PaymentMode extends Model
{
    protected $revisionEnabled = true;
}

<?php

namespace App\Notifications;

use App\User;
use App\Feed;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewFeedLike extends Notification
{
    use Queueable;

    public $liked_user;
    public $feed;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Feed $feed)
    {
        $this->liked_user = $user;
        $this->feed = $feed;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    public function toArray($notifiable)
    {
        return [
            'feed_id' => $this->feed->id,
            'liked_user_id' => $this->liked_user->id,
            'first_name' => $this->liked_user->first_name,
            'last_name' => $this->liked_user->last_name,
            'avatar' => get_user_profile_pic($this->liked_user, true),
            'text' => $this->liked_user->name." liked your post.",
        ];
    }
}

<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewUserFollower extends Notification
{
    use Queueable;

    public $follower_user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->follower_user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    public function toArray($notifiable)
    {
        return [
            'follower_user_id' => $this->follower_user->id,
            'first_name' => $this->follower_user->first_name,
            'last_name' => $this->follower_user->last_name,
            'avatar' => get_user_profile_pic($this->follower_user, true),
            'text' => $this->follower_user->name." started following you.",
        ];
    }

    // public function toBroadcast($notifiable)
    // {
    //     return new BroadcastMessage([
    //         'text' => $this->follower_user->name." started following you.",
    //     ]);
    // }
}

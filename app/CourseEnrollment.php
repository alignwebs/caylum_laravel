<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Akaunting\Money\Money;
class CourseEnrollment extends Model
{
    
    protected $appends = ['formatted_money'];
    protected $casts = ['course_details' => 'object'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function course()
    {
    	return $this->belongsTo('App\Course','course_id');
    }

    public function payment()
    {
        return $this->hasMany('App\Payment','course_enrollment_id');
    }

    public function payment2()
    {
        return $this->belongsTo('App\Payment');
    }

    public function getFormattedMoneyAttribute()
    {
        if(!empty($this->payment2->amount))
            return  formatted_money($this->payment2->amount);
        else
            return "";
    }
   
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
class PaymentType extends Model
{
    protected $revisionEnabled = true;
}

<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class PortfolioTransaction extends Model
{
    public function attachment()
    {
        return $this->morphOne('App\Attachment', 'attachmentable');
    }
    
    public function stock()
    {
        return $this->belongsTo('App\Stock','stock_id');
    }

    public function market()
    {
        return $this->belongsTo(Market::class);
    }

    public function portfolio_trade()
    {
        return $this->belongsTo(PortfolioTrade::class);
    }

    public static function calculateBalance($type,$amount,$user_id)
    {
        $user = User::find($user_id);
        $trade_portfolio_balance = $user->trade_portfolio_balance;
        
        if($type == 'deposit' || $type == 'sell')
            $trade_portfolio_balance = $trade_portfolio_balance + $amount;

        if($type == 'withdraw' || $type == 'buy')
            $trade_portfolio_balance = $trade_portfolio_balance - $amount;
        
        $user->trade_portfolio_balance = $trade_portfolio_balance;
        $user->save();
    }
}

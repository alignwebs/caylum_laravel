<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisInsiderTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_insider_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock');
            $table->integer('no_of_shares')->nullable();
            $table->double('price',8,2)->nullable();
            $table->double('total',8,2)->nullable();
            $table->string('ad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_insider_transactions');
    }
}

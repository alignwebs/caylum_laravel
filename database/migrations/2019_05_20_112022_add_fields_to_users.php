<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->nullable()->after('id');
            $table->string('last_name')->nullable()->after('first_name');
            $table->boolean('alumini_member')->default(0)->after('last_name');
            $table->string('alumini_course')->nullable()->after('alumini_member');
            $table->string('alumini_attend')->nullable()->after('alumini_course');
            $table->integer('alumini_subscription_id')->nullable()->after('alumini_attend');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('alumini_member');
            $table->dropColumn('alumini_course');
            $table->dropColumn('alumini_attend');
            $table->dropColumn('alumini_subscription_id');
        });
    }
}

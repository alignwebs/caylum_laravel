<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisHistoricalWatchlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_historical_watchlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date_added')->index();
            $table->string('stock');
            $table->double('price_added',8,2);
            $table->date('date_deleted')->nullable();
            $table->double('price_deleted')->nullable();
            $table->float('return')->nullable();
            $table->string('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_historical_watchlists');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisBtmStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_btm_stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name');
            $table->string('category');
            $table->float('market_cap');
            $table->longText('narrative')->nullable();
            $table->float('last_price')->nullable();
            $table->float('tp')->nullable();
            $table->float('fy19')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_btm_stocks');
    }
}

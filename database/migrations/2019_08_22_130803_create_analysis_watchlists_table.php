<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisWatchlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_watchlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock');
            $table->double('price_added',8,2)->nullable();
            $table->string('category')->nullable();
            $table->date('date_added')->nullable();;
            $table->longText('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_watchlists');
    }
}

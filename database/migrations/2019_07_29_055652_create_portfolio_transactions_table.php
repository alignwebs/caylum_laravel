<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('portfolio_trade_id')->nullable()->index();
            $table->integer('market_id');
            $table->integer('user_id');
            $table->date('date');
            $table->string('type');
            $table->string('stock')->nullable();
            $table->integer('stock_qty')->nullable();
            $table->double('per_stock_rate', 8, 2)->nullable();
            $table->float('fee_percentage')->nullable();
            $table->double('fee', 8, 2)->nullable();
            $table->double('amount',8,2);
            $table->string('comments')->nullable();
            $table->timestamps();
        });
        \DB::statement('ALTER TABLE portfolio_transactions AUTO_INCREMENT = 10000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_transactions');
    }
}

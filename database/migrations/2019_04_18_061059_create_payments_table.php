<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->integer('payment_mode_id');
            $table->double('amount');
            $table->string('txn_id')->nullable();
            $table->string('transaction');
            $table->integer('payment_type_id');
            $table->string('payer_name')->nullable();
            $table->string('payer_email')->nullable();
            $table->string('payer_mobile')->nullable();
            $table->integer('course_enrollment_id')->nullable();
            $table->date('date');
            $table->string('remark')->nullable();
            $table->longText('callback_response')->nullable();
            $table->timestamps();
        });

        DB::statement('ALTER TABLE payments AUTO_INCREMENT = 100000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

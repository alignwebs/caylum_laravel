<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_trades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('market_id');
            $table->string('trade_name');
            $table->string('portfolio_category');
            $table->string('portfolio_subcategory')->nullable();
            $table->string('comments')->nullable();
            $table->boolean('is_private')->default(0);
            $table->timestamps();
            $table->index(['user_id', 'market_id']);
        });
        \DB::statement('ALTER TABLE portfolio_trades AUTO_INCREMENT = 10000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_trades');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToCourseEnrollments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->string('last_name')->nullable()->after('custom_form');
            $table->string('first_name')->nullable()->after('last_name');
            $table->string('mobile')->nullable()->after('first_name');
            $table->string('email')->nullable()->after('mobile');
            $table->date('birthday')->nullable()->after('email');
            $table->string('gender', 20)->nullable()->after('birthday');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->dropColumn('first_name');
            $table->dropColumn('mobile');
            $table->dropColumn('email');
            $table->dropColumn('birthday');
            $table->dropColumn('gender');
        });
    }
}

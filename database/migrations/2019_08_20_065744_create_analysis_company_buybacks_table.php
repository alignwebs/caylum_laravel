<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisCompanyBuybacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_company_buybacks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock_code');
            $table->double('buyback_program',8,2)->nullable();
            $table->double('amount_left',8,2)->nullable();
            $table->double('total_amount',8,2)->nullable();
            $table->double('repurchased_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_company_buybacks');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisDividendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_dividends', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type_of_dividends');
            $table->date('date_closed')->index();
            $table->string('stock');
            $table->double('amount',8,2)->nullable();
            $table->date('ex_date')->nullable();
            $table->date('record_date')->nullable();
            $table->date('payment_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_dividends');
    }
}

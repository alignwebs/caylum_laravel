<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiDataMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_data_markets', function (Blueprint $table) {
            $table->string('exchange', 50);
            $table->date('date');
            $table->string('stock');
            $table->string('category')->nullable();
            $table->boolean('is_category')->default(0);
            $table->double('open');
            $table->double('high');
            $table->double('low');
            $table->double('close');
            $table->double('volume');
            $table->double('value');

            $table->index(['exchange', 'date', 'stock', 'category']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_data_markets');
    }
}

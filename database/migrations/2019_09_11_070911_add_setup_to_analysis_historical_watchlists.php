<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSetupToAnalysisHistoricalWatchlists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analysis_historical_watchlists', function (Blueprint $table) {
           $table->string('setup')->nullable()->after('price_added');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analysis_historical_watchlists', function (Blueprint $table) {
             $table->dropColumn('setup');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWatchlistStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('watchlist_stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('market_id')->default(1);
            $table->integer('watchlist_id');
            $table->string('stock_symbol', 10);
            $table->string('stock_name');
            $table->timestamps();
         
            $table->index(['user_id', 'watchlist_id', 'market_id', 'stock_symbol'], 'watchlist_stocks_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('watchlist_stocks');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisConsensusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_consensuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticker');
            $table->float('market_cap');
            $table->double('highest_target_price',8,2);
            $table->float('sales_4Q18')->nullable();
            $table->float('sales_1Q19')->nullable();
            $table->float('net_income_4Q18')->nullable();
            $table->float('net_income_1Q19')->nullable();
            $table->float('pe')->nullable();
            $table->float('roe')->nullable();
            $table->float('div_yield')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_consensuses');
    }
}

<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'guard_name' => 'web',
                'created_at' => '2019-04-15 08:56:47',
                'updated_at' => '2019-04-15 08:56:47',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'sub_admin',
                'guard_name' => 'web',
                'created_at' => '2019-04-15 08:57:06',
                'updated_at' => '2019-04-15 08:57:06',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'user',
                'guard_name' => 'web',
                'created_at' => '2019-04-15 08:57:40',
                'updated_at' => '2019-04-15 08:57:40',
            ),

        ));
        
        
    }
}
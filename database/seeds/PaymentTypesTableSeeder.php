<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_types')->delete();
        
        \DB::table('payment_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Course Fee',
                'created_at' => '2019-04-18 00:00:00',
                'updated_at' => '2019-04-18 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Subscription Fee',
                'created_at' => '2019-04-18 00:00:00',
                'updated_at' => '2019-04-18 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Others',
                'created_at' => '2019-04-18 00:00:00',
                'updated_at' => '2019-04-18 00:00:00',
            ),
        ));
        
        
    }
}
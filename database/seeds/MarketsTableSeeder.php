<?php

use Illuminate\Database\Seeder;

class MarketsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('markets')->delete();
        
        \DB::table('markets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'symbol' => 'pse',
                'name' => 'Philippines Stock Exchange',
                'created_at' => '2019-07-29 00:00:00',
                'updated_at' => '2019-07-29 00:00:00',
            ),
        ));
        
        
    }
}
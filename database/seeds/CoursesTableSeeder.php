<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('courses')->delete();
        
        \DB::table('courses')->insert(array (
            0 => 
            array (
                'id' => 4,
                'slug' => 'executive-course',
                'name' => 'Executive Course',
                'description' => NULL,
                'price' => 1000.0,
                'is_paid' => 0,
                'subscribe_default' => 0,
                'created_at' => '2019-05-03 15:37:29',
                'updated_at' => '2019-05-07 13:54:51',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5,
                'slug' => 'introductory-seminar',
                'name' => 'Introductory Seminar',
                'description' => NULL,
                'price' => 250.0,
                'is_paid' => 1,
                'subscribe_default' => 1,
                'created_at' => '2019-05-03 15:38:12',
                'updated_at' => '2019-05-03 15:38:12',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 6,
                'slug' => 'recruitment-program',
                'name' => 'Recruitment Program',
                'description' => NULL,
                'price' => 500.0,
                'is_paid' => 1,
                'subscribe_default' => 0,
                'created_at' => '2019-05-03 15:38:48',
                'updated_at' => '2019-05-03 15:38:48',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
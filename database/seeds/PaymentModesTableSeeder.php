<?php

use Illuminate\Database\Seeder;

class PaymentModesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_modes')->delete();
        
        \DB::table('payment_modes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Paypal',
                'created_at' => '2019-04-18 00:00:00',
                'updated_at' => '2019-04-18 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Offline Payment',
                'created_at' => '2019-04-18 00:00:00',
                'updated_at' => '2019-04-18 00:00:00',
            ),
        ));
        
        
    }
}
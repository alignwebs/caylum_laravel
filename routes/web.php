<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','/login');
Route::redirect('/home','/portal/dashboard');

Route::get('/test','TestController@index');

//Auth::routes(['verify' => true]);
Auth::routes();

Broadcast::routes();
/* PUBLIC ROUTES */
Route::get('/user/logout','UserController@logout');
Route::get('/email','CourseEnrollmentController@email')->name('email');
Route::get('/enroll/course/{slug}','CourseEnrollmentController@enrollCourse')->name('enroll.course');
Route::post('/enroll/course/','CourseEnrollmentController@enrollCourseStore')->name('enroll.course.store');

Route::get('/payslip/', 'PaySlipController@createPayslip')->name('payslip.createPayslip');
Route::post('/payslip/store/','PaySlipController@storePaySlip')->name('payslip.storePaySlip');

Route::post('/execute/course/paypal/','PaymentController@executeCoursePayment')->name('execute.course.payment');
	
Route::get('/enrollement/success/{paymentID?}', 'PaymentController@enrollmentSuccess')->name('enrollment-pay-success');
Route::get('/enrollement/failed/{paymentID?}', 'PaymentController@enrollmentFailed')->name('enrollment-pay-failed');

Route::post('user/create','UserController@register')->name('user.register');

Route::group(['prefix' => 'portal'], function () {
       Route::get('portfolio/{market}/trade/{id}','PortfolioTradeController@tradeDetail')->name('portal.portfolio.trade.detail');
});


/* 	PORTAL ROUTES */
Route::group(['middleware' => ['auth','IsUserSubscribed'], 'prefix'=>'portal'], function () {
	Route::get('/', function () {
		return redirect('portal/dashboard');
	});

	Route::get('/search', 'HomeController@viewSearchPage')->name('portal.search');

	Route::post('/userid', function () { return response()->json(['user_id' => \Auth::id()]); }); /* Get logged in user id */

	Route::get('/dashboard', 'HomeController@userIndex')->name('portal.home');

	// chatroom
	Route::get('/chatroom', 'HomeController@chatroom')->name('portal.chatroom.index');
	
	Route::resource('/feed', 'FeedController');
	Route::group(['prefix' => '/feed'], function () {
		Route::get('/ajax/social/feed','FeedController@generateUserFeed')->name('portal.feed.ajax.socialfeed');
		Route::get('/ajax/user/{user_id}/feed','FeedController@getUserFeed')->name('portal.feed.ajax.userfeed');
		Route::get('/ajax/social/feed/search','FeedController@searchFeed')->name('portal.feed.ajax.search');
		Route::post('/ajax/like','FeedController@likeFeed')->name('portal.feed.ajax.like.post');
		Route::get('/ajax/likes','FeedController@getFeedLikes')->name('portal.feed.ajax.likes');
		Route::post('/ajax/comment','FeedController@commentFeed')->name('portal.feed.ajax.comment.post');
		Route::post('/ajax/comment/delete','FeedController@commentDelete')->name('portal.feed.ajax.comment.delete');
		Route::post('/ajax/delete','FeedController@destroy')->name('portal.feed.ajax.delete');
	});
	// USER PROFILE
	Route::group(['prefix' => 'profile'], function () {
		Route::get('/{username}','SocialController@profile')->name('portal.user.profile');
		Route::get('/{username}/edit','SocialController@editProfile')->name('portal.user.profile.edit');
		Route::post('/update','SocialController@updateProfile')->name('portal.user.profile.update');
		Route::post('/toggleFollow','SocialController@toggleFollow')->name('portal.user.profile.toggleFollow');
		Route::get('/{username}/followers','SocialController@followers')->name('portal.user.profile.followers');
		Route::get('/{username}/following','SocialController@following')->name('portal.user.profile.following');
		Route::get('/{username}/photos','SocialController@photos')->name('portal.user.profile.photos');
	});
	

	// TRADE PORTFOLIO
	Route::group(['prefix' => 'portfolio'], function () {

		Route::get('/profile','PortfolioController@index')->name('portal.portfolio.profile');
       	Route::get('/{market}/trades','PortfolioTradeController@index')->name('portal.portfolio.trades');
		Route::post('/{market}/trade/add','PortfolioTradeController@add')->name('portal.portfolio.trade.add');
		Route::get('/trade/edit/{id}','PortfolioTradeController@edit')->name('portal.portfolio.trade.edit');
		Route::post('/trade/update/{id}','PortfolioTradeController@update')->name('portal.portfolio.trade.update');
		Route::delete('/trade//delete/{id}', 'PortfolioTradeController@destroy')->name('portal.portfolio.trade.destroy');

		
		Route::get('/trade/transaction/create/{id}','PortfolioTradeController@createTransaction')->name('portal.portfolio.trade.transaction.create');
		Route::post('/trade/transaction/add/','PortfolioTradeController@addTransaction')->name('portal.portfolio.trade.transaction.add');

		Route::post('/upload/images', 'PortfolioTradeController@uploadImages')->name('upload.images');
		Route::post('/image/delete', 'PortfolioTradeController@imageDestroy')->name('image.destroy');

		Route::get('/{market}/transactions','PortfolioTransactionController@index')->name('portal.portfolio.transactions');
		// DEPOSIT
		Route::get('/{market}/transaction/deposit','PortfolioTransactionController@deposit')->name('portal.portfolio.transaction.deposit');
		Route::post('/{market}/transaction/deposit','PortfolioTransactionController@addDeposit')->name('portal.portfolio.transaction.deposit.add');
		// WITHDRAW
		Route::get('/{market}/transaction/withdraw','PortfolioTransactionController@withdraw')->name('portal.portfolio.transaction.withdraw');
		Route::post('/{market}/transaction/withdraw','PortfolioTransactionController@addWithdraw')->name('portal.portfolio.transaction.withdraw.add');

		// BUY
		Route::get('trade/buy','PortfolioTransactionController@buy')->name('portal.trade.portfolio.buy');
		Route::post('trade/buy','PortfolioTransactionController@addBuy')->name('portal.trade.portfolio.buy.add');

		// SELL
		Route::get('trade/sell','PortfolioTransactionController@sell')->name('portal.trade.portfolio.sell');
		Route::post('trade/sell','PortfolioTransactionController@addsell')->name('portal.trade.portfolio.sell.add');

		// SUB CATEGORY
		Route::get('/subcategory','PortfolioTransactionController@subcategory')->name('portal.trade.portfolio.subcategory');
		Route::delete('trade/delete/{id}', 'PortfolioTransactionController@destroy')->name('portal.portfolio.transaction.destroy');


	});
	//STOCK
	Route::group(['prefix'=>'stock'], function () {
			Route::get('/realTimeMonitoring','PortalPageController@realTimeMonitoring')->name('portal.stock.realTimeMonitoring');
			Route::get('/market/summary','PortalPageController@marketSummary')->name('portal.stock.marketSummary');
			Route::get('/market/status','PortalPageController@marketStatus')->name('portal.stock.marketStatus');
	});

	// CHARTING
	Route::group(['prefix'=>'chart'], function () {
			Route::get('/','ChartController@index')->name('portal.chart.index');
	});

	// STOCK WATCHLIST
	Route::group(['prefix'=>'stock-watchlist'], function () {
			Route::get('/','StockWatchlistController@index')->name('portal.watchlist.index');
			Route::get('add','StockWatchlistController@create')->name('portal.watchlist.add');
			Route::post('add','StockWatchlistController@store')->name('portal.watchlist.store');
			Route::get('add/watchlist','StockWatchlistController@createWatchlist')->name('portal.add.watchlist');
			Route::post('add/watchlist','StockWatchlistController@storeWatchlist')->name('portal.store.watchlist');
			Route::get('/html/watchlists','StockWatchlistController@getWatchlistSelectHtml')->name('portal.watchlist.html');
			Route::get('/view','StockWatchlistController@view')->name('portal.watchlist.view');
			Route::delete('/delete/{id}', 'StockWatchlistController@destroy')->name('portal.watchlist.destroy');
	});

	// STOCK WATCHLIST
	Route::group(['prefix'=>'stock-alert'], function () {
			Route::get('/','StockAlertController@index')->name('portal.stockalert.index');
			Route::get('/ajax/user-stock-alerts','StockAlertController@getUserAlerts')->name('portal.stockalert.ajax.list');
			Route::get('add','StockAlertController@create')->name('portal.stockalert.add');
			Route::post('add','StockAlertController@store')->name('portal.stockalert.store');
			Route::post('/ajax/status/update','StockAlertController@updateStatus')->name('portal.stockalert.ajax.update.status');
			Route::post('/ajax/delete','StockAlertController@destroy')->name('portal.stockalert.delete');
	});

	// USER NOTIFICATION
	Route::group(['prefix'=>'ajax'], function () {
			Route::get('user/notification/header','SocialController@userNotificationsHeader');
			Route::get('user/notification/markasread','SocialController@userNotificationsMarkAsRead');
	});
	Route::get('notification/{id}','NotificationController@redirect')->name('notification.redirect');

	//STOCK
	Route::group(['prefix'=>'analysis'], function () {
			Route::get('/watchlist','AnalysisPageController@watchlist')->name('portal.analysis.watchlist.index');
			Route::get('/historical/watchlist','AnalysisPageController@historicalWatchlist')->name('portal.historical.watchlist');
			Route::get('/consensus','AnalysisPageController@consensus')->name('portal.consensus');
			Route::get('/btm/stock','AnalysisPageController@btmStock')->name('portal.btm.stock');
			Route::get('/dividend','AnalysisPageController@dividend')->name('portal.dividend');
			Route::get('/company/buyback','AnalysisPageController@companyBuyback')->name('portal.company.buyback');
			Route::get('/insider/transaction/history','AnalysisPageController@insiderTransactionHistory')->name('portal.insider.transaction.history');
			Route::get('/insider/transaction','AnalysisPageController@insiderTransaction')->name('portal.insider.transaction');
			Route::get('/ph/data','AnalysisPageController@PHData')->name('portal.ph.data');
	});

	Route::get('/media','MediaController@medialList')->name('media');
});

Route::group(['middleware' => []], function () {
	/* Change Password */
	Route::get('/change/password', 'HomeController@changePassword')->name('change.password');
	Route::get('user/change/password', 'HomeController@userChangePassword')->name('user.change.password');
	Route::post('/change/password/update', 'HomeController@updatePassword')->name('update.password');

	Route::get('/subscription/plan/upgrade/{paymenttypeid}', 'PaymentController@changePlan')->name('subscription.change');
	Route::post('/execute/paypal/','PaymentController@execute')->name('execute.paypal');
	Route::get('/pay/success/{paymentID}', 'PaymentController@success')->name('pay-success');
	Route::get('/pay/failed/{paymentID}', 'PaymentController@failed')->name('pay-failed');
});

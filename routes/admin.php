<?php 

/* Admin Routes */
Route::group(['middleware' => ['auth','role:admin', 'admin.crud'], 'prefix'=>'admin'], function () {

	Route::get('/', function () {
		return redirect('admin/dashboard');
	});

	Route::get('/dashboard', 'HomeController@index')->name('admin.home');

	Route::get('/user/search', 'PaymentController@userSearch')->name('user.search');
	Route::get('/course/search', 'PaymentController@courseSearch')->name('course.search');
	Route::post('/course/restore', 'CourseController@restore')->name('course.restore');

	Route::resource('user','UserController');
	Route::resource('course','CourseController');
	Route::resource('media','MediaController');
	
	Route::group(['prefix'=>'analysis'], function () {

		// HISTORICAL WATCHLISTS
		Route::resource('watchlist/historical','AnalysisHistoricalWatchlistController');
		Route::post('watchlist/historical/import', 'AnalysisHistoricalWatchlistController@import')->name('historical.import');
		// HISTORICAL WATCHLISTS
		Route::resource('watchlist','AnalysisWatchlistController');
		Route::post('watchlist/import', 'AnalysisWatchlistController@import')->name('watchlist.import');


		// CONSENSUS
		Route::resource('consensus','AnalysisConsensusController');
		Route::post('consensus/import', 'AnalysisConsensusController@import')->name('consensus.import');

		// BTM STOCK
		Route::resource('stock/btm','AnalysisbtmStockController');
		Route::post('btm/import', 'AnalysisbtmStockController@import')->name('btm.import');

		// DIVIDEND
		Route::resource('dividend','AnalysisDividendController');
		Route::post('dividend/import', 'AnalysisDividendController@import')->name('dividend.import');

		// Company BuyBack
		Route::resource('company/buyback','AnalysisCompanyBuybackController');
		Route::post('company/buyback/import', 'AnalysisCompanyBuybackController@import')->name('buyback.import');

		// Insider Transactions History
		Route::resource('insider/transaction/history','AnalysisInsiderTransactionHistoryController');
		Route::post('insider/transaction/history/import', 'AnalysisInsiderTransactionHistoryController@import')->name('history.import');
		
		// Insider Transactions
		Route::resource('insider/transaction','AnalysisInsiderTransactionController');
		Route::post('insider/transaction/import', 'AnalysisInsiderTransactionController@import')->name('transaction.import');

		// Insider Transactions
		Route::resource('ph','AnalysisPHDataController');
		Route::post('ph/import', 'AnalysisPHDataController@import')->name('ph.import');


	});

	// view
	// Route::get('enrollment/{id}/view', 'CourseEnrollmentController@view')->name('enrollment.view');
	// EXPORT
	Route::get('enrollment/export', 'CourseEnrollmentController@export')->name('enrollment.export');
	Route::get('payment/export', 'PaymentController@export')->name('payment.export');
	Route::get('subscriber/export', 'SubscriberController@export')->name('subscriber.export');
	Route::get('subscriber/registered/export', 'SubscriberController@exportRegisteredUsers')->name('subscriber.export.registered');

	Route::get('subscriber/registered', 'SubscriberController@registeredUsers')->name('subscriber.registered');
	Route::get('subscriber/registered/edit/{id}', 'SubscriberController@editRegisteredUser')->name('subscriber.registered.edit');
	Route::delete('subscriber/registered/delete/{id}', 'SubscriberController@deleteRegisteredUser')->name('subscriber.registered.delete');
	Route::put('subscriber/registered/update/{id}', 'SubscriberController@updateRegisteredUser')->name('subscriber.registered.update');

	Route::resource('enrollment','CourseEnrollmentController');
	Route::resource('subscription','SubscriptionController');
	Route::resource('subscriber','SubscriberController');
	Route::resource('payment','PaymentController');

	Route::resource('payslip','PaySlipController');
	Route::resource('trade/portfolio/category','TradePortfolioCategoryController');
	Route::get('trade/portfolio/subcategory/{id}/edit', 'TradePortfolioCategoryController@subcategoryEdit')->name('subcategory.edit');
	Route::post('trade/portfolio/subcategory/update/{id}', 'TradePortfolioCategoryController@subcategoryUpdate')->name('subcategory.update');
	Route::delete('trade/portfolio/subcategory/{id}', 'TradePortfolioCategoryController@subcategoryDestroy')->name('subcategory.destroy');

	Route::get('setting', 'SettingController@create')->name('portal.setting.create');
	Route::post('setting', 'SettingController@store')->name('portal.setting.store');

	Route::resource('custom/form','CustomFormController');
	Route::post('custom/form/update', 'CustomFormController@customFormUpdate')->name('custom.form.update');



});

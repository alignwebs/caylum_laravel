@extends('frontend.app')

@section('content')
<div class="header">

  <div class="logo-container">
    <img src="{{ asset('theme/images/caylum-logo.png') }}" alt="">
  </div>
  <h5 class="title" style="text-transform: capitalize;">Register as a new member</h5>
</div>
<div class="container">
  <div class="col-md-12">
    <div class="card-plain" id="success">
      <form method="POST" action="{{ route('user.register') }}" id="registerForm">
        @csrf

        <div class="content">  
          <div id="errors"></div>

          <div class="row"> 
            <div class="col-md-6">                                             
              <div class="input-group input-lg">
               <input id="last_name" type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Last Name">
               @if ($errors->has('last_name'))
               <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('last_name') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group input-lg">
             <input id="first_name" type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus placeholder="First Name">
             @if ($errors->has('first_name'))
             <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('first_name') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>

      <div class="input-group input-lg">
        <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email Address">

        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
      </div>


      <div class="input-group pt-2">
        <label>Gender: &nbsp;&nbsp;</label>

        <label for="gender_male" style="font-weight: normal;"><input type="radio" name="gender" id="gender_male" value="male"> Male</label>  &nbsp;&nbsp;
        <label for="gender_female"  style="font-weight: normal;"><input type="radio" name="gender" id="gender_female" value="female"> Female</label> 
        @if ($errors->has('gender'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('gender') }}</strong>
        </span>
        @endif
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="input-group input-lg">
            <input id="birthday" type="text" class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }} datetimepickerForBirtday" name="birthday" value="{{ old('birthday') }}" required placeholder="Birthday">

            @if ($errors->has('birthday'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('birthday') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="input-group input-lg">
            <input id="mobile" type="text" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required placeholder="Contact No.">

            @if ($errors->has('mobile'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('mobile') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="input-group input-lg">
           <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

           @if ($errors->has('password'))
           <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="col-md-6">
        <div class="input-group input-lg">
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Re-Password">
        </div>
      </div>
    </div>
    <hr>
    <div class="">
      <p class="text-bold">Are you an alumni member of Caylum?</p>
      <ul class="list-inline">
        <li class="list-inline-item"><label><input type="radio"  name="alumini_member" value="1"> Yes</label></li>
        <li class="list-inline-item"><label><input type="radio" checked value="0" name="alumini_member"> No</label></li>
      </ul>

    </div>

    <div id="yes" style="display: none">
      <div class="input-group">
        <label>Which Course ?</label>
        {!! Form::select('alumini_course',$courses,null,['class'=>'form-control','placeholder'=>'Select Course','id'=>'alumini_course']) !!}
      </div>
      <div class="input-group">
        <label>What year did you attend Caylum?</label>
        {!! Form::select('alumini_attend',years(),null,['class'=>'form-control','placeholder'=>'Select Year','id'=>'alumini_attend']) !!}
      </div>

    </div>
    <div class="input-group">
      <label class="text-left">Would you like to subscribe to the full features of our alumni portal? </label>
      {!! Form::select('subscription_id',$subscriptions,null,['class'=>'form-control','id'=>'subscription_id']) !!}
    </div>
    <hr>
    <p class="text-justify text-sm"><strong>Disclaimer:</strong> Please be advised that Caylum will manually verify your status as an alumni member based on the information that you have submitted before we grant you access to the full members portal site. This may take 1-2 working days and we will notify you via email once your registration has been approved. However, if we are unable to locate your information in our alumni database, we will refund you back the membership payment fee and advise you of your status via email as well.</p>
  </div>
  <div class="footer text-center">
   <div id="paypal-button-container" style="display: none"></div>
   <button type="submit" id="submitBtn" class="btn btn-primary btn-round btn-lg btn-block ">
    {{ __('Register') }}
  </button>
</div>
</form>


</div>
</div>
</div>

<div class="mw-600 my-5">
 <a href="/login"  class="">Already a Member? Login</a>
</div>
@endsection
@push('scripts')

<script type="text/javascript">
  $("input[name='alumini_member']").change(function(){


    if($(this).val() == '1') {

      $("#yes").show();
    }

    if($(this).val() == '0'){
      $("#yes").hide();
    }
  });



</script>
<script type="text/javascript">

  var fee;
  $("#registerForm").submit(function(){

    $.post($(this).attr('action'),$(this).serialize()+"&getSubscriptionFee=true", function(data){

      if(data.errors != undefined)
      {

        $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
        $(window).scrollTop(0);
      }

      if(data.success != undefined && data.fee != undefined){
        $("#errors").html("");
        $("#submitBtn").hide();
        $("#paypal-button-container").show();

        fee = data.fee;
        paypal.Buttons({
         style: {
           layout:  'vertical',
           color:   'blue',
           shape:   'rect',
           label:   'paypal'
         },
         createOrder: function(data, actions) {

                    // Set up the transaction
                    return actions.order.create({
                      purchase_units: [{
                        amount: {
                          value: fee
                        }
                      }]
                    });
                  },
                  onApprove: function(data, actions) {
                   $('#preloader').show();
                   return actions.order.capture().then(function(details) {

                                     //alert('Transaction completed by ' + details.payer.name.given_name);
                                     // Call your server to save the transaction
                                     return fetch('{{ route("user.register") }}', {
                                       method: 'post',
                                       headers: {
                                         'content-type': 'application/json',
                                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                       },
                                       body: JSON.stringify({
                                         orderID: data.orderID,
                                         paymentTypeId: '1',
                                         first_name:$("#first_name").val(),
                                         last_name:$("#last_name").val(),
                                         email:$("#email").val(),
                                         mobile:$("#mobile").val(),
                                         password:$("#password").val(),
                                         gender: $("input[name=gender]").val(),
                                         birthday:$("#birthday").val(),
                                         password_confirmation:$("#password-confirm").val(),
                                         alumini_member:$('input[name=alumini_member]:checked', '#registerForm').val(),
                                         alumini_course:$("#alumini_course").val(),
                                         alumini_attend:$("#alumini_attend").val(),
                                         subscription_id:$("#subscription_id").val(),
                                         
                                       }),
                                       
                                     }).then(response => response.json())
                                     .then((body) => {
                                      $('#preloader').hide();
                                      if(body.errors != undefined)
                                        alert(body.errors);

                                      if(body.success != undefined)
                                        $("#success").html(` <br><br> <h2 style="color:#0fad00">Registration successful</h2>
                                         <i class="fa fa-check-circle text-success" style="font-size: 85px"></i><br><br><p>Please check your email for your account details.</p> <br> <a href="{{ route('login') }}">Click here to login</a>`);

                                    }).catch(function(error) {
                                            alert("Sorry an unknown error occurred. Refresh & Try Again.")
                                    }).finally(function () {
                                      $('#preloader').hide();
                                    });

                                  })
                                },
                              onCancel: function(data, actions) {
                                  $('#preloader').hide();
                                },

                                onError: function(err) {
                                  alert(err)
                                  $('#preloader').hide();

                                }
                              }).render('#paypal-button-container');


}

if(data.success != undefined && data.message != undefined)
{
 $("#success").html(` <br><br> <h2 style="color:#0fad00">Registration successful</h2>
  <i class="fa fa-check-circle text-success" style="font-size: 85px"></i><br><br><p>Please check your email for your account details</p>  <br> <a href="{{ route('login') }}">Click here to login</a>`);
}

});
return false;

})
</script>

@endpush

@extends('frontend.app')

@section('content')
<div class="header">
                    <div class="logo-container">
                        <img src="{{ asset('theme/images/caylum-logo.png') }}" alt="">
                    </div>
                    <h5>Reset Password</h5>
                </div>
<div class="container">
    <div class="col-md-12 content-center">
        <div class="card-plain">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('password.email') }}">
              @csrf
                <div class="content">   
                 @if(sizeof($errors) > 0)
                     <div class="alert alert-danger">{{ $errors->first() }}</div>                                     
                @endif                                             
                    <div class="input-group input-lg">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Enter Your Email Address">

                       
                    </div>
                    
                </div>
                <div class="mt-5 text-center">
                    <button type="submit" class="btn btn-primary btn-round btn-lg btn-block ">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="mw-600 my-5">
    <a href="{{ route('login') }}">< Back To Login</a>
</div>

@endsection
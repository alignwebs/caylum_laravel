@extends('frontend.app')

@section('content')
<div class="header">
                    <div class="logo-container">
                        <img src="{{ asset('theme/images/caylum-logo.png') }}" alt="">
                    </div>
                    <h5>Reset Password</h5>
                </div>
<div class="container">
    <div class="col-md-12 content-center">
        <div class="card-plain">
            <form method="POST" action="{{ route('password.update') }}">
              @csrf
              <input type="hidden" name="token" value="{{ $token }}">
                
                <div class="content">   
                 @if(sizeof($errors) > 0)
                     <div class="alert alert-danger">{{ $errors->first() }}</div>                                     
                @endif                                             
                    <div class="input-group input-lg">
                         <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Enter Your Email Address">


                    </div>
                    <div class="input-group input-lg">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Enter Password">

                    </div>
                    <div class="input-group input-lg">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Re-enter Your Password">
                    </div>
                </div>
                <div class="footer text-center">
                    <button type="submit" class="btn btn-primary btn-round btn-lg btn-block ">
                        {{ __('Reset Password') }}
                    </button>
                   
                    
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
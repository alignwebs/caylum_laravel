@extends('frontend.app')

@section('content')
<style>
    body { }
</style>
 <div class="header">
                    <div class="logo-container">
                        <img src="{{ asset('theme/images/caylum-logo.jpg') }}" alt="" width="250">
                    </div>
                    <h5 style="text-transform: capitalize;">Log in</h5>
                </div>
<div class="container">
    <div class="col-md-12 content-center">
        <div class="card-plain">
            <form method="POST" action="{{ route('login') }}">
              @csrf
               
                <div class="content">  
                    @include('flash::message')
                    @if(sizeof($errors) > 0)
                        <div class="alert alert-danger">{{ $errors->first() }}</div>                                     
                   @endif
                    <div class="input-group input-lg">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required  autofocus placeholder="Enter Your Email">
                    </div>
                    <div class="input-group input-lg">
                       <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required  placeholder="Password">
                    </div>
                         <a href="{{ route('password.request') }}" class="link float-right">Forgot Password?</a>

                </div>
                <div class="mt-5 text-center">
                    <button type="submit" class="btn btn-primary btn-round btn-lg btn-block ">
                        {{ __('Login') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
    
    <div class="mw-600 my-5">
                <a href="/register">Not a Member? Sign up.</a>
</div>
@endsection

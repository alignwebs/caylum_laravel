   <div class="modal-header">
    <h4 class="title" id="defaultModalLabel">Withdraw</h4>
    <button type="button" class="close" data-dismiss="modal">×</button>
  </div>
  {!! Form::open(['class'=>'form-material','id'=>'addWithdrawForm']) !!}
  <div class="modal-body">
    <div id="errors"></div>
    <div class="row">

      <div class="col-md-6">
       <div class="form-group">
        <label>Date</label>
        {!! Form::text('date',null,['class'=>'form-control datetimepicker']) !!}         
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group">
        <label>Amount</label>
        {!! Form::number('amount',null,['class'=>'form-control','step'=>0.01]) !!}
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-primary btn-round waves-effect" id="addWithdrawBtn">Submit</button>
</div>
{!! Form::close() !!}
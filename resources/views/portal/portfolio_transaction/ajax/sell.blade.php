<div class="modal-header">
    <h4 class="title" id="defaultModalLabel">Sell</h4>
    <button type="button" class="close" data-dismiss="modal">×</button>
  </div>
  {!! Form::open(['class'=>'form-material','id'=>'addSellForm']) !!}
  <div class="modal-body">
    <div id="errors"></div>
    <div class="row">

    <div class="col-md-6">
      <div class="form-group">
        <label>Stocks</label>
        <select name="stock" class='' id="stock">
                    <option value="">Select Stock</option>
                    @foreach($stocks as $stock)
                      <option value="{{ $stock->full_stock_name }}">{{ $stock->full_stock_name }}</option>
                    @endforeach
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Stock Qty</label>
        {!! Form::number('stock_qty',null,['class'=>'form-control','id'=>'stock_qty']) !!}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Per Stock Rate</label>
        {!! Form::number('per_stock_rate',null,['class'=>'form-control','id'=>'per_stock_rate','step'=>0.01]) !!}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Fee</label>
        {!! Form::number('fee',null,['class'=>'form-control','step'=>0.01]) !!}
      </div>
    </div>
    <div class="col-md-6">
       <div class="form-group">
        <label>Date</label>
        {!! Form::text('date',null,['class'=>'form-control datetimepicker']) !!}         
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group">
        <label>Amount</label>
        {!! Form::number('amount',null,['class'=>'form-control','id'=>'amount','step'=>0.01]) !!}
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-primary btn-round waves-effect" id="addSellBtn">Submit</button>
</div>
{!! Form::close() !!}
<select name="portfolio_subcategory" class='form-control form-control2' id='portfolio_subcategory'>
    @foreach($portfolio_subcategories as $subcategory)
      <option value="{{ $subcategory->name }}" data-id="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
    @endforeach
</select>
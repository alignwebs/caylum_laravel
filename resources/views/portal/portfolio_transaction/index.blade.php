@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .form-control2 {padding: 0px !important;margin-top: -5px !important;}
  .table tr th, .table tr td{vertical-align: middle;}
  #searchForm{ padding: 21px 8px 15px 10px;padding-left: 8px;margin: -20px -20px 23px -19px;background: #566e7a}
  #searchForm .datetimepicker{background-color: #f7f7f7 !important; border: 1px solid #ffffff !important;}
  #searchForm .bootstrap-select .btn.btn-round.btn-simple{background-color: #f7f7f7 !important;}
  #searchForm .btn-primary{background: #fff !important;color: #566e7a !important;font-weight: 600;}
</style>

<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
        </h2>
      </div>            

    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-md-2">
        <div class="card">
          <div class="card-header">Market</div>
            <div class="card-body">
                <div class="row text-center">
                    <div class="col-md-12">
                      <h5>{{ $market->symbol }}</h5>

                    </div>

                 
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card">
          <div class="card-header">Balance</div>
            <div class="card-body">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h5>{{ Setting::get('currency').$user->trade_portfolio_balance }}</h5>
                    </div>
                </div>
            </div>
        </div>
      </div>
   
      <div class="col-md-7">
        <div class="card">
          <div class="card-header">Portfolio Transaction</div>
            <div class="card-body">
                <div class="row text-center">
                    <div class="col-md-6">
                      <button class="btn btn-primary btn-block" href="{{ route('portal.portfolio.transaction.deposit',$market->id) }}" id="addDeposit"><span class="fa fa-money-bill-alt"></span> Deposit</button>
                    </div>

                    <div class="col-md-6">
                      <button class="btn btn-primary btn-block" href="{{ route('portal.portfolio.transaction.withdraw',$market->id) }}"  id="addWithdraw"><span class="fa fa-money-bill-alt"></span> Withdraw</button>
                    </div>
                </div>
            </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
              @if(!$search)
                  List of Portfolio Transactions
              @else
                  TYPE transactions from DATE to DATE
              @endif
          </div>
          <div class="card-body">
            <div id="searchForm">
                {!! Form::open(['route' => ['portal.portfolio.transactions',$market->symbol],'method' => 'get']) !!}
                <div class="row">
                <div class="col-sm-2 form-group" style="margin-left: 20px;"> 
                 {!! Form::text('from',!empty($request->from) ? $request->from : null,['id'=>'from','class'=>'form-control datetimepicker','placeholder'=>'From']) !!}
                </div>
                 <div class="col-sm-2 form-group"> 
                 {!! Form::text('to',!empty($request->to) ? $request->to : null,['id'=>'to','class'=>'form-control datetimepicker','placeholder'=>'To']) !!}
                </div>
                 <div class="col-sm-2"> 
                {!! Form::select('type',['deposit'=>'Deposit','buy'=>'Buy','sell'=>'Sell','withdraw'=>'Withdraw'],!empty($request->type) ? $request->type : null,['id'=>'type','class'=>'form-control form-control2','placeholder'=>'Type']) !!}
               </div>
                <div class="col-sm-2"> 
                 <button type="submit" class="btn btn-primary" style="margin-top: 0px;">Search</button>
               </div>
               </div>
                {!! Form::close() !!}
          </div>
            
            <div class="table-responsive">
              {!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<!-- Default Size -->
<div class="modal fade" id="commonModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <br>
      <p class="text-center" id="progress-bar">Loading...</p>
    </div>
  </div>
</div>
@endsection


@push('scripts')

<script type="text/javascript">
  $(document).ready(function(){
    // GET DEPOSIT FORM
    $("#addDeposit").click(function(){
     $("#commonModal").modal();
     $.get("{{ route('portal.portfolio.transaction.deposit',$market->id) }}", function(data){
      $('.modal-content').html(data)
      $('.datetimepicker').bootstrapMaterialDatePicker({
        time:false
      });
      addDeposit();
    });
   });

    // GET WITHDRAW FORM
    $("#addWithdraw").click(function(){
     $("#commonModal").modal();
     $.get("{{ route('portal.portfolio.transaction.withdraw',$market->id) }}", function(data){
      $('.modal-content').html(data)
      $('.datetimepicker').bootstrapMaterialDatePicker({
        time:false
      });
      addWithdraw();
    });
   });

    // GET BUY FORM
    $("#addBuy").click(function(){
     $("#commonModal").modal();
     $.get("{{ route('portal.trade.portfolio.buy') }}", function(data){
      $('.modal-content').html(data)
      $('.form-control2').selectpicker('refresh')
      $('#stock').selectize();
      $('.datetimepicker').bootstrapMaterialDatePicker({
        time:false
      });
      addBuy();
      $("#addBuyForm #per_stock_rate,#addBuyForm #stock_qty").keyup(function(){
        var stock_qty = $("#stock_qty").val();
        var per_stock_rate = $("#per_stock_rate").val();
        var amount = stock_qty * per_stock_rate;
        $("#amount").val(amount);
      });
      get_sub_category();
    });
   });

    // GET SELL FORM
    $("#addSell").click(function(){
     $("#commonModal").modal();
     $.get("{{ route('portal.trade.portfolio.sell') }}", function(data){
      $('.modal-content').html(data)
      $('.form-control2').selectpicker('refresh')
      $('#stock').selectize();
      $('.datetimepicker').bootstrapMaterialDatePicker({
        time:false
      });
      addSell();
      $("#addSellForm #per_stock_rate,#addSellForm #stock_qty").keyup(function(){
        var stock_qty = $("#stock_qty").val();
        var per_stock_rate = $("#per_stock_rate").val();
        var amount = stock_qty * per_stock_rate;
        $("#amount").val(amount);
      });
      get_sub_category();
    });
   });

  });

  // ADD DEPOSIT
  function addDeposit()
  {   
    $('#addDepositForm').submit(function (data) {
      $("#addDepositBtn").prop('disabled',true);
      $.post("{{ route('portal.portfolio.transaction.deposit.add',$market->id) }}",$(this).serialize(), function(data){
        checkData(data);
      })
      return false;
    });
  }

  // ADD WITHDRAW
  function addWithdraw()
  {
    $('#addWithdrawForm').submit(function (data) {
      $("#addWithdrawtBtn").prop('disabled',true);
      $.post("{{ route('portal.portfolio.transaction.withdraw.add',$market->id) }}",$(this).serialize(), function(data){
        checkData(data);
      })
      return false;
    });
  }

  // ADD Buy
  function addBuy()
  {
    $('#addBuyForm').submit(function (data) {
      $("#addBuyBtn").prop('disabled',true);
      $.post("{{ route('portal.trade.portfolio.buy.add') }}",$(this).serialize(), function(data){
        checkData(data);
      })
      return false;
    });
  }

  // ADD Buy
  function addSell()
  { 
    $('#addSellForm').submit(function (data) {
      $("#addSellBtn").prop('disabled',true);
      $.post("{{ route('portal.trade.portfolio.sell.add') }}",$(this).serialize(), function(data){
        checkData(data);
      })
      return false;
    });
  }

// CHECK DATA
function checkData(data)
{
 if(data.success == 'true')
 {
  $("#depositModal").modal('hide');
  notification(data.message,'success')
  location.reload();

}else{

  $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
}
$("#addDepositBtn,#addBuyBtn,#addSellBtn,#addWithdrawBtn").prop('disabled',false);
}

  // GET SUB CATEGORY
  function get_sub_category()
  {
    $('#portfolio_category').on('change', function() {

      var trade_portfolio_category_id = $(this).find(':selected').data('id');
      $.get('{{ route("portal.trade.portfolio.subcategory") }}',{trade_portfolio_category_id:trade_portfolio_category_id}, function(data){

        $("#portfolio_subcategory").html(data);
        $('.form-control2').selectpicker('refresh')
      })
    });
  }

</script>
{!! $datatable->scripts() !!}
@endpush
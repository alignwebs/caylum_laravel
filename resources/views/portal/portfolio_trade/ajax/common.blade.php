<div class="modal-header">
    <h4 class="title" id="defaultModalLabel">{{ ucwords($type) }}</h4>
    <button type="button" class="close" data-dismiss="modal">×</button>
  </div>
  {!! Form::open(['route' => ['portal.portfolio.trade.transaction.add'],'class'=>'form-material','id'=>'addTransactionForm','files'=>true]) !!}
  {!! Form::hidden('type',$type) !!}
  {!! Form::hidden('market_id',$portfolio_trade->market_id) !!}
  <div class="modal-body">
    <div id="errors"></div>
    <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>Trade</label>
        {!! Form::select('portfolio_trade_id',$portfolio_trades,$portfolio_trade->id,['class'=>'form-control form-control2']) !!}
      </div>
    </div>
      <div class="col-md-6">
       <div class="form-group">
        <label>Date</label>
        {!! Form::text('date',null,['class'=>'form-control datetimepicker']) !!}         
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Stock</label>
        <select name="stock" class='' id="stock">
            <option value="">Select Stock</option>
            @foreach($stocks as $stock)
              <option value="{{ $stock->full_stock_name }}">{{ $stock->full_stock_name }}</option>
            @endforeach
        </select>
       
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Stock Qty</label>
        {!! Form::number('stock_qty',null,['class'=>'form-control','id'=>'stock_qty']) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Per Stock Rate</label>
        {!! Form::number('per_stock_rate',null,['class'=>'form-control','id'=>'per_stock_rate','step'=>0.01]) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Fee Percentage</label>
        {!! Form::number('fee_percentage',null,['class'=>'form-control','step'=>0.01,'id'=>'fee_percentage']) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Fee</label>
        {!! Form::number('fee',null,['class'=>'form-control','step'=>0.01,'id'=>"fee"]) !!}
      </div>
    </div>

    <div class="col-md-4">
      <div class="form-group">
        <label>Amount</label>
        {!! Form::number('amount',null,['class'=>'form-control','id'=>'amount','step'=>0.01]) !!}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Image</label>
        {!! Form::file('trade_transaction_image',null) !!}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Comment</label>
        {!! Form::textarea('comments',null,['class'=>'form-control','id'=>'amount','rows'=>2]) !!}
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-primary btn-round waves-effect" id="addTransactionBtn">Submit</button>
</div>
{!! Form::close() !!}
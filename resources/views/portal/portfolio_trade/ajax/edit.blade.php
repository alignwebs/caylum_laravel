    {!! Form::model($portfolio_trade, ['route' => ['portal.portfolio.trade.update', $portfolio_trade->id],'method'=>'POST','id'=>'editPortfolioForm','class'=>'form-material']) !!}
    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title"><b>Edit Portfolio Trade</b></h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>
    <!-- Modal body -->
    <div class="modal-body" >
       <div id="errors"></div>
       <div class="row">
       <div class="col-md-12">
          <div class="form-group">
               <label>Trade Name</label>
               {!! Form::text('trade_name',null,['class'=>'form-control','required'=>true]) !!}
           </div>
       </div>
       <div class="col-md-12">
           <div class="form-group">
           
                <label>Trade Setup</label>
                <select name="portfolio_category" class='' id="portfolio_category">
                    <option value="">Trade Setup</option>
                    @foreach($portfolio_categories as $category)
                      <option value="{{ $category->name }}" {{ ($category->name == $portfolio_trade->portfolio_category) ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
               
        
             
            </div>
        </div>
    
       <div class="col-md-12">
           <div class="form-group">
             <label>Comments</label>
             {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>2]) !!}
           </div>
       </div>
         <div class="col-md-12">
           <div class="form-group">
             <label>Is Private</label>
             {!! Form::checkbox('is_private',1) !!}
           </div>
       </div>
       
       
      </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editPortfolioForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editPortfolioForm").modal('hide');
                 notification(data.message,'success')
                  location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

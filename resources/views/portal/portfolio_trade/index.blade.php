@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .form-control2 {padding: 0px !important;margin-top: -5px !important;}
  .table tr th, .table tr td{vertical-align: middle;}
  .btn-group.bootstrap-select.selectized{display: none;}
</style>

<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
        </h2>
      </div> 
      <div class="col-md-7 text-right">  
          <button type="button" class="btn btn-warning" id="addPortfolioTrade">+ Add Portfolio Trade</button>
      </div>            

    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">

      <div class="col-md-12">
        <div class="card">

          <div class="card-body">
            
            <div class="table-responsive">
              {!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<!-- Default Size -->
<!-- Default Size -->
<div class="modal fade" id="portfolioTradeModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="title" id="defaultModalLabel">Add Portfolio Trade</h4>
            </div>
            {!! Form::open(['id'=>'addPortfolioTradeForm']) !!}
            <div class="modal-body">
                      <div id="errors"></div>
                      <div class="row">
                      <div class="col-md-12">
                         <div class="form-group">
                              <label>Trade Name</label>
                              {!! Form::text('trade_name',null,['class'=>'form-control','required'=>true]) !!}
                          </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                         
                              <label>Trade Setup</label>
                              <select name="portfolio_category" class='' id="portfolio_category">
                                  <option value="">Trade Setup</option>
                                  @foreach($portfolio_categories as $category)
                                    <option value="{{ $category->name }}">{{ $category->name }}</option>
                                  @endforeach
                              </select>
                             
                      
                           
                          </div>
                      </div>
                    
                      <div class="col-md-12">
                          <div class="form-group">
                            <label>Comments</label>
                            {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>2]) !!}
                          </div>
                      </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Is Private</label>
                            {!! Form::checkbox('is_private',1) !!}
                          </div>
                      </div>
                      
                      
                     </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect" id="portfolioTradeBtn">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection


@push('scripts')

<script type="text/javascript">
  $(document).ready(function(){
    // GET DEPOSIT FORM
    $("#addPortfolioTrade").click(function(){
     
     $("#portfolioTradeModal").modal();
    
   });

  });


    $('#addPortfolioTradeForm').submit(function (data) {

      $("#portfolioTradeBtn").prop('disabled',true);
      $.post("{{ route('portal.portfolio.trade.add',$market->id) }}",$(this).serialize(), function(data){
         if(data.success == 'true')
         {
          $("#portfolioTradeModal").modal('hide');
          notification(data.message,'success')
          location.reload();

        }else{

          $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
        }
        $("#portfolioTradeBtn").prop('disabled',false);
      })
      return false;
    });


      $('#portfolio_category').selectize({
       create: true,
       sortField: 'text'
    });
</script>
{!! $datatable->scripts() !!}
@endpush
@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .form-control2 {padding: 0px !important;margin-top: -5px !important;}
  .table tr th, .table tr td{vertical-align: middle;}
  #searchForm{ padding: 21px 8px 15px 10px;padding-left: 8px;margin: -20px -20px 23px -19px;background: #8c99e0}
  #searchForm .datetimepicker{background-color: #f7f7f7 !important; border: 1px solid #ffffff !important;}
  #searchForm .bootstrap-select .btn.btn-round.btn-simple{background-color: #f7f7f7 !important;}
  #searchForm .btn-primary{background: #fff !important;color: #8c99e0 !important;font-weight: 600;}
  .nonloggedin { margin-top: 0px !important; }
</style>

<section class="content home {{ (!Auth::check()) ? 'nonloggedin' :'' }}">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
          <small>Welcome to Caylum</small>
        </h2>
      </div> 
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-md-2">
        <div class="card">
          <div class="card-header">Market</div>
            <div class="card-body">
                <div class="row text-center">
                    <div class="col-md-12">
                      <h5>{{ $market->symbol }}</h5>

                    </div>

                 
                </div>
            </div>
        </div>
      </div>
       @if($is_owner)
      <div class="col-md-3">
        <div class="card">
          <div class="card-header">Balance</div>
            <div class="card-body">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h5>{{ Setting::get('currency').$user->trade_portfolio_balance }}</h5>
                    </div>
                </div>
            </div>
        </div>
      </div>
   
      <div class="col-md-7">
        <div class="card">
          <div class="card-header">Portfolio Transaction</div>
            <div class="card-body">
                <div class="row text-center">
                    
                    <div class="col-md-4">
                      <button class="btn btn-success btn-block add" href="{{ route('portal.trade.portfolio.buy') }}" data-type='buy'><i class="fas fa-shopping-cart" aria-hidden="true"></i> Buy</button>
                    </div>
                    <div class="col-md-4">
                      <button class="btn btn-danger btn-block add" href="{{ route('portal.trade.portfolio.buy') }}" data-type='sell'><i class="fas fa-shopping-cart" aria-hidden="true"></i> Sell</button> 
                    </div>
                    <div class="col-md-4">
                      <button class="btn btn-info btn-block" id="addImages"><i class="fa fa-file-image-o" aria-hidden="true"></i> Add Images</button> 
                    </div>
                 
                </div>
            </div>
        </div>
      </div>
    @endif
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
              @if(!$search)
                  List of Portfolio Transactions
              @else
                  TYPE transactions from DATE to DATE
              @endif
          </div>
          <div class="card-body">
            <div id="searchForm">
                {!! Form::open(['route' => ['portal.portfolio.trade.detail', $market->symbol, $id],'method' => 'get']) !!}
                <div class="row">
                <div class="col-sm-2 form-group" style="margin-left: 20px;"> 
                 {!! Form::text('from',!empty($request->from) ? $request->from : null,['id'=>'from','class'=>'form-control datetimepicker','placeholder'=>'From']) !!}
                </div>
                 <div class="col-sm-2 form-group"> 
                 {!! Form::text('to',!empty($request->to) ? $request->to : null,['id'=>'to','class'=>'form-control datetimepicker','placeholder'=>'To']) !!}
                </div>
                 <div class="col-sm-2"> 
                {!! Form::select('type',['buy'=>'Buy','sell'=>'Sell'],!empty($request->type) ? $request->type : null,['id'=>'type','class'=>'form-control form-control2','placeholder'=>'Type']) !!}
               </div>
                <div class="col-sm-2"> 
                 <button type="submit" class="btn btn-primary" style="margin-top: 0px;">Search</button>
               </div>
               </div>
                {!! Form::close() !!}
          </div>
            <div class="table-responsive">
              {!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
            </div>
           
          </div>
        </div>

      </div>

      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
              Images
          </div>
          <div class="card-body">
             <div class="row">
                @if(sizeof($PortfolioTrade->attachments) > 0)
                @foreach($PortfolioTrade->attachments as $row)
                      <div class="col-lg-2 col-xlg-2 col-md-2">
                        <div class="card">
                            <a class="" data-fancybox="gallery" href="{{ $row->file_url }}">
                              <img class="card-img-top" src="{{ $row->thumbnail_url }}" alt="Gallery Images" class="">
                            </a>
                          
                          <div class="card-body text-center">
                            {!! Form::open(['route' => ['image.destroy'], 'class' => 'deleteFrmAjax']) !!}
                                {!! Form::hidden('filename',basename($row->file)) !!}
                                {!! Form::hidden('is_not_dropzone',1) !!}
                                <button type="submit" class="btn-delete btn btn-danger btn-sm delete-btn">Delete</button>
                          
                            {{ Form::close() }}
                       
                          </div>
                        </div>
                        </div>
                @endforeach
                @else
                No Image Found
                @endif
              </div>
            
          </div>
        </div>

      </div>


    </div>
  </div>
</section>
<!-- Default Size -->
<!-- Default Size -->
<!-- Default Size -->
<div class="modal fade" id="commonModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <br>
      <p class="text-center" id="progress-bar">Loading...</p>
    </div>
  </div>
</div>

<!-- add Modal -->
<div class="modal fade" id="imagesModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Images</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="errors"></div>
            {!! Form::open(['route'=>'upload.images','class'=>'form-material','id'=>'dropzone','files'=>'true','class'=>'dropzone']) !!}
              {!! Form::hidden('portfolio_trade_id',$id) !!}
            {!! Form::close() !!}
        </div>
  </div>
</div>
</div>
@endsection


@push('scripts')

<script type="text/javascript">
  $(document).ready(function(){
    $("#addImages").click(function(){
      $("#errors").html("");
      $("#imagesModal").modal({
        "backdrop":'static'
      });
    });
     // GET BUY FORM
     $(".add").click(function(){

      var type = $(this).data('type');


      $("#commonModal").modal();
      $.get("{{ route('portal.portfolio.trade.transaction.create',$id) }}?type="+type, function(data){
       $('.modal-content').html(data)
       $('.form-control2').selectpicker('refresh')
       $('#stock').selectize();
       $('.datetimepicker').bootstrapMaterialDatePicker({
         time:false
       });
       addBuy();
       $("#addTransactionForm #per_stock_rate,#addTransactionForm #stock_qty,#addTransactionForm #fee_percentage").keyup(function(){
           var stock_qty = parseInt($("#stock_qty").val());
           var per_stock_rate = parseInt($("#per_stock_rate").val());
           var amount = stock_qty * per_stock_rate;
           var fee_percentage = parseInt($("#fee_percentage").val());
           if(!isNaN(fee_percentage)){
             var fee_percentage_amount = amount*fee_percentage/100;
             var amount = amount + fee_percentage_amount;
             $("#fee").val(fee_percentage_amount);
           }
           $("#amount").val(amount);
       });

     });
    });

});

      // ADD Buy
      function addBuy()
      {
        $('#addTransactionForm').submit(function (e) {
            e.preventDefault();
            $("#addTransactionBtn").prop('disabled', true);
            let action = $(this).attr('action');
            var form = $(this)[0];
            var data = new FormData(form);

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: action,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                      $("#commonModal").modal('hide');
                      notification(data.message,'success')
                        window.location.reload();
                    },
                    error: function (e) {
                        $("#addTransactionBtn").prop('disabled', false);
                        alert("ERROR : " + e);
                    }
                });

        });

      }

      // DROPZONE
        Dropzone.options.dropzone =
              {
                 maxFilesize: 12,
                 renameFile: function(file) {
                     var dt = new Date();
                     var time = dt.getTime();
                    return time+file.name;
                 },
                 acceptedFiles: ".jpeg,.jpg,.png,.gif",
                 addRemoveLinks: true,
                 timeout: 50000,
                 removedfile: function(file) 
                 {
                     var name = file.upload.filename;
                     
                     $.ajax({
                         headers: {
                                     'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                 },
                         type: 'POST',
                         url: '{{ route('image.destroy') }}',
                         data: {filename: name},
                         success: function (data){
                             console.log("File has been successfully removed!!");
                         },
                         error: function(e) {
                             console.log(e);
                         }});
                         var fileRef;
                         return (fileRef = file.previewElement) != null ? 
                         fileRef.parentNode.removeChild(file.previewElement) : void 0;
                 },
            
                 success: function(file, response) 
                 {
                     console.log(response);
                 },
                 error: function(file, response)
                 {
                    return false;
                 }
     };
     $('#imagesModal').on('hidden.bs.modal', function () {
       location.reload();
     })
</script>
{!! $datatable->scripts() !!}
@endpush
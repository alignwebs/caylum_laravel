<script>
    var watchlist_id;
    $(document).ready(function () { 
        getWatchlistHtml('#watchlistDropdown');
    });

    $(document).on('change', '#stockwatchlistNames', function () {
        watchlist_id = $(this).val();

        getWatchlistEntries(watchlist_id);
    })
    function getWatchlistHtml(target, selected=null)
    {
        $(target).load('{{ route('portal.watchlist.html') }}', null, function () {
            if(selected != null)
                $('#stockwatchlistNames').val(selected).trigger('change');
            else
                $('#stockwatchlistNames').trigger('change');
        });
    }

    function getWatchlistEntries(watchlist_id)
    {   
        $.get('{{ route('portal.watchlist.view') }}', { watchlist_id: watchlist_id}, function (data) {
                $('#watchlistentries-container').html(data);
        })
    }
</script>
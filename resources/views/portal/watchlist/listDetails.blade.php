<table class="table table-bordered">
	<thead>
		<tr>
			<th>Stock</th>
			<th>LTP</th>
			<th>Change</th>
			<th width="100">Alerts <br>(active/inactive)</th>
			<th width="300">Actions</th>
		</tr>
	</thead>

	<tbody>
		@forelse($watchlistStocks as $row)
			<tr>
				<td>{{ $row->stock_symbol }}</td>
				<td>{{ number_format(rand(111, 9999),2) }}</td>
				<td>{{ number_format(rand(-1111, 9999),2) }}</td>
				<td><a href="javascript:" onclick="showListStockAlertModal(true, '{{ $row->stock_symbol }}')">{{ $row->stockalerts->where('alerted', 0)->count() }} / {{ $row->stockalerts->where('alerted', 1)->count() }}</a></td>
				<td>
					{!! Form::open(['route' => ['portal.watchlist.destroy', $row->id], 'method' => 'delete', 'class' => 'deleteFrmAjax']) !!}
					<a href="javascript:" class="addStockAlertBtn  btn btn-info btn-sm" data-stock="{{ $row->stock_symbol }}">Set Alert</a>&nbsp;<a href="javascript:" class="btn btn-info btn-sm" onclick="showListStockAlertModal(true, '{{ $row->stock_symbol }}')">View Alerts</a>&nbsp;
					<a href="javascript:" class="btn btn-warning btn-sm" onclick="resetStockAlerts('{{ $row->stock_symbol }}')">Reset Alerts</a>&nbsp;
					<button type="submit" class="btn-delete btn btn-danger btn-sm">Delete</button>
					{!!Form::close() !!}
				</td>
			</tr>
		@empty
		    <tr><td class="text-center" colspan="5">No stocks added to watchlist.</td></tr>
		@endforelse
	</tbody>
</table>
@extends('portal.layouts.app')
@section('title', $title)
@section('content')

<section class="content home">
        <div class="block-header cstm-header">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h2>{{ $title }}</h2>
                             @include('portal.analysis.common.analysisDropdown')
                </div>   
       
                 <div class="col-md-6 text-right">  
                    <button type="button" class="btn btn-primary addWatchlistBtn"><i class="fas fa-plus-circle"></i> Create Watchlist</button>
                  <button type="button" class="btn btn-primary addWatchlistStockBtn"><i class="fas fa-plus-circle"></i> Add Stock</button>
              </div>           

            </div>
        </div>
<div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                
                    <div class="card">
                        <div class="body">
                            
                            <div class="row">
                                <div class="col-md-6" >
                                    <div class="form-group">
                                        <label>Watchlist</label> 
                                        <span id="watchlistDropdown"></span>
                                    </div>
                                </div>
                              
                            </div>
                            <div id="watchlistentries-container"></div>
                        
                        </div>
                    </div>
            </div>

            
            
        </div>
    </div>
</section>

@endsection


@push('scripts')

@include('portal.watchlist.common.script')

@endpush
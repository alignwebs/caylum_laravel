<style>
	.easy-autocomplete { width: 100%; }
</style>
{!! Form::open([ 'route' => 'portal.watchlist.store', 'id' => 'StockWatchlistAddFrm' ]) !!}
<div class="modal-header">
	 <h4 class="modal-title">Add Stock To Watchlist</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
 </div>
<div class="modal-body">
	<div class="row">
		{{-- 		<div class="col-6">
		        <label>Watchlist</label>
		        <input type="text" class="watchlist-name" name="watchlist-name" style="width: 100%">
			
		</div> --}}
		<input type="hidden" name="watchlist_id" id="watchlist_id">
		<div class="col-12">
			<div class="form-group">
		        <label>Stock</label>
		        <select name="stock" class='' id="stock">
			        <option value="">Select Stock</option>
			        @foreach($stocks as $stock)
			          <option value="{{ $stock->symbol }}">{{ $stock->full_stock_name }}</option>
			        @endforeach
		    	</select>
		      </div>		
		</div>
	</div>
</div>
<div class="modal-footer">
	<input type="submit" value="Submit" class="btn btn-primary">
</div>
{!! Form::close() !!}

<script>

	

	// setTimeout(function () { 
	// 	$(".watchlist-name").easyAutocomplete(options);
	// 	$('.selectpicker').selectpicker('refresh');

	// 	$('.watchlist-name').val(window.watchlistname)
	// }, 200)


	$('#StockWatchlistAddFrm').submit(function (e) {
		e.preventDefault();

		$.post($(this).attr('action'), $(this).serialize(), function (data) {
			if(data.success != undefined)
			{
				let watchlist_id = $('#watchlist_id').val();
				notification("Watchlist entry added successfully.",'success');
				getWatchlistHtml('#watchlistDropdown', watchlist_id);
				showAddWatchlistStockModal(false);
			}
			else
			{
				notification("Cannot add this watchlist entry.",'danger');
			}
		})
	});
	  $('#stock').selectize();
</script>
<style>
    .easy-autocomplete { width: 100%; }
</style>
{!! Form::open([ 'route' => 'portal.store.watchlist', 'id' => 'WatchlistAddFrm' ]) !!}
<div class="modal-header">
     <h4 class="modal-title">Add Watchlist</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
 </div>
<div class="modal-body">
    <div class="row">

        <div class="col-12">
            <div class="form-group">
                <label>Watchlist</label>
                <input type="text" name="name" id="name" class="form-control">
              </div>        
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="submit" value="Submit" class="btn btn-primary">
</div>
{!! Form::close() !!}

<script>

    

    // setTimeout(function () { 
    //  $(".watchlist-name").easyAutocomplete(options);
    //  $('.selectpicker').selectpicker('refresh');

    //  $('.watchlist-name').val(window.watchlistname)
    // }, 200)


    $('#WatchlistAddFrm').submit(function (e) {
        e.preventDefault();

        $.post($(this).attr('action'), $(this).serialize(), function (data) {
            if(data.success != undefined)
            {
                notification("Watchlist entry added successfully.",'success');
                getWatchlistHtml('#watchlistDropdown', data.watchlist_id);
                showAddWatchlistModal(false);
            }
            else
            {
                notification("Cannot add this watchlist entry.",'danger');
            }
        })
    });
      $('#stock').selectize();
</script>
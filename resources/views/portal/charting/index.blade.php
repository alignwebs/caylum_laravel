@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style>
  .tabs-card {  }
  .tabs-card .card-header { padding: 0;  }
  .tabs-card .card-header .nav-tabs>.nav-item>.nav-link { padding: 2px; }
  .tabs-card .card-body { padding: 6px; }
  .green{color: green}
  .red{color: red}
  #fullscreen{cursor: pointer;color: #18ce0f;}
</style>
<section class="content home">
  <div class="block-header">
            {{-- <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}
                    <small>Welcome to Caylum</small>
                    </h2>
                </div>            

              </div> --}}
            </div>
            <div class="container-fluid">
              <div class="row clearfix">
                <div class="col-12 mb-3">
                  <!-- TradingView Widget BEGIN -->
                  <div class="tradingview-widget-container">
                    <div class="tradingview-widget-container__widget"></div>

                    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
                      {
                        "symbols": [
                        {
                          "title": "S&P 500",
                          "proName": "OANDA:SPX500USD"
                        },
                        {
                          "title": "Nasdaq 100",
                          "proName": "OANDA:NAS100USD"
                        },
                        {
                          "title": "EUR/USD",
                          "proName": "FX_IDC:EURUSD"
                        },
                        {
                          "title": "BTC/USD",
                          "proName": "BITSTAMP:BTCUSD"
                        },
                        {
                          "title": "ETH/USD",
                          "proName": "BITSTAMP:ETHUSD"
                        }
                        ],
                        "colorTheme": "light",
                        "isTransparent": false,
                        "displayMode": "adaptive",
                        "locale": "en"
                      }
                    </script>
                  </div>
                  <!-- TradingView Widget END -->
                </div>

                <div class="col-md-8">
                  <div class="socialfeed-post-feed-container">
                    <div class="card">
                      <div class="body">
                        <!-- TradingView Widget BEGIN -->
                        <div class="tradingview-widget-container">
                          <div id="fullscreen-container">
                                <p class="text-right">
                                  <span class="fa fa-expand" id="fullscreen"></span>
                                  <div id="tradingview_8d100"></div>
                                </p>
                          </div>
                          <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                          <script type="text/javascript">
                            new TradingView.widget(
                            {
                              "width": '100%',
                              "height": 610,
                              "symbol": "NASDAQ:AAPL",
                              "interval": "D",
                              "timezone": "Etc/UTC",
                              "theme": "Light",
                              "style": "1",
                              "locale": "en",
                              "toolbar_bg": "#f1f3f6",
                              "enable_publishing": false,
                              "withdateranges": true,
                              "hide_side_toolbar": false,
                              "allow_symbol_change": true,
                              "details": true,
                              "hotlist": true,
                              "calendar": true,
                              "news": [
                              "stocktwits",
                              "headlines"
                              ],
                              "container_id": "tradingview_8d100"
                            }
                            );
                          </script>
                        </div>
                        <!-- TradingView Widget END -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="card tabs-card">
                    <div class="card-header">

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs nav-fill">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#watchlist">Watchlist</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#news">News & Disclosure</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#stockalert">Stock Alert</a></li>
                      </ul>             
                    </div>
                    <div class="card-body">

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane in active" id="watchlist">
                          <div class="row">
                            <div class="col-9" id="watchlistDropdown"></div>
                            <div class="col-3"><button class="btn btn-sm btn-success btn-block addWatchlistBtn"><i class="fas fa-plus-circle"></i></button></div>
                          </div>
                          <div id="watchlistentries-container" class="overflow-x"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="news"> <b>News & Disclosure</b>
                          <p> Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                            Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                            pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                          sadipscing mel. </p>
                        </div>
                        <div role="tabpanel" class="tab-pane overflow-x" id="stockalert"></div>

                      </div>    
                    </div>
                  </div>

                  <div class="card tabs-card">
                    <div class="card-header">

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs nav-fill">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#activeStock">Active Stock</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activeStockBid">Active stock Bid/Ask Data</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activeStockTransaction">Active Stock Transaction</a></li>
                      </ul>             
                    </div>
                    <div class="card-body">

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane in active" id="activeStock">

                          <table class="table">
                            <thead>
                              <tr>
                                <th>PREVIOUS</th>
                                <th>LOW</th>
                                <th>52WKLOW</th>
                                <th>VOLUME</th>
                                <th># OF TRADES</th>
                                <th>OPEN</th>
                                <th>HIGH</th>
                                <th>52WKHIGH</th>
                                <th>VALUE</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>10.76</td>
                                <td class="green">10.8</td>
                                <td class="red">-9.8</td>
                                <td class="green">4.2</td>
                                <td class="green">6.8</td>
                                <td class="red">-3.2</td>
                                <td class="red">-4.7</td>
                                <td class="red">-8.9</td>
                                <td class="green">7.3</td>
                              </tr>
                              <tr>
                                <td>10.76</td>
                                <td class="green">10.8</td>
                                <td class="red">-9.8</td>
                                <td class="green">4.2</td>
                                <td class="green">6.8</td>
                                <td class="red">-3.2</td>
                                <td class="red">-4.7</td>
                                <td class="red">-8.9</td>
                                <td class="green">7.3</td>
                              </tr>
                              <tr>
                                <td>10.76</td>
                                <td class="green">10.8</td>
                                <td class="red">-9.8</td>
                                <td class="green">4.2</td>
                                <td class="green">6.8</td>
                                <td class="red">-3.2</td>
                                <td class="red">-4.7</td>
                                <td class="red">-8.9</td>
                                <td class="green">7.3</td>
                              </tr>
                              <tr>
                                <td>10.76</td>
                                <td class="green">10.8</td>
                                <td class="red">-9.8</td>
                                <td class="green">4.2</td>
                                <td class="green">6.8</td>
                                <td class="red">-3.2</td>
                                <td class="red">-4.7</td>
                                <td class="red">-8.9</td>
                                <td class="green">7.3</td>
                              </tr>
                              <tr>
                                <td>10.76</td>
                                <td class="green">10.8</td>
                                <td class="red">-9.8</td>
                                <td class="green">4.2</td>
                                <td class="green">6.8</td>
                                <td class="red">-3.2</td>
                                <td class="red">-4.7</td>
                                <td class="red">-8.9</td>
                                <td class="green">7.3</td>
                              </tr>
                            </tbody>
                          </table>

                          <div id="watchlistentries-container"></div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="activeStockBid">
                         <table class="table">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>VOL</th>
                              <th>BID</th>
                              <th>ASK</th>
                              <th>VOL</th>
                              <th>#</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>

                              <td class="green">10.8</td>
                              <td class="red">-9.8</td>
                              <td class="green">4.2</td>
                              <td class="green">6.8</td>
                              <td class="red">-3.2</td>
                              <td class="red">-4.7</td>

                            </tr>
                            <tr>

                              <td class="green">10.8</td>
                              <td class="red">-9.8</td>
                              <td class="green">4.2</td>
                              <td class="green">6.8</td>
                              <td class="red">-3.2</td>
                              <td class="red">-4.7</td>

                            </tr>
                            <tr>

                              <td class="green">10.8</td>
                              <td class="red">-9.8</td>
                              <td class="green">4.2</td>
                              <td class="green">6.8</td>
                              <td class="red">-3.2</td>
                              <td class="red">-4.7</td>

                            </tr>
                            <tr>

                              <td class="green">10.8</td>
                              <td class="red">-9.8</td>
                              <td class="green">4.2</td>
                              <td class="green">6.8</td>
                              <td class="red">-3.2</td>
                              <td class="red">-4.7</td>

                            </tr>
                            <tr>

                              <td class="green">10.8</td>
                              <td class="red">-9.8</td>
                              <td class="green">4.2</td>
                              <td class="green">6.8</td>
                              <td class="red">-3.2</td>
                              <td class="red">-4.7</td>

                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div role="tabpanel" class="tab-pane" id="activeStockTransaction">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>PER TRANSACTION TIME</th>
                              <th>VOL</th>
                              <th>PRICE</th>
                              <th>BUYER</th>
                              <th>SELLER</th>

                            </tr>
                          </thead>
                          <tbody>
                            <tr>

                              <td>10 mins 10 sec</td>
                              <td class="red">-9.8</td>
                              <td>100</td>
                              <td>xyz</td>
                              <td>abc</td>

                            </tr>
                            <tr>

                             <td>10 mins 10 sec</td>
                             <td class="red">-9.8</td>
                             <td>100</td>
                             <td>xyz</td>
                             <td>abc</td>

                           </tr>
                           <tr>

                             <td>10 mins 10 sec</td>
                             <td class="green">-9.8</td>
                             <td>100</td>
                             <td>xyz</td>
                             <td>abc</td>

                           </tr>
                           <tr>
                             <td>10 mins 10 sec</td>
                             <td class="green">-9.8</td>
                             <td>100</td>
                             <td>xyz</td>
                             <td>abc</td>

                           </tr>
                           <tr>

                             <td>10 mins 10 sec</td>
                             <td class="green">-9.8</td>
                             <td>100</td>
                             <td>xyz</td>
                             <td>abc</td>

                           </tr>
                         </tbody>
                       </table>
                     </div>

                   </div>    
                 </div>
               </div>

             </div>


           </div>
         </div>
       </section>

       @endsection


       @push('scripts')

       @include('portal.watchlist.common.script')

       <script>
        $(document).ready(function () {
          getUserStockAlerts('#stockalert');


        })

        $('#tradingview_8d100 iframe').ready(function () {

          let snapshotInterval = setInterval(function () {

            let iframe = $('#tradingview_8d100 iframe').contents();

            if(iframe.find('div.copyBtn-1oB8tEqo-').length > 0)
            {
              console.log(iframe.find('div.copyBtn-1oB8tEqo-').html());
              clearInterval(snapshotInterval);
            }

          }, 500)

        })

        $('#fullscreen').on('click', function(){
          // if already full screen; exit
          // else go fullscreen
          if (
            document.fullscreenElement ||
            document.webkitFullscreenElement ||
            document.mozFullScreenElement ||
            document.msFullscreenElement
          ) {
            if (document.exitFullscreen) {
              $("#fullscreen-container span").removeClass('fa fa-window-close')
              $("#fullscreen-container span").addClass('fa fa-expand')
              document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
              document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
              document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
              document.msExitFullscreen();
            }
          } else {
            element = $('#fullscreen-container').get(0);
            if (element.requestFullscreen) {
              $("#fullscreen-container span").removeClass('fa fa-expand')
              $("#fullscreen-container span").addClass('fa fa-window-close')
              element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
              element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
              element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if (element.msRequestFullscreen) {
              element.msRequestFullscreen();
            }
          }
        });
      </script>
      @endpush
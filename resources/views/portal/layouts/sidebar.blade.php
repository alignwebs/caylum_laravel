
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">

    
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            <div class="image"><a href="{{ route('portal.user.profile',Auth::user()->username) }}"><img src="{{ get_user_profile_pic(Auth::user()) }}" alt="User"></a></div>
                            <div class="detail">
                                <h4>{{ Auth::user()->name }}</h4>
                                <a href="{{ route('portal.user.profile',Auth::user()->username) }}" class="btn btn-primary btn-round" style="color:#fff !important;font-size: 12px;padding: 7px 10px;">View My Profile</a>
                            </div>
                            @if(!empty(Auth::user()->facebook_url))
                                <a title="facebook" href="{{ Auth::user()->facebook_url }}"><i class="zmdi zmdi-facebook"></i></a>
                            @endif
                            @if(!empty(Auth::user()->twitter_url))
                                <a title="twitter" href="{{  Auth::user()->twitter_url }}"><i class="zmdi zmdi-twitter"></i></a>
                            @endif
                            @if(!empty(Auth::user()->instagram_url))
                                <a title="instagram" href="{{ Auth::user()->instagram_url }}"><i class="zmdi zmdi-instagram"></i></a>
                            @endif
                        </div>
                    </li>

                    <li class="d-block d-sm-none">
                    
                        {!! Form::open([ 'route' => 'portal.search', 'method' => 'GET']) !!}
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..." autocomplete="off" required value="{{ app('request')->input('q')  }}">
                            <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                        </div>
                        {!! Form::close() !!}
   
                    </li>
                   
                    <li class="{{ request()->is('portal/dashboard') ? 'active open' : '' }}"> <a href="{{ route('portal.home') }}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
                     <li class=""> <a href="{{ route('portal.chart.index') }}"><i class="zmdi zmdi-chart"></i><span>Charts</span></a></li>
                    <li class=""> <a href="{{ route('portal.portfolio.profile') }}"><i class="zmdi zmdi-account-box"></i><span>Portfolio</span></a></li>
                    <li class="{{ request()->is('portal/analysis') ? 'active open' : '' }}"> <a href="{{ route('portal.watchlist.index') }}"><i class="zmdi zmdi-chart"></i><span>Analysis</span></a></li>
{{--                     <li class="{{ request()->is('portal/analysis') ? 'active open' : '' }}"> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-chart"></i><span>Analysis</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ route('portal.watchlist.index') }}">My Watchlist</a> </li>
                            <li><a href="{{ route('portal.analysis.watchlist.index') }}">Caylum Watchlist</a> </li>
                            <li><a href="{{ route('portal.historical.watchlist') }}">Historical Watchlist</a> </li>
                            <li><a href="{{ route('portal.consensus') }}">Consensus</a></li>
                            <li><a href="{{ route('portal.btm.stock') }}">BTM Stocks</a></li>
                            <li><a href="{{ route('portal.dividend') }}">Dividends</a></li>
                            <li><a href="{{ route('portal.company.buyback') }}">Company Buyback</a></li>
                            <li><a href="{{ route('portal.insider.transaction') }}">Insider Transactions</a></li>
                            <li><a href="{{ route('portal.insider.transaction.history') }}">Historical Insider Transactions</a></li>
                            <li><a href="{{ route('portal.ph.data') }}">PH Data</a></li>
                        </ul>
                    </li> --}}
                     {{-- <li class=""> <a href="{{ route('portal.watchlist.index') }}"><i class="zmdi zmdi-search"></i><span>My Watchlist</span></a></li> --}}
                     <li class=""> <a href="{{ route('portal.stock.marketSummary') }}"><i class="fas fa-exchange-alt"></i><span>Market</span></a></li>
                   {{--  <li class="{{ request()->is('portal/stock') ? 'active open' : '' }}"> <a href="javascript:void(0);" class="menu-toggle"><i class="fas fa-exchange-alt"></i><span>Market</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ route('portal.stock.realTimeMonitoring') }}">Real Time Monitoring</a> </li>
                            <li><a href="{{ route('portal.stock.marketSummary') }}">Market Summary</a></li>
                            <li><a href="{{ route('portal.stock.marketStatus') }}">Market Status</a></li>
                        </ul>
                    </li> --}}
                    <li class=""> <a href="{{ route('portal.chatroom.index') }}"><i class="zmdi zmdi-email"></i><span>Chatroom</span></a></li>
                    <li class=""> <a href="{{ route('media') }}"><i class="zmdi zmdi-image"></i><span>Media</span></a></li>
                    <li class=""> <a href="{{ setting('news_website_url') }}" target="_blank"><i class="zmdi zmdi-link"></i><span>News</span></a></li>
                    <li class="{{ request()->is('portal/setting') ? 'active open' : '' }}"> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-settings"></i><span>Setting</span></a>
                        <ul class="ml-menu">
                            <li> <a href="{{ route('user.change.password') }}">Change Password</a></li>
                            <li> <a href="{{ route('subscription.change', 2) }}">Change Plan</a></li>
                           <!--  <li><a href="#">Security</a> </li> -->
                        </ul>
                    </li>
                    
                    

                </ul>
            </div>
        
</aside>
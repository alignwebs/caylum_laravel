
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>Caylum</a></li>
        
    </ul>
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    
                    <li class="{{request()->is('register') ? 'active open' : '' }}}}"> <a href="{{ route('register') }}"><i class="zmdi zmdi-account"></i><span></span>Register</a></li>
                    <li class="{{ request()->is('login') ? 'active open' : '' }}}}"> <a href="{{ route('login') }}"><i class="zmdi zmdi-account"></i><span></span>Login</a></li>
               
                </ul>
            </div>
        </div>

    </div>    
</aside>
<!-- Top Bar -->
<nav class="navbar p-l-5 p-r-5" id="NavBar">
    <ul class="nav navbar-nav navbar-left">
        <li>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="{{ route('portal.home') }}"><img src="{{ asset('theme/images/caylum-logo-white.png') }}" alt="{{ config('app.name') }}" style="max-width: 140px;"></a>
            </div>
        </li>
        <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
        <li class="hidden-md-down"><a href="{{ route('portal.chatroom.index') }}" title="Inbox"><i class="zmdi zmdi-email"></i></a></li>
        <li class="dropdown"> <a v-on:click="markAsRead()" href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="zmdi zmdi-notifications"></i>
            <div v-if="unreadNotificationCount > 0" class="notify"><span class="heartbit"></span><span class="point"></span></div>
            </a>
            <ul class="dropdown-menu pullDown">
                <li class="body">
                    <ul class="menu list-unstyled">
                        <li v-if="!notifications.length">
                            <div class="text-center py-3" style="width: 295px;">No notification</div>
                        </li>
                        <li v-for="notification in notifications">
                            <a :href="'/portal/notification/' + notification.id">
                                <div class="media">
                                    <img class="media-object" :src="notification.data.avatar" alt="">
                                    <div class="media-body">
                                        <span class="name" v-text="notification.data.first_name"> {{-- <span class="time">30min ago</span> --}}</span>
                                        <span class="message" v-text="notification.data.text"></span>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="footer"> <a href="javascript:void(0);">View All</a> </li>
            </ul>
        </li>
        
        <li class="hidden-sm-down d-none d-xl-inline-block">
            {!! Form::open([ 'route' => 'portal.search', 'method' => 'GET']) !!}
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." autocomplete="off" required value="{{ app('request')->input('q')  }}">
                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
            </div>
            {!! Form::close() !!}
        </li>    
        <li class="ml-3 hidden-sm-down  d-none d-md-inline-block"> <a title="Facebook" href="{{ setting('facebook') }}" target="_blank"><i class="zmdi zmdi-facebook"></i></a></li>
        <li class="hidden-sm-down d-none d-md-inline-block"> <a title="Twitter" href="{{ setting('twitter') }}" target="_blank"><i class="zmdi zmdi-twitter"></i></a></li>
        <li class="hidden-sm-down d-none d-md-inline-block"> <a title="Youtube" href="{{ setting('youtube') }}" target="_blank"><i class="zmdi zmdi-youtube"></i></a></li>
        <!-- <li class="hidden-sm-down"> <a title="Sound Cloud" href="{{ setting('sound_cloud') }}" target="_blank"><i class="zmdi zmdi-soundcloud"></i></a></li>    -->
        <li class="float-right">
            <a class="" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="zmdi zmdi-power"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</nav>
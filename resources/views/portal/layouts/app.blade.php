<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link rel="stylesheet" href="{{ asset('theme/css/bootstrap.min.css') }}">
    
    <script src="https://kit.fontawesome.com/976af856af.js"></script>
    <link rel="stylesheet" href="{{ asset('theme/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/jquery-jvectormap-2.0.3.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('theme/css/bootstrap-select.css') }}" />

    <link rel="stylesheet" href="{{ asset('theme/css/morris.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme/css/authentication.css') }}">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/color_skins.css') }}">
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('theme/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />  
    <!-- Select2 -->
    {{-- <link rel="stylesheet" href="{{ asset('theme/css/select2.css') }}" /> --}}
    <link rel="stylesheet" href="{{ asset('theme/css/selectize.css') }}" />
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('plugins/emoji/css/emoji.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <link rel="stylesheet" href="{{ asset('/plugins/switchery/switchery.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme/css/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme/css/portal.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme/css/socialfeed.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/css/lightcase.css') }}">
   

    <style type="text/css">
      .media-object{width: 65px !important;}
      label { font-weight: 700; }
      .cstm-status{margin:20px 20px;}
      #NavBar .media-object { width: 51px !important; }
      .modal-content .modal-body+.modal-footer { padding: 1rem;background: #97979724; }
      .modal-content .modal-footer { justify-content: flex-end;    -webkit-justify-content: flex-end; }
      .modal-content .modal-header .title, .modal-content .modal-header .modal-title  { margin-top: 0;    font-size: 20px; }
      .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
.table thead {background: #e0e0e04f;}
.table td, .table th {
    padding: 5px;
    vertical-align: middle;
}
.font-14, .card .body, .dropdown-menu ul.menu .menu-info h4, .dropdown-menu>li>a, .chat-launcher:before, .chat-launcher:after, .inbox-widget .inbox-inner .inbox-item-info .author, .profile-page #timeline .timeline {
    font-size: 12px;
}
#analysisPagesCombo .bootstrap-select .btn.btn-round.btn-simple {
    border-color: #E3E3E3;
    padding-left: 10px;
    padding-right: 10px;
    background: #fff;
}
  #preloader {  display: none; }
  #preloader .preloader-container { width: 100%; height: 100%; position: fixed; background-color: #00000075; z-index: 999; display: flex; align-items: center; justify-content: center; }
  #preloader .preloader-container img { max-width: 70px; }

        .overflow { overflow: auto; } 
          .tabs-card .card-body { overflow: auto; } 

    .country_flag { margin-top: -2px; }

    .ls-closed .bars {
        color: #ffffff !important;
    }

    </style>
</head>
<body class="theme-purple {{ (Auth::check()) ? '' : 'authentication sidebar-collapse' }}">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="{{ asset('theme/images/logo.svg') }}" width="48" height="48" alt="Oreo"></div>
            <p>Please wait...</p>        
        </div>
    </div>
  <div id="preloader">
    <div class="preloader-container">
      <div><img src="{{ asset('theme/images/fb-load.gif') }}"></div> 
    </div>
  </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    @if(Auth::check())
    @include('portal.layouts.header')
    @include('portal.layouts.sidebar')  
        @yield('content')
    @else
        @include('portal.layouts.sidebar_nonloggedin')  
        @yield('content')
    @endif

<!-- Edit Modal -->
<div class="modal fade" id="editModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"></div>
  </div>
</div>

<!-- Add Watchlist Modal -->
<div class="modal fade" id="addWatchlistModal">
  <div class="modal-dialog">
    <div class="modal-content"></div>
  </div>
</div>

<!-- Add Watchlist Modal -->
<div class="modal fade" id="addWatchlistStockModal">
  <div class="modal-dialog">
    <div class="modal-content"></div>
  </div>
</div>

<!-- Add Stock Alert Modal -->
<div class="modal fade" id="addStockAlertModal">
  <div class="modal-dialog">
    <div class="modal-content"></div>
  </div>
</div>

<!-- Add Stock Alert List Modal -->
<div class="modal fade" id="listStockAlertModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="title">Stock Alerts</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-round waves-effect" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- FEED LIKES MODAL -->
<div class="modal fade" id="FeedLikesModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title"><b>Likes</b></h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>

{{-- <script src="{{ asset('theme/js/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->  --}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('theme/js/vendorscripts.bundle.js') }}"></script> <!-- slimscroll, waves Scripts Plugin Js -->
<script src="{{ asset('theme/js/bootstrap-notify.js') }}"></script> <!-- Bootstrap Notify Plugin Js -->

<script src="{{ asset('theme/js/index.js') }}"></script>
<script src="{{ asset('theme/js/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('theme/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.print.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script src="{{ asset('theme/js/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('theme/js/jquery-datatable.js') }}"></script>
<script src="{{ asset('theme/js/bootstrap-tagsinput.js') }}"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="{{ asset('theme/js/moment.js') }}"></script> <!-- Moment Plugin Js --> 
<script src="{{ asset('theme/js/bootstrap-material-datetimepicker.js') }}"></script> 
<script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script> 
{{-- <script src="{{ asset('theme/js/select2.min.js') }}"></script> --}}
<script src="{{ asset('theme/js/selectize.js') }}"></script> 
<script src="{{ asset('plugins/emoji/js/config.js') }}"></script>
<script src="{{ asset('plugins/emoji/js/util.js') }}"></script>
<script src="{{ asset('plugins/emoji/js/jquery.emojiarea.js') }}"></script>
<script src="{{ asset('plugins/emoji/js/emoji-picker.js') }}"></script>
<script src="{{ asset('plugins/uploadPreview/jquery.uploadPreview.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="{{ asset('theme/js/jquery.maxlength.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/lightcase.js') }}"></script>
<script src="https://www.paypal.com/sdk/js?client-id={{ env('CLIENT_ID') }}&currency=@setting('currency')"></script>


<script>
      $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '/plugins/emoji/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
    </script>

<script>
  var userId = {{ Auth::id() }};
  Echo.private('App.User.' + userId)
    .notification((notification) => {
      
        $.notify({
          // options
          message: notification.text,
      },{
          // settings
          placement: {
                  from: "bottom",
                  align: "right"
              },
          type: 'info'  
      });
    });
</script>

<script type="text/javascript">

$(document).ready(function () {
    initJs();
});

  function initJs()
  {
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
      var switchery = new Switchery(html);
    });
  }

  var ajax;
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(document).ajaxComplete(function() {

  });

    function notification(message,type)
    {
      $.notify({
          // options
          message: message,
      },{
          // settings
          placement: {
                  from: "bottom",
                  align: "right"
              },
          type: type  
      });
    }

    function editAjax(url)
      {
          $("#editModal .modal-content").html('Loading...');
            $("#editModal").modal();
            $.get(url,function(data){
              $("#editModal .modal-content").html(data);
              $('.datetimepicker').bootstrapMaterialDatePicker({
                  time:false
              });
              $('#editModal #portfolio_category').selectize({
                 create: true,
                 sortField: 'text'
              });
              $('.form-control2').selectpicker('refresh')
              
            }); 
      }

      $(document).on('submit','.deleteFrmAjax', function () {
          let poll = confirm("Confirm Delete?");

          if(poll)
          {
              $.post($(this).attr('action'),$(this).serialize(),function(data){
                    notification(data.message,'success');
                    location.reload();  
                });
          }

          return false;
      });

      $('.datetimepicker').bootstrapMaterialDatePicker({
          time:false
      });

      $(document).on('click','.addWatchlistStockBtn', function () {
            
            var watchlist_id = $("#stockwatchlistNames").val();

            if(watchlist_id > 0)
            {
              $.get('{{ route('portal.watchlist.add') }}', function (data) {

                    $('#addWatchlistStockModal .modal-content').html(data);
                    $("#watchlist_id").val(watchlist_id);
                    showAddWatchlistStockModal(true);

              })
            }
            else
            {
              alert("Select a watchlist");
            }
      });

      function showAddWatchlistStockModal(show)
      {
          if(show)
            $('#addWatchlistStockModal').modal('show');
          else
            $('#addWatchlistStockModal').modal('hide');
      } 


      $(document).on('click','.addStockAlertBtn', function () {

            let stock = $(this).data('stock');
            $.get('{{ route('portal.stockalert.add') }}', { stock_symbol:stock } ,function (data) {
                  
                  $('#addStockAlertModal .modal-content').html(data);
                  showAddStockAlertModal(true);

            })
      });

      function showAddStockAlertModal(show)
      {
          if(show)
            $('#addStockAlertModal').modal('show');
          else
            $('#addStockAlertModal').modal('hide');
      }

      function showListStockAlertModal(show, stock=null)
      {
          if(show)
          {
            $.get('{{ route('portal.stockalert.ajax.list') }}', { stock_symbol:stock } ,function (data) {

                  $('#listStockAlertModal .modal-content .modal-body').html(data);
                 
                  $('#listStockAlertModal').modal('show');

                  $('#listStockAlertModal').on('hidden.bs.modal', function (e) {
                        $('#stockwatchlistNames').trigger('change');
                    })
            })
          }
          else
            $('#listStockAlertModal').modal('hide');
      }

      function resetStockAlerts(stock)
      {
          if(ajax)
            ajax.abort();

          ajax = $.post('{{ route('portal.stockalert.delete') }}', { stock_symbol:stock, reset: true } ,function (data) {

                    $('#stockwatchlistNames').trigger('change');
                    notification("Stock alert has been resetted successfully!", 'success');
                    ajax = null;

            });
      }



      // Watchlist
      $(document).on('click','.addWatchlistBtn', function () {
            $.get('{{ route('portal.add.watchlist') }}', function (data) {

                  $('#addWatchlistModal .modal-content').html(data);
               
                  showAddWatchlistModal(true);

            })
      });

      function showAddWatchlistModal(show)
      {
          if(show)
            $('#addWatchlistModal').modal('show');
          else
            $('#addWatchlistModal').modal('hide');
      } 

      function getUserStockAlerts(target)
      {
          $(target).html('Loading ...').load('{{ route('portal.stockalert.ajax.list') }}');
      }

      function updateStockAlertStatus(alert_id)
      {
          if(ajax)
            ajax.abort();

          ajax = $.post('{{ route('portal.stockalert.ajax.update.status') }}', { alert_id:alert_id, reset: false} ,function (data) {

                notification("Alert status updated!", 'info');
                ajax = null;
          });
      }

      $(document).on('change', '.updateAlertStatusBtn', function (e) {
        
          let alertid = $(this).attr('data-alertid');
          updateStockAlertStatus(alertid);
      })

      function deleteStockAlert(e,alert_id)
      {
          if(ajax)
            ajax.abort();

          ajax = $.post('{{ route('portal.stockalert.delete') }}', { alert_id:alert_id } ,function (data) {

                notification("Stock alert has been deleted!", 'info');
                $(e).parent().parent().remove();
                ajax = null;
          });
      }
      
</script>


<script>
var chat_appid =  '{{ env('COMETCHAT_APPID') }}';
var chat_auth = '{{ env('COMETCHAT_AUTH') }}';
</script>
<?php if(!empty(Auth::id())) { ?>
 <script>
  var chat_id = "<?php echo Auth::id(); ?>";
  var chat_name = "<?php echo isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email; ?>"; 
  var chat_link = ""; 
  var chat_avatar = "<?php echo Auth::user()->thumbnail_url; ?>"; 
  var chat_role = "user"; 
  var chat_friends = '<?php echo implode(',', Auth::user()->followers()->get()->pluck('id')->toArray()) ; ?>'; // eg: 14,16,20 in case if friends feature is enabled.
  </script>
<?php } ?>
@if(!isset($chat_popup_disable))
<script>
(function() {
    var chat_css = document.createElement('link'); chat_css.rel = 'stylesheet'; chat_css.type = 'text/css'; chat_css.href = 'https://fast.cometondemand.net/'+chat_appid+'x_xchat.css';
    document.getElementsByTagName("head")[0].appendChild(chat_css);
    var chat_js = document.createElement('script'); chat_js.type = 'text/javascript'; chat_js.src = 'https://fast.cometondemand.net/'+chat_appid+'x_xchat.js'; var chat_script = document.getElementsByTagName('script')[0]; chat_script.parentNode.insertBefore(chat_js, chat_script);
})();

    $("#analysisPagesCombo select").change(function () {
       var url = $(this).val();
       window.location.href = url;
      
    });
</script>
@endif

@stack('scripts')
</body>
</html>

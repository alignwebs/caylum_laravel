@extends('portal.layouts.app')
@section('title','Home')
@section('content')
<section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}
                    </h2>
                </div>            

            </div>
        </div>
            <div class="row clearfix">
                <div class="col-lg-4 col-md-5 offset-md-4">
                    <div class="card sprice">
                        <div class="body text-center">
                         
                          	<i class="fa fa-check-circle text-success" style="font-size: 85px"></i>
                        
                            <h4 class="text-center">Completed</h4>
                            <table class="table table-hovered">
                            	<tr>
	                            	<th>Order ID</th>
	                            	<td>{{ $payment->txn_id }}</td>
                            	</tr>
                            	<tr>
	                            	<th>Name</th>
	                            	<td>{{ $payment->payer_name }}</td>
                            	</tr>
                            	<tr>
	                            	<th>Email</th>
	                            	<td>{{ $payment->payer_email }}</td>
                            	</tr>
                            	<tr>
	                            	<th>Mobile</th>
	                            	<td>{{ $payment->payer_mobile }}</td>
                            	</tr>
                            	<tr>
	                            	<th>Amount</th>
	                            	<td>{{ $payment->amount }}</td>
                            	</tr>
                            	<tr>
	                            	<th>Subscription</th>
	                            	<td>{{ $subscription->name }}</td>
                            	</tr>
                            	<tr>
	                            	<th>Fee</th>
	                            	<td>{{ $subscription->fee }}</td>
                            	</tr>
                            	<tr>
	                            	<th>Month Duration</th>
	                            	<td>{{ $subscription->month_duration }} Months</td>
                            	</tr>
                            	<tr>
	                            	<th>Date</th>
	                            	<td>{{ $payment->date }}</td>
                            	</tr>
                            </table>
                        </div>
                    </div>
                </div>
       
            </div>
        </div>   
</section>

@endsection
@push('scripts')

@endpush

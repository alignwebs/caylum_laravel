@extends('portal.layouts.app')
@section('title','Home')
@section('content')
<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
          <small>Please select a subscription plan</small>
        </h2>
      </div>

    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-6">
        <div class="card">
          <div class="body">
            @include('flash::message')
            @if($plan_expired)
            <div class="alert alert-danger">Your Subscription has been expired on
              {{ Carbon\Carbon::parse(Auth::user()->subscriber->expires_at)->format('Y-m-d') }}. Please renew your
              subscription.</div>
            @endif
            <div id="errors"></div>
            <form method="POST" action="{{ route('execute.paypal') }}" id="renewForm">
              @csrf
              <table class="table">
                <tr>
                  <th>Subscription Type:</th>
                  <td>
                    <select id="subscription_id" name="subscription_id" class="form-control"
                      placeholder="Select Subscription">
                      <option value="">Select Subscription</option>
                      @foreach($subscriptions as $subscription)
                      <option data-fee="{{ $subscription->fee }}" value="{{ $subscription->id }}">
                        {{ $subscription->complete_package }}</option>
                      @endforeach
                    </select>

                  </td>
                </tr>
                <tr>
                  <th>Subscription Fee:</th>
                  <td id="subscriptionFee">{{ Auth::user()->subscriber->subscription->fee}}</td>
                </tr>
              </table>

              <div class="footer text-center">
                <div id="paypal-button-container" style="display: none"></div>
                <button type="submit" id="submitBtn" class="btn btn-primary btn-round btn-lg btn-block ">
                  {{ __('SUBMIT') }}
                </button>
              </div>
            </form>


          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
@push('scripts')

<script>
  $("#subscription_id").change(function(){
        $("#subscriptionFee").text($(this).find(':selected').data('fee'))
    });
    var fee;

   $("#renewForm").submit(function(){

        $.post($(this).attr('action'),$(this).serialize()+"&getSubscriptionFee=true", function(data){
            
            if(data.errors != undefined)
            {
                $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
                 $(window).scrollTop(0);
            }

            if(data.success != undefined && data.fee != undefined){
                        $("#errors").html("");
                         $("#submitBtn").hide();
                         $("#paypal-button-container").show();

                          fee = data.fee;
                          paypal.Buttons({
                          style: {
                              color:   'blue',
                              shape:   'pill',
                              label:   'pay'
                          },
                         createOrder: function(data, actions) {
                           // Set up the transaction
                           return actions.order.create({
                             purchase_units: [{
                               amount: {
                                 value: fee
                               }
                             }]
                           });
                         },
                         onApprove: function(data, actions) {
                              $('#preloader').show();    
                              return actions.order.capture().then(function(details) {

                                console.log(details);
                                // Call your server to save the transaction
                                return fetch('{{ route("execute.paypal") }}', {
                                  method: 'post',
                                  headers: {
                                    'content-type': 'application/json',
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  },
                                  body: JSON.stringify({
                                    orderID: data.orderID,
                                    subscription_id: $("#subscription_id").val(),
                                    paymentTypeId: '{{ $payment_type->id }}'
                                  }),
                                  
                                }).then(response => { response.json(); $('#preloader').hide(); })
                                  .then((body) => {
                                      $('#preloader').hide();
                                    let url = body.url;
                                    if(url != undefined)
                                    {
                                        window.location.href = url;
                                    }
                                  });
                            
                        });
                      }
                       }).render('#paypal-button-container');
            }
        });
             return false;
    })

  
</script>

@endpush
@extends('portal.layouts.app')
@section('title', $title)
@section('content')

<section class="content profile-page">

    <div class="container-fluid">
        @include('portal.social.profile.common.header')
        <div class="row clearfix">
            @include('portal.social.profile.common.sidebar')
            <div class="col-lg-8 col-md-12">

              @if(Auth::id() == $user->id)
                  <div class="socialfeed-post-feed-container">
                      <div class="card">
                          <div class="body">
                              {!! Form::open(['route' => ['feed.store'], 'id' => 'FeedPostFrm', 'files' => true]) !!}
                                  <textarea id="1" data-emojiable="true" name="feed_status_content" id="feed_status_content" class="socialfeed-post-feed-textarea form-control" placeholder="Share your Thoughts...">@php echo (Auth::id() != $user->id ? "@$user->username &nbsp;" : "") @endphp</textarea>
                                  <div class="socialfeed-post-feed-actions">
                                      <div class="row">
                                          <div class="col-6">
                                              
                                              <div id="image-preview">
                                                  <a href="javascript:void(0)" class="remove-image" onclick="clearImageUplaod()"><i class="fas fa-times"></i></a>
                                                <input type="file" name="feed_status_image" id="feed-image-upload" />
                                              </div>
                                              <button class="btn btn-default btn-sm" onclick="$('#feed-image-upload').click()" type="button"><i class="material-icons">camera_alt</i></button>
                                          </div>
                                          <div class="col-6 text-right">
                                              <button class="btn btn-raised btn-primary waves-effect" type="submit" id="FeedPostFrmBtn">Post</button>
                                          </div>
                                      </div>
                                     
                                  </div>
                              {!! Form::close() !!}
                          </div>
                      </div>
                  </div>

                  @endif
                  
                  <div class="primary-panel-heading">{{ $user->first_name }}'s Timeline</div>

                  <input type="hidden" id="profile_user_id" value="{{ $user->id }}">
                  @include('portal.social.feed.feeds')
        
            </div>
        </div>
    </div>
</section>

@endsection


@push('scripts')
@include('portal.social.profile.common.scripts')

<script type="text/javascript">
    $.uploadPreview({
        input_field: "#feed-image-upload",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: true,                 // Default: false
        success_callback: function() {
          $('#image-preview').show();
        }
      });

    function clearImageUplaod()
    {
        $('#feed-image-upload').val('');
        $('#image-preview').hide();
    }



    $('#FeedPostFrm').submit(function (e) {
        e.preventDefault();
        $("#FeedPostFrmBtn").prop('disabled', true);
        let action = $(this).attr('action');
        var form = $(this)[0];
        var data = new FormData(form);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: action,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    window.location.reload();
                },
                error: function (e) {
                    $("#FeedPostFrmBtn").prop('disabled', false);
                    alert("ERROR : " + e);
                }
            });

    });

</script>
@endpush
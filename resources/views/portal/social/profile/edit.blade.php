@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style>
    .primary-panel-heading { color: #8b99e0; font-size: 20px;    font-weight: 500;    border-bottom: 1px solid #cccccc4a;padding-bottom: 10px; }

    .socialfeed-post-feed-container {}
    .socialfeed-post-feed-container form {}
    .socialfeed-post-feed-container form .socialfeed-post-feed-textarea {     border: 1px solid #d3d3d3;padding: 8px 10px;border-radius: 4px;resize: vertical; }
    .socialfeed-post-feed-container form .emoji-picker-icon { right: 25px; top: 26px; }
    .socialfeed-post-feed-container form .emoji-menu .emoji-items-wrap {    overflow: auto; }
    .socialfeed-post-feed-container form .socialfeed-post-feed-actions { margin-top: 5px; }
    .socialfeed-post-feed-container form #image-preview { width: 100px;height: 100px; margin: 10px 0; display: none; position: relative;}
    .socialfeed-post-feed-container form #image-preview input {position: fixed;left: -500px;}
    .socialfeed-post-feed-container form #image-preview .remove-image { position: absolute; right: 4px; }
    .socialfeed-feed-single-container { margin: 10px 0; }
    .socialfeed-feed-single-container .social-feed-header {    position: relative;     height: 55px; margin-bottom: 5px; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-user-avatar {  }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-user-avatar img  {  width: 50px;height: 50px;    position: absolute;left: 0; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-user-name {     position: absolute;left: 65px;font-size: 18px;color: #000;font-weight: 500; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-username {     position: absolute;left: 65px;top: 28px;color: #878787; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-timestamp {     position: absolute; right: 0; font-size: 12px; display: flex; color: #878787;    align-items: center; }
    .socialfeed-feed-single-container .social-feed-body {  }
    .socialfeed-feed-single-container .social-feed-footer {  }
    .socialfeed-feed-single-container .social-feed-footer .social-feed-actions {  }
    .socialfeed-feed-single-container .social-feed-footer .social-feed-post-comment {  }
    .socialfeed-feed-single-container .social-feed-footer .social-feed-post-comment .emoji-picker-icon { right: 20px; }

    .list-thumbnail {  }
    .list-thumbnail .user-avatar { max-width: 65px; }
</style>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{{ $title }}
                </h2>
            </div>

        </div>
    </div>
    <div class="container-fluid">        

        <!-- #END# Vertical Layout -->        
        <!-- Horizontal Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Edit</strong> Profile</h2>

                    </div>
                    <div class="body">
                         {!! Form::open(['route' => ['portal.user.profile.update'], 'id' => 'updateProfileForm', 'files' => true,'class'=>'form-horizontal']) !!}
                        
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="profile_pic">Profile Picture</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <div class="form-group">
                                           <img src="{{ get_image_path($user->profile_pic) }}" width="100">
                                    </div>
                                        <div class="form-group">
                                            <input type="file" id="profile_pic" name="profile_pic" class="form-control">
                                        </div>
                                </div>
                            
                            </div>
                            <div class="row clearfix">
                               <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="cover_pic">Cover Photo</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <div class="form-group">
                                       <img src="{{ get_image_path($user->cover_pic) }}" width="100">
                                    </div>
                                    <div class="form-group">
                                        <input type="file" id="cover_pic" name="cover_pic" class="form-control">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row clearfix">
                               <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="username">Username</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" id="username" name="username" class="form-control" value="{{ $user->username }}" placeholder="Username">
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-4 form-control-label">
                                     <label for="facebook_url">Facebook</label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-8">
                                     <div class="form-group">
                                         <input type="text" id="facebook_url" name="facebook_url" class="form-control" value="{{ $user->facebook_url }}" placeholder="Facebook Url">
                                     </div>
                                 </div>
                                

                            </div>
                            <div class="row clearfix">
                               <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="display_name">Display Name</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" id="display_name" name="display_name" class="form-control" value="{{ $user->display_name }}" placeholder="Display Name">
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-4 form-control-label">
                                     <label for="twitter_url">Twitter</label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-8">
                                     <div class="form-group">
                                         <input type="text" id="twitter_url" name="twitter_url" class="form-control" value="{{ $user->twitter_url }}" placeholder="Twitter Url">
                                     </div>
                                 </div>
                            </div>
                            <div class="row clearfix">
                               <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="bio">Bio</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <div class="form-group">
                                        <textarea class="form-control" name="bio" id="bio"  placeholder="About Yourself" style="margin-bottom: 5px">{{ $user->bio }}</textarea>
                                       <span class="chars-count badge badge-pill badge-primary"></span> characters used<br>
                                       <span class="chars-remaining badge badge-pill badge-info"></span> characters remaining<br>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-4 form-control-label">
                                     <label for="instagram_url">Instagram</label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-8">
                                     <div class="form-group">
                                         <input type="text" id="instagram_url" name="instagram_url" class="form-control" value="{{ $user->instagram_url }}" placeholder="Instagram Url">
                                     </div>
                                 </div>
                            </div>
                            <div class="row clearfix">
                               <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="country">Country</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <div class="form-group">
                                       {!! Form::select('country',$countries,$user->country,['class'=>""]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                
                                <div class="col-sm-8 offset-sm-2">
                                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect" style="z-index: 1;">Update</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
</section>

@endsection


@push('scripts')
<script>

    function toggleFollow(loggedInUserId,user_id)
    {
        $.post("{{ route('portal.user.profile.toggleFollow') }}",{ loggedInUserId:loggedInUserId,user_id:user_id}, function(data){
          
            if(data.success == 'true')
            { 
                notification(data.message,'success')
                location.reload();
            }else{

                notification("Something went wrong",'danger')

            }

 

        });
    }

    $('#updateProfileForm').submit(function (e) {
        e.preventDefault();

        let action = $(this).attr('action');
        var form = $(this)[0];
        var data = new FormData(form);

        console.log($(this).serialize())

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: action,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    if(data.success == true)
                    {   
                        notification(data.message,'success')
                        location.reload();
                    }else{

                        notification(data.errors,'danger')
                    }
                    // window.location.reload();
                },
                error: function (e) {
                    
                    alert("ERROR : " + e);
                }
            });

    });

    $(function(){
      $("#bio").maxLength({
        maxChars: 300
      });
    });
</script>

@endpush
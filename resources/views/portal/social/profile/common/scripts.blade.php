<script>

      var xhr;

      function toggleFollow(user_id, username)
      {

        if(xhr && xhr.readyState != 4){
            xhr.abort();
        }

          xhr = $.post("{{ route('portal.user.profile.toggleFollow') }}",{user_id:user_id}, function(data){

              if(data.success == 'true')
              { 
                      if(data.action == 'followed')
                          $("#followUnfollow").html('<button class="btn btn-primary btn-round btn-simple" onclick="toggleFollow({{ $user->id }})">Unfollow</button>')
                                                                     
                      if(data.action == 'unfollowed')
                          $("#followUnfollow").html('<button class="btn btn-primary btn-round" onclick="toggleFollow({{ $user->id }})">Follow</button>');
                      
                      notification(data.message,'success');

              } else {

                  notification("Something went wrong",'danger')

              }

          });

      }
  
</script>
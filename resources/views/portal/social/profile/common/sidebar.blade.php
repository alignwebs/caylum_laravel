 <style type="text/css">
    .right_chat .media .message {
    color: #78909c;
    max-width: 100%;
    white-space: normal;
    overflow: auto;
}
 </style>
            <div class="col-lg-4 col-md-12">

                <div class="card">
                    <ul class="row profile_state list-unstyled">
                          <li class="col-lg-4 col-md-4 col-6">
                            <div class="body cstm_body">
                                <a href="{{ route('portal.user.profile',$user->username) }}">
                                    <i class="zmdi zmdi-account"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="{{ $data['posts'] }}" data-speed="1000" data-fresh-interval="700">{{ $data['posts'] }}</h5>
                                    <small>Posts</small>
                                </a>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-6">
                            <div class="body cstm_body">
                                <i class="zmdi zmdi-thumb-up"></i>
                                <h5 class="m-b-0 number count-to" data-from="0" data-to="{{ $data['likes'] }}" data-speed="1000" data-fresh-interval="700">{{ $data['likes'] }}</h5>
                                <small>Likes</small>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-6">
                            <div class="body cstm_body">
                                <a href="{{ route('portal.user.profile.following',$user->username) }}">
                                    <i class="zmdi zmdi-desktop-mac"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="{{ count($user->followings()->get()) }}" data-speed="1000" data-fresh-interval="700">{{ count($user->followings()->get()) }}</h5>
                                    <small>Following</small>
                                </a>
                            </div>
                        </li>
                      
                        <li class="col-lg-4 col-md-4 col-6" style="color: #888888">
                            <div class="body cstm_body">
                                <a href="{{ route('portal.user.profile.photos', $user->username) }}">
                                    <i class="zmdi zmdi-camera"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="{{ $data['photos'] }}" data-speed="1000" data-fresh-interval="700">{{ $data['photos'] }}</h5>
                                    <small>Photos</small>
                                </a>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-6">
                            <div class="body cstm_body">
                                <i class="zmdi zmdi-comment-text"></i>
                                <h5 class="m-b-0 number count-to" data-from="0" data-to="{{ $data['comments'] }}" data-speed="1000" data-fresh-interval="700">{{ $data['comments'] }}</h5>
                                <small>Comments</small>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-6">
                            <div class="body cstm_body">
                                <a href="{{ route('portal.user.profile.followers',$user->username) }}">
                                    <i class="zmdi zmdi-attachment"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="{{ count($user->followers()->get())}}" data-speed="1000" data-fresh-interval="700">{{ count($user->followers()->get())}}</h5>
                                    <small>Followers</small>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            
               {{--  <div class="card">
                    <div class="card-body">
                        
                            <small class="text-muted">Email address: </small>
                            <p>{{ $user->email }}</p>
                            <hr>
                          
                            <small class="text-muted">Mobile: </small>
                            <p>{{ $user->mobile }}</p>
                            <hr>
                            <small class="text-muted">Birth Date: </small>
                            <p class="m-b-0">{{ $user->birthday }}</p>
                        
                    </div>
                </div> --}}
                
                <div class="card">
                    <div class="header">
                        <h2><strong>Who</strong> to follow</h2>
                      
                    </div>
                    <div class="body">
                        <ul class="right_chat list-unstyled">
                            @if(sizeof($user->followers()->get()) > 0)
                            @foreach($user->followers()->get() as $follower)
                            <li class="online">
                                <a href="{{ route('portal.user.profile',$follower->username) }}">
                                    <div class="media">
                                        <img class="media-object cstm-media-object" src="{{ get_user_profile_pic($follower) }}" alt="">
                                        <div class="media-body">
                                            <span class="name">{{ $follower->name }}</span>
                                            <span class="message">{{ Str::limit($follower->bio, 100) }}</span>
                                            @if($follower->is_online)
                                                <span class="badge badge-outline status cstm-status"></span>
                                            @endif
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            @endforeach
                            @else
                                <p>No followers yet</p>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
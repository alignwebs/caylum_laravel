       
<style type="text/css">
    .card .cstm_body{color: #888888 !important}
    .profile-page:before { background: url('{{ get_image_path($user->cover_pic) }}'); background-size: cover;height: 275px !important; }
</style>
        <div class="row clearfix">
            <div class="col-xl-5 col-lg-5 col-md-12">
                <div class="card profile-header">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="profile-image text-center" data-fancybox="gallery" href="{{ get_user_profile_pic($user) }}" style="cursor: pointer;"> <img src="{{ get_user_profile_pic($user, true) }}" alt=""> </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-12">
                                <h4 class="m-t-0 m-b-0"><strong>{{ $user->name }}</strong>
                                    @if($user->country)
                                     <img class="country_flag" src="{{ getCountryFlag($user->country, 32) }}" alt="">
                                    @endif
                                </h4>
                                <span class="job_post">({{ $user->username }})</span>
                                <div class="social_icons">
                                    @if(!empty($user->facebook_url))
                                        <a title="facebook" href="{{ $user->facebook_url }}"><i class="zmdi zmdi-facebook"></i></a>
                                    @endif
                                    @if(!empty($user->twitter_url))
                                        <a title="twitter" href="{{  $user->twitter_url }}"><i class="zmdi zmdi-twitter"></i></a>
                                    @endif
                                    @if(!empty($user->instagram_url))
                                        <a title="instagram" href="{{ $user->instagram_url }}"><i class="zmdi zmdi-instagram"></i></a>
                                    @endif
                                </div>
                                <p> {{ $user->bio }}</p>

                                @if(Auth::id() != $user->id)
                               
                                <div>
                                    <div id="followUnfollow" style="display: inline">
                                       @if(Auth::user()->isFollowing($user))
                                           <button class="btn btn-primary btn-round btn-simple" onclick="toggleFollow({{$user->id}},'{{ $user->username }}')">Unfollow</button>
                                       @else
                                           <button class="btn btn-primary btn-round" onclick="toggleFollow({{$user->id}},'{{ $user->username }}')">Follow</button>
                                        @endif
                                    </div>
                                    {{-- <button class="btn btn-primary btn-round btn-simple">Message</button> --}}
                                </div>
                                @else

                                <div>
                                    <a href="{{ route('portal.user.profile.edit',Auth::user()->username) }}" class="btn btn-primary btn-round" style="color: #fff">Edit Profile</a>
                                </div>
                                @endif
                            
                            </div>                
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
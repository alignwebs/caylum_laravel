@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style>
    .primary-panel-heading { color: #8b99e0; font-size: 20px;    font-weight: 500;    border-bottom: 1px solid #cccccc4a;padding-bottom: 10px; }

    .socialfeed-post-feed-container {}
    .socialfeed-post-feed-container form {}
    .socialfeed-post-feed-container form .socialfeed-post-feed-textarea {     border: 1px solid #d3d3d3;padding: 8px 10px;border-radius: 4px;resize: vertical; }
    .socialfeed-post-feed-container form .emoji-picker-icon { right: 25px; top: 26px; }
    .socialfeed-post-feed-container form .emoji-menu .emoji-items-wrap {    overflow: auto; }
    .socialfeed-post-feed-container form .socialfeed-post-feed-actions { margin-top: 5px; }
    .socialfeed-post-feed-container form #image-preview { width: 100px;height: 100px; margin: 10px 0; display: none; position: relative;}
    .socialfeed-post-feed-container form #image-preview input {position: fixed;left: -500px;}
    .socialfeed-post-feed-container form #image-preview .remove-image { position: absolute; right: 4px; }
    .socialfeed-feed-single-container { margin: 10px 0; }
    .socialfeed-feed-single-container .social-feed-header {    position: relative;     height: 55px; margin-bottom: 5px; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-user-avatar {  }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-user-avatar img  {  width: 50px;height: 50px;    position: absolute;left: 0; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-user-name {     position: absolute;left: 65px;font-size: 18px;color: #000;font-weight: 500; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-username {     position: absolute;left: 65px;top: 28px;color: #878787; }
    .socialfeed-feed-single-container .social-feed-header .socialfeed-feed-timestamp {     position: absolute; right: 0; font-size: 12px; display: flex; color: #878787;    align-items: center; }
    .socialfeed-feed-single-container .social-feed-body {  }
    .socialfeed-feed-single-container .social-feed-footer {  }
    .socialfeed-feed-single-container .social-feed-footer .social-feed-actions {  }
    .socialfeed-feed-single-container .social-feed-footer .social-feed-post-comment {  }
    .socialfeed-feed-single-container .social-feed-footer .social-feed-post-comment .emoji-picker-icon { right: 20px; }

    .list-thumbnail {  }
    .list-thumbnail .user-avatar { max-width: 65px; }
 
    
</style>
<section class="content profile-page">
{{--     <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{{ $title }}
                <small>Welcome to Caylum</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Caylum</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ul>                
            </div>
        </div>
    </div>   --}}  
    <br>
    <div class="container-fluid">
        @include('portal.social.profile.common.header')
        <div class="row clearfix">
            @include('portal.social.profile.common.sidebar')
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Followers</strong></h2>
                      
                    </div>
                    <div class="body">
                        <ul class="right_chat list-unstyled">
                            @if(sizeof($followers) > 0)
                            @foreach($followers as $follower)
                             @if($user->id != $follower->id)
                            <li class="online">
                               
                                    <div class="media">
                                        <a href="{{ route('portal.user.profile',$follower->username) }}"><img class="media-object cstm-media-object" src="{{ get_user_profile_pic($follower) }}" alt=""></a>
                                        <div class="media-body">
                                         <a href="{{ route('portal.user.profile',$follower->username) }}">
                                            <span class="name">{{ $follower->name }}</span>
                                         </a> 
                                            <span>
                                                <div id="followUnfollowUser{{$follower->id}}" style="display: inline">
                                                    @if(Auth::id() != $follower->id)
                                                       @if(Auth::user()->isFollowing($follower))
                                                           <button class="btn btn-primary btn-round btn-simple" onclick="toggleFollowUser({{ Auth::id()}},{{$follower->id}})">Unfollow</button>
                                                       @else
                                                           <button class="btn btn-primary btn-round" onclick="toggleFollowUser({{ Auth::id()}},{{$follower->id}})">Follow</button>
                                                        @endif
                                                    @endif
                                                </div>
                                            </span>
                                            @if($follower->is_online)
                                                <span class="badge badge-outline status cstm-status"></span>
                                            @endif
                                        </div>
                                    </div>
                                                       
                            </li>
                            @endif
                            @endforeach
                            @else
                                <p>No followers yet</p>
                            @endif
                        </ul>
                    </div>
                    {{ $followers->links() }} 
                </div>
               
   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


@push('scripts')
@include('portal.social.profile.common.scripts')

@endpush
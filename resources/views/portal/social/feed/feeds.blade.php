<div class="socialfeed-feed-container" id="FeedVueApp">
  <div class="socialfeed-feed-single-container">
    <div v-if="!feeds">
      <h4 class="text-center">No Posts Found.</h4>
    </div>

    <div class="card no-overflow" v-for="(feed, feed_index) in feeds">
      <div class="body no-overflow">
        <div class="social-feed-header">
          <a :href="feed.user.profile_url" class="socialfeed-feed-user-avatar">
            <img :src="feed.user.thumbnail_url"> 
          </a>
          <a :href="feed.user.profile_url" class="socialfeed-feed-user-name" v-text="feed.user.display_name"></a>
          <small class="socialfeed-feed-username" v-text="feed.user.username"></small>
          <a href="#" class="socialfeed-feed-timestamp" v-text="feed.posted_at"><i class="material-icons">timer</i> about 6 hours ago</a>
        </div>

        <div class="social-feed-body">
          <div v-html="feed.feed_content_html" class="mb-2"></div>
          <div v-if="feed.attachment">
            <div class="" data-fancybox="gallery" :href="feed.attachment.file_url" style="cursor: pointer;">
              <img :src="feed.attachment.thumbnail_url" class="img-fluid">
            </div>
          </div>
        </div>

        <div class="social-feed-footer">
          <div class="social-feed-actions">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="javascript:" v-if="!feed.is_liked" @click="like(feed.id)"><i class="far fa-heart"></i> Like</a>
                <a href="javascript:" v-if="feed.is_liked" @click="like(feed.id)"><i class="fas fa-heart"></i> Liked</a>
                <small v-if="feed.likes_count > 0"> <a href="javascript:" @click="getFeedLikes(feed.id)">(@{{ feed.likes_count }})</a></small>
              </li>
              <li class="list-inline-item"><a href="javascript:"><i class="far fa-comment"></i> Comments (@{{ feed.comments.length }})</a></li>
             {{--  <li v-if="feed.user_id != user_id" class="list-inline-item"><a href="javascript:"><i class="fas fa-retweet"></i> Repost</a></li> --}}
             {{--  <li class="list-inline-item"><a href="javascript:"><i class="far fa-share-square"></i> Report</a></li> --}}
              <li v-if="feed.user_id == user_id" class="list-inline-item"><a @click="deleteFeed(feed_index, feed.id)" href="javascript:" class="text-danger"><i class="fas fa-trash"></i> Delete Post</a></li>
            </ul>
          </div>

          <div class="social-feed-post-comment">
            <div class="feed-comments-container" v-if="feed.comments.length > 0">
             <div class="comment" v-for="(comment, index) in feed.comments">
              <div class="user-profile-pic">
                <a :href="comment.commentator.profile_url"><img :src="comment.commentator.thumbnail_url"></a>
              </div>
              <div class="comment-container">
                <small v-text="comment.created_at" class="float-right"></small>
                <a class="user-name" :href="comment.commentator.profile_url" v-text="comment.commentator.display_name"></a> <small v-text="'@'+comment.commentator.username"></small>
                <span v-if="comment.commentator.id == user_id"><a href="javascript:" @click="deleteComment(index, comment.id)">Delete</a></span>
                <div class="comment-text" v-html="comment.comment"></div>
              </div>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-lg-9 col-sm-12 col-12">
              <textarea :id="'comment-feed-' + feed.id"  data-emojiable="true" class="comment-box form-control" placeholder="Write a comment...."></textarea>
            </div>
            <div class="col-lg-3 col-sm-12 col-12">
              <button class="btn btn-primary btn-block" @click="postComment(feed.id)">Reply</button>
            </div>
          </div> 
        </div>
      </div>

    </div>
  </div>

  <infinite-loading @infinite="getUserFeed"></infinite-loading>
</div>

</div>
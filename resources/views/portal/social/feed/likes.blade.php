<ul class="list-unstyled list-thumbnail">
  @if($likes->count() > 0)
  @foreach($likes as $liker)
       
            <li class="media">
             <a href="{{ $liker->profile_url }}"> <img src="{{ get_user_profile_pic($liker) }}" class="mr-3 user-avatar"></a>
              <div class="media-body">
              <h6 class="mt-0 mb-1"><a href="{{ $liker->profile_url }}">{{ $liker->name }}</a></h6>
                <div><small>{{ $liker->bio }}</small></div>
                @if($user->id != $liker->id)
                <div id="followUnfollowUser{{$liker->id}}" style="display: inline">
                    @if($user->isFollowing($liker))
                       <button class="btn btn-sm btn-primary btn-simple" onclick="toggleFollowUser({{ Auth::id()}},{{$liker->id}})">Unfollow</button>
                    @else
                            <button class="btn btn-sm btn-primary" onclick="toggleFollowUser({{ Auth::id()}},{{$liker->id}})">Follow</button>
                     @endif
                  </div>
                  @endif
                 @if($liker->is_online)
                     <span class="badge badge-outline status cstm-status"></span>
                 @endif
              </div>
            </li>
        
  @endforeach
  @else
      <p>No Likes Yet</p>
  @endif
</ul>
@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style>
    iframe{width: 100% !important; height:135px;}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>Hello {{ $user->first_name }}, How are you today?</h2>
                </div>            

            </div>
        </div>
<div class="container-fluid">
        <div class="row clearfix">

            <div class="col-md-8">
                <div class="socialfeed-post-feed-container">
                    <div class="card">
                        <div class="body">
                            {!! Form::open(['route' => ['feed.store'], 'id' => 'FeedPostFrm', 'files' => true]) !!}
                                <textarea id="1" data-emojiable="true" name="feed_status_content" id="feed_status_content" class="socialfeed-post-feed-textarea form-control" placeholder="Share your Thoughts..."></textarea>
                                <div class="socialfeed-post-feed-actions">
                                    <div class="row">
                                        <div class="col-6">
                                            
                                            <div id="image-preview">
                                                <a href="javascript:void(0)" class="remove-image" onclick="clearImageUplaod()"><i class="fas fa-times"></i></a>
                                              <input type="file" name="feed_status_image" id="feed-image-upload" />
                                            </div>
                                            <button class="btn btn-default btn-sm" onclick="$('#feed-image-upload').click()" type="button"><i class="material-icons">camera_alt</i></button>
                                        </div>
                                        <div class="col-6 text-right">
                                            <button class="btn btn-raised btn-primary waves-effect" type="submit" id="FeedPostFrmBtn">Post</button>
                                        </div>
                                    </div>
                                   
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                
                <div class="primary-panel-heading">Latest Updates</div>

                
                @include('portal.social.feed.feeds')
                
            </div>
            <div class="col-md-4">
                <div class="card">
                     <div class="card-body">
                        <div class="ad-block">
                            @if(!empty(setting('ad_image')))
                                <a href="{{ setting('ad_link') }}"><img src="{{ asset('storage/'.setting('ad_image')) }}"></a>
                            @endif
                        </div> 
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Latest Poll</div>
                    <div class="card-body">
                            <div id="qp_all2469425" style="width:100%;max-width:600px;"><link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'><STYLE>#qp_main2469425 .qp_btna:hover input {background:#566e7a!important} #qp_all2469425 {max-width:815px; margin:0 auto;}</STYLE><div id="qp_main2469425" fp='6cBae301-70' results=0 style="border-radius:0px;margin:0 auto 10px auto;padding:0.8em;background-color:#FFF;font-family: 'Open Sans', sans-serif, Arial;color:#658a9d;border: 1px solid #DBD9D9;max-width:815px;box-sizing:border-box;text-align:left"><div style="font-size:1.5em;border-bottom:1px solid #566e7a; color:#000;font-family:'Open Sans', sans-serif, Arial"><div style="padding:0.8em;line-height:1.3em">The PSEi is on its way to 8,600.</div></div><form id="qp_form2469425" action="//www.poll-maker.com/results2469425x6cBae301-70" method="post" target="_blank" style="display:inline;margin:0px;padding:0px"><div style="padding:0px"><input type=hidden name="qp_d2469425" value="43699.8373958363-43699.8373820622"><div style="display:block;color:#6B6B6B;font-family: 'Open Sans', sans-serif, Arial;font-size:1em;line-height:1.5;padding:13px 8px 11px;margin:10px 0px;clear:both" class="qp_a" onClick="var c=this.getElementsByTagName('INPUT')[0]; if((!event.target?event.srcElement:event.target).tagName!='INPUT'){c.checked=(c.type=='radio'?true:!c.checked)};var i=this.parentNode.parentNode.parentNode.getElementsByTagName('INPUT');for(var k=0;k!=i.length;k=k+1){i[k].parentNode.parentNode.setAttribute('sel',i[k].checked?1:0)}"><span style="display:block;padding-left:30px;cursor:inherit">
                                <input style="float:left;width:20px;margin-left:-25px;margin-top:2px;padding:0px;height:20px;-webkit-appearance:radio;" name="qp_v2469425" type="radio" value="1" />Agree</span></div><div style="display:block;color:#6B6B6B;font-family: 'Open Sans', sans-serif, Arial;font-size:1em;line-height:1.5;padding:13px 8px 11px;margin:10px 0px;clear:both" class="qp_a" onClick="var c=this.getElementsByTagName('INPUT')[0]; if((!event.target?event.srcElement:event.target).tagName!='INPUT'){c.checked=(c.type=='radio'?true:!c.checked)};var i=this.parentNode.parentNode.parentNode.getElementsByTagName('INPUT');for(var k=0;k!=i.length;k=k+1){i[k].parentNode.parentNode.setAttribute('sel',i[k].checked?1:0)}"><span style="display:block;padding-left:30px;cursor:inherit"><input style="float:left;width:20px;margin-left:-25px;margin-top:2px;padding:0px;height:20px;-webkit-appearance:radio;" name="qp_v2469425" type="radio" value="2" />Disagree</span></div></div><div style="padding-left:0px;clear:both;text-align:left;margin:1em auto"><a style="display:block;white-space: normal; box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-o-box-sizing:border-box;padding-right:5px;text-decoration:none" class="qp_btna" href="#"><input name="qp_b2469425" style="min-width:100%;padding:0.5em;background-color:#566e7a;font-family: 'Open Sans', sans-serif, Arial;font-size:16px;color:#FFF;cursor:pointer;cursor:hand;border:0px;-webkit-appearance:none;border-radius:0px" type="submit" btype="v" value="Vote" /></a><a style="display:block;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-o-box-sizing:border-box;padding-left:5px;text-decoration:none" class="qp_btna" href="#"><input name="qp_b2469425" style="min-width:100%;padding:0.5em;background-color:#566e7a;font-family: 'Open Sans', sans-serif, Arial;font-size:16px;color:#FFF;cursor:pointer;cursor:hand;border:0px;-webkit-appearance:none;border-radius:0px; margin-left:-5px;margin-top: 10px;" type="submit" btype="r" value="Results" /></a></div></form><div style="display:none"><div id="qp_rp2469425" style="font-size:14px;width:5ex;text-align:right;overflow:hidden;position:absolute;right:5px;height:1.5em;line-height:1.5em"></div><div id="qp_rv2469425" style="font-size:14px;line-height:1.5em;width:0%;text-align:right;color:#FFF;box-sizing:border-box;padding-right:3px"></div><div id="qp_rb2469425" style="font-size:14px;line-height:1.5em;color:#FFFFFF;display:block;padding-right:10px 5px"></div><div id="qp_rva2469425" style="background:#006FB9;border-color:#006FB9"></div><div id="qp_rvb2469425" style="background:#163463;border-color:#163463"></div><div id="qp_rvc2469425" style="background:#5BCFFC;border-color:#1481AB"></div></div></div></div><script src="//scripts.poll-maker.com/3012/scpolls.js" language="javascript"></script>
                    </div>
                </div>
                 <div class="card">
                    <div class="card-header">Watch this</div>
                    <div class="card-body text-center" style="padding: 0px;margin: 0px;line-height: 0px;overflow: hidden;">
                        <iframe  style="padding: 0px; margin: 0; height: 295px;" src="https://www.youtube.com/embed/{{ setting('youtube_url') }}?controls=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Suggested People to Follow</div>
                    <div class="card-body">
                        <ul class="list-unstyled list-thumbnail">
                            @if($suggested_followings->count() > 0)
                            @foreach($suggested_followings as $following)
                                 @if($user->id != $following->id)
                                      <li class="media">
                                       <a href="{{ route('portal.user.profile',$following->username) }}"> <img src="{{ get_user_profile_pic($following) }}" class="mr-3 user-avatar"></a>
                                        <div class="media-body">
                                        <h6 class="mt-0 mb-1"><a href="{{ route('portal.user.profile',$following->username) }}">{{ $following->name }}</a> 
                                            @if($following->country)
                                            <img class="country_flag" src="{{ getCountryFlag($following->country) }}" alt="">
                                            @endif
                                        </h6>
                                          <div><small>{{ Str::limit($following->bio, 100) }}</small></div>
                                          <div id="followUnfollowUser{{$following->id}}" style="display: inline">
                                              @if($user->isFollowing($following))
                                                 <button class="btn btn-sm btn-primary btn-simple" onclick="toggleFollowUser({{ Auth::id()}},{{$following->id}})">Unfollow</button>
                                              @else
                                                      <button class="btn btn-sm btn-primary" onclick="toggleFollowUser({{ Auth::id()}},{{$following->id}})">Follow</button>
                                               @endif
                                            </div>
                                           @if($following->is_online)
                                               <span class="badge badge-outline status cstm-status"></span>
                                           @endif
                                        </div>
                                      </li>
                                  @endif
                            @endforeach
                            @else
                                <p>No Suggestion.</p>
                            @endif
                          </ul>
                
                          {{-- <div class="text-center"><a href="{{ route('portal.user.profile.followers',$user->username) }}" class="btn btn-secondary">View All</a></div> --}}
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</section>
<iframe src="{!! getWpNewsLoginUrl() !!}" width="0" height="0" frameborder="0" style="display: none;"></iframe>
@endsection


@push('scripts')
<script>
$.uploadPreview({
    input_field: "#feed-image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: true,                 // Default: false
    success_callback: function() {
      $('#image-preview').show();
    }
  });

function clearImageUplaod()
{
    $('#feed-image-upload').val('');
    $('#image-preview').hide();
}



$('#FeedPostFrm').submit(function (e) {
    e.preventDefault();
    $("#FeedPostFrmBtn").prop('disabled', true);
    let action = $(this).attr('action');
    var form = $(this)[0];
    var data = new FormData(form);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: action,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                window.location.reload();
            },
            error: function (e) {
                $("#FeedPostFrmBtn").prop('disabled', false);
                alert("ERROR : " + e);
            }
        });

});

var xhr;
function toggleFollowUser(loggedInUserId,follower_id)
{

    if(xhr && xhr.readyState != 4){
        xhr.abort();
    }
    
     xhr = $.post("{{ route('portal.user.profile.toggleFollow') }}",{ loggedInUserId:loggedInUserId,user_id:follower_id}, function(data){
      
        if(data.success == 'true')
        {   
                if(data.action == 'followed')
                {                        
                    $("#followUnfollowUser"+follower_id).html('<button class="btn btn-primary btn-sm btn-simple" onclick="toggleFollowUser({{ Auth::id()}},'+follower_id+')">Unfollow</button>')
                }                                            
                if(data.action == 'unfollowed'){
                    $("#followUnfollowUser"+follower_id).html('<button class="btn btn-sm btn-primary" onclick="toggleFollowUser({{ Auth::id()}},'+follower_id+')">Follow</button>');
                }
                notification(data.message,'success');
        }else{
            notification("Something went wrong",'danger')
        }
    
    });
    
}


</script>

@endpush
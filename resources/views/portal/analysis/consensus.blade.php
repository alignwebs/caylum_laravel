@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
  .table tr th, .table tr td{vertical-align: middle;}
  #searchForm{ padding: 21px 8px 15px 10px;padding-left: 8px;margin: -20px -20px 23px -19px;background: #566e7a}
  #searchForm .datetimepicker{background-color: #f7f7f7 !important; border: 1px solid #ffffff !important;}
  #searchForm .bootstrap-select .btn.btn-round.btn-simple{background-color: #f7f7f7 !important;}
  #searchForm .btn-primary{background: #fff !important;color: #566e7a !important;font-weight: 600;}
</style>
<section class="content home">
        <div class="block-header cstm-header">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h2>{{ $title }}</h2>
                    @include('portal.analysis.common.analysisDropdown') 
                </div>
                    

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            @if(!$search)
                List of Consensus
            @else
                TYPE Consensus from DATE to DATE
            @endif
        </div>
        <div class="card-body">
         <p class="last_update">Last Updated: &nbsp;<i>{{ $last_updated_at }}</i></p>
          <div class="table-responsive">
            {!! $datatable->table(['class'=>'table table-bordered table-striped table-hover','id'=>'consensusTable']) !!}
          </div>
        </div>
      </div>

    </div>
	</div>
</div>
</section>

@endsection




@push('scripts')
<script type="text/javascript">
  $(function() {
      var $thead = $('#consensusTable').find('thead');
      var $tr = $("<tr>");
      $tr.prepend('<th></th>');
      $tr.prepend('<th></th>');
      $tr.prepend('<th></th>');
      $tr.prepend('<th colspan="2" style="text-align:center">Net Income (in Mil PHP)</th>');
      $tr.prepend('<th colspan="2" style="text-align:center">Sales (in Mil PHP)</th>');
      $tr.prepend('<th></th>');
      $tr.prepend('<th></th>');
      $tr.prepend('<th></th>');
      $thead.prepend($tr);
  });
</script>
{!! $datatable->scripts() !!}
@endpush
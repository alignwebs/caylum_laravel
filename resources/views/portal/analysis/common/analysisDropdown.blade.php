 <style>
 	#analysisPagesCombo { max-width: 300px; }

 	#analysisPagesCombo .bootstrap-select .btn.btn-round.btn-simple { padding:5px;border-color: #5f1419;    background: #5f070d;color: #fff; }
 </style>
 <div id="analysisPagesCombo">
    {!! Form::select('analysisPagesCombo',analysisPagesCombo(),Request::url(),['class'=>'form-control'])  !!}
</div>
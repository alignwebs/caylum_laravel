 <div id="searchForm">
              {!! Form::open(['route' => $route,'method' => 'get']) !!}
              <div class="row">
              <div class="col-sm-2 form-group" style="margin-left: 20px;"> 
               {!! Form::text('from',!empty($request->from) ? $request->from : null,['id'=>'from','class'=>'form-control datetimepicker','placeholder'=>'From']) !!}
              </div>
               <div class="col-sm-2 form-group"> 
               {!! Form::text('to',!empty($request->to) ? $request->to : null,['id'=>'to','class'=>'form-control datetimepicker','placeholder'=>'To']) !!}
              </div>
              <div class="col-sm-2"> 
               <button type="submit" class="btn btn-primary" style="margin-top: 0px;">Search</button>
             </div>
             </div>
              {!! Form::close() !!}
  </div>
          
@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
            </div>
        </div>
<div class="container-fluid">
    <div class="row clearfix">
        
        <div class="col-lg-8">
            
            <div class="yt-featured-player">
              <iframe frameborder="0" src="//www.youtube.com/embed/{{ getYoutubeId($featured->url) }}"></iframe>
              <h4>{{ $featured->title }}</h4>
            </div>

            <div class="card">
                <div class="body">
                  
                  <div class="row">
                    
                    @foreach($youtube as $row)
                    <div class="col-6 col-sm-3">
                        <a href="?id={{ $row->id }}&type=youtube" class="media-thumbnail">
                          <div class="media-image"><img src="//img.youtube.com/vi/{{ getYoutubeId($row->url) }}/mqdefault.jpg"></div>
                          <div class="media-title">{{ $row->title }}</div>
                        </a>
                    </div>
                    @endforeach

                  </div>
                              
                </div>
            </div>
        </div>
        <div class="col-lg-4">
          @if($soundcloud->count() > 0)
            <div class="card">
              <div class="card-header">
                Podcasts
              </div>
                <div class="body">

                  @foreach($soundcloud as $row)
                     <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url={{ $row->url }}&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

                  @endforeach
                              
                </div>
            </div>
          @endif
        </div>


                                        
                                  
    </div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="mediaModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel"></h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
        
            <div class="modal-body">
                
            </div>
          
        </div>
    </div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
  $(document).ready(function($) {
    $('a[data-rel=lightcase]').lightcase({
      transition: 'scrollHorizontal'
    });
    });


</script>


@endpush
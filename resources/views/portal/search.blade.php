@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style>
    .tab-pane { max-width: 900px;margin: 0 auto; }
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>            

            </div>
        </div>
<div class="container-fluid">
        <div class="row clearfix">
            <div class="col-sm-12">
            
                        
                  <div class="card tabs-card">
                    <div class="card-header">

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs nav-fill">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#posts">Posts (<span id="feed_count">0</span>)</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#users">Users ({{ $users->count() }})</a></li>
                      </ul>             
                    </div>
                    <div class="card-body">

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane in active" id="posts">
                            <input type="hidden" id="profile_user_search_query" value="{{ $q }}">
                            @include('portal.social.feed.feeds')
                        </div>
                        <div role="tabpanel" class="tab-pane" id="users">
                            
                        <ul class="list-unstyled list-thumbnail">
                            @if($users->count() > 0)
                            @foreach($users as $user)
                                      <li class="media">
                                       <a href="{{ route('portal.user.profile',$user->username) }}"> <img src="{{ get_user_profile_pic($user) }}" class="mr-3 user-avatar"></a>
                                        <div class="media-body">
                                        <h6 class="mt-0 mb-1"><a href="{{ route('portal.user.profile',$user->username) }}">{{ $user->name }}</a> 
                                            @if($user->country)
                                            <img class="country_flag" src="{{ getCountryFlag($user->country) }}" alt="">
                                            @endif
                                        </h6>
                                          <div><small>{{ Str::limit($user->bio, 100) }}</small></div>
                                          <div id="followUnfollowUser{{$user->id}}" style="display: inline">
                                              @if( Auth::user()->isFollowing($user))
                                                 <button class="btn btn-sm btn-primary btn-simple" onclick="toggleFollowUser({{ Auth::id()}},{{$user->id}})">Unfollow</button>
                                              @else
                                                      <button class="btn btn-sm btn-primary" onclick="toggleFollowUser({{ Auth::id()}},{{$user->id}})">Follow</button>
                                               @endif
                                            </div>
                                           @if($user->is_online)
                                               <span class="badge badge-outline status cstm-status"></span>
                                           @endif
                                        </div>
                                      </li>
                                
                            @endforeach
                            @else
                                <h4 class="text-center">No users found.</h4>
                            @endif
                          </ul>
                
                          {{-- <div class="text-center"><a href="{{ route('portal.user.profile.followers',$user->username) }}" class="btn btn-secondary">View All</a></div> --}}
                    
                        </div>
                      </div>    
                    </div>
                  </div>

                    
            </div>
        </div>
    </div>
</section>

@endsection

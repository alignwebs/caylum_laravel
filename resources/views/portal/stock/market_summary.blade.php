@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .green{color: #0e862c;}
  .red{color: red;}
  .green a{color: #0e862c !important;}
  .red a{color: red !important;}
  .green:hover{color: #5a9a69;}
  .red:hover{color:#bd424f;}
  .red{color: red;}
  .link{color: black;}
  /*.link,.link:hover,.link:visited{color: #8c99e0;}*/
  table th{padding-right: 25px;}
  table.market { border-collapse: collapse !important;}
/*  table.market .green td{border-bottom: 1px solid #5a9a69!important;}
  table.market .red td{border-bottom: 1px solid #bd424f!important;}*/
</style>

<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
          
        </h2>
      </div>         

    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-md-5">
        <div class="card">
            
          <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                             <input type="text" class="form-control datetimepicker input-lg" value="{{ date('Y/m/d')}}">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                            <button class="btn btn-default" style="margin-top:0px;">Change</button>
                    </div>
                  </div>
                </div>
          </div>
        </div>

      </div>
      <div class="col-md-12">
        <div class="card">
            <div class="card-header">Market Activity</div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                     <canvas id="market-activity"></canvas>
                </div>
               
                <div class="col-md-6">
                     <table>
                      <tr>
                       <th><b>Total Volume: </b></th>
                       <td>834.29M</td>
                     </tr>
                      <tr>
                       <th><b>Total Value: </b></th>
                       <td>6.25B</td>
                     </tr>
                     <tr> <td colspan="2"> <br> </td> </tr>
                      <tr>
                       <th><b>Foreign Buying: </b></th>
                       <td>3.24B</td>
                     </tr>
                      <tr>
                       <th><b>Foreign Selling: </b></th>
                       <td> 3.31B</td>
                     </tr>
                      <tr>
                       <th><b>Foreign Net: </b></th>
                       <td>-66.13M</td>
                     </tr>
                     <tr> <td colspan="2"> <br> </td> </tr>
                      <tr>
                       <th><b>Block Sale Volume: </b></th>
                       <td> 3.38M</td>
                     </tr>
                      <tr>
                       <th><b>Block Sale Value: </b></th>
                       <td> 291.27M</td>
                     </tr>
                     <tr> <td colspan="2"> <br> </td> </tr>
                      <tr>
                       <th><b>Oddlot Volume: </b></th>
                       <td> 301.01K</td>
                     </tr>
                     <tr>
                       <th><b>Oddlot Value: </b></th>
                       <td>361.67KM</td>
                     </tr>
                       <tr> <td colspan="2"> <br> </td> </tr>
                      <tr>
                       <th><b>Number of Traded Issues: </b></th>
                       <td> 239</td>
                     </tr>
                     <tr>
                       <th><b>Number of Trades: </b></th>
                       <td>76,775</td>
                     </tr>
                     </table>
                </div>
              </div>
          </div>
        </div>

      </div>
      <div class="col-md-12">
        <div class="card">

               <div class="card-header">Market Indices</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Stock</th>
                        <th>Last</th>
                        <th>Change</th>
                        <th>%Change</th>
                        <th>Open</th>
                        <th>Low</th>
                        <th>High</th>
                        <th>Close</th>
                        <th>Volume</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr>
                          <td  class="{{ $row->close_color }}"><a href="#" class="link">{{ $row->stock }}</a></td>
                          <td>{{ number_format($row->prev_close,2) }}</td>
                          <td class="{{ $row->close_change_color }}">{{ number_format($row->close_change,2) }}</td>
                          <td class="{{ $row->close_change_color }}">{{ number_format($row->close_change_percent,2) }}</td>
                          <td class="{{ $row->open_color }}">{{ number_format($row->open,2) }}</td>
                          <td class="{{ $row->low_color }}">{{ number_format($row->low,2) }}</td>
                          <td class="{{ $row->high_color }}">{{ number_format($row->high,2) }}</td>
                          <td class="{{ $row->close_color }}">{{ number_format($row->close,2) }}</td>
                          <td class="{{ $row->volume_color }}">{{ number_format($row->volume,2) }}</td>
                          <td class="{{ $row->value_color }}">{{ number_format($row->value,2) }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-6">
        <div class="card">

               <div class="card-header">Foreign Buying</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Stock</th>
                        <th>Last Price</th>
                        <th>%Change</th>
                       {{--  <th>Change</th>
                        <th>Volume</th> --}}
                        <th>Value</th>
                        <th>Net Foreign</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr class="{{ $row->close_change_color }}">
                          <td><a href="#" class="link">{{ $row->stock }}</a></td>
                          <td>{{ number_format($row->prev_close,2) }}</td>
                          <td>{{ number_format($row->close_change_percent,2) }}</td>
                        {{--   <td>{{ $row->close_change }}</td>
                          <td>{{ $row->volume }}</td> --}}
                          <td>{{ number_format($row->value,2) }}</td>
                          <td>{{ number_format($row->value,2) }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-6">
        <div class="card">

               <div class="card-header">Foreign Selling</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Stock</th>
                        <th>Last Price</th>
                        <th>%Change</th>
                      {{--   <th>Change</th>
                        <th>Volume</th> --}}
                        <th>Value</th>
                        <th>Net Foreign</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr class="{{ $row->close_change_color }}">
                          <td><a href="#" class="link">{{ $row->stock }}</a></td>
                          <td>{{ number_format($row->prev_close,2) }}</td>
                          <td>{{ number_format($row->close_change_percent,2) }}</td>
                        {{--   <td>{{ $row->close_change }}</td>
                          <td>{{ $row->volume }}</td> --}}
                          <td>{{ number_format($row->value,2) }}</td>
                          <td>{{ number_format($row->value,2) }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-12">
        <div class="card">

               <div class="card-header">Block Sales</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Security</th>
                        <th>Price</th>
                        <th>Volume</th>
                        <th>Value</th>
                  
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr>
                          <td><a href="#" class="link">{{ $row->stock }}</a></td>
                          <td>{{ number_format($row->prev_close,2) }}</td>
                          <td>{{ number_format($row->close_change_percent,2) }}</td>
                          <td>{{ number_format($row->close_change,2) }}</td>
                         
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>
{{--       <div class="col-md-12">
        <div class="card">

               <div class="card-header">Securities Under Suspension by the Exchange</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Stock Name</th>
                        <th>Stock Codee</th>
                       
                  
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr>
                          <td>Asia Amalgamated Holdings Corporation</td>
                          <td><a href="#" class="link">{{ $row->stock }}</a></td>     
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div> --}}

      <div class="col-md-12">
        <div class="card">

        <div class="card-header">Analytics </div>
          <div class="card-body">
            <div class="col-md-3">
           
                  
             
                  <div class="row">
                      <div class="col-md-8">
                          <div class="form-group">
                                   <input type="number" class="form-control input-lg" value="33">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                                  <button class="btn btn-default" style="margin-top:0px;">Change</button>
                          </div>
                        </div>
                      </div>
            </div>
            <div class="col-md-12">
              <canvas id="value-volume"></canvas>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">



    var ctx = document.getElementById('market-activity').getContext('2d');
    var myPieChart = new Chart(ctx, {
      type: 'pie',
      // The data for our dataset

      data: {
          labels: ['Advances', 'Decline','Unchanged'],
         
          datasets: [{
              data: [60, 20,20],
               backgroundColor: [
                  '#566e7a',
                  '#7d262e',
                  '#658a9d',
              ],
          }]
      },

      // Configuration options go here
      options: {
        legend: {'position' : 'bottom' }
      }
  });

        var ctx = document.getElementById('value-volume').getContext('2d');
    var myPieChart = new Chart(ctx, {
      type: 'bar',
      // The data for our dataset

      data: {
          labels: ['Advances', 'Decline','Unchanged'],
         
          datasets: [{
              data: [60, 20],
               backgroundColor: [
                  '#566e7a',
                  '#7d262e',
                  '#658a9d',
              ],
          }]
      },

      // Configuration options go here
      options: {
        legend: {'position' : 'bottom' }
      }
  });

  $(document).ready(function() {


      $('.market').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "paging": false,
        "searching": false
      });
  } );
</script>
@endpush
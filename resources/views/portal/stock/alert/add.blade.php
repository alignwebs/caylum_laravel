{!! Form::open([ 'route' => 'portal.stockalert.store', 'id' => 'StockAlertAddFrm' ]) !!}
{!! Form::hidden('stock_symbol', $stock->symbol) !!}
<div class="modal-header">
	 <h4 class="modal-title">Add Stock Alert</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
 </div>
<div class="modal-body">
	<p><b>Stock Name:</b> <span>{{ $stock->full_stock_market_name }}</span></p>
	<div class="alert-rows mb-3">
		
					<div class="row">
						<div class="col-6">
							<label>Alert when price is</label>
							<select name="type" class="form-control">
								<option value="above">above</option>
								<option value="below">below</option>
								<option value="equal">equal</option>
							</select>
						</div>
						<div class="col-6">
							<label>value</label>
							<input type="number" step="0.5" class="form-control" name="value">
						</div>
					</div>
		
	</div>
</div>
<div class="modal-footer">
	<input type="submit" value="Submit" class="btn btn-primary">
</div>
{!! Form::close() !!}

<script>
	$('#StockAlertAddFrm').submit(function (e) {

		e.preventDefault();

		$.post($(this).attr('action'), $(this).serialize(), function (data) {
			if(data.success != undefined)
			{
				notification("Stock alert added successfully.",'success');
				showAddStockAlertModal(false);

				let watchlist_id = $('#stockwatchlistNames').val();
				getWatchlistEntries(watchlist_id);
			}
			else
			{
				notification("Stock alert cannot be added.",'danger');
			}
		});

	})
</script>
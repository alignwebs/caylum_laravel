@extends('portal.layouts.app')
@section('title', $title)
@section('content')

<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}
                    <small>Manage your stock alerts</small>
                    </h2>
                </div>   

                 <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-success addWatchlistStockBtn"><i class="fas fa-plus-circle"></i> Add Stock</button>
                 </div>           

            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                        <div class="card">
                            <div class="body"></div>
                        </div>
                </div>
            </div>
        </div>
</section>

@endsection


@push('scripts')

@endpush
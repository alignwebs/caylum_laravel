{!! Form::open([ 'route' => 'portal.stockalert.store', 'id' => 'StockAlertAddFrm' ]) !!}
{!! Form::hidden('stock_symbol', $stock->symbol) !!}
<div class="modal-header">
	 <h4 class="modal-title">Stock Alert | {{ $stock->symbol }} - {{ $stock->name }}</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
 </div>
<div class="modal-body">

	<div class="alert-rows mb-3">
		@foreach($stock_alerts as $stock_alert)
					<div class="row">
						<div class="col-5">
							<select name="type[]" class="form-control">
								<option value="above" {{ ($stock_alert->type == 'above') ? 'checked' : '' }}>above</option>
								<option value="below" {{ ($stock_alert->type == 'below') ? 'checked' : '' }}>below</option>
								<option value="equal" {{ ($stock_alert->type == 'equal') ? 'checked' : '' }}>equal</option>
							</select>
						</div>
						<div class="col-5">
							<input type="number" step="0.5" class="form-control" name="value[]" value="{{ $stock_alert->value }}">
						</div>
						<div class="col-2">
							<input type="button"  class="btn btn-danger btn-sm remove-row" value="X">
						</div>
					</div>
		@endforeach
	</div>
	<button type="button" class="btn btn-success add-row">Add</button>
</div>
<div class="modal-footer">
	<input type="submit" value="Submit" class="btn btn-primary">
</div>
{!! Form::close() !!}

<script>
	
	$(document).ready(function () {

		renderHtml();

	})
	function renderHtml()
	{
		$('#StockAlertAddFrm .modal-body .alert-rows').append(`<div class="row">
			<div class="col-5">
				<select name="type[]" class="form-control">
					<option value="above">above</option>
					<option value="below">below</option>
					<option value="equal">equal</option>
				</select>
			</div>
			<div class="col-5">
				<input type="number" step="0.5" class="form-control" name="value[]">
			</div>
			<div class="col-2">
				<input type="button"  class="btn btn-danger btn-sm remove-row" value="X">
			</div>
		</div>`);
	}

	$(document).on('click', '.remove-row', function () { 

			$(this).closest('.row').remove();
	 });

	$('.add-row').click(function () { 

			renderHtml();
	 });

	$('#StockAlertAddFrm').submit(function (e) {

		e.preventDefault();

		$.post($(this).attr('action'), $(this).serialize(), function (data) {
			if(data.success != undefined)
			{
				notification("Stock alert added successfully.",'success');
				showAddStockAlertModal(false);
			}
			else
			{
				notification("Stock alert cannot be added.",'danger');
			}
		});

	})
</script>
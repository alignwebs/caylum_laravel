<table class="table table-codensed">
	<thead>
		<tr>
			<th>Active</th>
			<th>Stock</th>
			<th>Type</th>
			<th>Value</th>
			<th>Alerted at</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
		@forelse($stock_alerts as $alert)
			<tr>
				<td><input type="checkbox" data-alertid="{{ $alert->id }}" id="{{ $alert->id }}" class="updateAlertStatusBtn js-switch" {{ ($alert->alerted > 0 ? "" : "checked") }} /></td>
				<td>{{ $alert->stock->full_stock_name }}</td>
				<td>{{ $alert->type }}</td>
				<td>{{ $alert->value }}</td>
				<td>{{ ($alert->alerted > 0 ? $alert->updated_at  : "") }}</td>
				<td><a href="javascript:" onclick="deleteStockAlert(this, {{ $alert->id }})"><i class="fas fa-trash-alt"></i></a></td>
			</tr>
		@empty
			<tr><td class="text-center" colspan="5">No alerts found</td></tr>
		@endforelse
	</tbody>
</table>

<script>
	initJs();
</script>
@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .green{color: green}
  .red{color: red}
  .link,.link:hover,.link:visited{color: #8c99e0}
</style>

<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
          
        </h2>
      </div>         

    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">

      <div class="col-md-12">
        <div class="card">

          <div class="card-body">
            
            <div class="table-responsive">
                <table class="table table-hovered" id="realTimeMonitoringTable">
                    <thead>
                      <tr>
                        <th>Stock</th>
                        <th>Last</th>
                        <th>Change</th>
                        <th>%Change</th>
                        <th>Open</th>
                        <th>Low</th>
                        <th>High</th>
                        <th>Close</th>
                        <th>Volume</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr>
                          <td><a href="#" class="link">{{ $row->stock }}</a></td>
                          <td>{{ $row->prev_close }}</td>
                          <td class="{{ $row->close_change_color }}">{{ $row->close_change }}</td>
                          <td class="{{ $row->close_change_color }}">{{ $row->close_change_percent }}</td>
                          <td class="{{ $row->open_color }}">{{ $row->open }}</td>
                          <td class="{{ $row->low_color }}">{{ $row->low }}</td>
                          <td class="{{ $row->high_color }}">{{ $row->high }}</td>
                          <td class="{{ $row->close_color }}">{{ $row->close }}</td>
                          <td class="{{ $row->volume_color }}">{{ $row->volume }}</td>
                          <td class="{{ $row->value_color }}">{{ $row->value }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
      $('#realTimeMonitoringTable').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "paging": false
      });
  } );
</script>
@endpush
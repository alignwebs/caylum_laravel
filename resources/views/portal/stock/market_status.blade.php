@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .green{border-left: #09bca8 solid 4px;}
  .red{border-left: #ff6862 solid 4px;}
  .green-text{color:#09bca8;}
  .red-text{color: #ff6862;}
  .link,.link:hover,.link:visited{color: #8c99e0;}
  table th{padding-right: 25px;}
  table.market { border-collapse: collapse !important;}
  .form-control2 {padding: 0px !important;margin-top: -5px !important;}
.btn-group.bootstrap-select.selectized{display: none;}
</style>

<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
          
        </h2>
      </div>         

    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-md-12">
        <div class="card">
            
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                  <div class="form-group">
                        <label>Exchange</label>
                        <select class="form-control form-control2">
                            <option value=" PSE - Philippine Stock Exchange "> PSE - Philippine Stock Exchange </option>
                            <option value=" PSE - Philippine Stock Exchange "> PSE - Philippine Stock Exchange </option>
                        </select>
                         
                  </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                              <label>Date</label>
                             <input type="text" class="form-control  datetimepicker input-lg" value="{{ date('Y/m/d')}}" >
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                            <button class="btn btn-default" style="margin-top:26px;">Change</button>
                    </div>
                  </div>
                </div>
          </div>
        </div>

      </div>

      <div class="col-md-4">
        <div class="card">

        <div class="card-header text-center">Most Active</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Sector</th>
                        <th>Last</th>
                        <th>%Change</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr  class="{{ $row->close_change_color }}">
                          <td><img src="{{ asset('theme/images/PH.png') }}" width="20"> &nbsp;<a href="#" class="link">{{ $row->stock }}</a></td>
                          <td class="{{ $row->close_change_color }}-text">{{ $row->prev_close }}</td>
                          <td class="{{ $row->close_change_color }}-text">{{ $row->close_change_percent }}</td>
                          <td>{{ $row->value }}</td>
                          
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-4">
        <div class="card">

        <div class="card-header text-center">Top Gainers</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Sector</th>
                        <th>Last</th>

                        <th>%Change</th>
      
                        <th>Value</th>
                    
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr class="{{ $row->close_change_color }}">
                          <td><img src="{{ asset('theme/images/PH.png') }}" width="20"> &nbsp;<a href="#" class="link">{{ $row->stock }}</a></td>
                          <td class="{{ $row->close_change_color }}-text">{{ $row->prev_close }}</td>
                          <td class="{{ $row->close_change_color }}-text">{{ $row->close_change_percent }}</td>
                          <td>{{ $row->value }}</td>
                          
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-4">
        <div class="card">

        <div class="card-header text-center">Top Losers</div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hovered market">
                    <thead>
                      <tr>
                        <th>Sector</th>
                        <th>Last</th>
                        <th>%Change</th>
      
                        <th>Value</th>
                    
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rows as $row)
                      <tr class="{{ $row->close_change_color }}">
                          <td><img src="{{ asset('theme/images/PH.png') }}" width="20"> &nbsp;<a href="#" class="link">{{ $row->stock }}</a></td>
                          <td class="{{ $row->close_change_color }}-text">{{ $row->prev_close }}</td>
                          <td class="{{ $row->close_change_color }}-text">{{ $row->close_change_percent }}</td>
                          <td>{{ $row->value }}</td>
                          
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>


    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
    var ctx = document.getElementById('market-activity').getContext('2d');
    var myPieChart = new Chart(ctx, {
      type: 'pie',
      // The data for our dataset

      data: {
          labels: ['Advances', 'Decline','Unchanged'],
         
          datasets: [{
              data: [60, 20,20],
               backgroundColor: [
                  '#006400',
                  '#8b0000',
                  '#e8bd19',
              ],
          }]
      },

      // Configuration options go here
      options: {
        legend: {'position' : 'bottom' }
      }
  });
  $(document).ready(function() {


      $('.market').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "paging": false,
        "searching": false
      });
  } );
</script>
@endpush
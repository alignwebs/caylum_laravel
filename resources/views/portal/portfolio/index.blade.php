@extends('portal.layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .form-control2 {padding: 0px !important;margin-top: -5px !important;}
  .table tr th, .table tr td{vertical-align: middle;}
  .btn-group.bootstrap-select.selectized{display: none;}
</style>

<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}
        </h2>
      </div> 
      <div class="col-md-7 text-right">  
           <a href="{{ route('portal.portfolio.trades','pse') }}" class="btn btn-primary"><i class="zmdi zmdi-money"></i><span> Trade Entry</span></a>&nbsp;&nbsp;
           <a href="{{ route('portal.portfolio.transactions','pse') }}" class="btn btn-primary"><i class="zmdi zmdi-money"></i><span> Transactions</span></a>
      </div> 
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">

      <div class="col-md-8">
        
        <div class="card">
          <div class="card-body g-bg-blue">
            <div class="row">
                <div class="col-6">
                  <h4>Available Cash</h4>
                  <h3>PHP 93,556.64</h3>
                   <h4>Total Equity</h4>
                  <h3>PHP 93,556.64</h3>
                </div>
             
                 <div class="col-6">
                    <canvas id="portfolio-allocation-chart"></canvas>
                 </div>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">Portfolio Status</div>
          <div class="card-body">
           <canvas id="portfolio-status-chart"></canvas>
          </div>
        </div>
      </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-4">
           <div class="card">
              <div class="card-header">Top 3 Gainers</div>
              <div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Stock</th>
                      <th>Avg. Price</th>
                      <th>Profit</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>2GO</td>
                      <td>255</td>
                      <td class="text-green">25.55%</td>
                    </tr>
                    <tr>
                      <td>HJU</td>
                      <td>255</td>
                      <td class="text-green">12.55%</td>
                    </tr>
                    <tr>
                      <td>SMU</td>
                      <td>255</td>
                      <td class="text-green">35.55%</td>
                    </tr>
                  </tbody>
                </table>
              </div>
          </div>
        </div>


        <div class="col-md-4">
           <div class="card">
              <div class="card-header">Top 3 Losers</div>
              <div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Stock</th>
                      <th>Avg. Price</th>
                      <th>Loss</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>2GO</td>
                      <td>255</td>
                      <td class="text-green">25.55%</td>
                    </tr>
                    <tr>
                      <td>HJU</td>
                      <td>255</td>
                      <td class="text-green">12.55%</td>
                    </tr>
                    <tr>
                      <td>SMU</td>
                      <td>255</td>
                      <td class="text-green">35.55%</td>
                    </tr>
                  </tbody>
                </table>
              </div>
          </div>
        </div>

        <div class="col-md-4">
           <div class="card">
              <div class="card-header">Monthly Holdings - 6 Months</div>
              <div class="card-body">
                <canvas id="portfolio-monthly-holding-chart"></canvas>
              </div>
            </div>
        </div>
    
    </div>

  </div>
</section>

@endsection


@push('scripts')
<script>

      var ctx = document.getElementById('portfolio-allocation-chart').getContext('2d');
      var myPieChart = new Chart(ctx, {
        type: 'pie',
        // The data for our dataset

        data: {
            labels: ['Available Cash', 'Total Equity'],
           
            datasets: [{
                data: [20, 80],
                 backgroundColor: [
                    'green',
                    'orange',
                ],
            }]
        },

        // Configuration options go here
        options: {}
    });

      var ctx = document.getElementById('portfolio-status-chart').getContext('2d');
      var myPieChart = new Chart(ctx, {
        type: 'pie',
        // The data for our dataset

        data: {
            labels: ['Rising Stocks', 'Falling Stocks'],
           
            datasets: [{
                data: [60, 40],
                 backgroundColor: [
                    'green',
                    'red',
                ],
            }]
        },

        // Configuration options go here
        options: {
          legend: {'position' : 'left' }
        }
    });

    var ctx = document.getElementById('portfolio-monthly-holding-chart').getContext('2d');
      var myPieChart = new Chart(ctx, {
        type: 'bar',
       
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June'],
           
            datasets: [{
                data: [60, 40, 50, 90, 100, 25]
                
            }]
        },

        // Configuration options go here
        options: {
          legend: false
        }
    });

</script>
@endpush
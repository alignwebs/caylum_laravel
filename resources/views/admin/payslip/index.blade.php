@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>            
            </div>
        </div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive">
                            {!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">

</script>
{!! $datatable->scripts() !!}

@endpush
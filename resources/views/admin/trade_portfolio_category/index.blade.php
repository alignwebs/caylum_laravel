@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
  .form-control2 {padding: 0px !important;}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-8">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-warning" id="addCategory">+ Add Trade Setup</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
        <div class="col-lg-4">
            <div class="card">
                <div class="body">
                    <div id="errors"></div> 
                   {!! Form::open(['class'=>'form-material','id'=>'addTradePortfolioCategoryForm']) !!}
                               <div class="row">
                                   <div class="col-md-12">
                                      <div class="form-group">
                                           <label>Name</label>
                                           {!! Form::text('name',null,['class'=>'form-control']) !!}
                                       </div>
                                   </div>

                                   <div class="col-md-12">
                                       <div class="form-group">
                                         <label>Parent Trade Setup</label>
                                         {!! Form::select('trade_portfolio_category_id',$trade_portfolio_categories, null,['class'=>'form-control form-control-line','placeholder'=>'None']) !!}
                                       </div>
                                   </div>
                                   <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-round waves-effect btn-block" id="tradePortfolioCategoryBtn">Submit</button>
                                    </div>
                              </div>
                       
                   {!! Form::close() !!}
                   </div>
                </div>
            </div>
    
	    <div class="col-lg-8">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                	  <table class="table table-bordered table-striped table-bordered" id="categoryTable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                <tr>
                                    <td>
                                        {{ $category->name }}
                                    </td>
                                    <td>
                                      {!!  ModelBtn2('category',$category->id) !!}
                                      @foreach($category->trade_portfolio_subcategory as $sub_category)
                                          <tr>
                                              <td> ----- {{ $sub_category->name }}</td>
                                              <td>
                                                <div class="btn-group btn-options" role="group">
                                                  {!! Form::open(['route' => ['subcategory.destroy', $sub_category->id], 'method' => 'delete', 'class' => 'deleteFrmAjax']) !!}
                                                  <button type="button" onclick="editAjax('{{ route("subcategory.edit",$sub_category->id) }}')" class="btn btn-primary btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></button>
                                                  <button type="submit" class="btn-delete btn btn-danger btn-sm" data-title="'.$title.'"><span class="fa fa-trash" aria-hidden="true"></span> </button>
                                                  {!! Form::close() !!}
                                                </div>
                                  
                                              </td>
                                          </tr>
                                      @endforeach

                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                           
                          </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="editModal">
  <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>
@endsection




@push('scripts')

<script type="text/javascript">
   

	$(document).ready(function(){
  
	
	$('#addTradePortfolioCategoryForm').submit(function (data) {
    $("#tradePortfolioCategoryBtn").prop('disabled',true);
		$.post("{{ route('category.store') }}",$(this).serialize(), function(data){
      if(data.success == 'true')
      {
        $("#tradePortfolioCategoryModal").modal('hide');
        notification(data.message,'success')
        location.reload();
      }else{

        $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

      }
      $("#tradePortfolioCategoryBtn").prop('disabled',false);
	    })

			return false;

	});

	});


</script>

@endpush
    {!! Form::model($category, ['route' => ['category.update', $category->id],'method'=>'PUT','id'=>'editPortfolioCategoryForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Trade Portfolio Category</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                <div class="row">
               
                      <div class="col-md-12">
                         <div class="form-group">
                              <label>Name</label>
                              {!! Form::text('name',null,['class'=>'form-control','required'=>true]) !!}
                          </div>
                      </div>
               </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editPortfolioCategoryForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

    {!! Form::model($subcategory, ['route' => ['subcategory.update', $subcategory->id],'method'=>'post','id'=>'editPortfolioSubCategoryForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Course</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                <div class="row">
               
                      <div class="col-md-12">
                         <div class="form-group">
                              <label>Name</label>
                              {!! Form::text('name',null,['class'=>'form-control','required'=>true]) !!}
                          </div>
                      </div>

                      <div class="col-md-12">
                          <div class="form-group">
                            <label>Parent Portfolio Category</label>
                            {!! Form::select('trade_portfolio_category_id',$portfolio_categories, $subcategory->trade_portfolio_category_id,['class'=>'form-control form-control-line form-control2']) !!}
                          </div>
                      </div>
                
               </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editPortfolioSubCategoryForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{{ $title }}</h2>

            </div> 

        </div>
    </div>

    {!! Form::open(['route'=>'portal.setting.store','files'=>true]) !!}
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                @if ($errors->any())
                <div class="alert alert-danger">{{ $errors->first() }}</div>    
                @endif
                @include('flash::message')

            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-6">

                <div class="card">
                    <div class="card-header">Social Profile</div>
                    <div class="body">   
                        <div class="row">
                            <div class="col-md-12">
                               <div class="form-group">
                                <label>Facebook</label>
                                {!! Form::text('facebook',setting('facebook'),['class'=>'form-control','placeholder'=>'Enter Facebook Url']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                            <label>Twitter</label>
                            {!! Form::text('twitter',setting('twitter'),['class'=>'form-control','placeholder'=>'Enter Twitter Url']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                        <label>Youtube</label>
                        {!! Form::text('youtube',setting('youtube'),['class'=>'form-control','placeholder'=>'Enter Youtube Url']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                    <label>SoundClound</label>
                    {!! Form::text('sound_cloud',setting('sound_cloud'),['class'=>'form-control','placeholder'=>'Enter SoundCloud Url']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">System Setting</div>
    <div class="body"> 
        <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                <label>Enable Member Portal</label>
                {!! Form::select('enable_member_portal', [1 => 'Enable', 0 => 'Disable'], setting('enable_member_portal'),['class'=>'form-control', 'id' => 'enable_member_portal']) !!}
                </div>
            </div>
            <div class="col-md-12">
               <div class="form-group demo-tagsinput-area">
                <label>Send all notification to this email</label>
                <div class="form-line">
                {!! Form::text('notification_emails', setting('notification_emails'),['class'=>'form-control', 'id' => 'notification_emails', "data-role" => "tagsinput", 'placeholder' => 'Add Email']) !!}
                </div>
                </div>
            </div>
        </div> 
    </div>
</div>

<div class="card">
    <div class="card-header">Course Enrollment Setting</div>
    <div class="body"> 
        <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                <label>Enrollment Expiry in Days</label>
                {!! Form::number('course_enrollment_expiry_days', setting('course_enrollment_expiry_days'),['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
               <div class="form-group">
                <label>Bank Deposit Mail Text - use [EXPIRY_DATE]</label>
                {!! Form::textarea('course_enrollment_bank_deposit_mail_text', setting('course_enrollment_bank_deposit_mail_text'),['class'=>'form-control', 'rows' => '2']) !!}
                </div>
            </div>
        </div> 
    </div>
</div>

</div>
<div class="col-lg-6">

    <div class="card">
        <div class="card-header">Youtube</div>
        <div class="body">
            <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                    <label>Youtube Video ID </label> - <small>https://www.youtube.com/<b>XNRENx3LGZA</b></small>
                    {!! Form::text('youtube_url',setting('youtube_url'),['class'=>'form-control']) !!}
                </div>
            </div>
        </div>

    </div>
</div>


<div class="card">
    <div class="card-header">Poll</div>
    <div class="body"> 
        <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                <label>Poll Code</label>
                {!! Form::textarea('poll_code',setting('poll_code'),['class'=>'form-control','rows'=>2]) !!}
            </div>
        </div>
    </div> 
</div>
</div>

<div class="card">
    <div class="card-header">Ads</div>
    <div class="body"> 
        <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                <label>Ad Link</label>
                {!! Form::text('ad_link',setting('ad_link'),['class'=>'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>Ad Image</label>
            <div class="form-group">
                {!! Form::file('ad_image',null,['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6 text-right">
           <div class="form-group">
              @if(!empty(setting('ad_image')))
              <img src="{{ asset('storage/'.setting('ad_image')) }}" width="200" class="img-thumbnail">
              @endif
          </div>
      </div>
  </div>


</div>
</div>

</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
        </div>
    </div>
</div>
</div>
{!! Form::close() !!}
</section>
@endsection

@push('scripts')
<script type="text/javascript">

</script>

@endpush

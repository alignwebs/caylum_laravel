@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}</h2>
      </div>
      <div class="col-md-7 text-right">  
        <button type="button" class="btn btn-warning" id="addAdminUser">+ Add Admin User</button>
      </div>          

    </div>
  </div>
  <div class="container-fluid">
   <div class="row clearfix">
     <div class="col-lg-12">
       <div class="card">
         <div class="body">
           <div class="table-responsive">
             {!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</section>

<!-- Default Size -->
<div class="modal fade" id="adminUserModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="title" id="defaultModalLabel">Add Administration User</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      {!! Form::open(['route'=>'user.store','class'=>'form-material','id'=>'addAdminUserForm']) !!}
      <div class="modal-body">
      <div id="errors"></div>
      <div id="success"></div>
        {!! Form::open(['class'=>'form-material','id'=>'addUserForm']) !!}
        <div class="row">
         <div class="col-md-6">
           <div class="form-group">
             <label>Last Name</label>
             {!! Form::text('last_name',null,['class'=>'form-control form-control-line','required'=>true]) !!}
           </div>
         </div>
         <div class="col-md-6">
           <div class="form-group">
             <label>First Name</label>
             {!! Form::text('first_name',null,['class'=>'form-control form-control-line','required'=>true]) !!}
           </div>
         </div>
       <div class="col-md-6">
         <div class="form-group">
           <label>Email </label>
           {!! Form::text('email',null,['class'=>'form-control form-control-line','id'=>'email','required'=>true]) !!}
         </div>
       </div>
       <div class="col-md-6">
         <div class="form-group">
           <label>Password</label>
           {!! Form::password('password',['class'=>'form-control form-control-line','id'=>'password','required'=>true]) !!}
         </div>
       </div>
       <div class="col-md-6">
         <div class="form-group">
           <label>Confirm Password</label>

           {!! Form::password('password_confirmation',['class'=>'form-control form-control-line', 'id'=>"password-confirm",'required'=>true]) !!}
         </div>
       </div>
       <div class="col-md-6">
         <div class="form-group">
           <label>Role</label>

           {!! Form::select('role',getRoles(),null,['class'=>'form-control form-control-line','required'=>true]) !!}
         </div>
       </div>
       <div class="col-md-6">
        <div class="form-group">
         <label>Active</label>
         {!! Form::radio('active',1,true,['class'=>'check','data-radio'=>'iradio_flat-red']) !!}
         <label>Inactive</label>
         {!! Form::radio('active',0,null,['class'=>'check','data-radio'=>'iradio_flat-red']) !!}
       </div> 
     </div>
   </div>
 </div>
 <div class="modal-footer">
  <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
</div>
{!! Form::close() !!}
</div>
</div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){


   $("#addAdminUser").click(function(){
     $("#adminUserModal #errors").html("");
     $("#adminUserModal").modal();
   });

   $('#addAdminUserForm').submit(function (data) {
    $.post("{{ route('user.store') }}",$(this).serialize(), function(data){


     if(data.errors)
     {
      $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
    
    }else{

     if(data.success == 'true')
     {
       $("#adminUserModal").modal('hide');

       notification(data.message,'success')
       location.reload();
     }else{

      notification('Something Went Wrong','danger')

    }

  }


})

    return false;

  });

 });



</script>
{!! $datatable->scripts() !!}

@endpush
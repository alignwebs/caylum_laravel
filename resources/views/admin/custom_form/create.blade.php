@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                     

            </div>
        </div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">

                                {!! Form::open(['route'=>'form.store','class'=>'form-material','id'=>'customForm','files'=>true]) !!}
                            
                                          <div id="errors"></div>
                                          <div class="row">
                                          

                                          <div class="col-md-12">
                                             <div class="form-group">
                                                  <label>Name</label>
                                                  {!! Form::text('form_name',null,['class'=>'form-control','required'=>true,'id'=>'form_name']) !!}
                                              </div>
                                          </div>
                                          <div class="col-md-12">
                                          <label>Form</label>
                                          <div id="build-wrap"></div>
                                          </div>
                                         </div>
                        
                              
                                    <button type="submit" class="btn btn-primary btn-round waves-effect" id="getJSON">Submit</button>
                    
                                {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
</div>
</section>


@endsection
@push('scripts')
<script>
var fbEditor = document.getElementById('build-wrap');
var formBuilder = $(fbEditor).formBuilder();

$("#customForm").submit(function(){

  let form = $(this)[0];
  let data = new FormData(form);
  data.append('form_name', $("#form_name").val());
  data.append('form_json', formBuilder.actions.getData('json', true));
  let action = $(this).attr('action');


   $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url: action,
      data: data,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function (response) {   

          if(response.success == 'true')
          {
             notification(response.message,'success')

          }else{

                $("#errors").html("<div class='alert alert-danger'>"+response.errors+"</div>");
                
              }
          

      },
      error: function (response) {
       
          notification('Something Went Wrong','danger')
      }
  });

  return false;
});

// document.getElementById('getJSON').addEventListener('click', function() {
//     alert();
// });

</script>
@endpush
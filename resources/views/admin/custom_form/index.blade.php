@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
td{
    max-width:40%;
word-wrap:break-word
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <a href="{{ route('form.create') }}" class="btn btn-warning">+ Add Custom Form</a>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
 @include('flash::message')
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>



@endsection




@push('scripts')
{!! $datatable->scripts() !!}
@endpush
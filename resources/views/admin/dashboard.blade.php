@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>            

            </div>
        </div>
<div class="container-fluid">
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                <div class="body">
                                    <h2 class="number count-to m-t-0 m-b-5" data-from="0" data-to="{{ $total['totalRegistrantsCount'] }}" data-speed="1000" data-fresh-interval="700">{{ $total['totalRegistrantsCount'] }}</h2>
                                    <p class="text-muted">Total Registrants</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                <div class="body">
                                    <h2 class="number count-to m-t-0 m-b-5" data-from="0" data-to="{{ $total['subscriberCounts'] }}" data-speed="2000" data-fresh-interval="700">{{ $total['subscriberCounts'] }}</h2>
                                    <p class="text-muted">Total Subscribers</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                <div class="body">
                                    <h2 class="number count-to m-t-0 m-b-5" data-from="0" data-to="{{ $total['enrollmentCounts'] }}" data-speed="2000" data-fresh-interval="700">{{ $total['enrollmentCounts'] }}</h2>
                                    <p class="text-muted ">Total Enrollments</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2><strong>Latest</strong> Payments</h2>
                    </div>
                    <div class="body table-responsive members_profiles">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Subscription</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($total['payments'] as $row)
                                <tr>
                                    <td>{{ $row->payer_name }}</td>
                                    <td>{{ @$row->payer_email }}</td>
                                    <td>{{ @$row->subscriber->subscription->name }}</td>
                                    <td>{{ $row->date }}</td>
                                   
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2><strong>Latest</strong> Enrollments</h2>
                    </div>
                    <div class="body table-responsive members_profiles">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Course</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($total['enrollments'] as $row)
                                <tr>
                                    <td>{{ @$row->first_name." ".@$row->last_name }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->course->name }}</td>
                                    <td>{{ \Carbon\Carbon::parse($row->created_at)->format('Y-m-d') }}</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                        <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Latest</strong> Registrants</h2>
                    </div>
                    <div class="body table-responsive members_profiles">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Alumnus</th>
                                    <th>Year Attended</th>
                                    <th>Subscription</th>
                                    <th>Date</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($total['totalRegistrants'] as $row)
                                <tr>
                                    <td>{{ @$row->first_name." ".@$row->last_name }}</td>
                                    <td>{{ @$row->email }}</td>
                                    <td>{{ @$row->alumini_course }}</td>
                                    <td>{{ @$row->alumini_attend }}</td>
                                    <td>{{ @$row->subscription->name }} Months</td>
                                    <td>{{ \Carbon\Carbon::parse($row->created_at)->format('Y-m-d')}}</td>
                                  
                                   
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

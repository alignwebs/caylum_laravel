    {!! Form::model($subscription, ['route' => ['subscription.update', $subscription->id],'method'=>'PUT','id'=>'editSubscriptionForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Subscription</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >
      <div id="errors"></div>
          <div class="row">
          <div class="col-md-12">
             <div class="form-group">
                  <label>Subscription</label>
                  {!! Form::text('name',null,['class'=>'form-control','required'=>true]) !!}
              </div>
          </div>
        
            <div class="col-md-12">
              <div class="form-group">
                <label>Fee</label>
                {!! Form::number('fee',null,['class'=>'form-control','required'=>true, 'step' => '0.01']) !!}
              </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
                 <label>Duration (Days)</label>
                 {!! Form::number('days_duration',null,['class'=>'form-control','required'=>true]) !!}
            </div>
          </div>
         
            <div class="col-md-6">
              <div class="form-group">
                <label>Default</label>
                {!! Form::select('default',binary(), null,['class'=>'form-control','required'=>true]) !!}
              </div>
          </div>
                 <div class="col-md-6">
                     <div class="form-group">
                       <label>Active</label>
                       {!! Form::select('active',binary(), null,['class'=>'form-control','required'=>true]) !!}
                     </div>
                 </div>
          
         </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editSubscriptionForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
              
            }else{

              if(data.success == 'true')
              {
                $("#editSubscriptionModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

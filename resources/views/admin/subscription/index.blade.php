@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>            
                <div class="col-md-7 text-right">  
                   <button type="button" class="btn btn-warning" id="addSubscription">+ Add Subscription</button>
                </div> 
            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="subscriptionModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Subscription</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'subscription.store','class'=>'form-material','id'=>'addSubscriptionForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        

            	    		<div class="col-md-12">
            			       <div class="form-group">
            			            <label>Subscription</label>
            			            {!! Form::text('name',null,['class'=>'form-control','required'=>true]) !!}
            			        </div>
            	    		</div>
            	      
            	        	<div class="col-md-12">
            			        <div class="form-group">
            			          <label>Fee</label>
            			          {!! Form::number('fee',null,['class'=>'form-control','required'=>true, 'step' => '0.01']) !!}
            			        </div>
            	    		</div>
            	    		<div class="col-md-12">
            			      <div class="form-group">
            			           <label>Duration (Days)</label>
            			           {!! Form::number('days_duration',null,['class'=>'form-control','required'=>true]) !!}
            			      </div>
            	    		</div>
            	       
            	        	<div class="col-md-6">
            			        <div class="form-group">
            			          <label>Default</label>
            			          {!! Form::select('default',binary(), null,['class'=>'form-control','required'=>true]) !!}
            			        </div>
            	    		</div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label>Active</label>
                                  {!! Form::select('active',binary(), null,['class'=>'form-control','required'=>true]) !!}
                                </div>
                            </div>
            	    		
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

	  $("#addSubscription").click(function(){
	    $("#subscriptionModal").modal();
	  });

	$('#addSubscriptionForm').submit(function (data) {
		$.post($(this).attr('action'),$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#subscriptionModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

</script>
{!! $datatable->scripts() !!}

@endpush

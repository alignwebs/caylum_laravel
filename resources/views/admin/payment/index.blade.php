@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
  .ui-autocomplete {
      z-index: 2147483647;
  }
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                  <button type="button" class="btn btn-warning" id="addPayment">+ Add Payment</button>
                   <a href="{{ route('payment.export') }}" target="_blank" class="btn btn-success">Export</a>
                </div>            
  
            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	     <div class="card">
	        <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="paymentModal">
  <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Payment</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'payment.store','class'=>'form-material','id'=>'addPaymentForm']) !!}
            <div class="modal-body">
              <div id="errors"></div>
                      <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">

                               
                                 <input id="users" class="form-control" placeholder="Search Student or Member" />
                                 <input type="hidden" name="user_id" id="user_id" >
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Payment Mode</label>
                                {!! Form::select('payment_mode_id',$payment_modes,null,['class'=>'form-control','required'=>'true']) !!}
                              </div>
                            </div>
                              <div class="col-md-6">
                           <div class="form-group">
                            <label>Amount </label>
                            {!! Form::number('amount',null,['class'=>'form-control','required'=>'true']) !!}
                          </div>  
                        </div>
                          <div class="col-md-6">
                          <div class="form-group">
                            <label>Txn ID</label>
                            {!! Form::text('txn_id',null,['class'=>'form-control']) !!}
                          </div>
                        </div>
      <!--                   <div class="col-md-6">
                          <div class="form-group">
                            <label>Transaction</label><Br>
                            <label class="radio-inline">{!! Form::radio('transaction','credit') !!} Credit</label>
                            <lable class="radio-inline">{!! Form::radio('transaction','debit') !!} Debit</lable>
                          </div>
                        </div> -->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payment Type</label>
                            {!! Form::select('payment_type_id',$payment_types,null,['placeholder'=>'Select Payment Type','class'=>'form-control','required'=>'true','id'=>'payment_type']) !!}
                          </div>
                        </div>
                        <div id="course_enrollment">
                        </div>
                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payer Name</label>
                            {!! Form::text('payer_name',null,['class'=>'form-control','id'=>'payer_name']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payer Email</label>
                            {!! Form::text('payer_email',null,['class'=>'form-control','id'=>'payer_email']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payer Mobile</label>
                            {!! Form::text('payer_mobile',null,['class'=>'form-control','id'=>'payer_mobile']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Date</label>
                            {!! Form::text('date',null,['class'=>'form-control datetimepicker']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Comments</label>
                            {!! Form::textarea('remark',null,['class'=>'form-control','rows'=>4]) !!}
                          </div>
                        </div>
                        
            </div>
            </div>     
            	        
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
 $(document).ready(function(){
  var cache = {};
  $( "#users" ).autocomplete({
    minLength: 2,
    source: function( request, response ) {
      var term = request.term;
      if ( term in cache ) {
        response( cache[ term ] );
        return;
      }

      $.getJSON( "{{ route('user.search') }}", request, function( data, status, xhr ) {

        cache[ term ] = data.label;
        response( data );
      });
    },
    select: function( event, ui ) {
        $( "#user_id" ).val( ui.item.id );
        var full_name = ui.item.label;
        var myarr = full_name.split(" - ");
        $( "#payer_name" ).val( myarr[0]);
        $( "#payer_email" ).val( myarr[1]);
        $( "#payer_mobile" ).val( myarr[2]);
      
      },
      change: function (event, ui) {
         if(!ui.item){
             $("#users").val("");
         }             

        }
  });
     } );

 $("#payment_type").change(function(){

    var user_id = $("#user_id").val();
    
    if($(this).val() == 1){

      $.get("{{ route('course.search') }}",{user_id:user_id}, function(data){

        var html = "";
        html += "<div class='col-md-6' id='addCourseEnrollment'>";
        html += "<div class='form-group'>";
        html += "<label>Course Enrollment</label>";
        html += "<select name='course_enrollment_id' class='form-control'>Course Enrollment</label>";
        for(var i = 0; i < data.length; i++) {
            html += "<option value='"+data[i].value+"'>"+data[i].text+"</option>";
        }
        html+= "</select>";
        $("#course_enrollment").replaceWith(html);
      });
  

    }else{
          $("#addCourseEnrollment").remove();
      }


 });
     </script>
    <script type="text/javascript">
	$(document).ready(function(){
	  $("#addPayment").click(function(){
	    $("#paymentModal").modal();

      $('.datetimepicker').bootstrapMaterialDatePicker({
           time:false
       });


	  });

	$('#addPaymentForm').submit(function (data) {
		$.post($(this).attr('action'),$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#paymentModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});


</script>
{!! $datatable->scripts() !!}

@endpush
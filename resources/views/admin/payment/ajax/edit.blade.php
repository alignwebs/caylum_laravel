    {!! Form::model($payment, ['route' => ['payment.update', $payment->id],'method'=>'PUT','id'=>'editPaymentForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Payment</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >
      <div class="row">
            {!! Form::hidden('user_id',null,['id'=>'user_id']) !!}
      
       <div class="col-md-6">
        <div class="form-group">
          <label>Payment Mode</label>
          {!! Form::select('payment_mode_id',$payment_modes,null,['class'=>'form-control','required'=>'true']) !!}
        </div>
      </div>
      <div class="col-md-6">
       <div class="form-group">
        <label>Amount </label>
        {!! Form::number('amount',null,['class'=>'form-control','required'=>'true']) !!}
      </div>  
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Txn ID</label>
        {!! Form::text('txn_id',null,['class'=>'form-control']) !!}
      </div>
    </div>
      <!--                   <div class="col-md-6">
                          <div class="form-group">
                            <label>Transaction</label><Br>
                            <label class="radio-inline">{!! Form::radio('transaction','credit') !!} Credit</label>
                            <lable class="radio-inline">{!! Form::radio('transaction','debit') !!} Debit</lable>
                          </div>
                        </div> -->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payment Type</label>
                            {!! Form::select('payment_type_id',$payment_types,null,['placeholder'=>'Select Payment Type','class'=>'form-control','required'=>'true','id'=>'payment_type']) !!}
                          </div>
                        </div>
                        <div id="course_enrollment">
                        </div>
                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payer Name</label>
                            {!! Form::text('payer_name',null,['class'=>'form-control','id'=>'payer_name']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payer Email</label>
                            {!! Form::text('payer_email',null,['class'=>'form-control','id'=>'payer_email']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Payer Mobile</label>
                            {!! Form::text('payer_mobile',null,['class'=>'form-control','id'=>'payer_mobile']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Date</label>
                            {!! Form::text('date',null,['class'=>'form-control datetimepicker']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Comments</label>
                            {!! Form::textarea('remark',null,['class'=>'form-control','rows'=>4]) !!}
                          </div>
                        </div>
                        
                      </div>

                    </div>

                    <div class="modal-footer">
                      <input type="submit" value="Submit" class="btn btn-primary">

                    </div>


                    <!-- Modal footer -->

                    {!! Form::close() !!}

                    <script type="text/javascript">

                      $('#editPaymentForm').submit(function () { 

                        $.post($(this).attr('action'),$(this).serialize(), function(data){

                          if(data.errors)
                          {
                            notification(data.errors,'danger')

                          }else{

                            if(data.success == 'true')
                            {
                              $("#editPaymentModal").modal('hide');
                              notification(data.message,'success')
                              location.reload();


                            }else{

                             notification('Something went wrong','danger')

                           }

                         }

                       });




                        return false;

                      });
                      
                    </script>

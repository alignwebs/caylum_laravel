    {!! Form::model($course, ['route' => ['course.update', $course->id],'method'=>'PUT','id'=>'editCourseForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Course</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

              <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                        <label>Course Title</label>
                        {!! Form::text('name',null,['class'=>'form-control','required'=>true]) !!}
                    </div>
                </div>
              
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Description</label>
                      {!! Form::textarea('description',null,['class'=>'form-control','rows'=>4]) !!}
                    </div>
                </div>
                  <div class="col-md-6">
                              <div class="form-group">
                                   <label>Standard Rate</label>
                                   {!! Form::number('std_rate',null,['class'=>'form-control','required'=>true, 'step' => 0.01]) !!}
                              </div>
                            </div>
                <div class="col-md-6">
                  <div class="form-group">
                       <label>Published Rate</label>
                       {!! Form::number('price',null,['class'=>'form-control','required'=>true, 'step' => 0.01]) !!}
                  </div>
                </div>
               
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Is Paid</label>
                      {!! Form::select('is_paid',binary(), null,['class'=>'form-control form-control-line','required'=>true]) !!}
                    </div>
                </div>
                <div class="col-md-6 d-none">
                    <div class="form-group">
                      <label>Subscribe Default</label>
                      {!! Form::select('subscribe_default',binary(), 0,['class'=>'form-control form-control-line','required'=>true]) !!}
                    </div>
                </div>
               <div class="col-md-6">
                   <div class="form-group">
                     <label>Custom Forms</label>
                     {!! Form::select('custom_form_id',$custom_forms, $course->custom_form_id,['class'=>'form-control form-control-line','placeholder'=>'Select Custom Form']) !!}
                   </div>
               </div>
               </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editCourseForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editCourseModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

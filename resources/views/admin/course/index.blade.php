@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-warning" id="addCourse">+ Add Course</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="courseModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Course</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'course.store','class'=>'form-material','id'=>'addCourseForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        

            	    		<div class="col-md-12">
            			       <div class="form-group">
            			            <label>Course Title</label>
            			            {!! Form::text('name',null,['class'=>'form-control','required'=>true]) !!}
            			        </div>
            	    		</div>
            	      
            	        	<div class="col-md-12">
            			        <div class="form-group">
            			          <label>Description</label>
            			          {!! Form::textarea('description',null,['class'=>'form-control','rows'=>4]) !!}
            			        </div>
            	    		</div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Standard Rate</label>
                                   {!! Form::number('std_rate',null,['class'=>'form-control','required'=>true, 'step' => 0.01]) !!}
                              </div>
                            </div>
            	    		<div class="col-md-6">
            			      <div class="form-group">
            			           <label>Published Rate</label>
            			           {!! Form::number('price',null,['class'=>'form-control','required'=>true, 'step' => 0.01]) !!}
            			      </div>
            	    		</div>
            	       
            	        	<div class="col-md-6">
            			        <div class="form-group">
            			          <label>Is Paid</label>
            			          {!! Form::select('is_paid',binary(), null,['class'=>'form-control form-control-line']) !!}
            			        </div>
            	    		</div>
                            <div class="col-md-6 d-none">
                                <div class="form-group">
                                  <label>Subscribe Default</label>
                                  {!! Form::select('subscribe_default',binary(), 0,['class'=>'form-control form-control-line']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label>Custom Forms</label>
                                  {!! Form::select('custom_form_id',$custom_forms, null,['class'=>'form-control form-control-line', 'placeholder' => 'Select Custom Form']) !!}
                                </div>
                            </div>
            	    		
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){


	  $("#addCourse").click(function(){
	    $("#courseModal").modal();
	  });

	$('#addCourseForm').submit(function (data) {
		$.post("{{ route('course.store') }}",$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#courseModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

function copy(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
    notification('Copied!','success')
}



</script>
<script type="text/javascript">
    $(".abc").css({"background-color": "yellow", "font-size": "200%"});
</script>
{!! $datatable->scripts() !!}
@endpush

    {!! Form::model($user, ['route' => ['subscriber.registered.update', $user->id],'method'=>'PUT','id'=>'editSubscriberForm','class'=>'form-material', 'autocomplete' => 'off']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Subscriber</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >
      <div id="errors"></div>
      <div class="row">
        {!! Form::hidden('id',$user->id) !!}
        <div class="col-md-6">
          <div class="form-group">
            <label>Last Name</label>
            {!! Form::text('last_name',$user->last_name,['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>First Name</label>
            {!! Form::text('first_name',$user->first_name,['class'=>'form-control']) !!}
          </div>
        </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Email</label>
          {!! Form::text('email',$user->email,['class'=>'form-control','id'=>'email']) !!}
        </div>
      </div>
        <div class="col-md-6">
         <div class="form-group">
          <label>Contact</label>
          {!! Form::text('mobile',$user->mobile,['class'=>'form-control','id'=>'mobile']) !!}
        </div>  
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Gender</label>
          {!! Form::select('gender',gender(), $user->gender,['class'=>'form-control','required'=>true]) !!}
        </div>
      </div>
       <div class="col-md-6">
        <div class="form-group">
         <label>Birthdate</label>
         {!! Form::text('birthday',$user->birthday,['class'=>'form-control form-control-line datetimepickerForBirtday','id'=>'birthday']) !!}
       </div>  
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label class="radio-inline">Are you an alumni member of Caylum? <input type="radio"  {{ ($user->alumini_member) ? "checked" : "" }} name="alumini_member" value="1"> Yes <input type="radio" {{ (!$user->alumini_member) ? "checked" : "" }} value="0" name="alumini_member"> No</label>
        </div>
      </div>


      <div id="yes" class="col-12">
        <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Courses</label>
            {!! Form::select('alumini_course',$courses,$user->alumini_course,['class'=>'form-control','placeholder'=>'Select Course','id'=>'alumini_course']) !!}
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>What year did you attend Caylum?</label>
            {!! Form::select('alumini_attend',years(),$user->alumini_attend,['class'=>'form-control','placeholder'=>'Select Year','id'=>'alumini_attend']) !!}
          </div>
        </div>
      </div>
      </div>

 
      <div class="col-md-6">
        <div class="form-group">
          <label>Active</label>

          {!! Form::select('active',binary(),($user->active) ? true:false,['class'=>'form-control']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Password</label>
          {!! Form::password('password',['class'=>'form-control','id'=>'password']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Confirm Password</label>
          
          {!! Form::password('password_confirmation',['class'=>'form-control','id'=>"password-confirm"]) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Comments</label>
          {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>2]) !!}
        </div>
      </div>
      <div class="col-md-12">
        <h4><b>Subscription</b></h4>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Subscription</label>

          {!! Form::select('subscription_id',$subscriptions,null,['class'=>'form-control', 'placeholder' => 'Select Subscription Plan']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Start Date</label>

          {!! Form::text('starts_at',null,['class'=>'form-control datetimepicker']) !!}
        </div>
      </div>
     
      <div class="col-md-6">
        <div class="form-group">
          <label>Active Subscription</label>
          {!! Form::select('active_subscription',binary(), 0,['class'=>'form-control']) !!}
        </div>
      </div>

      

    </div>
    
  </div>

  <div class="modal-footer">
    <input type="submit" value="Submit" class="btn btn-primary">

  </div>


  <!-- Modal footer -->

  {!! Form::close() !!}

  <script type="text/javascript">

    $(document).ready(function(){

      $('#editSubscriberForm').disableAutoFill();

      if($('input[name=alumini_member]:checked', '#editSubscriberForm').val() == '1') {

        $("#editSubscriberForm #yes").show();
      }

      if($('input[name=alumini_member]:checked', '#editSubscriberForm').val() == '0'){
      
        $("#editSubscriberForm #yes").hide();
      }
    });


    $('#editSubscriberForm').submit(function () { 

      $.post($(this).attr('action'),$(this).serialize(), function(data){

        if(data.errors)
        {
            $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
        }else{

          if(data.success == 'true')
          {
            $("#editSubscriberModal").modal('hide');
            notification(data.message,'success')
            location.reload();
            

          }else{

           notification('Something went wrong','danger')

         }

       }

     });




      return false;

    });

    $("input[name='alumini_member']").change(function(){


      if($(this).val() == '1') {

        $("#editSubscriberForm #yes").show();
      }

      if($(this).val() == '0'){
        $("#editSubscriberForm #yes").hide();
      }
    });
  </script>

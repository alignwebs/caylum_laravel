@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                   <button type="button" class="btn btn-warning" id="addSubscriber">+ Add Subscriber</button>
                   <a href="{{ route('subscriber.export') }}" target="_blank" class="btn btn-success" id="addPayment">Export</a>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="subscriberModal">
  <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Subscriber</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'subscriber.store','class'=>'form-material','id'=>'addSubscriberForm']) !!}
            <div class="modal-body">
              <div id="errors"></div>
                      <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Last Name</label>
                                {!! Form::text('last_name',null,['class'=>'form-control','required'=>'true']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>First Name</label>
                                {!! Form::text('first_name',null,['class'=>'form-control','required'=>'true']) !!}
                              </div>
                            </div>
                          <div class="col-md-6">
                          <div class="form-group">
                            <label>Email</label>
                            {!! Form::text('email',null,['class'=>'form-control','required'=>'true','id'=>'email']) !!}
                          </div>
                        </div>
                              <div class="col-md-6">
                           <div class="form-group">
                            <label>Contact</label>
                            {!! Form::text('mobile',null,['class'=>'form-control','required'=>'true','id'=>'mobile']) !!}
                          </div>  
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Gender</label>
                            {!! Form::select('gender',gender(), null,['class'=>'form-control','required'=>true]) !!}
                          </div>
                        </div>
                         <div class="col-md-6">
                          <div class="form-group">
                           <label>Birthdate</label>
                           {!! Form::text('birthday',null,['class'=>'form-control form-control-line datetimepickerForBirtday','id'=>'birthday']) !!}
                         </div>  
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="radio-inline">Are you an alumni member of Caylum? <input type="radio"  name="alumini_member" value="1"> Yes <input type="radio" checked value="0" name="alumini_member"> No</label>
                          </div>
                        </div>
                        <div id="yes" style="display: none;" class="col-12">
                          <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Courses</label>
                           {!! Form::select('alumini_course',$courses,null,['class'=>'form-control','placeholder'=>'Select Course','id'=>'alumini_course']) !!}
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label>What year did you attend Caylum?</label>
                           {!! Form::select('alumini_attend',years(),null,['class'=>'form-control','placeholder'=>'Select Year','id'=>'alumini_attend']) !!}
                          </div>
                        </div>
                        </div>
                        </div>
                          <div class="col-md-6">
                          <div class="form-group">
                            <label>Password</label>
                            {!! Form::password('password',['class'=>'form-control','required'=>'true','id'=>'password']) !!}
                          </div>
                        </div>
                          <div class="col-md-6">
                          <div class="form-group">
                            <label>Confirm Password</label>
                            
                            {!! Form::password('password_confirmation',['class'=>'form-control','required'=>'true', 'id'=>"password-confirm"]) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Active User</label>

                            {!! Form::select('active',binary(),null,['class'=>'form-control','required'=>'true']) !!}
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Comments</label>
                            {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>2]) !!}
                          </div>
                        </div>
                        <div class="col-md-12">
                          <h4><b>Subscription</b></h4>
                        </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Subscription</label>

                                  {!! Form::select('subscription_id',$subscriptions,null,['class'=>'form-control','required'=>'true']) !!}
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Start Date</label>

                                  {!! Form::text('starts_at',null,['class'=>'form-control datetimepicker','required'=>'true']) !!}
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Active Subscription</label>

                                  {!! Form::select('active_subscription',binary(),null,['class'=>'form-control','required'=>'true']) !!}
                                </div>
                              </div>

                        
            </div>
            </div>     
            	        
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

	  $("#addSubscriber").click(function(){
	    $("#subscriberModal").modal();
      $('.datetimepicker').bootstrapMaterialDatePicker({
           time:false
       });
	  });

	$('#addSubscriberForm').submit(function (data) {
		$.post($(this).attr('action'),$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#subscriberModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

  $("input[name='alumini_member']").change(function(){


      if($(this).val() == '1') {
  
          $("#yes").show();
      }

      if($(this).val() == '0'){
          $("#yes").hide();
      }
  });

</script>
{!! $datatable->scripts() !!}

@endpush
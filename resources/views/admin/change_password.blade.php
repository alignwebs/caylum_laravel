@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>            

            </div>
        </div>
<div class="container-fluid">
       <!-- Input -->
       <div class="row clearfix">
           <div class="col-lg-12 col-md-12 col-sm-12">
               <div class="card">
                   <div class="header">
                       <h2><strong>Change Password</strong> </h2>                        
                   </div>
                   <div class="body">

                       {!! Form::open(['class'=>'form-material','id'=>'changePassword']) !!}
                       <div class="row clearfix">
                       <div class="col-sm-4">
                               <label for="old_password">Old Password</label>
                               <div class="form-group">                                    
                                   {!! Form::password('old_password',['class'=>'form-control']) !!}                                   
                               </div>
                        </div>
                        </div>
                        <div class="row clearfix">
                       <div class="col-sm-4">
                                <label for="new_password">New Password</label>
                               <div class="form-group">                                    
                                  {!! Form::password('new_password',['class'=>'form-control']) !!}                                   
                               </div>
                        </div>
                        </div>
                        <div class="row clearfix">
                       <div class="col-sm-4">
                        <label for="new_password">Confirm Password</label>
                               <div class="form-group">                                    
                                  {!! Form::password('new_password_confirmation',['class'=>'form-control form-control-line', 'id'=>"new_password-confirm"]) !!}                                   
                               </div>
                        </div>
                        </div>
                         <div class="row clearfix">
                        <div class="col-sm-4">
                                <div class="form-group">                                    
                                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Update</button>                                   
                                </div>
                         </div>
                         </div>   
                       
                   </div>
               </div>
           </div>
       </div>
  </div>
</section>
@endsection
@push('scripts')

<script type="text/javascript">
    
  $('#changePassword').submit(function () {

    $.post("{{ route('update.password') }}",$(this).serialize(), function(data){
        
      if(data.errors)
      {
        notification(data.errors,'danger');

      }else{

        if(data.success == 'true')
        { 
            notification(data.message,'success')
          
        }else{

            notification(data.message,'danger')

        }

      }

    });
    
    return false;

  });


</script>



@endpush
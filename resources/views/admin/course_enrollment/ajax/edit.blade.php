    {!! Form::model($enrollment, ['route' => ['enrollment.update', $enrollment->id],'method'=>'PUT','id'=>'editCourseEnrollmentForm','class'=>'form-material', 'autocomplete' => 'off']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Course Enrollment</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >
      <div id="errors"></div>
      <div class="row">

         <div class="col-md-6">
            <div class="form-group">
              <label>Last Name</label>
              {!! Form::text('last_name',$enrollment->last_name,['class'=>'form-control','required'=>'true']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>First Name</label>
              {!! Form::text('first_name',$enrollment->first_name,['class'=>'form-control','required'=>'true']) !!}
            </div>
          </div>
        <div class="col-md-6">
         <div class="form-group">
          <label>Mobile </label>
          {!! Form::text('mobile',$enrollment->mobile,['class'=>'form-control','required'=>'true','id'=>'mobile']) !!}
        </div>  
      </div>
       <div class="col-md-6">
        <div class="form-group">
         <label>Birthday</label>
         {!! Form::text('birthday',$enrollment->birthday,['class'=>'form-control form-control-line datetimepickerForBirtday','id'=>'birthday']) !!}
       </div>  
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Gender</label>
          {!! Form::select('gender',gender(), null,['class'=>'form-control','required'=>true]) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Email</label>
          {!! Form::text('email',$enrollment->email,['class'=>'form-control','required'=>'true','id'=>'email']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Active</label>

          {!! Form::select('active',binary(),($enrollment->active) ? true:false,['class'=>'form-control','required'=>'true']) !!}
        </div>
      </div>
   
      <div class="col-md-6">
        <div class="form-group">
          <label>Course</label>

          {!! Form::select('course_id',$courses,$enrollment->course_id,['class'=>'form-control','required'=>'true']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Comments</label>
          {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>2]) !!}
        </div>
      </div>
     <!--  @if(!empty($subscriber))
     <div class="col-md-12">
         <h4 class="title" id="defaultModalLabel" style="font-size: 20px;">Subscription</h4>
       </div>
     {!! Form::hidden('subscriber_id',$subscriber->id) !!}
     <div class="col-md-6">
       <div class="form-group">
         <label>Subscription</label>
         {!! Form::select('subscription_id',$subscriptions,$subscriber->subscription_id,['placeholder'=>'Select Subscription','class'=>'form-control']) !!}
       </div>
     </div>
     <div class="col-md-6">
       <div class="form-group">
         <label>Start Date</label>
         {!! Form::text('starts_at',$subscriber->starts_at,['class'=>'form-control datetimepicker','required'=>'true']) !!}
       </div>
     </div>
     <div class="col-md-6">
       <div class="form-group">
         <label>Expire Date</label>
         {!! Form::text('expires_at',$subscriber->expires_at,['class'=>'form-control datetimepicker','required'=>'true']) !!}
       </div>
     </div>
     @endif -->



    </div>

  </div>

  <div class="modal-footer">
    <input type="submit" value="Submit" class="btn btn-primary">

  </div>


  <!-- Modal footer -->

  {!! Form::close() !!}

  <script type="text/javascript">
    
    $('#editCourseEnrollmentForm').disableAutoFill();
    $('#editCourseEnrollmentForm').submit(function () { 

      $.post($(this).attr('action'),$(this).serialize(), function(data){

        if(data.errors)
        {
          $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

        }else{

          if(data.success == 'true')
          {
            $("#editCourseEnrollmentModal").modal('hide');
            notification(data.message,'success')
            location.reload();


          }else{

           notification('Something went wrong','danger')

         }

       }

     });




      return false;

    });

  </script>

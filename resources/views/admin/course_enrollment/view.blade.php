@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Course Enrollment #{{ $enrollment->id }}</h2>
      </div> 
      <div class="col-md-7 text-right">  
         <button type="button" class="btn btn-warning" onclick="editAjax('{{ route('enrollment.edit',$enrollment->id) }}')" >Edit</button>
        {!! Form::open(['route' => ['enrollment.destroy', $enrollment->id], 'method' => 'delete', 'class' => 'deleteFrmAjax','style'=>'display:inline']) !!}
        <button type="submit" class="btn-delete btn btn-danger">Delete</button>
        {!! Form::close() !!}
        <button onClick="window.print()" class="btn btn-info">Print</button>

      </div>              
    </div>
  </div>
  <div class="container-fluid">
   <div class="row ">
     <div class="col-md-6">
          <div class="card">
            <div class="card-header">Enrollment Details</div>
            <div class="body">
             <table class="table table-bordered">
               <tr>
                 <th>Last Name</th>
                 <td>{{ $enrollment->last_name }}</td>
               </tr>
               <tr>
                <th>First Name</th>
                 <td>{{ $enrollment->first_name }}</td>
               </tr>
               <tr>  
                  <th>Mobile</th>
                  <td>{{ $enrollment->mobile }}</td></tr>
               <tr>
                 <th>Birthday</th>
                 <td>{{ $enrollment->birthday }}</td>
               </tr>
               <tr> 
                <th>Email</th>
                <td>{{ $enrollment->email }}</td></tr>
               <tr>
                <th>Gender</th>
                <td>{{ $enrollment->gender }}</td>
               </tr>
               <tr>
                <th>Payment Mode</th>
                <td>{{ $enrollment->payment_mode }}</td>
               </tr>
                <tr>
                <th>Comments</th>
                <td>{{ $enrollment->comments }}</td>
               </tr>
             </table>
           </div>
          </div>
        </div>
          
          @if($enrollment->course_details)
          <div class="col-md-6">
          <div class="card">
            <div class="card-header">Course Details</div>
            <div class="body">
             <table class="table table-bordered">
               <tr>
                 <th>Course Title</th>
                 <td>{{ $enrollment->course_details->name }}</td>
               </tr>
               <tr>
                 <th>Description</th>
                 <td>{{ $enrollment->course_details->description }}</td>
               </tr>
               <tr>
                  <th>Standard Rate</th>
                 <td>{{ $enrollment->course_details->std_rate }}</td>
               </tr>
               <tr>
                 <th>Published Rate</th>
                 <td>{{ $enrollment->course_details->price }}</td>
               </tr>
             </table>
           </div>
          </div>
        </div>
        @endif
      </div>
        @if($subscriber)
    <div class="row">
          <div class="col-md-6">
          <div class="card">
            <div class="card-header">Subscription Details</div>
            <div class="body">
             <table class="table table-bordered">
               <tr>
                 <th>Subscription</th>
                 <td>{{ $subscriber->subscription->name }}</td>
                 <th>Fee</th>
                 <td>{{ $subscriber->subscription->fee }}</td>
               </tr>
               <tr>
                 <th>Duration (Months)</th>
                 <td>{{ $subscriber->subscription->duration }}</td>
                 <th>Active</th>
                 <td>{{ ($subscriber->subscription->active) ? 'True' : 'False' }}</td>
               </tr>
             </table>
           </div>
          </div>
        </div>
    </div>
        @endif
        @if(!empty($enrollment->custom_form) && $enrollment->custom_form != '[]')
        <div class="row">
          <div class="col-md-12">
          <div class="card">
            <div class="card-header">Custom Form Response</div>
            <div class="body">
             <table class="table table-bordered">
              @foreach(json_decode($enrollment->custom_form,true) as $key => $custom_form)
               <tr>
                 <th>{!! array_keys($custom_form)[0] !!}</th>
                 <td>
                  @if(gettype($custom_form) == 'array')
                      @if(isset($custom_form['is_file']))
                        @php 
                          unset($custom_form['is_file']);
                          $keys = array_keys($custom_form)[0];
                        @endphp
                        <a href="{{ Storage::url($custom_form[$keys]) }}" target="_new">View Attachement</a>
                      @else
                        {{ implode(", ",$custom_form) }}
                      @endif
                  @else
                      {{ $custom_form}}
                  @endif
                  </td>
               </tr>
              @endforeach
             </table>
           </div>
          </div>
          </div>
        </div>
        @endif
 </div>
</div>
</section>

@endsection




@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){

   $("#addCourseEnrollment").click(function(){
     $("#courseEnrollmentModal").modal();
     $('.datetimepicker').bootstrapMaterialDatePicker({
       time:false
     });
   });

   $('#addCourseEnrollmentForm').submit(function (data) {
    $.post($(this).attr('action'),$(this).serialize(), function(data){

     if(data.errors)
     {
      $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

    }else{

     if(data.success == 'true')
     {
       $("#courseEnrollmentModal").modal('hide');
       notification(data.message,'success')
       location.reload();
     }else{

      notification('Something Went Wrong','danger')

    }

  }


})

    return false;

  });

 });



</script>

@endpush

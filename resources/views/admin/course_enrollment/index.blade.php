@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12">
        <h2>{{ $title }}</h2>
      </div>            
      <div class="col-md-7 text-right">  
         <button type="button" class="btn btn-warning" id="addCourseEnrollment">+ Add Course Enrollment</button>
         <a href="{{ route('enrollment.export') }}" target="_blank" class="btn btn-success">Export</a>
      </div>   
    </div>
  </div>
  <div class="container-fluid">
   <div class="row clearfix">
     <div class="col-lg-12">
       <div class="card">
         <div class="body">

         <div class="table-responsive">
           {!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="courseEnrollmentModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="title" id="defaultModalLabel">Add New Course Enrollment</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      {!! Form::open(['route'=>'enrollment.store','class'=>'form-material','id'=>'addCourseEnrollmentForm']) !!}
      <div class="modal-body">
        <div id="errors"></div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Last Name</label>
              {!! Form::text('last_name',null,['class'=>'form-control','required'=>'true']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>First Name</label>
              {!! Form::text('first_name',null,['class'=>'form-control','required'=>'true']) !!}
            </div>
          </div>
          <div class="col-md-6">
           <div class="form-group">
            <label>Mobile</label>
            {!! Form::text('mobile',null,['class'=>'form-control','required'=>'true','id'=>'mobile']) !!}
          </div>  
        </div>
         <div class="col-md-6">
          <div class="form-group">
           <label>Birthday</label>
           {!! Form::text('birthday',null,['class'=>'form-control form-control-line datetimepickerForBirtday','id'=>'birthday']) !!}
         </div>  
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Gender</label>
            {!! Form::select('gender',gender(), null,['class'=>'form-control','required'=>true]) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Email</label>
            {!! Form::text('email',null,['class'=>'form-control','required'=>'true','id'=>'email']) !!}
          </div>
        </div>
         <div class="col-md-6">
          <div class="form-group">
            <label>Active</label>

            {!! Form::select('active',binary(),null,['class'=>'form-control','required'=>'true']) !!}
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Course</label>

            {!! Form::select('course_id',$courses,null,['class'=>'form-control','required'=>'true']) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Comments</label>
            {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>2]) !!}
          </div>
        </div>


        <!-- <div class="col-md-12">
          <h4 class="title" id="defaultModalLabel" style="font-size: 20px;">Subscription</h4>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Subscription</label>
        
            {!! Form::select('subscription_id',$subscriptions,null,['placeholder'=>'Select Subscription','class'=>'form-control']) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Start Date</label>
        
            {!! Form::text('starts_at',null,['class'=>'form-control datetimepicker']) !!}
          </div>
        </div> -->


      </div>
    </div>     

    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
    </div>
  </div>
  {!! Form::close() !!}
</div>
</div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

   $("#addCourseEnrollment").click(function(){
     $("#courseEnrollmentModal").modal();
     $('.datetimepicker').bootstrapMaterialDatePicker({
       time:false
     });
   });

   $('#addCourseEnrollmentForm').submit(function (data) {
    $.post($(this).attr('action'),$(this).serialize(), function(data){

     if(data.errors)
     {
      $("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

    }else{

     if(data.success == 'true')
     {
       $("#courseEnrollmentModal").modal('hide');
       notification(data.message,'success')
       location.reload();
     }else{

      notification('Something Went Wrong','danger')

    }

  }


})

    return false;

  });

 });



</script>
{!! $datatable->scripts() !!}

@endpush
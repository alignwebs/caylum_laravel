    {!! Form::model($consensus, ['route' => ['consensus.update', $consensus->id],'method'=>'PUT','id'=>'editConsensusForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Consensus Watchlist</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                <div class="row">
              
                <div class="col-md-6">
                   <div class="form-group">
                        <label>Ticker</label>
                        {!! Form::text('ticker',null,['class'=>'form-control','required'=>true]) !!}
                    </div>
                </div>
                   <div class="col-md-6">
                        <div class="form-group">
                             <label>Market Cap (In Bil PHP)</label>
                             {!! Form::number('market_cap',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Highest Tareget Price</label>
                             {!! Form::number('highest_target_price',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Sales 4Q18</label>
                             {!! Form::number('sales_4Q18',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Sales 1Q19</label>
                             {!! Form::number('sales_1Q19',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Net Income 4Q18</label>
                             {!! Form::number('net_income_4Q18',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Net Income 1Q19</label>
                             {!! Form::number('net_income_1Q19',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group">
                             <label>P/E</label>
                             {!! Form::number('pe',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                             <label>ROE (%)</label>
                             {!! Form::number('roe',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group">
                             <label>DIV YIELD</label>
                             {!! Form::number('div_yield',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
               </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editConsensusForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

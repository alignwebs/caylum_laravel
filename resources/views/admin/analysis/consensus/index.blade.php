@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-warning" id="addConsensus">+ Add Consensus</button>
                    <button type="button" class="btn btn-success" id="importConsensus"><i class="fa fa-upload"></i> Import</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="consensusModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Consensus</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'consensus.store','class'=>'form-material','id'=>'addConsensusForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        

            	    		<div class="col-md-6">
            			       <div class="form-group">
            			            <label>Ticker</label>
            			            {!! Form::text('ticker',null,['class'=>'form-control','required'=>true]) !!}
            			        </div>
            	    		</div>
            	           <div class="col-md-6">
                              <div class="form-group">
                                   <label>Market Cap (In Bil PHP)</label>
                                   {!! Form::number('market_cap',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Highest Tareget Price</label>
                                   {!! Form::number('highest_target_price',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Sales 4Q18</label>
                                   {!! Form::number('sales_4Q18',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Sales 1Q19</label>
                                   {!! Form::number('sales_1Q19',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Net Income 4Q18</label>
                                   {!! Form::number('net_income_4Q18',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Net Income 1Q19</label>
                                   {!! Form::number('net_income_1Q19',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                             <div class="col-md-6">
                              <div class="form-group">
                                   <label>P/E</label>
                                   {!! Form::number('pe',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>ROE (%)</label>
                                   {!! Form::number('roe',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                             <div class="col-md-6">
                              <div class="form-group">
                                   <label>DIV YIELD</label>
                                   {!! Form::number('div_yield',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Import Excel -->
<div class="modal fade" id="importConsensusModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Import Consensus</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'consensus.import','class'=>'form-material','id'=>'importConsensusForm','files'=>true]) !!}
            <div class="modal-body">
                      <div id="errors"></div>
                      <div class="row">
                      

                         <div class="form-group">
                          <div class="col-md-12">
                              <input type="file" class="form-control" id="consensus" name="consensus" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">  
                              
                          </div>
                      </div>
                     </div>
            </div>
           <div class="modal-footer">
                 <a  href="{{ asset('theme/templates/consensus.xlsx') }}" class="btn btn-default btn-round waves-effect mr-auto">Download Template</a>
                 <button type="submit" class="btn btn-primary btn-round waves-effect" id="importConsensusFormBtn">Import</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

    // SIMPLE MODAL
	  $("#addConsensus").click(function(){
	    $("#consensusModal").modal();
	  });

    //IMPORT FORM MODAL
    $("#importConsensus").click(function(){
      $("#importConsensusModal").modal();
    });

  // SUBMIT SIMPLE FORM
	$('#addConsensusForm').submit(function (data) {
		$.post("{{ route('consensus.store') }}",$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#consensusModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

  // SUBMIT IMPORT FORM
  $('#importConsensusForm').submit(function (e) {
      e.preventDefault();
      $("#importConsensusFormBtn").prop('disabled', true);
      let action = $(this).attr('action');
      var form = $(this)[0];
      var data = new FormData(form);

          $.ajax({
              type: "POST",
              enctype: 'multipart/form-data',
              url: action,
              data: data,
              processData: false,
              contentType: false,
              cache: false,
              timeout: 600000,
              success: function (data) {
                  notification(data.message,'success')
                  location.reload();
              },
              error: function (e) {
                  $("#importConsensusFormBtn").prop('disabled', false);
                  alert("ERROR : " + e);
              }
          });

  });


</script>
{!! $datatable->scripts() !!}
@endpush
    {!! Form::model($ph, ['route' => ['ph.update', $ph->id],'method'=>'PUT','id'=>'editPHDataForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit PH Data</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                      <div class="row">
                      
                         <div class="col-md-6">
                             <div class="form-group">
                                  <label>Country</label>
                                  {!! Form::select('country',$countries,$ph->country,['class'=>'form-control']) !!}
                              </div>
                          </div>  

                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Landbased</label>
                                   {!! Form::text('landbased',null,['class'=>'form-control']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Seabased</label>
                                   {!! Form::text('seabased',null,['class'=>'form-control']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Month</label>
                                   {!! Form::text('month',null,['class'=>'form-control datetimepicker']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Year</label>
                                   {!! Form::text('year',null,['class'=>'form-control datetimepicker']) !!}
                              </div>
                            </div>
                          
                            
                     </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editPHDataForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

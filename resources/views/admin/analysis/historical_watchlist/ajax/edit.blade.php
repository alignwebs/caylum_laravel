    {!! Form::model($historical, ['route' => ['historical.update', $historical->id],'method'=>'PUT','id'=>'editHistoricalForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Historical Watchlist</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                      <div class="row">
                      

                      <div class="col-md-6">
                         <div class="form-group">
                              <label>Date Added</label>
                              {!! Form::text('date_added',null,['class'=>'form-control datetimepicker','required'=>true]) !!}
                          </div>
                      </div>
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label>Stock</label>
                                    <select name="stock" class='form-control form-control2' id="stock">
                                        <option value="">Select Stock</option>
                                        @foreach($stocks as $stock)
                                          <option value="{{ $stock->full_stock_name }}" {{ ($stock->full_stock_name == $historical->stock) ? 'selected' : '' }}>{{ $stock->full_stock_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                         <div class="col-md-6">
                              <div class="form-group">
                                   <label>Price Added</label>
                                   {!! Form::number('price_added',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                              </div>
                            </div>
                             <div class="col-md-6">
                              <div class="form-group">
                                   <label>Setup</label>
                                   {!! Form::text('setup',null,['class'=>'form-control','required'=>true]) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Date Deleted</label>
                                   {!! Form::text('date_deleted',null,['class'=>'form-control datetimepicker']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Price Deleted</label>
                                   {!! Form::number('price_deleted',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Return (%)</label>
                                   {!! Form::number('return',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Comments</label>
                            {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>4]) !!}
                          </div>
                      </div>
                     </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editHistoricalForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                  <button type="button" class="btn btn-warning" id="addHistoricalWatchlist">+ Add Historical Watchlist</button>
                  <button type="button" class="btn btn-success" id="importHistoricalWatchlist"><i class="fa fa-upload"></i> Import</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="historicalWatchlistModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Historical Watchlist</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'historical.store','class'=>'form-material','id'=>'addHistoricalWatchlistForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        

            	    		<div class="col-md-6">
            			       <div class="form-group">
            			            <label>Date Added</label>
            			            {!! Form::text('date_added',null,['class'=>'form-control datetimepicker','required'=>true]) !!}
            			        </div>
            	    		</div>
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label>Stock</label>
                                    <select name="stock" class='' id="stock" required>
                                        <option value="">Select Stock</option>
                                        @foreach($stocks as $stock)
                                          <option value="{{ $stock->full_stock_name }}">{{ $stock->full_stock_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
            	           <div class="col-md-6">
                              <div class="form-group">
                                   <label>Price Added</label>
                                   {!! Form::number('price_added',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                              </div>
                            </div>
                             <div class="col-md-6">
                              <div class="form-group">
                                   <label>Setup</label>
                                   {!! Form::text('setup',null,['class'=>'form-control','required'=>true]) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Date Deleted</label>
                                   {!! Form::text('date_deleted',null,['class'=>'form-control datetimepicker']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Price Deleted</label>
                                   {!! Form::number('price_deleted',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Return (%)</label>
                                   {!! Form::number('return',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
            	        	<div class="col-md-12">
            			        <div class="form-group">
            			          <label>Comments</label>
            			          {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>4]) !!}
            			        </div>
            	    		</div>
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Import Excel -->
<div class="modal fade" id="importHistoricalWatchlistModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Import Historical Watchlist</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'historical.import','class'=>'form-material','id'=>'importHistoricalWatchlistForm','files'=>true]) !!}
            <div class="modal-body">
                      <div id="errors"></div>
                      <div class="row">
                      

                         <div class="form-group">
                          <div class="col-md-12">
                              <input type="file" class="form-control" id="historical_watchlists" name="historical_watchlists" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">  
                              
                          </div>
                      </div>
                     </div>
            </div>
             <div class="modal-footer">
                 <a  href="{{ asset('theme/templates/historical_watchlists.xlsx') }}" class="btn btn-default btn-round waves-effect mr-auto">Download Template</a>
                 <button type="submit" class="btn btn-primary btn-round waves-effect" id="importHistoricalWatchlistFormBtn">Import</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

	  $("#addHistoricalWatchlist").click(function(){
	    $("#historicalWatchlistModal").modal();
	  });

    $("#importHistoricalWatchlist").click(function(){
      $("#importHistoricalWatchlistModal").modal();
    });

	$('#addHistoricalWatchlistForm').submit(function (data) {
		$.post("{{ route('historical.store') }}",$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#historicalWatchlistModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});


  $('#importHistoricalWatchlistForm').submit(function (e) {
      e.preventDefault();
      $("#importHistoricalWatchlistFormBtn").prop('disabled', true);
      let action = $(this).attr('action');
      var form = $(this)[0];
      var data = new FormData(form);

          $.ajax({
              type: "POST",
              enctype: 'multipart/form-data',
              url: action,
              data: data,
              processData: false,
              contentType: false,
              cache: false,
              timeout: 600000,
              success: function (data) {
                  notification(data.message,'success')
                  location.reload();
              },
              error: function (e) {
                  $("#importHistoricalWatchlistFormBtn").prop('disabled', false);
                  alert("ERROR : " + e);
              }
          });

  });

	});

</script>
{!! $datatable->scripts() !!}
@endpush
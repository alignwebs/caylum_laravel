    {!! Form::model($dividend, ['route' => ['dividend.update', $dividend->id],'method'=>'PUT','id'=>'editDividendForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Dividend</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                <div class="row">
              
                        
                            

                                <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Type of Dividend</label>
                                        {!! Form::text('type_of_dividends',null,['class'=>'form-control','required'=>true]) !!}
                                    </div>
                                </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                            <label>Date Closed</label>
                                            {!! Form::text('date_closed',null,['class'=>'form-control datetimepicker','required'=>true]) !!}
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                     <div class="form-group">
                                          <label>Stock</label>
                                          <select name="stock" class='form-control form-control2' id="stock">
                                              <option value="">Select Stock</option>
                                              @foreach($stocks as $stock)
                                                <option value="{{ $stock->full_stock_name }}" {{ ($stock->full_stock_name == $dividend->stock) ? 'selected' : '' }}>{{ $stock->full_stock_name }}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                         <label>Amount</label>
                                         {!! Form::number('amount',null,['class'=>'form-control','step' => '0.01']) !!}
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                     <div class="form-group">
                                          <label>Ex Date</label>
                                          {!! Form::text('ex_date',null,['class'=>'form-control datetimepicker']) !!}
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                     <div class="form-group">
                                          <label>Record Date</label>
                                          {!! Form::text('record_date',null,['class'=>'form-control datetimepicker']) !!}
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                     <div class="form-group">
                                          <label>Payment Date</label>
                                          {!! Form::text('payment_date',null,['class'=>'form-control datetimepicker']) !!}
                                      </div>
                                  </div>
                          
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editDividendForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

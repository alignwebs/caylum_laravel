@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-warning" id="addDividend">+ Add Dividend</button>
                    <button type="button" class="btn btn-success" id="importDividend"><i class="fa fa-upload"></i> Import</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="dividendModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Dividend</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'dividend.store','class'=>'form-material','id'=>'addDividendForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        

                	    		<div class="col-md-6">
                			       <div class="form-group">
                			            <label>Type of Dividend</label>
                			            {!! Form::text('type_of_dividends',null,['class'=>'form-control','required'=>true]) !!}
                			        </div>
                	    		</div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                      <label>Date Closed</label>
                                      {!! Form::text('date_closed',null,['class'=>'form-control datetimepicker','required'=>true]) !!}
                                  </div>
                              </div>
              	           <div class="col-md-6">
                               <div class="form-group">
                                    <label>Stock</label>
                                    <select name="stock" class='' id="stock" required>
                                        <option value="">Select Stock</option>
                                        @foreach($stocks as $stock)
                                          <option value="{{ $stock->full_stock_name }}">{{ $stock->full_stock_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Amount</label>
                                   {!! Form::number('amount',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label>Ex Date</label>
                                    {!! Form::text('ex_date',null,['class'=>'form-control datetimepicker']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label>Record Date</label>
                                    {!! Form::text('record_date',null,['class'=>'form-control datetimepicker']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label>Payment Date</label>
                                    {!! Form::text('payment_date',null,['class'=>'form-control datetimepicker']) !!}
                                </div>
                            </div>
                            
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Import Excel -->
<div class="modal fade" id="importDividendModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Import Dividend</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'dividend.import','class'=>'form-material','id'=>'importDividendForm','files'=>true]) !!}
            <div class="modal-body">
                      <div id="errors"></div>
                      <div class="row">
                      

                         <div class="form-group">
                          <div class="col-md-12">
                              <input type="file" class="form-control" id="dividend" name="dividend" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">  
                              
                          </div>
                      </div>
                     </div>
            </div>
             <div class="modal-footer">
                 <a  href="{{ asset('theme/templates/dividend.xlsx') }}" class="btn btn-default btn-round waves-effect mr-auto">Download Template</a>
                 <button type="submit" class="btn btn-primary btn-round waves-effect" id="importDividendFormBtn">Import</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

    // SIMPLE MODAL
	  $("#addDividend").click(function(){
	    $("#dividendModal").modal();
	  });

    //IMPORT FORM MODAL
    $("#importDividend").click(function(){
      $("#importDividendModal").modal();
    });

  // SUBMIT SIMPLE FORM
	$('#addDividendForm').submit(function (data) {
		$.post("{{ route('dividend.store') }}",$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#dividendModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

  // SUBMIT IMPORT FORM
  $('#importDividendForm').submit(function (e) {
      e.preventDefault();
      $("#importDividendFormBtn").prop('disabled', true);
      let action = $(this).attr('action');
      var form = $(this)[0];
      var data = new FormData(form);

          $.ajax({
              type: "POST",
              enctype: 'multipart/form-data',
              url: action,
              data: data,
              processData: false,
              contentType: false,
              cache: false,
              timeout: 600000,
              success: function (data) {
                  notification(data.message,'success')
                  location.reload();
              },
              error: function (e) {
                  $("#importDividendFormBtn").prop('disabled', false);
                  alert("ERROR : " + e);
              }
          });

  });


</script>
{!! $datatable->scripts() !!}
@endpush
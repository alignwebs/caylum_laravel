    {!! Form::model($buyback, ['route' => ['buyback.update', $buyback->id],'method'=>'PUT','id'=>'editCompanyBuybackForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Company Buyback</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                <div class="row">
                
                   <div class="col-md-6">
                       <div class="form-group">
                            <label>Stock Code</label>
                            <select name="stock_code" class='form-control form-control2' id="stock_code" required>
                                <option value="">Select Stock Code</option>
                                @foreach($stocks as $stock)
                                  <option value="{{ $stock->symbol }}" {{ ($stock->symbol == $buyback->stock_code) ? 'selected' : '' }}>{{ $stock->symbol }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>  

                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Buyback Program</label>
                             {!! Form::number('buyback_program',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Amount Left to Repurchased</label>
                             {!! Form::number('amount_left',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                             <label>Total Repurchased Amount</label>
                             {!! Form::number('total_amount',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                             <label>Repurchased Amount for the week ended.</label>
                             {!! Form::number('repurchased_amount',null,['class'=>'form-control','step' => '0.01']) !!}
                        </div>
                      </div>
                    
                      
               </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editCompanyBuybackForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-warning" id="addCompanyBuyback">+ Add Company Buyback</button>
                    <button type="button" class="btn btn-success" id="importCompanyBuyback"><i class="fa fa-upload"></i> Import</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="companyBuybackModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Company Buyback</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'buyback.store','class'=>'form-material','id'=>'addCompanyBuybackForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        
                         <div class="col-md-6">
                             <div class="form-group">
                                  <label>Stock Code</label>
                                  <select name="stock_code" class='' id="stock_code" required>
                                      <option value="">Select Stock Code</option>
                                      @foreach($stocks as $stock)
                                        <option value="{{ $stock->symbol }}">{{ $stock->symbol }}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div>  

                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Buyback Program</label>
                                   {!! Form::number('buyback_program',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Amount Left to Repurchased</label>
                                   {!! Form::number('amount_left',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Total Repurchased Amount</label>
                                   {!! Form::number('total_amount',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                   <label>Repurchased Amount for the week ended.</label>
                                   {!! Form::number('repurchased_amount',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                          
                            
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Import Excel -->
<div class="modal fade" id="importCompanyBuybackModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Import Company Buyback</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'buyback.import','class'=>'form-material','id'=>'importCompanyBuybackForm','files'=>true]) !!}
            <div class="modal-body">
                      <div id="errors"></div>
                      <div class="row">
                         <div class="form-group">
                          <div class="col-md-12">
                              <input type="file" class="form-control" id="company_buyback" name="company_buyback" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"> 
                          </div>
                      </div>
                     </div>
            </div>
            <div class="modal-footer">
                 <a  href="{{ asset('theme/templates/buyback.xlsx') }}" class="btn btn-default btn-round waves-effect mr-auto">Download Template</a>
                 <button type="submit" class="btn btn-primary btn-round waves-effect" id="importCompanyBuybackFormBtn">Import</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

    // SIMPLE MODAL
	  $("#addCompanyBuyback").click(function(){
	    $("#companyBuybackModal").modal();
	  });

    //IMPORT FORM MODAL
    $("#importCompanyBuyback").click(function(){
      $("#importCompanyBuybackModal").modal();
    });

  // SUBMIT SIMPLE FORM
	$('#addCompanyBuybackForm').submit(function (data) {
		$.post("{{ route('buyback.store') }}",$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#companyBuybackModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

  // SUBMIT IMPORT FORM
  $('#importCompanyBuybackForm').submit(function (e) {
      e.preventDefault();
      $("#importCompanyBuybackFormBtn").prop('disabled', true);
      let action = $(this).attr('action');
      var form = $(this)[0];
      var data = new FormData(form);

          $.ajax({
              type: "POST",
              enctype: 'multipart/form-data',
              url: action,
              data: data,
              processData: false,
              contentType: false,
              cache: false,
              timeout: 600000,
              success: function (data) {
                  notification(data.message,'success')
                  location.reload();
              },
              error: function (e) {
                  $("#importCompanyBuybackFormBtn").prop('disabled', false);
                  alert("ERROR : " + e);
              }
          });

  });


</script>
{!! $datatable->scripts() !!}
@endpush
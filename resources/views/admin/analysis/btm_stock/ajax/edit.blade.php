    {!! Form::model($btm, ['route' => ['btm.update', $btm->id],'method'=>'PUT','id'=>'editBtmStocksForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit BTM Stocks</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                      <div class="row">
                      

                      <div class="col-md-6">
                         <div class="form-group">
                              <label>Company Name</label>
                              {!! Form::text('company_name',null,['class'=>'form-control','required'=>true]) !!}
                          </div>
                      </div>
                      <div class="col-md-6">
                         <div class="form-group">
                              <label>Category</label>
                              {!! Form::text('category',null,['class'=>'form-control','required'=>true]) !!}
                          </div>
                      </div>
                         <div class="col-md-6">
                              <div class="form-group">
                                   <label>Mkt Cap (Php Bil)</label>
                                   {!! Form::number('market_cap',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                              </div>
                            </div>
                            
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Last Price (Php)</label>
                                   {!! Form::number('last_price',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>TP (Php)</label>
                                   {!! Form::number('tp',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>FY19 EV/EBITDA</label>
                                   {!! Form::number('fy19',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                   <label>Narrative</label>
                                   {!! Form::textarea('narrative',null,['class'=>'form-control','rows'=>4]) !!}
                              </div>
                            </div>

                     </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editBtmStocksForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-warning" id="addBtmStock">+ Add BTM Stocks</button>
                    <button type="button" class="btn btn-success" id="importBtmStock"><i class="fa fa-upload"></i> Import</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="btmStockModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New BTM Stock</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'btm.store','class'=>'form-material','id'=>'addBtmStockForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        

            	    		<div class="col-md-6">
            			       <div class="form-group">
            			            <label>Company Name</label>
            			            {!! Form::text('company_name',null,['class'=>'form-control','required'=>true]) !!}
            			        </div>
            	    		</div>
                      <div class="col-md-6">
                         <div class="form-group">
                              <label>Category</label>
                              {!! Form::text('category',null,['class'=>'form-control','required'=>true]) !!}
                          </div>
                      </div>
            	           <div class="col-md-6">
                              <div class="form-group">
                                   <label>Mkt Cap (Php Bil)</label>
                                   {!! Form::number('market_cap',null,['class'=>'form-control','required'=>true,'step' => '0.01']) !!}
                              </div>
                            </div>
                            
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Last Price (Php)</label>
                                   {!! Form::number('last_price',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>TP (Php)</label>
                                   {!! Form::number('tp',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>FY19 EV/EBITDA</label>
                                   {!! Form::number('fy19',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                   <label>Narrative</label>
                                   {!! Form::textarea('narrative',null,['class'=>'form-control','rows'=>4]) !!}
                              </div>
                            </div>

            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Import Excel -->
<div class="modal fade" id="importBtmStockModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Import BTM Stocks</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'btm.import','class'=>'form-material','id'=>'importBtmStockForm','files'=>true]) !!}
            <div class="modal-body">
                      <div id="errors"></div>
                      <div class="row">
                      

                         <div class="form-group">
                          <div class="col-md-12">
                              <input type="file" class="form-control" id="btm_stocks" name="btm_stocks" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">  
                              
                          </div>
                      </div>
                     </div>
            </div>
            <div class="modal-footer">
                 <a  href="{{ asset('theme/templates/btm_stocks.xlsx') }}" class="btn btn-default btn-round waves-effect mr-auto">Download Template</a>
                 <button type="submit" class="btn btn-primary btn-round waves-effect" id="importBtmStockFormBtn">Import</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

    // SIMPLE MODAL
	  $("#addBtmStock").click(function(){
	    $("#btmStockModal").modal();
	  });

    //IMPORT FORM MODAL
    $("#importBtmStock").click(function(){
      $("#importBtmStockModal").modal();
    });

  // SUBMIT SIMPLE FORM
	$('#addBtmStockForm').submit(function (data) {
		$.post("{{ route('btm.store') }}",$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#btmStockModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

  // SUBMIT IMPORT FORM
  $('#importBtmStockForm').submit(function (e) {
      e.preventDefault();
      $("#importBtmStockFormBtn").prop('disabled', true);
      let action = $(this).attr('action');
      var form = $(this)[0];
      var data = new FormData(form);

          $.ajax({
              type: "POST",
              enctype: 'multipart/form-data',
              url: action,
              data: data,
              processData: false,
              contentType: false,
              cache: false,
              timeout: 600000,
              success: function (data) {
                  notification(data.message,'success')
                  location.reload();
              },
              error: function (e) {
                  $("#importBtmStockFormBtn").prop('disabled', false);
                  alert("ERROR : " + e);
              }
          });

  });


</script>
{!! $datatable->scripts() !!}
@endpush
    {!! Form::model($watchlist, ['route' => ['watchlist.update', $watchlist->id],'method'=>'PUT','id'=>'editWatchlistForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Watchlist</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                      <div class="row">
                        
                        <div class="col-md-6">
                             <div class="form-group">
                                  <label>Stock</label>
                                  <select name="stock" class='form-control form-control2' id="stock" required>
                                      <option value="">Select Stock Code</option>
                                      @foreach($stocks as $stock)
                                        <option value="{{ $stock->symbol }}" {{ ($stock->symbol == $watchlist->stock) ? 'selected' : '' }}>{{ $stock->symbol }}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div>  
                          <div class="col-md-6">
                             <div class="form-group">
                                  <label>Price Added</label>
                                  {!! Form::number('price_added',null,['class'=>'form-control', 'step' => '0.01']) !!}
                              </div>
                          </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Category</label>
                                   {!! Form::text('category',null,['class'=>'form-control']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Date Added</label>
                                   {!! Form::text('date_added',null,['class'=>'form-control datetimepicker']) !!}
                              </div>
                            </div>
                        
                            <div class="col-md-12">
                              <div class="form-group">
                                   <label>Comments</label>
                                   {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>4]) !!}
                              </div>
                            </div>
                          
                            
                     </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editWatchlistForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

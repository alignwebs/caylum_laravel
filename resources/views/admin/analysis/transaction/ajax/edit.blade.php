    {!! Form::model($transaction, ['route' => ['transaction.update', $transaction->id],'method'=>'PUT','id'=>'editInsiderTransactionForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Insider Transaction</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors"></div>

                      <div class="row">
                      
                         <div class="col-md-6">
                             <div class="form-group">
                                  <label>Stock</label>
                                  <select name="stock" class='form-control form-control2' id="stock" required>
                                      <option value="">Select Stock Code</option>
                                      @foreach($stocks as $stock)
                                        <option value="{{ $stock->symbol }}" {{ ($stock->symbol == $transaction->stock) ? 'selected' : '' }}>{{ $stock->symbol }}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div>  

                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Number of Shares</label>
                                   {!! Form::number('no_of_shares',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Price</label>
                                   {!! Form::number('price',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Total</label>
                                   {!! Form::number('total',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                   <label>A/D</label>
                                   {!! Form::text('ad',null,['class'=>'form-control']) !!}
                              </div>
                            </div>
                          
                            
                     </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editInsiderTransactionForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");

            }else{

              if(data.success == 'true')
              {
                $("#editModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

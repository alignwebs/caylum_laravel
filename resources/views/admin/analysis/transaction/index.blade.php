@extends('layouts.app')
@section('title', $title)
@section('content')
<style type="text/css">
    .table-inactive td {
    background-color: #ececec;
    color: #b9b9b9;
}
</style>
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>
                <div class="col-md-7 text-right">  
                    <button type="button" class="btn btn-warning" id="addInsiderTransaction">+ Add Insider Transaction</button>
                    <button type="button" class="btn btn-success" id="importInsiderTransaction"><i class="fa fa-upload"></i> Import</button>
                </div>          

            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="insiderTransactionModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Insider Transaction</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'transaction.store','class'=>'form-material','id'=>'addInsiderTransactionForm']) !!}
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
            	        
                         <div class="col-md-6">
                             <div class="form-group">
                                  <label>Stock</label>
                                  <select name="stock" class='' id="stock" required>
                                      <option value="">Select Stock Code</option>
                                      @foreach($stocks as $stock)
                                        <option value="{{ $stock->symbol }}">{{ $stock->symbol }}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div>  

                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Number of Shares</label>
                                   {!! Form::number('no_of_shares',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Price</label>
                                   {!! Form::number('price',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                   <label>Total</label>
                                   {!! Form::number('total',null,['class'=>'form-control','step' => '0.01']) !!}
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                   <label>A/D</label>
                                   {!! Form::text('ad',null,['class'=>'form-control']) !!}
                              </div>
                            </div>
                          
                            
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Import Excel -->
<div class="modal fade" id="importInsiderTransactionModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Import Insider Transaction</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'transaction.import','class'=>'form-material','id'=>'importInsiderTransactionForm','files'=>true]) !!}
            <div class="modal-body">
                      <div id="errors"></div>
                      <div class="row">
                         <div class="form-group">
                          <div class="col-md-12">
                              <input type="file" class="form-control" id="insider_transaction" name="insider_transaction" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"> 
                          </div>
                      </div>
                     </div>
            </div>
            <div class="modal-footer">
                 <a  href="{{ asset('theme/templates/insider_transaction.xlsx') }}" class="btn btn-default btn-round waves-effect mr-auto">Download Template</a>
                 <button type="submit" class="btn btn-primary btn-round waves-effect" id="importInsiderTransactionFormBtn">Import</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

    // SIMPLE MODAL
	  $("#addInsiderTransaction").click(function(){
	    $("#insiderTransactionModal").modal();
	  });

    //IMPORT FORM MODAL
    $("#importInsiderTransaction").click(function(){
      $("#importInsiderTransactionModal").modal();
    });

  // SUBMIT SIMPLE FORM
	$('#addInsiderTransactionForm').submit(function (data) {
		$.post("{{ route('transaction.store') }}",$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#insiderTransactionModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

  // SUBMIT IMPORT FORM
  $('#importInsiderTransactionForm').submit(function (e) {
      e.preventDefault();
      $("#importInsiderTransactionFormBtn").prop('disabled', true);
      let action = $(this).attr('action');
      var form = $(this)[0];
      var data = new FormData(form);

          $.ajax({
              type: "POST",
              enctype: 'multipart/form-data',
              url: action,
              data: data,
              processData: false,
              contentType: false,
              cache: false,
              timeout: 600000,
              success: function (data) {
                  notification(data.message,'success')
                  location.reload();
              },
              error: function (e) {
                  $("#importInsiderTransactionFormBtn").prop('disabled', false);
                  alert("ERROR : " + e);
              }
          });

  });


</script>
{!! $datatable->scripts() !!}
@endpush
@extends('layouts.app')
@section('title', $title)
@section('content')
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>{{ $title }}</h2>
                </div>            
                <div class="col-md-7 text-right">  
                   <button type="button" class="btn btn-warning" id="addMedia">+ Add Media</button>
                </div> 
            </div>
        </div>
<div class="container-fluid">
	<div class="row clearfix">
	    <div class="col-lg-12">
	        <div class="card">
	            <div class="body">
	                <div class="table-responsive">
	                		{!! $datatable->table(['class'=>'table table-bordered table-striped table-hover']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
</section>

<!-- Default Size -->
<div class="modal fade" id="mediaModal">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add New Media</h4>
            	<button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route'=>'media.store','class'=>'form-material','id'=>'addMediaForm']) !!}
            
            <div class="modal-body">
            	        <div id="errors"></div>
            	        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                  <label>Title</label>
                                  {!! Form::text('title',null,['class'=>'form-control','required'=>true]) !!}
                                </div>
                            </div>
            	    		<div class="col-md-12">
            			       <div class="form-group">
            			            <label>Type</label>
            			            {!! Form::select('type',['youtube'=>'Youtube','soundcloud'=>'Sound Cloud'],null,['class'=>'form-control','required'=>true]) !!}
            			        </div>
            	    		</div>
            	      
            	        	<div class="col-md-12">
            			        <div class="form-group">
            			          <label>URL</label>
            			          {!! Form::text('url',null,['class'=>'form-control','required'=>true]) !!}
            			        </div>
            	    		</div>
                            <div class="col-md-12">
                                <div class="form-group">
                                  <label>Sort Order</label>
                                  {!! Form::number('sort_order',null,['class'=>'form-control','required'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                  <label>Featured</label>
                                  {!! Form::select('featured', binary(), null, ['class'=>'form-control']) !!}
                                </div>
                            </div>

            	    		
            	       </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-round waves-effect">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection




@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){

	  $("#addMedia").click(function(){
	    $("#mediaModal").modal();
	  });

	$('#addMediaForm').submit(function (data) {
		$.post($(this).attr('action'),$(this).serialize(), function(data){


	    if(data.errors)
	    {
	    	$("#errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
	     
	    }else{

	      if(data.success == 'true')
	      {
	        $("#mediaModal").modal('hide');
	        notification(data.message,'success')
	      	location.reload();
	      }else{

	      	notification('Something Went Wrong','danger')
	        
	      }

	    }


	    })

			return false;

	});

	});

</script>
{!! $datatable->scripts() !!}

@endpush
    {!! Form::model($media, ['route' => ['media.update', $media->id],'method'=>'PUT','id'=>'editMediaForm','class'=>'form-material']) !!}
    {!! Form::hidden('id',$media->id) !!}
    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Media</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >
      <div id="errors"></div>
              <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                    <label>Title</label>
                    {!! Form::text('title',null,['class'=>'form-control','required'=>true]) !!}
                  </div>
              </div>
              <div class="col-md-12">
                 <div class="form-group">
                      <label>Type</label>
                      {!! Form::select('type',['soundcloud'=>'Sound Cloud','youtube'=>'Youtube'],null,['class'=>'form-control','required'=>true]) !!}
                  </div>
              </div>
            
                <div class="col-md-12">
                  <div class="form-group">
                    <label>URL</label>
                    {!! Form::text('url',null,['class'=>'form-control','required'=>true]) !!}
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                    <label>Sort Order</label>
                    {!! Form::number('sort_order',null,['class'=>'form-control','required'=>true]) !!}
                  </div>
              </div>
               <div class="col-md-12">
                    <div class="form-group">
                      <label>Featured</label>
                      {!! Form::select('featured', binary(), null, ['class'=>'form-control']) !!}
                    </div>
                </div>
              
             </div>
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-primary">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editMediaForm').submit(function () { 

        $.post($(this).attr('action'),$(this).serialize(), function(data){

            if(data.errors)
            {
              $("#editModal #errors").html("<div class='alert alert-danger'>"+data.errors+"</div>");
              
            }else{

              if(data.success == 'true')
              {
                $("#editMediaModal").modal('hide');
                 notification(data.message,'success')
                location.reload();
                

              }else{

                 notification('Something went wrong','danger')

              }

            }

        });




        return false;

      });

    </script>

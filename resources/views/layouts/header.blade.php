<!-- Top Bar -->
<nav class="navbar p-l-5 p-r-5">
    <ul class="nav navbar-nav navbar-left">
        <li>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/"><img src="{{ asset('theme/images/caylum-logo-white.png') }}" alt="{{ config('app.name') }}" style="max-width: 140px;"></a>
            </div>
        </li>
        <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
        
       <!--  <li class="hidden-sm-down">
           <div class="input-group">
               <input type="text" class="form-control" placeholder="Search...">
               <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
           </div>
       </li>         -->
        <li class="float-right">
            <a class="" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="zmdi zmdi-power"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</nav>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('theme/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/jquery-jvectormap-2.0.3.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('theme/css/bootstrap-select.css') }}" />

    <link rel="stylesheet" href="{{ asset('theme/css/morris.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme/css/authentication.css') }}">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/color_skins.css') }}">
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('theme/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />  
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('theme/css/select2.css') }}" />
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

     <link rel="stylesheet" href="{{ asset('theme/css/custom.css') }}" />


     <style>
        * {font-family: 'Roboto', sans-serif;}
       .page-header:before { background-color: rgba(0, 0, 0, 0.85); }
        .table td { vertical-align: middle; }
       /* MODAL */
       .modal-content .modal-header .title, .modal-content .modal-header .modal-title  { margin-top: 0;    font-size: 20px; }
       /* END MODAL */

       /* SELECT */
       .bootstrap-select.form-control { margin: 0; padding:0; }
       .bootstrap-select .btn.btn-round.btn-simple { margin: 0; }
       .modal-content .modal-body+.modal-footer { padding: 1rem;background: #97979724; }
       .modal-content .modal-footer { justify-content: flex-end;    -webkit-justify-content: flex-end; }
         #preloader {  display: none; }
  #preloader .preloader-container { width: 100%; height: 100%; position: fixed; background-color: #00000075; z-index: 999; display: flex; align-items: center; justify-content: center; }
  #preloader .preloader-container img { max-width: 70px; }

     </style>
</head>
<body class="theme-purple {{ (Auth::check()) ? '' : 'authentication sidebar-collapse' }}">
    <!-- Page Loader -->
  <div id="preloader">
    <div class="preloader-container">
      <div><img src="{{ asset('theme/images/fb-load.gif') }}"></div> 
    </div>
  </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    @if(Auth::check())
    @include('layouts.header')
    @include('layouts.sidebar')

        
        @yield('content')

  
    @else
        @yield('content')
    @endif

<!-- Edit Modal -->
<div class="modal fade" id="editModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"></div>
  </div>
</div>

<script src="{{ asset('theme/js/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('theme/js/vendorscripts.bundle.js') }}"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="{{ asset('theme/js/morrisscripts.bundle.js') }}"></script><!-- Morris Plugin Js -->
<script src="{{ asset('theme/js/jvectormap.bundle.js') }}"></script> <!-- JVectorMap Plugin Js -->
<script src="{{ asset('theme/js/knob.bundle.js') }}"></script> <!-- Jquery Knob, Count To, Sparkline Js -->
<script src="{{ asset('theme/js/bootstrap-notify.js') }}"></script> <!-- Bootstrap Notify Plugin Js -->

<script src="{{ asset('theme/js/index.js') }}"></script>
<script src="{{ asset('theme/js/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('theme/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('theme/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('theme/js/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('theme/js/jquery-datatable.js') }}"></script>
<script src="{{ asset('theme/js/bootstrap-tagsinput.js') }}"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="{{ asset('theme/js/moment.js') }}"></script> <!-- Moment Plugin Js --> 
<script src="{{ asset('theme/js/bootstrap-material-datetimepicker.js') }}"></script> 
<script src="{{ asset('theme/js/select2.min.js') }}"></script> <!-- Select2 Js -->
<script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/disableautofill/src/jquery.disableAutoFill.min.js"></script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $( document ).ajaxStart(function() {
          preloader(1);
      });

  $( document ).ajaxComplete(function() {
      preloader(0);
  });
  
  $( document ).ajaxStop(function() {
      preloader(0);
  });


  function preloader(show) {
      if(show > 0)
        $('#preloader').show();
      else
        $('#preloader').hide();
  }

    function notification(message,type)
    {
      $.notify({
          // options
          message: message,
      },
      {
          // settings
          placement: {
                  from: "bottom",
                  align: "right"
              },
          z_index: 9999,
          type: type  
      });
    }

    function courseSearch(){
    
    var user_id = $("#editPaymentForm #user_id").val();
    
    if($("#editPaymentForm #payment_type").val() == 1){

      $.get("{{ route('course.search') }}",{user_id:user_id}, function(data){

        var html = "";
        html += "<div class='col-md-6'>";
        html += "<div class='form-group'>";
        html += "<label>Course Enrollment</label>";
        html += "<select name='course_enrollment_id' class='form-control'>Course Enrollment</label>";
        for(var i = 0; i < data.length; i++) {
            html += "<option value='"+data[i].value+"'>"+data[i].text+"</option>";
        }
        html+= "</select>";
        $("#editPaymentForm #course_enrollment").replaceWith(html);
      });
    

    }
}
    function editAjax(url,type='')
      {
          $("#editModal .modal-content").html('Loading...');
            $("#editModal").modal();
            $.get(url+'?type='+type,function(data){
              $("#preloader").hide();
              $("#editModal .modal-content").html(data);
              $('.datetimepicker').bootstrapMaterialDatePicker({
                  time:false
              });             
              datetimepickerForBirthday()
              $('#editModal #portfolio_category').selectize({
                 create: true,
                 sortField: 'text'
              });
              $('.form-control2').selectpicker('refresh')
              courseSearch();
              $("#editPaymentForm #payment_type").change(function(){
                
                 var user_id = $("#editPaymentForm #user_id").val();
                 
                 if($(this).val() == 1){

                   $.get("{{ route('course.search') }}",{user_id:user_id}, function(data){

                     var html = "";
                     html += "<div class='col-md-6' id='addCourseEnrollment'>";
                     html += "<div class='form-group'>";
                     html += "<label>Course Enrollment</label>";
                     html += "<select name='course_enrollment_id' class='form-control'>Course Enrollment</label>";
                     for(var i = 0; i < data.length; i++) {
                         html += "<option value='"+data[i].value+"'>"+data[i].text+"</option>";
                     }
                     html+= "</select>";
                     $("#editPaymentForm #course_enrollment").replaceWith(html);
                   });
               

                 }else{
                    
                     $("#editPaymentForm #addCourseEnrollment").remove();
                  }


              });
            }); 
      }

      $(document).on('submit','.deleteFrmAjax', function () {
          let poll = confirm("Confirm Delete?");

          if(poll)
          {
              $.post($(this).attr('action'),$(this).serialize(),function(data){
                    notification(data.message,'success');
                    
                    var url = window.location.pathname;
                    var parts = url.split("/");
                    var last_part = parts[parts.length-2];
                   
                   if(last_part == 'enrollment')
                    window.location.href="{{ route('enrollment.index') }}";
                   else
                    location.reload();  
                });
          }

          return false;
      });

      $('.datetimepicker').bootstrapMaterialDatePicker({
          time:false
      });
      datetimepickerForBirthday();

      function datetimepickerForBirthday()
      {
         var date = new Date();
        date.setFullYear( date.getFullYear() - 15 );
        $('.datetimepickerForBirtday').bootstrapMaterialDatePicker({
            time:false,
            maxDate: date
        });
      }
</script>

</body>
</html>
@stack('scripts')


<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    
    <div class="tab-content">
        <div class="tab-pane active" id="dashboard">
            <div class="menu">

                <ul class="list">
                    <li>
                        <div class="user-info" style="padding: 25px 0">
                            <div class="detail">
                                <h4>{{ Auth::user()->username }}</h4>
                                <small>LOGGED IN</small>                        
                            </div>
                          <!--   <a title="facebook" href="javascript:void(0);"><i class="zmdi zmdi-facebook"></i></a>
                                       <a title="twitter" href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a>
                                       <a title="instagram" href="javascript:void(0);"><i class="zmdi zmdi-instagram"></i></a>                -->             
                        </div>
                    </li>
                   
                    <li class="{{ request()->is('admin/dashboard') ? 'active open' : '' }}"> <a href="{{ route('admin.home') }}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
                    <li class="{{ request()->is('admin/course') ? 'active open' : '' }}"> <a href="{{ route('course.index') }}"><i class="zmdi zmdi-book"></i><span>Manage Courses</span></a></li>
                    <li class="{{ request()->is('admin/enrollment') ? 'active open' : '' }}"> <a href="{{ route('enrollment.index') }}"><i class="zmdi zmdi-book"></i><span>Manage Enrollees</span></a></li>
                    <li class="{{ request()->is('admin/subscription') ? 'active open' : '' }}"> <a href="{{ route('subscription.index') }}"><i class="zmdi zmdi-chart"></i><span>Manage Subscriptions</span></a></li>
                    <li class="{{ request()->is('admin/subscriber') ? 'active open' : '' }}"> <a href="{{ route('subscriber.index') }}"><i class="zmdi zmdi-assignment-account"></i><span>Manage Subscribers</span></a></li>
                    <li class="{{ request()->is('admin/subscriber/registered') ? 'active open' : '' }}"> <a href="{{ route('subscriber.registered') }}"><i class="zmdi zmdi-assignment-account"></i><span>Manage Registered Users</span></a></li>
                    <li class="{{ request()->is('admin/analysis') ? 'active open' : '' }}"> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-chart"></i><span>Manage Analysis</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ route('watchlist.index') }}">Caylum Watchlist</a> </li>
                            <li><a href="{{ route('historical.index') }}">Historical Watchlist</a> </li>
                            <li><a href="{{ route('consensus.index') }}">Consensus</a></li>
                            <li><a href="{{ route('btm.index') }}">Btm Stocks</a></li>
                            <li><a href="{{ route('dividend.index') }}">Dividends</a></li>
                            <li><a href="{{ route('buyback.index') }}">Company Buyback</a></li>
                            <li><a href="{{ route('transaction.index') }}">Insider Transaction</a></li>
                            <li><a href="{{ route('history.index') }}">Historical Insider Transaction</a></li>
                            <li><a href="{{ route('ph.index') }}">PH Data</a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->is('admin/payment') ? 'active open' : '' }}"> <a href="{{ route('payment.index') }}"><i class="zmdi zmdi-money"></i><span>Manage Payments</span></a></li>
                    <li class="{{ request()->is('admin/payslip') ? 'active open' : '' }}"> <a href="{{ route('payslip.index') }}"><i class="zmdi zmdi-money"></i><span>Pay Slips</span></a></li>
                    <li class="{{ request()->is('admin/trade/portfolio/category') ? 'active open' : '' }}"> <a href="{{ route('category.index') }}"><i class="fa fa-list-alt"></i><span>Manage Trade Setup</span></a></li>
                    <li class="{{ request()->is('admin/media') ? 'active open' : '' }}"> <a href="{{ route('media.index') }}"><i class="zmdi zmdi-camera"></i><span>Manage Media</span></a></li>
                    <li class="{{ request()->is('custom/form') ? 'active open' : '' }}"> <a href="{{ route('form.index') }}"><i class="zmdi zmdi-assignment-account"></i><span>Manage Custom Form</span></a></li>
                  
                    <li class="{{ request()->is('admin/setting') ? 'active open' : '' }}"> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-settings"></i><span>Settings</span></a>
                        <ul class="ml-menu">
                            <li> <a href="{{ route('change.password') }}">Change Password</a></li>
                            <li><a href="{{ route('portal.setting.create') }}">Portal Setting</a> </li>
                        </ul>
                    </li>
                    <li class="{{ request()->is('admin/user') ? 'active open' : '' }}"> <a href="{{ route('user.index') }}"><i class="zmdi zmdi-assignment-account"></i><span>Administration Users</span></a></li>

                  
                </ul>
            </div>
        </div>
        
    </div>    
</aside>
@component('mail::message')
#  New Subscriber Added

Please user's find subscription details below:

<table class="table" width="100%">
 <tr>
   <td>Reference No:</td>
   <th>{{ $subscriber->id }}</th>
 </tr>
 <tr>
   <td>Subscription:</td>
   <th>{{ $subscriber->subscription->name }}</th>
 </tr>
 <tr>
   <td>Fee:</td>
   <th>{{ makeCurrencyFormat($subscriber->subscription->fee) }}</th>
 </tr>
  <tr>
   <td>Expires At:</td>
   <th>{{ $subscriber->expires_at }}</th>
 </tr>
 <tr>
   <td>Login Email:</td>
   <th>{{ $subscriber->user->email }}</th>
 </tr>

</table>

Regards,<br>
{{ config('app.name') }}
@endcomponent

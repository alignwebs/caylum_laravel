@component('mail::message')
# Pay Slip Uploaded

Pay slip with reference no. {{ $payslip->ref_id }} has been received successfully.

Thanks,<br>
{{ config('app.name') }}
@endcomponent

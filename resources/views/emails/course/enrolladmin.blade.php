@component('mail::message')
# New Course Enrollment

Course enrollment details:

<table class="table" width="100%">
<tr>
   <td width="35%">Name:</td>
   <th>{{ $courseEnrollment->last_name }} {{ $courseEnrollment->first_name }}</th>
 </tr>
 <tr>
   <td width="35%">Reference No:</td>
   <th>{{ $courseEnrollment->id }}</th>
 </tr>
 <tr>
   <td>Course Name:</td>
   <th>{{ $courseEnrollment->course->name }}</th>
 </tr>
 <tr>
   <td>Fee:</td>
   <th>{{ makeCurrencyFormat($courseEnrollment->course->price) }}</th>
 </tr>
 <tr>
   <td>Payment Mode:</td>
   <th>{{ $courseEnrollment->payment_mode }}</th>
 </tr>
  <tr>
   <td>Description:</td>
   <th>{{ $courseEnrollment->course->description }}</th>
 </tr>
</table>

<br>

Regards,<br>
{{ config('app.name') }}
@endcomponent

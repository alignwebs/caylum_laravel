@component('mail::message')
# Enrollment Successful
Dear {{ $courseEnrollment->first_name }}, you have been successfully enrolled. <br>
Please find course enrollment details below:

<table class="table" width="100%">
 <tr>
   <td width="35%">Reference No:</td>
   <th>{{ $courseEnrollment->id }}</th>
 </tr>
 <tr>
   <td>Course Name:</td>
   <th>{{ $courseEnrollment->course->name }}</th>
 </tr>
 <tr>
   <td>Fee:</td>
   <th>{{ makeCurrencyFormat($courseEnrollment->course->price) }}</th>
 </tr>
  <tr>
   <td>Description:</td>
   <th>{{ $courseEnrollment->course->description }}</th>
 </tr>
</table>

<br>

Regards,<br>
{{ config('app.name') }}
@endcomponent

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('theme/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/jquery-jvectormap-2.0.3.min.css') }}"/>
     <link rel="stylesheet" href="{{ asset('theme/css/bootstrap-select.css') }}" />
       <link rel="stylesheet" href="{{ asset('theme/css/morris.min.css') }}" />
        <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('theme/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />  
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/color_skins.css') }}">
        <link rel="stylesheet" href="{{ asset('theme/css/custom.css') }}" />
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
     <style>
        * {
    font-family: 'Roboto', sans-serif;
          }
           p {
               line-height: 1.61em;
          }
           body {
               background-color: #fff;
               font-size: 14px;
          }
           label {
               font-weight: 500;
          }
           .text-bold {
               font-weight: 500;
          }
           .text-sm {
               font-size: 12px;
          }
           .page-header {
               position: fixed;
               left: 0;
               z-index: 99999;
               width: 50%;
          }
           .page-header:before {
               background-color: rgba(0, 0, 0, 0.85);
          }
           .theme-purple .btn-primary {
               background: #7f252c;
          }
           .theme-purple .btn-primary:active, .theme-purple .btn-primary:hover, .theme-purple .btn-primary:focus {
               background: #a81c27;
          }
           .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show>.btn-primary.dropdown-toggle {
               color: #fff;
               background-color: #7f252c;
               border-color: #7f252c;
          }
          /* MODAL */
           .modal-content .modal-header .title {
               margin-top: 0;
               font-size: 20px;
          }
          /* END MODAL */
          /* SELECT */
           .bootstrap-select.form-control {
               margin: 0;
               padding:0;
          }
           .bootstrap-select .btn.btn-round.btn-simple {
               margin: 0;
          }
           .modal-content .modal-body+.modal-footer {
               padding: 1rem;
              background: #97979724;
          }
           .modal-content .modal-footer {
               justify-content: flex-end;
               -webkit-justify-content: flex-end;
          }
           .header .logo-container {
               margin-bottom: 50px;
          }
           .header h5 {
               text-transform: uppercase;
          }
           .half-screen {
               padding:0;
          }
           .half-screen.row {
               margin: 0;
          }
           .half-screen .screen-left {
               padding:0;
          }
           .half-screen .screen-right {
               background: #fff;
          }
           .mw-600 {
               max-width: 600px;
               margin: 0 auto;
          }
           .half-screen .screen-right .container {
               max-width: 600px;
               border: 1px solid #00000012;
               padding: 50px 20px;
              box-shadow: 0px 3px 4px 1px #a5a5a51f;
          }
           .dtp{
              z-index: 9999999;
          }
           #preloader {
               display: none;
          }
           #preloader .preloader-container {
               width: 100%;
               height: 100%;
               position: fixed;
               background-color: #00000075;
               z-index: 999;
               display: flex;
               align-items: center;
               justify-content: center;
          }
           #preloader .preloader-container img {
               max-width: 70px;
          }
           .form-control {
              border-radius: 5px !important;
               border-left: 1px solid #e4e4e4 !important;
               border-right: 1px solid #e4e4e4 !important;
               font-size: 16px;
          }
           .form-control:focus {
              border: 1px solid #f96332 !important;
          }
           .form-group.input-lg .form-control, .input-group.input-lg .form-control {
               padding: 12px 15px;
          }
           .bootstrap-select .btn-round{
              border-radius: 5px !important;
          }
           .input-group-addon{
              border-radius: 5px !important;
          }
           textarea.form-control {
               max-width: 100%;
               padding: 10px;
               resize: none;
               border: 1px solid #E3E3E3;
               border-bottom: 1px solid #E3E3E3;
               border-radius: 0;
               line-height: 1;
          }
           .sidebar .menu .list li a {
               font-size: 14px !important;
          }
  
           
     </style>
</head>
<body class="theme-purple sidebar-collapse">
    <!-- Page Loader -->
    <!-- Page Loader -->
  <div id="preloader">
    <div class="preloader-container">
      <div><img src="{{ asset('theme/images/fb-load.gif') }}"></div> 
    </div>
  </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <div class="half-screen container-fluid">
      {{-- <div class="half-screen row"> --}}
{{--       <div class="screen-left col-lg-6 d-none d-lg-block">
        <div class="page-header">
            <div class="page-header-image" style="background-image:url({{ asset('theme/images/login.jpg') }}"></div>
        </div>
      </div> --}}
      <div class="screen-right col-lg-12 text-center align-items-center py-5">@yield('content')</div>
      {{-- </div> --}}
    </div>
       


<!-- Edit Modal -->
<div class="modal fade" id="editModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"></div>
  </div>
</div>

<script src="{{ asset('theme/js/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('theme/js/vendorscripts.bundle.js') }}"></script> <!-- slimscroll, waves Scripts Plugin Js -->
<script src="{{ asset('theme/js/morrisscripts.bundle.js') }}"></script><!-- Morris Plugin Js -->
<script src="{{ asset('theme/js/jvectormap.bundle.js') }}"></script> <!-- JVectorMap Plugin Js -->
<script src="{{ asset('theme/js/bootstrap-notify.js') }}"></script> <!-- Bootstrap Notify Plugin Js -->

<script src="{{ asset('theme/js/index.js') }}"></script>

<script src="{{ asset('theme/js/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('theme/js/moment.js') }}"></script> <!-- Moment Plugin Js --> 
<script src="{{ asset('theme/js/bootstrap-material-datetimepicker.js') }}"></script> 
 <script src="{{ asset('theme/js/jquery.form.js') }}"></script> 

 <script
src="https://www.paypal.com/sdk/js?client-id={{ env('CLIENT_ID') }}&currency=@setting('currency')">
</script>

<script type="text/javascript">
  $( document ).ajaxStart(function() {
          preloader(1);
      });

  $( document ).ajaxComplete(function() {
      preloader(0);
  });
  
  $( document ).ajaxStop(function() {
      preloader(0);
  });


  function preloader(show) {
      if(show > 0)
        $('#preloader').show();
      else
        $('#preloader').hide();
  }

  var date = new Date();
  date.setFullYear( date.getFullYear() - 15 );
  $('.datetimepickerForBirtday').bootstrapMaterialDatePicker({
      maxDate: date,
      time:false,
      
  });

    $('.datetimepicker').bootstrapMaterialDatePicker({

      time:false,
      
  });
  
    function notification(message,type)
    {
      $.notify({
          // options
          message: message,
      },
      {
          // settings
          placement: {
                  from: "bottom",
                  align: "right"
              },
          z_index: 9999,
          type: type  
      });
    }
</script>
@stack('scripts')
</body>
</html>


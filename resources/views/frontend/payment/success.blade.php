@extends('frontend.app')

@section('content')

<div class="container">
    <div class="col-md-12 content-center">
        <i class="fa fa-check-circle text-success" style="font-size: 85px"></i>
                        
                            <h4 class="text-center">Enrollment Successful</h4>
                            <p>You have been successfully enrolled to this course.
                            <br>We will email you more course details soon.</p>

                </div>
    </div>

@endsection
@push('scripts')

@endpush

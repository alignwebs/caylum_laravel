@extends('frontend.app')

@section('content')
<div class="page-header">
    <div class="page-header-image" style="background-image:url({{ asset('theme/images/login.jpg') }}"></div>
<div class="container">
    <div class="col-md-12 content-center">
        <i class="fa fa-times text-danger" style="font-size: 85px"></i>
                        
                            <h4 class="text-center">Failed</h4>
                            @if(!empty($payment))
                            <table class="table table-hovered">
                                <tr>
                                    <th>Order ID</th>
                                    <td>{{ $payment->txn_id }}</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $courseEnrollment->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $courseEnrollment->email }}</td>
                                </tr>
                                <tr>
                                    <th>Mobile</th>
                                    <td>{{ $courseEnrollment->mobile }}</td>
                                </tr>
                                <tr>
                                    <th>Course Name</th>
                                    <td>{{ $courseEnrollment->course->name }}</td>
                                </tr>
                                <tr>
                                    <th>Course Price</th>
                                    <td>{{ $courseEnrollment->course->price }}</td>
                                </tr>
                                @if($courseEnrollment->course->subscribe_default)
                                <tr>
                                    <th>Subscription</th>
                                    <td>{{ $subscription->name }}</td>
                                </tr>
                                <tr>
                                    <th>Fee</th>
                                    <td>{{ $subscription->fee }}</td>
                                </tr>
                                <tr>
                                    <th>Month Duration</th>
                                    <td>{{ $subscription->month_duration }} Months</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Date</th>
                                    <td>{{ $payment->date }}</td>
                                </tr>
                            </table>
                            @endif

                            @if(empty($payment))
                            <table class="table table-hovered">
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $courseEnrollment->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $courseEnrollment->email }}</td>
                                </tr>
                                <tr>
                                    <th>Mobile</th>
                                    <td>{{ $courseEnrollment->mobile }}</td>
                                </tr>
                                <tr>
                                    <th>Course Name</th>
                                    <td>{{ $courseEnrollment->course->name }}</td>
                                </tr>
                                <tr>
                                    <th>Course Price</th>
                                    <td>{{ $courseEnrollment->course->price }}</td>
                                </tr>
                                @if($courseEnrollment->course->subscribe_default)
                              
                                <tr>
                                    <th>Subscription</th>
                                    <td>{{ $subscription->name }}</td>
                                </tr>
                                <tr>
                                    <th>Fee</th>
                                    <td>{{ $subscription->fee }}</td>
                                </tr>
                                <tr>
                                    <th>Month Duration</th>
                                    <td>{{ $subscription->month_duration }} Months</td>
                                </tr>
                                @endif
                            </table>
                            @endif

                </div>

            </form>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')

@endpush

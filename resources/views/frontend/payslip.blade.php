@extends('frontend.app')

@section('content')
<style type="text/css">
  .cstm-input .form-control  { 
 padding:0px !important;
}
.input-group-btn .btn {
    padding: 17px .7rem;
    }
</style>
<div class="container">
    <div class="col-md-12">
        <div class="card-plain">
                <div class="header">
                    <div class="logo-container">
                        <img src="{{ asset('theme/images/caylum-logo.jpg') }}" alt="" width="250">
                    </div>
                    <h5 style="text-transform: capitalize;">Pay Slip</h5>
                </div>
                <div class="content">  
                    @include('flash::message')
                    <div id="errors"></div>
                    <div id="success"></div>
                    {!! Form::open(['route'=>'payslip.storePaySlip','class'=>'form-material','id'=>'payslipForm','files'=>true]) !!}
                    <div class="input-group input-lg cstm-input">
                      <select name="type" class="form-control">
                        <option value="course" {{ ($type == 'course') ? "selected" : "" }}>Course Fee</option>
                        <option value="subscription" {{ ($type == 'subscription') ? "selected" : "" }}>Subscription</option>
                        <option value="others" {{ ($type == 'others') ? "selected" : "" }}>Others</option>
                      </select>
                    </div>
                    <div class="input-group input-lg">
                       {!! Form::number('ref_id',$ref_id,['class'=>'form-control','required'=>true,'placeholder'=>'Enter Reference ID']) !!}
                    </div>
                     <div class="input-group input-lg">
                       {!! Form::text('payment_date',null,['class'=>'form-control datetimepicker','required'=>true,'placeholder'=>'Enter Payment Date']) !!}
                    </div>
                    <div class="input-group input-lg">
                       {!! Form::text('name',null,['class'=>'form-control','required'=>true,'placeholder'=>'Your Name']) !!}
                    </div>
                    <div class="input-group input-lg">
                       {!! Form::text('mobile',null,['class'=>'form-control','required'=>true,'placeholder'=>'Your Mobile']) !!}
                    </div>
                    <p style="text-align: left;"><b>Upload Pay Slip</b></p>
                    <div class="input-group input-lg">
                       {!! Form::file('file',null,['class'=>'form-control','required'=>true]) !!}
                    </div>

        
                    <div class="input-group input-lg">
                       {!! Form::textarea('comments',null,['class'=>'form-control','placeholder'=>'Enter Comments (if any)', 'rows' => '5']) !!}
                    </div>
                </div>
                <div class="footer text-center">
                    <button type="submit" class="btn btn-primary btn-round btn-lg btn-block ">
                        {{ __('Submit') }}
                    </button>
                   
                  
                </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() { 
               // bind 'myForm' and provide a simple callback function 
               $('#payslipForm').ajaxForm(function(data) { 
                  
                  if(data.errors)
                  {
                    $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

                  }else{

                    if(data.success == 'true')
                    {
                      $("#errors").html("");
                      $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                      $("#payslipForm")[0].reset();


                    }else{

                      $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

                    }

                  }

               }); 
           }); 
</script>

@endpush

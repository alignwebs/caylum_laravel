@extends('frontend.app')

@section('content')
<style type="text/css">
  .container{max-width: 650px !important}
  .header{font-size: 16px;margin-bottom: 20px;}
</style>
<div class="container text-left">
    <div class="col-md-12">
        <div class="card-plain">
          
            <form id="courseEnrollForm" action="{{ route('enroll.course.store') }}" enctype="multipart/form-data">
                <input type="hidden" name="course_id" id="course_id" value="{{ $courseDetail->id }}">
                <input type="hidden" name="subscription_id" id="subscription_id" value="{{ $subscription_id }}">
              @csrf
      
                <div class="header">
                    <div><b>Course Name:</b> {{ $courseDetail->name }}</div>
                    {{-- <p>Course Name: {{ $courseDetail->name }}</p> --}}
                    <div><b>Course Fee:</b> PHP {{ number_format($courseDetail->price,2) }}</div>
                    {{-- <h5 class="text-center">{{ $courseDetail->name }}</h5> --}}
                    {{-- <span>Fee: {{ $courseDetail->price }}</span> --}}
                </div>
                      
                <div id="error"></div>
                <div id="success"></div>
                <div class="content">                 
                <p>Please enter your details below</p>      
               <div class="row">       
                  <div class="col-md-6">
                      <label><span style="color:red">*</span> Last Name</label>
                    <div class="form-group input-lg">
                       <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" autofocus placeholder="Enter Your Last Name" required="">
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                   <div class="col-md-6">
                    <label><span style="color:red">*</span> First Name</label>
                    <div class="form-group input-lg">
                       <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" autofocus placeholder="Enter Your First Name" required="">
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
               <div class="col-md-6">
                  <label><span style="color:red">*</span> Email</label>
                    <div class="form-group input-lg">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Enter Your Email">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label><span style="color:red">*</span> Mobile</label>
                    <div class="form-group input-lg">
                        <input id="mobile" type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required placeholder="Enter Your Mobile" required="">

                        @if ($errors->has('mobile'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label><span style="color:red">*</span> Birthdate</label>
                    <div class="form-group input-lg">
                        <input id="birthday" type="text" class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }} datetimepickerForBirtday" name="birthday" value="{{ old('birthday') }}" required placeholder="Enter Your Birthday">

                        @if ($errors->has('birthday'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('birthday') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                      <label><span style="color:red">*</span> Gender</label>

                          <div class="form-group input-lg">
                          <label for="gender_male"><input type="radio" name="gender" id="gender_male" value="male"> Male</label>&nbsp;&nbsp;
                          <label for="gender_female"><input type="radio" name="gender" id="gender_female" value="female"> Female</label> 
                            @if ($errors->has('gender'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('gender') }}</strong>
                                </span>
                            @endif
                        </div>  
                      </div>

                    </div>
                     <br>
                   <div class="row"> 
                    @if($courseDetail->custom_form_id)
                        @foreach(json_decode($courseDetail->custom_form->form_json,true) as $json)
                          
                              @if($json['type'] == 'radio-group')
                                      <div class="col-md-12">
                                      <label>@php echo $json['label'] @endphp</label>
                                        <div class="form-group">
                                                  @foreach($json['values'] as $value)
                                                    <span class="radio-inline"><input type="radio" name="custom_form[{{ $json['name'] }}]" value="{{ $value['value'] }}"> {{ $value['label'] }}</span>&nbsp;
                                                  @endforeach
                                                  <p></p>
                                        </div>
                                      </div>
                              @elseif($json['type'] == 'checkbox-group')
                                    <div class="col-md-12">
                                        <label>@php echo $json['label'] @endphp</label>
                                        <div class="form-group">
                                                @foreach($json['values'] as $value)
                                                    <label class="checkbox-inline"><input type="checkbox" name="custom_form[{{ $json['name'] }}][]" value="{{ $value['value'] }}"> {{ $value['label'] }}</label>
                                                @endforeach
                                        </div>
                                      </div>
                              @elseif($json['type'] == 'select')
                                <div class="col-md-12">
                                  <label>{{ $json['label'] }}</label>
                                    <div class="form-group">
                                      <select name="custom_form[{{ $json['name'] }}]" class="{{ $json['className'] }}">
                                          @foreach($json['values'] as $value)
                                              <option value="{{ $value['value'] }}">{{ $value['label'] }}</option>
                                          @endforeach
                                      </select>
                                    </div>
                                  </div>
                              @elseif($json['type'] == 'textarea')
                               <div class="col-md-12">
                                  <div class="from-group">
                                        <label>{{ $json['label'] }}</label>
                                        <textarea name="custom_form[{{ $json['name'] }}]" rows="{{ (isset($json['rows']) ? $json['rows'] : 3) }}" class="{{ $json['className'] }}"></textarea>
                                  </div>
                                </div>
                             @elseif($json['type'] == 'file')
                             <div class="col-md-12">
                                <div class="from-group">
                                      <label>{{ $json['label'] }}</label>
                                      <input type="{{ $json['type'] }}" name="custom_form[{{ $json['name'] }}]" class="{{ $json['className'] }}">
                                      
                                </div>
                              </div>
                              @elseif(in_array($json['type'], ['paragraph', 'header']))
                               <div class="col-md-12">
                                  <div class="form-group input-lg">
                                     {!! '<'.$json['subtype'].'>'. $json['label'] .'</'.$json['subtype'].'>' !!}
                                  </div>
                                </div>
                              @else
                               <div class="col-md-12">
                                <label>{{ $json['label'] }}</label>
                                  <div class="form-group input-lg">
                                     <input type="{{ $json['type'] }}" name="custom_form[{{ $json['name'] }}]" class="{{ $json['className'] }}">
                                  </div>
                                </div>
                              @endif       
                        @endforeach
                    @endif
                  </div>
                     @if($courseDetail->is_paid)
                    
                    <hr>
                     <div class="row">
                        <div class="col-md-12">
                          <label>Payment Mode</label>
                          <select id="payment_mode" name="payment_mode" class="form-control" required="">
                              <option value="paypal">Credit Card via PayPal</option>
                              <option value="bank_deposit">Bank Deposit via BDO or Security Bank</option>
                          </select>
                        </div>
                     </div>      

                     <input type="hidden" name="is_paid_review" id="is_paid_review" value="1">

                     @endif

                </div>
                <div class="footer text-center">
                        <div id="paypal-button-container" style="display: none"></div>
                        <button type="submit" id="submitBtn" class="btn btn-primary btn-round btn-lg btn-block ">
                            {{ __('Proceed') }}
                        </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@push('scripts')
  <script type="text/javascript">
    $('#courseEnrollForm').submit(function (e) {
        e.preventDefault();
  
         let form = $(this)[0];
         let data = new FormData(form);
         let action = $(this).attr('action');

         $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: action,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                if(data.errors != undefined)
                    $("#error").html("<div class='alert alert-danger'>"+data.errors+"</div>")

                if(data.success != undefined)
                    window.location.href = data.url; 
                
                if(data.payment != undefined) {
                  $("#error").hide();
                  $("#last_name,#first_name,#email,#birthday,#mobile").prop("readonly", true);
                  $('#payment_mode').prop('readonly', true);
                  $('#is_paid_review').val(0);
                  $("#submitBtn").hide();
                  $("#paypal-button-container").show();

                }
                
            },
            error: function (data) {
                $("#error").html("<div class='alert alert-danger'>"+data.errors+"</div>")
            }
        });

    })
  </script>
 
   <script>
    paypal.Buttons({
        style: {
            layout:  'vertical',
            color:   'blue',
            shape:   'rect',
            label:   'paypal'
        },
       createOrder: function(data, actions) {
       
         // Set up the transaction
         return actions.order.create({
           purchase_units: [{
             amount: {
               value: '{{ $courseDetail->price }}'
             }
           }]
         });
       },
       onApprove: function(data, actions) {

      return actions.order.capture().then(function(details) {

        //alert('Transaction completed by ' + details.payer.name.given_name);

        let form_fields = $('#courseEnrollForm').serialize();
        form_fields = form_fields + "&orderID=" + data.orderID + "&paymentTypeId=1";
        // Call your server to save the transaction

        return $.post('{{ route("enroll.course.store") }}', form_fields, function (data) {
              let url = data.url;
              if(url != undefined)
              {
                  window.location.href = url;
              }
        });
    
      });
    }
     }).render('#paypal-button-container');
  </script>

@endpush

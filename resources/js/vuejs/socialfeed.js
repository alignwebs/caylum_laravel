import InfiniteLoading from 'vue-infinite-loading';

var FeedVueApp = new Vue({
  el: '#FeedVueApp',
  data: {
      loading: true,
      user_id: null,
      profile_user_id: null,
      profile_user_search_query: null,
      feeds: [],
      total_feeds_count: 0,
      url: {
          userid: '/portal/userid',
          feednextpage: '/portal/feed/ajax/social/feed?page=1',
          feedlike: '/portal/feed/ajax/like',
          feedlikes: '/portal/feed/ajax/likes',
          feedcomment: '/portal/feed/ajax/comment',
          feedcommentdelete: '/portal/feed/ajax/comment/delete',
          feeddelete: '/portal/feed/ajax/delete',
        }
  },
  watch: {
      total_feeds_count(val)
      {
        document.getElementById('feed_count').innerHTML = val;
      }
  },
  components: {
        InfiniteLoading
      },

  mounted() {
        if(document.getElementById('profile_user_id'))
        {
          let profile_user_id = document.getElementById('profile_user_id').value;
          this.profile_user_id = + profile_user_id;
          if(this.profile_user_id > 0)
                this.url.feednextpage = '/portal/feed/ajax/user/'+ this.profile_user_id +'/feed';

        }

         if(document.getElementById('profile_user_search_query'))
        {
          let profile_user_search_query = document.getElementById('profile_user_search_query').value;
          this.profile_user_search_query = profile_user_search_query;
          if(this.profile_user_search_query)
                this.url.feednextpage = '/portal/feed/ajax/social/feed/search?page1&q=' + this.profile_user_search_query;
        }
    },
  created() {
      this.getUserId()
  },
  methods: {
        getUserId()
        {
            axios.post(this.url.userid).then(resp => {
                  this.user_id = resp.data.user_id
            });
        },

        getUserFeed($state)
        {
				    let $this = this;

            axios.get(this.url.feednextpage).then(function (resp) {

            	$this.url.feednextpage = resp.data.next_page_url;
              $this.total_feeds_count = resp.data.total;
            	$this.feeds.push(...resp.data.data);
            	$this.loading = false;
              
              setTimeout(function () {
                $('.feed-comments-container').animate({ scrollTop: 999 }, 'fast');
                $(function() {
                  // Initializes and creates emoji set from sprite sheet
                  window.emojiPicker = new EmojiPicker({
                    emojiable_selector: '[data-emojiable=true]',
                    assetsPath: '/plugins/emoji/img/',
                    popupButtonClasses: 'fa fa-smile-o'
                  });
                  // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
                  // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
                  // It can be called as many times as necessary; previously converted input fields will not be converted again
                  window.emojiPicker.discover();
                });
              }, 1000)
              

            	if(resp.data.next_page_url)
            	{
                $state.loaded();
            	}
            	else
            	{
            		$state.complete();
            	}
            	


            }).catch(function (error) {
            	console.log(error);
            	$this.loading = false;
            })
        },
        likeFeed: _.debounce(function (feed_id) {
          let $this = this;
          axios.post(this.url.feedlike, { feed_id }).then(function (resp) {

                  if(resp.data.success)
                  {
                      $this.feeds.filter( feed => { 
                            if(feed.id == feed_id)
                            {
                              /* unliked */
                              if(resp.data.resp.detached.length > 0)
                              {
                                feed.likes_count--;
                                feed.is_liked = false;
                              }

                              /* liked */
                              if(resp.data.resp.attached.length > 0)
                              {
                                feed.likes_count++;
                                feed.is_liked = true;
                              }
                            }
                       });
                  }

            }).catch(function (error) {

            });

            }, 300),
    
        like(feed_id) {
            return this.likeFeed(feed_id);   
        },
        getFeedLikes(feed_id)
        {
            let $this = this;

            $('#FeedLikesModal').modal('show');
            axios.get(this.url.feedlikes+'?feed_id='+feed_id).then(function (resp) {
              $('#FeedLikesModal .modal-body').html(resp.data);
            }).catch(function (error) {
              console.log(error);
            });
        },
        postComment(feed_id)
        {
            let $this = this;
            let comment_textarea = $('#comment-feed-'+feed_id);
            let comment = comment_textarea.val();

            if(comment.length > 0)
            {
              axios.post(this.url.feedcomment, { feed_id, comment }).then(function (resp) {

                      if(resp.data.success)
                      {
                          comment_textarea.val('');
                          $('.comment-box').text('');
                          comment_textarea.closest('.social-feed-post-comment').find('.feed-comments-container').animate({ scrollTop: 999 }, 'fast')

                          $this.feeds.filter( feed => { 
                                if(feed.id == feed_id)
                                {
                                    feed.comments.push(resp.data.resp);
                                }
                          });
                      }
                  });
            }
        },
        deleteComment(comment_index, comment_id)
        {
            let $this = this;
            let confirm = window.confirm("Delete this comment ?");
            if(confirm)
            {
                axios.post(this.url.feedcommentdelete, { comment_id }).then(function (resp) {

                        if(resp.data.success)
                        {
                            notification("Comment has been deleted.", "success");
                            $this.feeds.filter( feed => { 
                                  feed.comments.splice(comment_index, 1);
                            });
                        }
                    });
            }
         
        },

        deleteFeed(feed_index, feed_id)
        {
            let $this = this;
            let confirm = window.confirm("Delete this Post ?");
            if(confirm)
            {
                axios.post(this.url.feeddelete, { feed_id }).then(function (resp) {

                        if(resp.data.success)
                        {
                            notification("Post has been deleted.", "success");
                            $this.feeds.splice(feed_index, 1);
                        }
                    });
            }
         
        },

  },
})

var navbar = new Vue({
  el: '#NavBar',
  data: {
    notifications: {},
    unreadNotificationCount: 0,
  },
  created () {
  		
  		let $this = this;

  		this.getUserNotifications();
  		
  		setInterval(function () {
  			$this.getUserNotifications();
  		}, 15000);
  },
  methods: {

  		getUserNotifications() {
  			let $this = this;
  			axios.get('/portal/ajax/user/notification/header')
			  .then(function (response) {
				    $this.unreadNotificationCount = response.data.unread;
				    $this.notifications = response.data.notifications;
			  })
			  .catch(function (error) {
			    // handle error
			    console.log(error);
			  })
			  .finally(function () {
			    // always executed
			  });
  		}, 

  		markAsRead() {
  			let $this = this;
  			axios.get('/portal/ajax/user/notification/markasread')
			  .then(function (response) {
				    $this.unreadNotificationCount = 0;
			  })
			  .catch(function (error) {
			    // handle error
			    console.log(error);
			  })
			  .finally(function () {
			    // always executed
			  });
  		}	

  }

})